﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmTransactionDebug : Form
    {
        public DataTable Transaction_DataTable
        {
            get { return this._Transaction_DataTable; }
            set { this._Transaction_DataTable = value; }
        }

        public DataTable Payment_DataTable
        {
            get { return this._Payment_DataTable; }
            set { this._Payment_DataTable = value; }
        }

        private DataTable _Transaction_DataTable;
        private DataTable _Payment_DataTable;

        public frmTransactionDebug()
        {
            try
            {
                this.InitializeComponent();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void frmTransactionDebug_Load(object sender, EventArgs e)
        {
            try
            {
                ND.UI.Winform.Form.PopulateDataGridView(ref this.dgvTransaction, this._Transaction_DataTable);
                ND.UI.Winform.Form.PopulateDataGridView(ref this.dgvPayment, this._Payment_DataTable);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }
    }
}
