﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmVendor : Form
    {
        #region Declarations

            private STP.Logic.General _Logic_General;
            private STP.Logic.Common.SaveType _SaveType;

            private int _SearchType;
            private String _Current_Sort;
            private ND.Standard.Validation.Winform.Form _ValidatorNew;
            private ND.Standard.Validation.Winform.Form _ValidatorEdit;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtVendorName;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildVendorList;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmVendor_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this.EditDeleteEnable(false);
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.SearchVendor();
                        this.BuildAutoComplete();
                        this._SaveType = STP.Logic.Common.SaveType.None;
                        this.SetValidation();
                        this.lstVendorListing.Focus();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void frmVendor_FormClosing(object sender, FormClosingEventArgs e)
                {
                    try
                    {
                        this.CloseForm(e);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstVendorListing_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.EditDeleteEnable(this.lstVendorListing.SelectedItems.Count > 0);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstVendorListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, e.Column, this.ldm_BuildVendorList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstVendorListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.ShowDetail(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstVendorListing_MouseDoubleClick(object sender, MouseEventArgs e)
                {
                    try
                    {
                        this.ShowDetail(true);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtVendorName_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this._SearchType = 1;                            
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, columnIndex, this.ldm_BuildVendorList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnDelete_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SaveType = STP.Logic.Common.SaveType.Delete;
                        if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.lstVendorListing.SelectedItems.Count > 0)
                            {
                                this.DeleteVendor();
                                this._SearchType = 0;
                                if (this.txtVendorSearch.Text.Trim() != String.Empty)
                                {
                                    this._SearchType = 1;
                                }
                                else
                                {
                                    this._SearchType = 0;
                                }
                                String[] sortInfo = null;
                                int columnIndex = 0;
                                if (this._Current_Sort != String.Empty)
                                {
                                    sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                    columnIndex = int.Parse(sortInfo[2]);
                                    if (sortInfo[1] == "ASC")
                                    {
                                        this._Current_Sort = String.Empty;
                                    }
                                    else
                                    {
                                        this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                    }
                                }
                                ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, columnIndex, this.ldm_BuildVendorList);                                
                                this.BuildAutoComplete();
                            }
                        }
                        this.lstVendorListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnEdit_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowDetail(true);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnAdd_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowDetail(false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SearchType = 1;
                        this.SearchVendor();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSave_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.ValidateForm() == true)
                            {
                                if (this.SaveVendor() == true)
                                {
                                    this.HideDetail();
                                    this._SearchType = 0;
                                    if (this.txtVendorSearch.Text.Trim() != String.Empty)
                                    {
                                        this._SearchType = 1;
                                    }
                                    else
                                    {
                                        this._SearchType = 0;
                                    }
                                    String[] sortInfo = null;
                                    int columnIndex = 0;
                                    if (this._Current_Sort != String.Empty)
                                    {
                                        sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                        columnIndex = int.Parse(sortInfo[2]);
                                        if (sortInfo[1] == "ASC")
                                        {
                                            this._Current_Sort = String.Empty;
                                        }
                                        else
                                        {
                                            this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                        }
                                    }
                                    else
                                    {
                                        this._Current_Sort = this.lstVendorListing.Columns[0].Name + "[..]DESC[..]" + this.lstVendorListing.Columns[0].Index.ToString();
                                    }
                                    ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, columnIndex, this.ldm_BuildVendorList);
                                    this.BuildAutoComplete();
                                    this.lstVendorListing.Focus();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnCancel_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.HideDetail();
                        this.lstVendorListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Procedures and Functions

            #region Constructor

                public frmVendor()
                {
                    try
                    {
                        this.InitializeComponent();
                        this.Text = "Vendor";

                        this._Logic_General = new STP.Logic.General();

                        this._ValidatorNew = new ND.Standard.Validation.Winform.Form();
                        this._ValidatorEdit = new ND.Standard.Validation.Winform.Form();

                        this.ct_txtVendorName = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtVendorName_Validate);
                        this.ldm_BuildVendorList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchVendor);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Validations

                private Boolean ct_txtVendorName_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        ND.Data.ConditionField conditionFieldVendorName = new ND.Data.ConditionField();
                        conditionFieldVendorName.Field = "Vendorname";
                        conditionFieldVendorName.Value = this.txtVendorName.Text.Trim();
                        conditionFieldVendorName.DataType = ND.Data.DataObject.DataType.NString;
                        conditionFieldVendorName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                        conditions.Add(conditionFieldVendorName);
                        result = !this._Logic_General.CheckVendorExist(conditions);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void ShowDetail(Boolean isEdit)
                {
                    try
                    {
                        this.ClearField();
                        if (isEdit == true)
                        {
                            this._SaveType = STP.Logic.Common.SaveType.Update;
                            this.LoadVendor();
                        }
                        else
                        {
                            this._SaveType = STP.Logic.Common.SaveType.Insert;
                        }
                        this.pnlDetail.Visible = true;
                        this.pnlDetail.BringToFront();
                        this.txtVendorName.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void HideDetail()
                {
                    try
                    {
                        this.ClearField();
                        this._SaveType = STP.Logic.Common.SaveType.None;
                        this.pnlDetail.Visible = false;
                        this.pnlDetail.SendToBack();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildButton()
                {
                    try
                    {
                        this.btnSearch.Image = Image.FromFile("image/Search.png");
                        this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnAdd.Image = Image.FromFile("image/New.png");
                        this.btnAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnEdit.Image = Image.FromFile("image/Edit.png");
                        this.btnEdit.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnDelete.Image = Image.FromFile("image/Delete.png");
                        this.btnDelete.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnClose.Image = Image.FromFile("image/Cancel.png");
                        this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnSave.Image = Image.FromFile("image/Confirm.png");
                        this.btnSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnCancel.Image = Image.FromFile("image/Cancel.png");
                        this.btnCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void EditDeleteEnable(Boolean enable)
                {
                    try
                    {
                        this.btnEdit.Enabled = enable;
                        this.btnDelete.Enabled = enable;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SearchVendor()
                {
                    try
                    {
                        this.SearchVendor(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SearchVendor(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstVendorListing.Visible = false;
                        this.lstVendorListing.Clear();
                        this.BuildVendorListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        switch (this._SearchType)
                        {
                            case 0:
                                {
                                    break;
                                }
                            case 1:
                                {
                                    ND.Data.ConditionField conditionFieldVendorName = new ND.Data.ConditionField();
                                    conditionFieldVendorName.Field = "Vendorname";
                                    conditionFieldVendorName.Value = "%" + this.txtVendorSearch.Text + "%";
                                    conditionFieldVendorName.DataType = ND.Data.DataObject.DataType.String;
                                    conditionFieldVendorName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                    conditions.Add(conditionFieldVendorName);
                                    break;
                                }
                        }
                        this._Logic_General.BuildVendorList(this.lstVendorListing, conditions, sortType, sortField);
                        this.FormatVendorListColumn();
                        this.lstVendorListing.Visible = true;
                        if (this.lstVendorListing.Items.Count > 0)
                        {
                            this.lstVendorListing.Items[0].Selected = true;
                            this.EditDeleteEnable(true);
                        }
                        else
                        {
                            this.EditDeleteEnable(false);
                        }
                        this.lstVendorListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildVendorListColumn()
                {
                    try
                    {
                        this.lstVendorListing.Columns.Clear();
                        this.lstVendorListing.Columns.Add("id", "ID");
                        this.lstVendorListing.Columns.Add("Vendorcode", "Code");
                        this.lstVendorListing.Columns.Add("Vendorname", "Name");
                        this.lstVendorListing.Columns.Add("address1", "Address 1");
                        this.lstVendorListing.Columns.Add("address2", "Address 2");
                        this.lstVendorListing.Columns.Add("postcode", "Post Code");
                        this.lstVendorListing.Columns.Add("city", "City");
                        this.lstVendorListing.Columns.Add("area", "Area");
                        this.lstVendorListing.Columns.Add("state", "State");
                        this.lstVendorListing.Columns.Add("country", "Country");
                        this.lstVendorListing.Columns.Add("tel", "Tel");
                        this.lstVendorListing.Columns.Add("fax", "Fax");
                        this.lstVendorListing.Columns.Add("contact", "Contact");
                        this.lstVendorListing.Columns.Add("createddate", "Created Date");
                        this.lstVendorListing.Columns.Add("flag", "Flag");
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatVendorListColumn()
                {
                    try
                    {
                        this.lstVendorListing.Columns["Vendorname"].Width = 150;
                        this.lstVendorListing.Columns["Vendorname"].TextAlign = HorizontalAlignment.Left;
                        this.lstVendorListing.Columns["city"].Width = 100;
                        this.lstVendorListing.Columns["city"].TextAlign = HorizontalAlignment.Left;
                        this.lstVendorListing.Columns["area"].Width = 100;
                        this.lstVendorListing.Columns["area"].TextAlign = HorizontalAlignment.Left;
                        this.lstVendorListing.Columns["tel"].Width = 70;
                        this.lstVendorListing.Columns["tel"].TextAlign = HorizontalAlignment.Left;
                        this.lstVendorListing.Columns["fax"].Width = 70;
                        this.lstVendorListing.Columns["fax"].TextAlign = HorizontalAlignment.Left;
                        this.lstVendorListing.Columns["contact"].Width = 70;
                        this.lstVendorListing.Columns["contact"].TextAlign = HorizontalAlignment.Left;

                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["id"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["Vendorcode"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["address1"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["address2"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["postcode"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["state"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["country"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["createddate"]);
                        this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildAutoComplete()
                {
                    try
                    {
                        this.txtVendorSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        this.txtVendorSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        this.txtVendorSearch.AutoCompleteCustomSource = this._Logic_General.VendorStringCollection();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._ValidatorNew.AddValidator(this.txtVendorName, this.erpVendor, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorNew.AddValidator(this.txtVendorName, this.erpVendor, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtVendorName);

                        this._ValidatorEdit.AddValidator(this.txtVendorName, this.erpVendor, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void CloseForm(FormClosingEventArgs e)
                {
                    try
                    {
                        ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void DeleteVendor()
                {
                    try
                    {
                        if (this._Logic_General.DeleteVendor(int.Parse(this.lstVendorListing.SelectedItems[0].SubItems[0].Text)) == true)
                        {
                            MessageBox.Show("Delete Success", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            MessageBox.Show("Delete Failed", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean ClearField()
                {
                    Boolean result = false;
                    try
                    {
                        this.txtVendorID.Text = String.Empty;
                        this.txtVendorCode.Text = String.Empty;
                        this.txtVendorName.Text = String.Empty;
                        this.txtVendorAddress1.Text = String.Empty;
                        this.txtVendorAddress2.Text = String.Empty;
                        this.txtVendorPostCode.Text = String.Empty;
                        this.txtVendorCity.Text = String.Empty;
                        this.txtVendorState.Text = String.Empty;
                        this.txtVendorCountry.Text = String.Empty;
                        this.txtVendorTelephone.Text = String.Empty;
                        this.txtVendorFax.Text = String.Empty;
                        this.txtVendorContactPerson.Text = String.Empty;
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private void LoadVendor()
                {
                    try
                    {
                        STP.Base.Vendor_Object objectVendor = new STP.Base.Vendor_Object();
                        if (this.lstVendorListing.SelectedItems.Count > 0)
                        {
                            objectVendor = this._Logic_General.GetVendor(int.Parse(this.lstVendorListing.SelectedItems[0].SubItems[0].Text));
                            if (objectVendor != null)
                            {
                                this.txtVendorID.Text = objectVendor.ID.ToString();
                                this.txtVendorCode.Text = objectVendor.Vendor_Code;
                                this.txtVendorName.Text = objectVendor.Vendor_Name;
                                this.txtVendorAddress1.Text = objectVendor.Address_1;
                                this.txtVendorAddress2.Text = objectVendor.Address_2;
                                this.txtVendorPostCode.Text = objectVendor.Post_Code;
                                this.txtVendorCity.Text = objectVendor.City;
                                this.txtVendorArea.Text = objectVendor.Area;
                                this.txtVendorState.Text = objectVendor.State;
                                this.txtVendorCountry.Text = objectVendor.Country;
                                this.txtVendorTelephone.Text = objectVendor.Telephone;
                                this.txtVendorFax.Text = objectVendor.Fax;
                                this.txtVendorContactPerson.Text = objectVendor.Contact_Person;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean SaveVendor()
                {
                    Boolean result = false;
                    try
                    {
                        STP.Base.Vendor_Object objectVendor = new STP.Base.Vendor_Object();
                        objectVendor.ID = this.txtVendorID.Text.Trim() == String.Empty ? (int?)null : int.Parse(this.txtVendorID.Text.Trim());
                        objectVendor.Vendor_Code = this.txtVendorCode.Text.Trim();
                        objectVendor.Vendor_Name = this.txtVendorName.Text.Trim();
                        objectVendor.Address_1 = this.txtVendorAddress1.Text.Trim();
                        objectVendor.Address_2 = this.txtVendorAddress2.Text.Trim();
                        objectVendor.Post_Code = this.txtVendorPostCode.Text.Trim();
                        objectVendor.Area = this.txtVendorArea.Text.Trim();
                        objectVendor.City = this.txtVendorCity.Text.Trim();
                        objectVendor.State = this.txtVendorState.Text.Trim();
                        objectVendor.Country = this.txtVendorCountry.Text.Trim();
                        objectVendor.Telephone = this.txtVendorTelephone.Text.Trim();
                        objectVendor.Fax = this.txtVendorFax.Text.Trim();
                        objectVendor.Contact_Person = this.txtVendorContactPerson.Text.Trim();

                        if (this._Logic_General.SaveVendor(objectVendor, this._SaveType) == true)
                        {
                            switch (this._SaveType)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        result = true;
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        result = true;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            switch (this._SaveType)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        result = false;
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        result = false;
                                        break;
                                    }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ValidateForm()
                {
                    Boolean result = false;
                    try
                    {
                        switch (this._SaveType)
                        {
                            case STP.Logic.Common.SaveType.Insert:
                                {
                                    result = this._ValidatorNew.Validate();
                                    break;
                                }
                            case STP.Logic.Common.SaveType.Update:
                                {
                                    result = this._ValidatorEdit.Validate();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

        #endregion
    }
}
