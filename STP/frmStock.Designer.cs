﻿namespace STP
{
    partial class frmStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpListing = new System.Windows.Forms.TabPage();
            this.pnlDetail = new System.Windows.Forms.Panel();
            this.lblStockPackagePrice2 = new System.Windows.Forms.Label();
            this.txtStockPackagePrice2 = new System.Windows.Forms.TextBox();
            this.lblStockPackagePrice1 = new System.Windows.Forms.Label();
            this.txtStockPackagePrice1 = new System.Windows.Forms.TextBox();
            this.txtGUID = new System.Windows.Forms.TextBox();
            this.cmbSubCategory = new System.Windows.Forms.ComboBox();
            this.lblSubCategory = new System.Windows.Forms.Label();
            this.lblPackageSize_E2 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_PackageUOM2 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_UOM2 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_2 = new System.Windows.Forms.Label();
            this.txtStockPackageUOM2 = new System.Windows.Forms.TextBox();
            this.lblPackageUnit2 = new System.Windows.Forms.Label();
            this.txtStockPackageSize2 = new System.Windows.Forms.TextBox();
            this.lblPackageSize2 = new System.Windows.Forms.Label();
            this.lblPriceLabel = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblPackageSize_E1 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_PackageUOM1 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_UOM1 = new System.Windows.Forms.Label();
            this.lblStockPackageSize_1 = new System.Windows.Forms.Label();
            this.txtStockPackageUOM1 = new System.Windows.Forms.TextBox();
            this.lblPackageUnit1 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.grpControlDetail = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtStockUOM = new System.Windows.Forms.TextBox();
            this.txtStockPackageSize1 = new System.Windows.Forms.TextBox();
            this.txtStockPrice = new System.Windows.Forms.TextBox();
            this.txtStockDesc2 = new System.Windows.Forms.TextBox();
            this.txtStockDesc1 = new System.Windows.Forms.TextBox();
            this.txtStockName = new System.Windows.Forms.TextBox();
            this.txtStockCode = new System.Windows.Forms.TextBox();
            this.txtStockID = new System.Windows.Forms.TextBox();
            this.lblStockUOM = new System.Windows.Forms.Label();
            this.lblPackageSize1 = new System.Windows.Forms.Label();
            this.lblStockDesc2 = new System.Windows.Forms.Label();
            this.lblStockDesc1 = new System.Windows.Forms.Label();
            this.lblStockName = new System.Windows.Forms.Label();
            this.lblStockCode = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.pnlListing = new System.Windows.Forms.Panel();
            this.lstSubCategory = new System.Windows.Forms.ListView();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstStockListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtStockSearch = new System.Windows.Forms.TextBox();
            this.tpReceive = new System.Windows.Forms.TabPage();
            this.pnlReceive = new System.Windows.Forms.Panel();
            this.txtReceiveSubCat = new System.Windows.Forms.TextBox();
            this.lblReceiveStockCode = new System.Windows.Forms.Label();
            this.txtReceiveStockCode = new System.Windows.Forms.TextBox();
            this.lblReceiveFrom = new System.Windows.Forms.Label();
            this.txtReceiveFrom = new System.Windows.Forms.TextBox();
            this.txtReceiveStockCategory = new System.Windows.Forms.TextBox();
            this.grpControl = new System.Windows.Forms.GroupBox();
            this.btnReceiveDelete = new System.Windows.Forms.Button();
            this.btnReceiveClear = new System.Windows.Forms.Button();
            this.btnReceiveClose = new System.Windows.Forms.Button();
            this.btnReceiveSave = new System.Windows.Forms.Button();
            this.btnReceiveRemove = new System.Windows.Forms.Button();
            this.lstReceiveListing = new System.Windows.Forms.ListView();
            this.txtReceiveStockID = new System.Windows.Forms.TextBox();
            this.btnReceiveAdd = new System.Windows.Forms.Button();
            this.lblReceiveUnit = new System.Windows.Forms.Label();
            this.cmbReceiveUnit = new System.Windows.Forms.ComboBox();
            this.txtReceiveQuantity = new System.Windows.Forms.TextBox();
            this.lblReceiveQuantity = new System.Windows.Forms.Label();
            this.txtReceiveStockName = new System.Windows.Forms.TextBox();
            this.lblReceiveStockName = new System.Windows.Forms.Label();
            this.btnReceiveSearchItem = new System.Windows.Forms.Button();
            this.txtReceiveRemark = new System.Windows.Forms.TextBox();
            this.lblReceiveRemark = new System.Windows.Forms.Label();
            this.txtReceiveDocumentNo = new System.Windows.Forms.TextBox();
            this.lblReceiveTransactionDate = new System.Windows.Forms.Label();
            this.lblReceiveDocumentNo = new System.Windows.Forms.Label();
            this.dtpReceiveTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.lblReceiveTransactionNo = new System.Windows.Forms.Label();
            this.txtReceiveTransactionNo = new System.Windows.Forms.TextBox();
            this.btnReceiveSelect = new System.Windows.Forms.Button();
            this.rbtReceiveEdit = new System.Windows.Forms.RadioButton();
            this.rbtReceiveNew = new System.Windows.Forms.RadioButton();
            this.tpAdjustment = new System.Windows.Forms.TabPage();
            this.pnlAdjustment = new System.Windows.Forms.Panel();
            this.lblAdjustmentUOM3 = new System.Windows.Forms.Label();
            this.lblAdjustmentUOM2 = new System.Windows.Forms.Label();
            this.lblAdjustmentUOM1 = new System.Windows.Forms.Label();
            this.txtAdjustmentSubCat = new System.Windows.Forms.TextBox();
            this.lblAdjustmentStockCode = new System.Windows.Forms.Label();
            this.txtAdjustmentStockCode = new System.Windows.Forms.TextBox();
            this.txtAdjustmentStockCategory = new System.Windows.Forms.TextBox();
            this.btnAdjustmentRemove = new System.Windows.Forms.Button();
            this.lblAdjustmentList = new System.Windows.Forms.Label();
            this.grpAdjustmentControl = new System.Windows.Forms.GroupBox();
            this.txtAdjustmentRemark = new System.Windows.Forms.TextBox();
            this.lblAdjustmentRemark = new System.Windows.Forms.Label();
            this.btnAdjustmentSave = new System.Windows.Forms.Button();
            this.btnAdjustmentClose = new System.Windows.Forms.Button();
            this.btnAdjustmentClear = new System.Windows.Forms.Button();
            this.btnAdjustmentSearchItem = new System.Windows.Forms.Button();
            this.lblAdjustmentStockName = new System.Windows.Forms.Label();
            this.txtAdjustmentStockName = new System.Windows.Forms.TextBox();
            this.lblAdjustmentNewBalance = new System.Windows.Forms.Label();
            this.lblAdjustmentQuantity = new System.Windows.Forms.Label();
            this.btnAdjustmentAdd = new System.Windows.Forms.Button();
            this.lstAdjustmentListing = new System.Windows.Forms.ListView();
            this.lblAdjustmentOldBalance = new System.Windows.Forms.Label();
            this.txtAdjustmentQuantity = new System.Windows.Forms.TextBox();
            this.txtAdjustmentNewBalance = new System.Windows.Forms.TextBox();
            this.txtAdjustmentOldBalance = new System.Windows.Forms.TextBox();
            this.txtAdjustmentStockID = new System.Windows.Forms.TextBox();
            this.tpCategory = new System.Windows.Forms.TabPage();
            this.pnlCategory = new System.Windows.Forms.Panel();
            this.grpSubCategory = new System.Windows.Forms.GroupBox();
            this.lblSubCategoryCategory = new System.Windows.Forms.Label();
            this.cmbSubCategoryCategory = new System.Windows.Forms.ComboBox();
            this.txtSubCategoryShortcut = new System.Windows.Forms.TextBox();
            this.lblSubCategoryShortcut = new System.Windows.Forms.Label();
            this.txtSubCategoryName = new System.Windows.Forms.TextBox();
            this.lblSubCategoryName = new System.Windows.Forms.Label();
            this.txtSubCategoryID = new System.Windows.Forms.TextBox();
            this.lblSubCategoryID = new System.Windows.Forms.Label();
            this.btnSubCategoryCancel = new System.Windows.Forms.Button();
            this.btnSubCategorySave = new System.Windows.Forms.Button();
            this.btnSubCategoryDelete = new System.Windows.Forms.Button();
            this.btnSubCategoryEdit = new System.Windows.Forms.Button();
            this.btnSubCategoryNew = new System.Windows.Forms.Button();
            this.lstCategorySubCategory = new System.Windows.Forms.ListView();
            this.btnCategoryClose = new System.Windows.Forms.Button();
            this.grpCategory = new System.Windows.Forms.GroupBox();
            this.txtCategoryShortcut = new System.Windows.Forms.TextBox();
            this.lblCategoryShortcut = new System.Windows.Forms.Label();
            this.txtCategoryName = new System.Windows.Forms.TextBox();
            this.lblCategoryName = new System.Windows.Forms.Label();
            this.txtCategoryID = new System.Windows.Forms.TextBox();
            this.lblCategoryID = new System.Windows.Forms.Label();
            this.btnCategoryCancel = new System.Windows.Forms.Button();
            this.btnCategorySave = new System.Windows.Forms.Button();
            this.btnCategoryDelete = new System.Windows.Forms.Button();
            this.btnCategoryEdit = new System.Windows.Forms.Button();
            this.btnCategoryNew = new System.Windows.Forms.Button();
            this.lstCategoryCategory = new System.Windows.Forms.ListView();
            this.erpStock = new System.Windows.Forms.ErrorProvider(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tabMain.SuspendLayout();
            this.tpListing.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.grpControlDetail.SuspendLayout();
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.tpReceive.SuspendLayout();
            this.pnlReceive.SuspendLayout();
            this.grpControl.SuspendLayout();
            this.tpAdjustment.SuspendLayout();
            this.pnlAdjustment.SuspendLayout();
            this.grpAdjustmentControl.SuspendLayout();
            this.tpCategory.SuspendLayout();
            this.pnlCategory.SuspendLayout();
            this.grpSubCategory.SuspendLayout();
            this.grpCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpStock)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tpListing);
            this.tabMain.Controls.Add(this.tpReceive);
            this.tabMain.Controls.Add(this.tpAdjustment);
            this.tabMain.Controls.Add(this.tpCategory);
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Margin = new System.Windows.Forms.Padding(0);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Drawing.Point(5, 5);
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(888, 630);
            this.tabMain.TabIndex = 1;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tpListing
            // 
            this.tpListing.BackColor = System.Drawing.Color.Transparent;
            this.tpListing.Controls.Add(this.pnlDetail);
            this.tpListing.Controls.Add(this.pnlListing);
            this.tpListing.Location = new System.Drawing.Point(4, 29);
            this.tpListing.Name = "tpListing";
            this.tpListing.Padding = new System.Windows.Forms.Padding(3);
            this.tpListing.Size = new System.Drawing.Size(880, 597);
            this.tpListing.TabIndex = 0;
            this.tpListing.Text = "Listing";
            this.tpListing.UseVisualStyleBackColor = true;
            // 
            // pnlDetail
            // 
            this.pnlDetail.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDetail.Controls.Add(this.lblStockPackagePrice2);
            this.pnlDetail.Controls.Add(this.txtStockPackagePrice2);
            this.pnlDetail.Controls.Add(this.lblStockPackagePrice1);
            this.pnlDetail.Controls.Add(this.txtStockPackagePrice1);
            this.pnlDetail.Controls.Add(this.txtGUID);
            this.pnlDetail.Controls.Add(this.cmbSubCategory);
            this.pnlDetail.Controls.Add(this.lblSubCategory);
            this.pnlDetail.Controls.Add(this.lblPackageSize_E2);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_PackageUOM2);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_UOM2);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_2);
            this.pnlDetail.Controls.Add(this.txtStockPackageUOM2);
            this.pnlDetail.Controls.Add(this.lblPackageUnit2);
            this.pnlDetail.Controls.Add(this.txtStockPackageSize2);
            this.pnlDetail.Controls.Add(this.lblPackageSize2);
            this.pnlDetail.Controls.Add(this.lblPriceLabel);
            this.pnlDetail.Controls.Add(this.cmbType);
            this.pnlDetail.Controls.Add(this.lblType);
            this.pnlDetail.Controls.Add(this.lblPackageSize_E1);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_PackageUOM1);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_UOM1);
            this.pnlDetail.Controls.Add(this.lblStockPackageSize_1);
            this.pnlDetail.Controls.Add(this.txtStockPackageUOM1);
            this.pnlDetail.Controls.Add(this.lblPackageUnit1);
            this.pnlDetail.Controls.Add(this.cmbCategory);
            this.pnlDetail.Controls.Add(this.lblCategory);
            this.pnlDetail.Controls.Add(this.grpControlDetail);
            this.pnlDetail.Controls.Add(this.txtStockUOM);
            this.pnlDetail.Controls.Add(this.txtStockPackageSize1);
            this.pnlDetail.Controls.Add(this.txtStockPrice);
            this.pnlDetail.Controls.Add(this.txtStockDesc2);
            this.pnlDetail.Controls.Add(this.txtStockDesc1);
            this.pnlDetail.Controls.Add(this.txtStockName);
            this.pnlDetail.Controls.Add(this.txtStockCode);
            this.pnlDetail.Controls.Add(this.txtStockID);
            this.pnlDetail.Controls.Add(this.lblStockUOM);
            this.pnlDetail.Controls.Add(this.lblPackageSize1);
            this.pnlDetail.Controls.Add(this.lblStockDesc2);
            this.pnlDetail.Controls.Add(this.lblStockDesc1);
            this.pnlDetail.Controls.Add(this.lblStockName);
            this.pnlDetail.Controls.Add(this.lblStockCode);
            this.pnlDetail.Controls.Add(this.lblID);
            this.pnlDetail.Location = new System.Drawing.Point(0, 0);
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.Size = new System.Drawing.Size(862, 586);
            this.pnlDetail.TabIndex = 1;
            this.pnlDetail.Visible = false;
            // 
            // lblStockPackagePrice2
            // 
            this.lblStockPackagePrice2.AutoSize = true;
            this.lblStockPackagePrice2.Location = new System.Drawing.Point(250, 430);
            this.lblStockPackagePrice2.Name = "lblStockPackagePrice2";
            this.lblStockPackagePrice2.Size = new System.Drawing.Size(117, 16);
            this.lblStockPackagePrice2.TabIndex = 74;
            this.lblStockPackagePrice2.Text = "Package Price (B)";
            // 
            // txtStockPackagePrice2
            // 
            this.txtStockPackagePrice2.Location = new System.Drawing.Point(370, 430);
            this.txtStockPackagePrice2.MaxLength = 7;
            this.txtStockPackagePrice2.Name = "txtStockPackagePrice2";
            this.txtStockPackagePrice2.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackagePrice2.TabIndex = 24;
            // 
            // lblStockPackagePrice1
            // 
            this.lblStockPackagePrice1.AutoSize = true;
            this.lblStockPackagePrice1.Location = new System.Drawing.Point(250, 340);
            this.lblStockPackagePrice1.Name = "lblStockPackagePrice1";
            this.lblStockPackagePrice1.Size = new System.Drawing.Size(117, 16);
            this.lblStockPackagePrice1.TabIndex = 72;
            this.lblStockPackagePrice1.Text = "Package Price (A)";
            // 
            // txtStockPackagePrice1
            // 
            this.txtStockPackagePrice1.Location = new System.Drawing.Point(370, 340);
            this.txtStockPackagePrice1.MaxLength = 7;
            this.txtStockPackagePrice1.Name = "txtStockPackagePrice1";
            this.txtStockPackagePrice1.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackagePrice1.TabIndex = 21;
            // 
            // txtGUID
            // 
            this.txtGUID.Enabled = false;
            this.txtGUID.Location = new System.Drawing.Point(402, 10);
            this.txtGUID.Name = "txtGUID";
            this.txtGUID.Size = new System.Drawing.Size(100, 22);
            this.txtGUID.TabIndex = 70;
            this.txtGUID.Visible = false;
            // 
            // cmbSubCategory
            // 
            this.cmbSubCategory.FormattingEnabled = true;
            this.cmbSubCategory.Location = new System.Drawing.Point(130, 100);
            this.cmbSubCategory.Name = "cmbSubCategory";
            this.cmbSubCategory.Size = new System.Drawing.Size(252, 24);
            this.cmbSubCategory.TabIndex = 13;
            // 
            // lblSubCategory
            // 
            this.lblSubCategory.AutoSize = true;
            this.lblSubCategory.Location = new System.Drawing.Point(10, 100);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(90, 16);
            this.lblSubCategory.TabIndex = 69;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // lblPackageSize_E2
            // 
            this.lblPackageSize_E2.AutoSize = true;
            this.lblPackageSize_E2.Location = new System.Drawing.Point(212, 460);
            this.lblPackageSize_E2.Name = "lblPackageSize_E2";
            this.lblPackageSize_E2.Size = new System.Drawing.Size(15, 16);
            this.lblPackageSize_E2.TabIndex = 67;
            this.lblPackageSize_E2.Text = "=";
            // 
            // lblStockPackageSize_PackageUOM2
            // 
            this.lblStockPackageSize_PackageUOM2.AutoSize = true;
            this.lblStockPackageSize_PackageUOM2.Location = new System.Drawing.Point(152, 460);
            this.lblStockPackageSize_PackageUOM2.Name = "lblStockPackageSize_PackageUOM2";
            this.lblStockPackageSize_PackageUOM2.Size = new System.Drawing.Size(0, 16);
            this.lblStockPackageSize_PackageUOM2.TabIndex = 66;
            // 
            // lblStockPackageSize_UOM2
            // 
            this.lblStockPackageSize_UOM2.AutoSize = true;
            this.lblStockPackageSize_UOM2.Location = new System.Drawing.Point(360, 460);
            this.lblStockPackageSize_UOM2.Name = "lblStockPackageSize_UOM2";
            this.lblStockPackageSize_UOM2.Size = new System.Drawing.Size(0, 16);
            this.lblStockPackageSize_UOM2.TabIndex = 65;
            // 
            // lblStockPackageSize_2
            // 
            this.lblStockPackageSize_2.AutoSize = true;
            this.lblStockPackageSize_2.Location = new System.Drawing.Point(132, 460);
            this.lblStockPackageSize_2.Name = "lblStockPackageSize_2";
            this.lblStockPackageSize_2.Size = new System.Drawing.Size(15, 16);
            this.lblStockPackageSize_2.TabIndex = 64;
            this.lblStockPackageSize_2.Text = "1";
            // 
            // txtStockPackageUOM2
            // 
            this.txtStockPackageUOM2.Location = new System.Drawing.Point(130, 430);
            this.txtStockPackageUOM2.MaxLength = 50;
            this.txtStockPackageUOM2.Name = "txtStockPackageUOM2";
            this.txtStockPackageUOM2.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackageUOM2.TabIndex = 23;
            this.txtStockPackageUOM2.TextChanged += new System.EventHandler(this.txtStockPackageUOM2_TextChanged);
            // 
            // lblPackageUnit2
            // 
            this.lblPackageUnit2.AutoSize = true;
            this.lblPackageUnit2.Location = new System.Drawing.Point(10, 430);
            this.lblPackageUnit2.Name = "lblPackageUnit2";
            this.lblPackageUnit2.Size = new System.Drawing.Size(109, 16);
            this.lblPackageUnit2.TabIndex = 63;
            this.lblPackageUnit2.Text = "Package Unit (B)";
            // 
            // txtStockPackageSize2
            // 
            this.txtStockPackageSize2.Location = new System.Drawing.Point(250, 460);
            this.txtStockPackageSize2.MaxLength = 7;
            this.txtStockPackageSize2.Name = "txtStockPackageSize2";
            this.txtStockPackageSize2.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackageSize2.TabIndex = 25;
            // 
            // lblPackageSize2
            // 
            this.lblPackageSize2.AutoSize = true;
            this.lblPackageSize2.Location = new System.Drawing.Point(10, 460);
            this.lblPackageSize2.Name = "lblPackageSize2";
            this.lblPackageSize2.Size = new System.Drawing.Size(112, 16);
            this.lblPackageSize2.TabIndex = 60;
            this.lblPackageSize2.Text = "Package Size (B)";
            // 
            // lblPriceLabel
            // 
            this.lblPriceLabel.AutoSize = true;
            this.lblPriceLabel.Location = new System.Drawing.Point(250, 280);
            this.lblPriceLabel.Name = "lblPriceLabel";
            this.lblPriceLabel.Size = new System.Drawing.Size(39, 16);
            this.lblPriceLabel.TabIndex = 59;
            this.lblPriceLabel.Text = "Price";
            // 
            // cmbType
            // 
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(130, 40);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(252, 24);
            this.cmbType.TabIndex = 11;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(10, 40);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 16);
            this.lblType.TabIndex = 50;
            this.lblType.Text = "Type";
            // 
            // lblPackageSize_E1
            // 
            this.lblPackageSize_E1.AutoSize = true;
            this.lblPackageSize_E1.Location = new System.Drawing.Point(212, 370);
            this.lblPackageSize_E1.Name = "lblPackageSize_E1";
            this.lblPackageSize_E1.Size = new System.Drawing.Size(15, 16);
            this.lblPackageSize_E1.TabIndex = 28;
            this.lblPackageSize_E1.Text = "=";
            // 
            // lblStockPackageSize_PackageUOM1
            // 
            this.lblStockPackageSize_PackageUOM1.AutoSize = true;
            this.lblStockPackageSize_PackageUOM1.Location = new System.Drawing.Point(152, 370);
            this.lblStockPackageSize_PackageUOM1.Name = "lblStockPackageSize_PackageUOM1";
            this.lblStockPackageSize_PackageUOM1.Size = new System.Drawing.Size(0, 16);
            this.lblStockPackageSize_PackageUOM1.TabIndex = 27;
            // 
            // lblStockPackageSize_UOM1
            // 
            this.lblStockPackageSize_UOM1.AutoSize = true;
            this.lblStockPackageSize_UOM1.Location = new System.Drawing.Point(360, 370);
            this.lblStockPackageSize_UOM1.Name = "lblStockPackageSize_UOM1";
            this.lblStockPackageSize_UOM1.Size = new System.Drawing.Size(0, 16);
            this.lblStockPackageSize_UOM1.TabIndex = 26;
            // 
            // lblStockPackageSize_1
            // 
            this.lblStockPackageSize_1.AutoSize = true;
            this.lblStockPackageSize_1.Location = new System.Drawing.Point(132, 370);
            this.lblStockPackageSize_1.Name = "lblStockPackageSize_1";
            this.lblStockPackageSize_1.Size = new System.Drawing.Size(15, 16);
            this.lblStockPackageSize_1.TabIndex = 25;
            this.lblStockPackageSize_1.Text = "1";
            // 
            // txtStockPackageUOM1
            // 
            this.txtStockPackageUOM1.Location = new System.Drawing.Point(130, 340);
            this.txtStockPackageUOM1.MaxLength = 50;
            this.txtStockPackageUOM1.Name = "txtStockPackageUOM1";
            this.txtStockPackageUOM1.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackageUOM1.TabIndex = 20;
            this.txtStockPackageUOM1.TextChanged += new System.EventHandler(this.txtStockPackageUOM1_TextChanged);
            // 
            // lblPackageUnit1
            // 
            this.lblPackageUnit1.AutoSize = true;
            this.lblPackageUnit1.Location = new System.Drawing.Point(10, 340);
            this.lblPackageUnit1.Name = "lblPackageUnit1";
            this.lblPackageUnit1.Size = new System.Drawing.Size(109, 16);
            this.lblPackageUnit1.TabIndex = 23;
            this.lblPackageUnit1.Text = "Package Unit (A)";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(130, 70);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(252, 24);
            this.cmbCategory.TabIndex = 12;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(10, 70);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(63, 16);
            this.lblCategory.TabIndex = 22;
            this.lblCategory.Text = "Category";
            // 
            // grpControlDetail
            // 
            this.grpControlDetail.Controls.Add(this.btnCancel);
            this.grpControlDetail.Controls.Add(this.btnSave);
            this.grpControlDetail.Location = new System.Drawing.Point(6, 507);
            this.grpControlDetail.Name = "grpControlDetail";
            this.grpControlDetail.Size = new System.Drawing.Size(848, 70);
            this.grpControlDetail.TabIndex = 21;
            this.grpControlDetail.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(722, 21);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 43);
            this.btnCancel.TabIndex = 30;
            this.btnCancel.Text = "Ca&ncel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(596, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 43);
            this.btnSave.TabIndex = 29;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtStockUOM
            // 
            this.txtStockUOM.Location = new System.Drawing.Point(130, 280);
            this.txtStockUOM.MaxLength = 50;
            this.txtStockUOM.Name = "txtStockUOM";
            this.txtStockUOM.Size = new System.Drawing.Size(100, 22);
            this.txtStockUOM.TabIndex = 18;
            this.txtStockUOM.TextChanged += new System.EventHandler(this.txtStockUOM_TextChanged);
            // 
            // txtStockPackageSize1
            // 
            this.txtStockPackageSize1.Location = new System.Drawing.Point(250, 370);
            this.txtStockPackageSize1.MaxLength = 7;
            this.txtStockPackageSize1.Name = "txtStockPackageSize1";
            this.txtStockPackageSize1.Size = new System.Drawing.Size(100, 22);
            this.txtStockPackageSize1.TabIndex = 22;
            // 
            // txtStockPrice
            // 
            this.txtStockPrice.Location = new System.Drawing.Point(370, 280);
            this.txtStockPrice.MaxLength = 7;
            this.txtStockPrice.Name = "txtStockPrice";
            this.txtStockPrice.Size = new System.Drawing.Size(100, 22);
            this.txtStockPrice.TabIndex = 19;
            // 
            // txtStockDesc2
            // 
            this.txtStockDesc2.Enabled = false;
            this.txtStockDesc2.Location = new System.Drawing.Point(130, 220);
            this.txtStockDesc2.MaxLength = 255;
            this.txtStockDesc2.Name = "txtStockDesc2";
            this.txtStockDesc2.Size = new System.Drawing.Size(378, 22);
            this.txtStockDesc2.TabIndex = 17;
            // 
            // txtStockDesc1
            // 
            this.txtStockDesc1.Enabled = false;
            this.txtStockDesc1.Location = new System.Drawing.Point(130, 190);
            this.txtStockDesc1.MaxLength = 255;
            this.txtStockDesc1.Name = "txtStockDesc1";
            this.txtStockDesc1.Size = new System.Drawing.Size(378, 22);
            this.txtStockDesc1.TabIndex = 16;
            // 
            // txtStockName
            // 
            this.txtStockName.Location = new System.Drawing.Point(130, 160);
            this.txtStockName.MaxLength = 255;
            this.txtStockName.Name = "txtStockName";
            this.txtStockName.Size = new System.Drawing.Size(252, 22);
            this.txtStockName.TabIndex = 15;
            // 
            // txtStockCode
            // 
            this.txtStockCode.Enabled = false;
            this.txtStockCode.Location = new System.Drawing.Point(130, 130);
            this.txtStockCode.MaxLength = 50;
            this.txtStockCode.Name = "txtStockCode";
            this.txtStockCode.Size = new System.Drawing.Size(252, 22);
            this.txtStockCode.TabIndex = 14;
            // 
            // txtStockID
            // 
            this.txtStockID.Enabled = false;
            this.txtStockID.Location = new System.Drawing.Point(130, 10);
            this.txtStockID.MaxLength = 6;
            this.txtStockID.Name = "txtStockID";
            this.txtStockID.Size = new System.Drawing.Size(100, 22);
            this.txtStockID.TabIndex = 10;
            // 
            // lblStockUOM
            // 
            this.lblStockUOM.AutoSize = true;
            this.lblStockUOM.Location = new System.Drawing.Point(10, 280);
            this.lblStockUOM.Name = "lblStockUOM";
            this.lblStockUOM.Size = new System.Drawing.Size(31, 16);
            this.lblStockUOM.TabIndex = 10;
            this.lblStockUOM.Text = "Unit";
            // 
            // lblPackageSize1
            // 
            this.lblPackageSize1.AutoSize = true;
            this.lblPackageSize1.Location = new System.Drawing.Point(10, 370);
            this.lblPackageSize1.Name = "lblPackageSize1";
            this.lblPackageSize1.Size = new System.Drawing.Size(112, 16);
            this.lblPackageSize1.TabIndex = 7;
            this.lblPackageSize1.Text = "Package Size (A)";
            // 
            // lblStockDesc2
            // 
            this.lblStockDesc2.AutoSize = true;
            this.lblStockDesc2.Location = new System.Drawing.Point(10, 220);
            this.lblStockDesc2.Name = "lblStockDesc2";
            this.lblStockDesc2.Size = new System.Drawing.Size(86, 16);
            this.lblStockDesc2.TabIndex = 5;
            this.lblStockDesc2.Text = "Description 2";
            // 
            // lblStockDesc1
            // 
            this.lblStockDesc1.AutoSize = true;
            this.lblStockDesc1.Location = new System.Drawing.Point(10, 190);
            this.lblStockDesc1.Name = "lblStockDesc1";
            this.lblStockDesc1.Size = new System.Drawing.Size(86, 16);
            this.lblStockDesc1.TabIndex = 4;
            this.lblStockDesc1.Text = "Description 1";
            // 
            // lblStockName
            // 
            this.lblStockName.AutoSize = true;
            this.lblStockName.Location = new System.Drawing.Point(10, 160);
            this.lblStockName.Name = "lblStockName";
            this.lblStockName.Size = new System.Drawing.Size(45, 16);
            this.lblStockName.TabIndex = 3;
            this.lblStockName.Text = "Name";
            // 
            // lblStockCode
            // 
            this.lblStockCode.AutoSize = true;
            this.lblStockCode.Location = new System.Drawing.Point(10, 130);
            this.lblStockCode.Name = "lblStockCode";
            this.lblStockCode.Size = new System.Drawing.Size(78, 16);
            this.lblStockCode.TabIndex = 2;
            this.lblStockCode.Text = "Stock Code";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(10, 10);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(21, 16);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.SystemColors.Control;
            this.pnlListing.Controls.Add(this.lstSubCategory);
            this.pnlListing.Controls.Add(this.lstCategory);
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstStockListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtStockSearch);
            this.pnlListing.Location = new System.Drawing.Point(0, 0);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 586);
            this.pnlListing.TabIndex = 0;
            // 
            // lstSubCategory
            // 
            this.lstSubCategory.FullRowSelect = true;
            this.lstSubCategory.GridLines = true;
            this.lstSubCategory.HideSelection = false;
            this.lstSubCategory.Location = new System.Drawing.Point(204, 34);
            this.lstSubCategory.MultiSelect = false;
            this.lstSubCategory.Name = "lstSubCategory";
            this.lstSubCategory.Size = new System.Drawing.Size(192, 467);
            this.lstSubCategory.TabIndex = 6;
            this.lstSubCategory.UseCompatibleStateImageBehavior = false;
            this.lstSubCategory.View = System.Windows.Forms.View.Details;
            this.lstSubCategory.SelectedIndexChanged += new System.EventHandler(this.lstSubCategory_SelectedIndexChanged);
            this.lstSubCategory.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstSubCategory_ColumnClick);
            // 
            // lstCategory
            // 
            this.lstCategory.FullRowSelect = true;
            this.lstCategory.GridLines = true;
            this.lstCategory.HideSelection = false;
            this.lstCategory.Location = new System.Drawing.Point(6, 34);
            this.lstCategory.MultiSelect = false;
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(192, 467);
            this.lstCategory.TabIndex = 5;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            this.lstCategory.View = System.Windows.Forms.View.Details;
            this.lstCategory.SelectedIndexChanged += new System.EventHandler(this.lstCategory_SelectedIndexChanged);
            this.lstCategory.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCategory_ColumnClick);
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnDelete);
            this.gpbControlListing.Controls.Add(this.btnEdit);
            this.gpbControlListing.Controls.Add(this.btnAdd);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 507);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(258, 21);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 43);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(132, 21);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(120, 43);
            this.btnEdit.TabIndex = 7;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(6, 21);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(120, 43);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "&Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstStockListing
            // 
            this.lstStockListing.FullRowSelect = true;
            this.lstStockListing.GridLines = true;
            this.lstStockListing.HideSelection = false;
            this.lstStockListing.Location = new System.Drawing.Point(402, 34);
            this.lstStockListing.MultiSelect = false;
            this.lstStockListing.Name = "lstStockListing";
            this.lstStockListing.Size = new System.Drawing.Size(452, 467);
            this.lstStockListing.TabIndex = 0;
            this.lstStockListing.UseCompatibleStateImageBehavior = false;
            this.lstStockListing.View = System.Windows.Forms.View.Details;
            this.lstStockListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstStockListing_MouseDoubleClick);
            this.lstStockListing.SelectedIndexChanged += new System.EventHandler(this.lstStockListing_SelectedIndexChanged);
            this.lstStockListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstStockListing_ColumnClick);
            this.lstStockListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstStockListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(720, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(130, 25);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search &Item";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtStockSearch
            // 
            this.txtStockSearch.Location = new System.Drawing.Point(6, 6);
            this.txtStockSearch.MaxLength = 255;
            this.txtStockSearch.Name = "txtStockSearch";
            this.txtStockSearch.Size = new System.Drawing.Size(700, 22);
            this.txtStockSearch.TabIndex = 2;
            this.txtStockSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStockSearch_KeyDown);
            // 
            // tpReceive
            // 
            this.tpReceive.BackColor = System.Drawing.Color.Transparent;
            this.tpReceive.Controls.Add(this.pnlReceive);
            this.tpReceive.Location = new System.Drawing.Point(4, 29);
            this.tpReceive.Name = "tpReceive";
            this.tpReceive.Size = new System.Drawing.Size(880, 597);
            this.tpReceive.TabIndex = 2;
            this.tpReceive.Text = "Stock In";
            this.tpReceive.UseVisualStyleBackColor = true;
            // 
            // pnlReceive
            // 
            this.pnlReceive.BackColor = System.Drawing.SystemColors.Control;
            this.pnlReceive.Controls.Add(this.txtReceiveSubCat);
            this.pnlReceive.Controls.Add(this.lblReceiveStockCode);
            this.pnlReceive.Controls.Add(this.txtReceiveStockCode);
            this.pnlReceive.Controls.Add(this.lblReceiveFrom);
            this.pnlReceive.Controls.Add(this.txtReceiveFrom);
            this.pnlReceive.Controls.Add(this.txtReceiveStockCategory);
            this.pnlReceive.Controls.Add(this.grpControl);
            this.pnlReceive.Controls.Add(this.btnReceiveRemove);
            this.pnlReceive.Controls.Add(this.lstReceiveListing);
            this.pnlReceive.Controls.Add(this.txtReceiveStockID);
            this.pnlReceive.Controls.Add(this.btnReceiveAdd);
            this.pnlReceive.Controls.Add(this.lblReceiveUnit);
            this.pnlReceive.Controls.Add(this.cmbReceiveUnit);
            this.pnlReceive.Controls.Add(this.txtReceiveQuantity);
            this.pnlReceive.Controls.Add(this.lblReceiveQuantity);
            this.pnlReceive.Controls.Add(this.txtReceiveStockName);
            this.pnlReceive.Controls.Add(this.lblReceiveStockName);
            this.pnlReceive.Controls.Add(this.btnReceiveSearchItem);
            this.pnlReceive.Controls.Add(this.txtReceiveRemark);
            this.pnlReceive.Controls.Add(this.lblReceiveRemark);
            this.pnlReceive.Controls.Add(this.txtReceiveDocumentNo);
            this.pnlReceive.Controls.Add(this.lblReceiveTransactionDate);
            this.pnlReceive.Controls.Add(this.lblReceiveDocumentNo);
            this.pnlReceive.Controls.Add(this.dtpReceiveTransactionDate);
            this.pnlReceive.Controls.Add(this.lblReceiveTransactionNo);
            this.pnlReceive.Controls.Add(this.txtReceiveTransactionNo);
            this.pnlReceive.Controls.Add(this.btnReceiveSelect);
            this.pnlReceive.Controls.Add(this.rbtReceiveEdit);
            this.pnlReceive.Controls.Add(this.rbtReceiveNew);
            this.pnlReceive.Location = new System.Drawing.Point(0, 0);
            this.pnlReceive.Name = "pnlReceive";
            this.pnlReceive.Size = new System.Drawing.Size(862, 586);
            this.pnlReceive.TabIndex = 0;
            // 
            // txtReceiveSubCat
            // 
            this.txtReceiveSubCat.Enabled = false;
            this.txtReceiveSubCat.Location = new System.Drawing.Point(542, 218);
            this.txtReceiveSubCat.Name = "txtReceiveSubCat";
            this.txtReceiveSubCat.Size = new System.Drawing.Size(100, 22);
            this.txtReceiveSubCat.TabIndex = 77;
            this.txtReceiveSubCat.Visible = false;
            // 
            // lblReceiveStockCode
            // 
            this.lblReceiveStockCode.AutoSize = true;
            this.lblReceiveStockCode.Location = new System.Drawing.Point(330, 160);
            this.lblReceiveStockCode.Name = "lblReceiveStockCode";
            this.lblReceiveStockCode.Size = new System.Drawing.Size(41, 16);
            this.lblReceiveStockCode.TabIndex = 76;
            this.lblReceiveStockCode.Text = "Code";
            // 
            // txtReceiveStockCode
            // 
            this.txtReceiveStockCode.Enabled = false;
            this.txtReceiveStockCode.Location = new System.Drawing.Point(330, 190);
            this.txtReceiveStockCode.Name = "txtReceiveStockCode";
            this.txtReceiveStockCode.Size = new System.Drawing.Size(73, 22);
            this.txtReceiveStockCode.TabIndex = 75;
            // 
            // lblReceiveFrom
            // 
            this.lblReceiveFrom.AutoSize = true;
            this.lblReceiveFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveFrom.Location = new System.Drawing.Point(146, 40);
            this.lblReceiveFrom.Name = "lblReceiveFrom";
            this.lblReceiveFrom.Size = new System.Drawing.Size(39, 16);
            this.lblReceiveFrom.TabIndex = 74;
            this.lblReceiveFrom.Text = "From";
            // 
            // txtReceiveFrom
            // 
            this.txtReceiveFrom.Location = new System.Drawing.Point(146, 60);
            this.txtReceiveFrom.MaxLength = 50;
            this.txtReceiveFrom.Name = "txtReceiveFrom";
            this.txtReceiveFrom.Size = new System.Drawing.Size(100, 22);
            this.txtReceiveFrom.TabIndex = 73;
            // 
            // txtReceiveStockCategory
            // 
            this.txtReceiveStockCategory.Enabled = false;
            this.txtReceiveStockCategory.Location = new System.Drawing.Point(436, 218);
            this.txtReceiveStockCategory.Name = "txtReceiveStockCategory";
            this.txtReceiveStockCategory.Size = new System.Drawing.Size(100, 22);
            this.txtReceiveStockCategory.TabIndex = 72;
            this.txtReceiveStockCategory.Visible = false;
            // 
            // grpControl
            // 
            this.grpControl.Controls.Add(this.btnReceiveDelete);
            this.grpControl.Controls.Add(this.btnReceiveClear);
            this.grpControl.Controls.Add(this.btnReceiveClose);
            this.grpControl.Controls.Add(this.btnReceiveSave);
            this.grpControl.Location = new System.Drawing.Point(6, 507);
            this.grpControl.Name = "grpControl";
            this.grpControl.Size = new System.Drawing.Size(848, 70);
            this.grpControl.TabIndex = 71;
            this.grpControl.TabStop = false;
            // 
            // btnReceiveDelete
            // 
            this.btnReceiveDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveDelete.Location = new System.Drawing.Point(4, 21);
            this.btnReceiveDelete.Name = "btnReceiveDelete";
            this.btnReceiveDelete.Size = new System.Drawing.Size(120, 43);
            this.btnReceiveDelete.TabIndex = 35;
            this.btnReceiveDelete.Text = "&Delete";
            this.btnReceiveDelete.UseVisualStyleBackColor = true;
            this.btnReceiveDelete.Click += new System.EventHandler(this.btnReceiveDelete_Click);
            // 
            // btnReceiveClear
            // 
            this.btnReceiveClear.Location = new System.Drawing.Point(596, 21);
            this.btnReceiveClear.Name = "btnReceiveClear";
            this.btnReceiveClear.Size = new System.Drawing.Size(120, 43);
            this.btnReceiveClear.TabIndex = 34;
            this.btnReceiveClear.Text = "&Clear";
            this.btnReceiveClear.UseVisualStyleBackColor = true;
            this.btnReceiveClear.Click += new System.EventHandler(this.btnReceiveClear_Click);
            // 
            // btnReceiveClose
            // 
            this.btnReceiveClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveClose.Location = new System.Drawing.Point(722, 21);
            this.btnReceiveClose.Name = "btnReceiveClose";
            this.btnReceiveClose.Size = new System.Drawing.Size(120, 43);
            this.btnReceiveClose.TabIndex = 27;
            this.btnReceiveClose.Text = "&Close";
            this.btnReceiveClose.UseVisualStyleBackColor = true;
            this.btnReceiveClose.Click += new System.EventHandler(this.btnReceiveClose_Click);
            // 
            // btnReceiveSave
            // 
            this.btnReceiveSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveSave.Location = new System.Drawing.Point(470, 21);
            this.btnReceiveSave.Name = "btnReceiveSave";
            this.btnReceiveSave.Size = new System.Drawing.Size(120, 43);
            this.btnReceiveSave.TabIndex = 26;
            this.btnReceiveSave.Text = "&Save";
            this.btnReceiveSave.UseVisualStyleBackColor = true;
            this.btnReceiveSave.Click += new System.EventHandler(this.btnReceiveSave_Click);
            // 
            // btnReceiveRemove
            // 
            this.btnReceiveRemove.Location = new System.Drawing.Point(10, 476);
            this.btnReceiveRemove.Name = "btnReceiveRemove";
            this.btnReceiveRemove.Size = new System.Drawing.Size(180, 25);
            this.btnReceiveRemove.TabIndex = 70;
            this.btnReceiveRemove.Text = "&Remove From List";
            this.btnReceiveRemove.UseVisualStyleBackColor = true;
            this.btnReceiveRemove.Click += new System.EventHandler(this.btnReceiveRemove_Click);
            // 
            // lstReceiveListing
            // 
            this.lstReceiveListing.FullRowSelect = true;
            this.lstReceiveListing.GridLines = true;
            this.lstReceiveListing.HideSelection = false;
            this.lstReceiveListing.Location = new System.Drawing.Point(10, 249);
            this.lstReceiveListing.MultiSelect = false;
            this.lstReceiveListing.Name = "lstReceiveListing";
            this.lstReceiveListing.Size = new System.Drawing.Size(838, 221);
            this.lstReceiveListing.TabIndex = 69;
            this.lstReceiveListing.UseCompatibleStateImageBehavior = false;
            this.lstReceiveListing.View = System.Windows.Forms.View.Details;
            this.lstReceiveListing.SelectedIndexChanged += new System.EventHandler(this.lstReceiveListing_SelectedIndexChanged);
            this.lstReceiveListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstReceiveListing_ColumnClick);
            // 
            // txtReceiveStockID
            // 
            this.txtReceiveStockID.Enabled = false;
            this.txtReceiveStockID.Location = new System.Drawing.Point(330, 218);
            this.txtReceiveStockID.Name = "txtReceiveStockID";
            this.txtReceiveStockID.Size = new System.Drawing.Size(100, 22);
            this.txtReceiveStockID.TabIndex = 66;
            this.txtReceiveStockID.Visible = false;
            this.txtReceiveStockID.TextChanged += new System.EventHandler(this.txtReceiveStockID_TextChanged);
            // 
            // btnReceiveAdd
            // 
            this.btnReceiveAdd.Location = new System.Drawing.Point(620, 190);
            this.btnReceiveAdd.Name = "btnReceiveAdd";
            this.btnReceiveAdd.Size = new System.Drawing.Size(74, 25);
            this.btnReceiveAdd.TabIndex = 65;
            this.btnReceiveAdd.Text = "&Add";
            this.btnReceiveAdd.UseVisualStyleBackColor = true;
            this.btnReceiveAdd.Click += new System.EventHandler(this.btnReceiveAdd_Click);
            // 
            // lblReceiveUnit
            // 
            this.lblReceiveUnit.AutoSize = true;
            this.lblReceiveUnit.Location = new System.Drawing.Point(490, 160);
            this.lblReceiveUnit.Name = "lblReceiveUnit";
            this.lblReceiveUnit.Size = new System.Drawing.Size(31, 16);
            this.lblReceiveUnit.TabIndex = 68;
            this.lblReceiveUnit.Text = "Unit";
            // 
            // cmbReceiveUnit
            // 
            this.cmbReceiveUnit.FormattingEnabled = true;
            this.cmbReceiveUnit.Location = new System.Drawing.Point(490, 190);
            this.cmbReceiveUnit.Name = "cmbReceiveUnit";
            this.cmbReceiveUnit.Size = new System.Drawing.Size(120, 24);
            this.cmbReceiveUnit.TabIndex = 64;
            this.cmbReceiveUnit.SelectedIndexChanged += new System.EventHandler(this.cmbReceiveUnit_SelectedIndexChanged);
            // 
            // txtReceiveQuantity
            // 
            this.txtReceiveQuantity.Location = new System.Drawing.Point(409, 190);
            this.txtReceiveQuantity.MaxLength = 7;
            this.txtReceiveQuantity.Name = "txtReceiveQuantity";
            this.txtReceiveQuantity.Size = new System.Drawing.Size(70, 22);
            this.txtReceiveQuantity.TabIndex = 63;
            this.txtReceiveQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtReceiveQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReceiveQuantity_KeyDown);
            // 
            // lblReceiveQuantity
            // 
            this.lblReceiveQuantity.AutoSize = true;
            this.lblReceiveQuantity.Location = new System.Drawing.Point(409, 160);
            this.lblReceiveQuantity.Name = "lblReceiveQuantity";
            this.lblReceiveQuantity.Size = new System.Drawing.Size(56, 16);
            this.lblReceiveQuantity.TabIndex = 67;
            this.lblReceiveQuantity.Text = "Quantity";
            // 
            // txtReceiveStockName
            // 
            this.txtReceiveStockName.Location = new System.Drawing.Point(10, 190);
            this.txtReceiveStockName.MaxLength = 255;
            this.txtReceiveStockName.Name = "txtReceiveStockName";
            this.txtReceiveStockName.Size = new System.Drawing.Size(314, 22);
            this.txtReceiveStockName.TabIndex = 61;
            this.txtReceiveStockName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReceiveStockName_KeyDown);
            this.txtReceiveStockName.Leave += new System.EventHandler(this.txtReceiveStockName_Leave);
            // 
            // lblReceiveStockName
            // 
            this.lblReceiveStockName.AutoSize = true;
            this.lblReceiveStockName.Location = new System.Drawing.Point(10, 160);
            this.lblReceiveStockName.Name = "lblReceiveStockName";
            this.lblReceiveStockName.Size = new System.Drawing.Size(82, 16);
            this.lblReceiveStockName.TabIndex = 60;
            this.lblReceiveStockName.Text = "Stock Name";
            // 
            // btnReceiveSearchItem
            // 
            this.btnReceiveSearchItem.Location = new System.Drawing.Point(10, 218);
            this.btnReceiveSearchItem.Name = "btnReceiveSearchItem";
            this.btnReceiveSearchItem.Size = new System.Drawing.Size(130, 25);
            this.btnReceiveSearchItem.TabIndex = 62;
            this.btnReceiveSearchItem.Text = "Select &Item";
            this.btnReceiveSearchItem.UseVisualStyleBackColor = true;
            this.btnReceiveSearchItem.Click += new System.EventHandler(this.btnReceiveSearchItem_Click);
            // 
            // txtReceiveRemark
            // 
            this.txtReceiveRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiveRemark.Location = new System.Drawing.Point(10, 116);
            this.txtReceiveRemark.MaxLength = 255;
            this.txtReceiveRemark.Name = "txtReceiveRemark";
            this.txtReceiveRemark.Size = new System.Drawing.Size(692, 22);
            this.txtReceiveRemark.TabIndex = 59;
            // 
            // lblReceiveRemark
            // 
            this.lblReceiveRemark.AutoSize = true;
            this.lblReceiveRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveRemark.Location = new System.Drawing.Point(10, 96);
            this.lblReceiveRemark.Name = "lblReceiveRemark";
            this.lblReceiveRemark.Size = new System.Drawing.Size(56, 16);
            this.lblReceiveRemark.TabIndex = 58;
            this.lblReceiveRemark.Text = "Remark";
            // 
            // txtReceiveDocumentNo
            // 
            this.txtReceiveDocumentNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiveDocumentNo.Location = new System.Drawing.Point(252, 60);
            this.txtReceiveDocumentNo.MaxLength = 50;
            this.txtReceiveDocumentNo.Name = "txtReceiveDocumentNo";
            this.txtReceiveDocumentNo.Size = new System.Drawing.Size(130, 22);
            this.txtReceiveDocumentNo.TabIndex = 47;
            // 
            // lblReceiveTransactionDate
            // 
            this.lblReceiveTransactionDate.AutoSize = true;
            this.lblReceiveTransactionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveTransactionDate.Location = new System.Drawing.Point(392, 40);
            this.lblReceiveTransactionDate.Name = "lblReceiveTransactionDate";
            this.lblReceiveTransactionDate.Size = new System.Drawing.Size(111, 16);
            this.lblReceiveTransactionDate.TabIndex = 49;
            this.lblReceiveTransactionDate.Text = "Transaction Date";
            // 
            // lblReceiveDocumentNo
            // 
            this.lblReceiveDocumentNo.AutoSize = true;
            this.lblReceiveDocumentNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveDocumentNo.Location = new System.Drawing.Point(252, 40);
            this.lblReceiveDocumentNo.Name = "lblReceiveDocumentNo";
            this.lblReceiveDocumentNo.Size = new System.Drawing.Size(119, 16);
            this.lblReceiveDocumentNo.TabIndex = 48;
            this.lblReceiveDocumentNo.Text = "Delivery Order No.";
            // 
            // dtpReceiveTransactionDate
            // 
            this.dtpReceiveTransactionDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpReceiveTransactionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpReceiveTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpReceiveTransactionDate.Location = new System.Drawing.Point(392, 60);
            this.dtpReceiveTransactionDate.Name = "dtpReceiveTransactionDate";
            this.dtpReceiveTransactionDate.ShowCheckBox = true;
            this.dtpReceiveTransactionDate.Size = new System.Drawing.Size(130, 22);
            this.dtpReceiveTransactionDate.TabIndex = 50;
            // 
            // lblReceiveTransactionNo
            // 
            this.lblReceiveTransactionNo.AutoSize = true;
            this.lblReceiveTransactionNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceiveTransactionNo.Location = new System.Drawing.Point(10, 40);
            this.lblReceiveTransactionNo.Name = "lblReceiveTransactionNo";
            this.lblReceiveTransactionNo.Size = new System.Drawing.Size(103, 16);
            this.lblReceiveTransactionNo.TabIndex = 46;
            this.lblReceiveTransactionNo.Text = "Transaction No.";
            // 
            // txtReceiveTransactionNo
            // 
            this.txtReceiveTransactionNo.Enabled = false;
            this.txtReceiveTransactionNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiveTransactionNo.Location = new System.Drawing.Point(10, 60);
            this.txtReceiveTransactionNo.Name = "txtReceiveTransactionNo";
            this.txtReceiveTransactionNo.Size = new System.Drawing.Size(130, 22);
            this.txtReceiveTransactionNo.TabIndex = 45;
            this.txtReceiveTransactionNo.TextChanged += new System.EventHandler(this.txtReceiveTransactionNo_TextChanged);
            // 
            // btnReceiveSelect
            // 
            this.btnReceiveSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveSelect.Location = new System.Drawing.Point(200, 10);
            this.btnReceiveSelect.Name = "btnReceiveSelect";
            this.btnReceiveSelect.Size = new System.Drawing.Size(150, 25);
            this.btnReceiveSelect.TabIndex = 39;
            this.btnReceiveSelect.Text = "Select &Transaction";
            this.btnReceiveSelect.UseVisualStyleBackColor = true;
            this.btnReceiveSelect.Click += new System.EventHandler(this.btnReceiveSelect_Click);
            // 
            // rbtReceiveEdit
            // 
            this.rbtReceiveEdit.AutoSize = true;
            this.rbtReceiveEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtReceiveEdit.Location = new System.Drawing.Point(100, 10);
            this.rbtReceiveEdit.Name = "rbtReceiveEdit";
            this.rbtReceiveEdit.Size = new System.Drawing.Size(49, 20);
            this.rbtReceiveEdit.TabIndex = 38;
            this.rbtReceiveEdit.Text = "Edit";
            this.rbtReceiveEdit.UseVisualStyleBackColor = true;
            this.rbtReceiveEdit.CheckedChanged += new System.EventHandler(this.rbtReceiveEdit_CheckedChanged);
            // 
            // rbtReceiveNew
            // 
            this.rbtReceiveNew.AutoSize = true;
            this.rbtReceiveNew.Checked = true;
            this.rbtReceiveNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtReceiveNew.Location = new System.Drawing.Point(10, 10);
            this.rbtReceiveNew.Name = "rbtReceiveNew";
            this.rbtReceiveNew.Size = new System.Drawing.Size(53, 20);
            this.rbtReceiveNew.TabIndex = 37;
            this.rbtReceiveNew.TabStop = true;
            this.rbtReceiveNew.Text = "New";
            this.rbtReceiveNew.UseVisualStyleBackColor = true;
            this.rbtReceiveNew.CheckedChanged += new System.EventHandler(this.rbtReceiveNew_CheckedChanged);
            // 
            // tpAdjustment
            // 
            this.tpAdjustment.BackColor = System.Drawing.Color.Transparent;
            this.tpAdjustment.Controls.Add(this.pnlAdjustment);
            this.tpAdjustment.Location = new System.Drawing.Point(4, 29);
            this.tpAdjustment.Name = "tpAdjustment";
            this.tpAdjustment.Padding = new System.Windows.Forms.Padding(3);
            this.tpAdjustment.Size = new System.Drawing.Size(880, 597);
            this.tpAdjustment.TabIndex = 1;
            this.tpAdjustment.Text = "Adjustment";
            this.tpAdjustment.UseVisualStyleBackColor = true;
            // 
            // pnlAdjustment
            // 
            this.pnlAdjustment.BackColor = System.Drawing.SystemColors.Control;
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentUOM3);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentUOM2);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentUOM1);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentSubCat);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentStockCode);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentStockCode);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentStockCategory);
            this.pnlAdjustment.Controls.Add(this.btnAdjustmentRemove);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentList);
            this.pnlAdjustment.Controls.Add(this.grpAdjustmentControl);
            this.pnlAdjustment.Controls.Add(this.btnAdjustmentSearchItem);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentStockName);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentStockName);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentNewBalance);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentQuantity);
            this.pnlAdjustment.Controls.Add(this.btnAdjustmentAdd);
            this.pnlAdjustment.Controls.Add(this.lstAdjustmentListing);
            this.pnlAdjustment.Controls.Add(this.lblAdjustmentOldBalance);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentQuantity);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentNewBalance);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentOldBalance);
            this.pnlAdjustment.Controls.Add(this.txtAdjustmentStockID);
            this.pnlAdjustment.Location = new System.Drawing.Point(0, 0);
            this.pnlAdjustment.Name = "pnlAdjustment";
            this.pnlAdjustment.Size = new System.Drawing.Size(888, 586);
            this.pnlAdjustment.TabIndex = 31;
            // 
            // lblAdjustmentUOM3
            // 
            this.lblAdjustmentUOM3.AutoSize = true;
            this.lblAdjustmentUOM3.Location = new System.Drawing.Point(725, 41);
            this.lblAdjustmentUOM3.Name = "lblAdjustmentUOM3";
            this.lblAdjustmentUOM3.Size = new System.Drawing.Size(0, 16);
            this.lblAdjustmentUOM3.TabIndex = 37;
            // 
            // lblAdjustmentUOM2
            // 
            this.lblAdjustmentUOM2.AutoSize = true;
            this.lblAdjustmentUOM2.Location = new System.Drawing.Point(605, 40);
            this.lblAdjustmentUOM2.Name = "lblAdjustmentUOM2";
            this.lblAdjustmentUOM2.Size = new System.Drawing.Size(0, 16);
            this.lblAdjustmentUOM2.TabIndex = 36;
            // 
            // lblAdjustmentUOM1
            // 
            this.lblAdjustmentUOM1.AutoSize = true;
            this.lblAdjustmentUOM1.Location = new System.Drawing.Point(485, 40);
            this.lblAdjustmentUOM1.Name = "lblAdjustmentUOM1";
            this.lblAdjustmentUOM1.Size = new System.Drawing.Size(0, 16);
            this.lblAdjustmentUOM1.TabIndex = 35;
            // 
            // txtAdjustmentSubCat
            // 
            this.txtAdjustmentSubCat.Enabled = false;
            this.txtAdjustmentSubCat.Location = new System.Drawing.Point(501, 71);
            this.txtAdjustmentSubCat.Name = "txtAdjustmentSubCat";
            this.txtAdjustmentSubCat.Size = new System.Drawing.Size(100, 22);
            this.txtAdjustmentSubCat.TabIndex = 34;
            this.txtAdjustmentSubCat.Visible = false;
            // 
            // lblAdjustmentStockCode
            // 
            this.lblAdjustmentStockCode.AutoSize = true;
            this.lblAdjustmentStockCode.Location = new System.Drawing.Point(297, 10);
            this.lblAdjustmentStockCode.Name = "lblAdjustmentStockCode";
            this.lblAdjustmentStockCode.Size = new System.Drawing.Size(41, 16);
            this.lblAdjustmentStockCode.TabIndex = 33;
            this.lblAdjustmentStockCode.Text = "Code";
            // 
            // txtAdjustmentStockCode
            // 
            this.txtAdjustmentStockCode.Enabled = false;
            this.txtAdjustmentStockCode.Location = new System.Drawing.Point(297, 40);
            this.txtAdjustmentStockCode.Name = "txtAdjustmentStockCode";
            this.txtAdjustmentStockCode.Size = new System.Drawing.Size(92, 22);
            this.txtAdjustmentStockCode.TabIndex = 32;
            // 
            // txtAdjustmentStockCategory
            // 
            this.txtAdjustmentStockCategory.Enabled = false;
            this.txtAdjustmentStockCategory.Location = new System.Drawing.Point(395, 71);
            this.txtAdjustmentStockCategory.Name = "txtAdjustmentStockCategory";
            this.txtAdjustmentStockCategory.Size = new System.Drawing.Size(100, 22);
            this.txtAdjustmentStockCategory.TabIndex = 31;
            this.txtAdjustmentStockCategory.Visible = false;
            // 
            // btnAdjustmentRemove
            // 
            this.btnAdjustmentRemove.Location = new System.Drawing.Point(10, 476);
            this.btnAdjustmentRemove.Name = "btnAdjustmentRemove";
            this.btnAdjustmentRemove.Size = new System.Drawing.Size(180, 25);
            this.btnAdjustmentRemove.TabIndex = 30;
            this.btnAdjustmentRemove.Text = "&Remove From List";
            this.btnAdjustmentRemove.UseVisualStyleBackColor = true;
            this.btnAdjustmentRemove.Click += new System.EventHandler(this.btnAdjustmentRemove_Click);
            // 
            // lblAdjustmentList
            // 
            this.lblAdjustmentList.AutoSize = true;
            this.lblAdjustmentList.Location = new System.Drawing.Point(10, 115);
            this.lblAdjustmentList.Name = "lblAdjustmentList";
            this.lblAdjustmentList.Size = new System.Drawing.Size(134, 16);
            this.lblAdjustmentList.TabIndex = 23;
            this.lblAdjustmentList.Text = "Stock Adjustment List";
            // 
            // grpAdjustmentControl
            // 
            this.grpAdjustmentControl.Controls.Add(this.txtAdjustmentRemark);
            this.grpAdjustmentControl.Controls.Add(this.lblAdjustmentRemark);
            this.grpAdjustmentControl.Controls.Add(this.btnAdjustmentSave);
            this.grpAdjustmentControl.Controls.Add(this.btnAdjustmentClose);
            this.grpAdjustmentControl.Controls.Add(this.btnAdjustmentClear);
            this.grpAdjustmentControl.Location = new System.Drawing.Point(6, 507);
            this.grpAdjustmentControl.Name = "grpAdjustmentControl";
            this.grpAdjustmentControl.Size = new System.Drawing.Size(848, 70);
            this.grpAdjustmentControl.TabIndex = 22;
            this.grpAdjustmentControl.TabStop = false;
            // 
            // txtAdjustmentRemark
            // 
            this.txtAdjustmentRemark.Location = new System.Drawing.Point(10, 40);
            this.txtAdjustmentRemark.MaxLength = 255;
            this.txtAdjustmentRemark.Name = "txtAdjustmentRemark";
            this.txtAdjustmentRemark.Size = new System.Drawing.Size(452, 22);
            this.txtAdjustmentRemark.TabIndex = 31;
            // 
            // lblAdjustmentRemark
            // 
            this.lblAdjustmentRemark.AutoSize = true;
            this.lblAdjustmentRemark.Location = new System.Drawing.Point(10, 10);
            this.lblAdjustmentRemark.Name = "lblAdjustmentRemark";
            this.lblAdjustmentRemark.Size = new System.Drawing.Size(125, 16);
            this.lblAdjustmentRemark.TabIndex = 3;
            this.lblAdjustmentRemark.Text = "Adjustment Remark";
            // 
            // btnAdjustmentSave
            // 
            this.btnAdjustmentSave.Location = new System.Drawing.Point(470, 21);
            this.btnAdjustmentSave.Name = "btnAdjustmentSave";
            this.btnAdjustmentSave.Size = new System.Drawing.Size(120, 43);
            this.btnAdjustmentSave.TabIndex = 32;
            this.btnAdjustmentSave.Text = "&Save";
            this.btnAdjustmentSave.UseVisualStyleBackColor = true;
            this.btnAdjustmentSave.Click += new System.EventHandler(this.btnAdjustmentSave_Click);
            // 
            // btnAdjustmentClose
            // 
            this.btnAdjustmentClose.Location = new System.Drawing.Point(722, 21);
            this.btnAdjustmentClose.Name = "btnAdjustmentClose";
            this.btnAdjustmentClose.Size = new System.Drawing.Size(120, 43);
            this.btnAdjustmentClose.TabIndex = 34;
            this.btnAdjustmentClose.Text = "&Close";
            this.btnAdjustmentClose.UseVisualStyleBackColor = true;
            this.btnAdjustmentClose.Click += new System.EventHandler(this.btnAdjustmentClose_Click);
            // 
            // btnAdjustmentClear
            // 
            this.btnAdjustmentClear.Location = new System.Drawing.Point(596, 21);
            this.btnAdjustmentClear.Name = "btnAdjustmentClear";
            this.btnAdjustmentClear.Size = new System.Drawing.Size(120, 43);
            this.btnAdjustmentClear.TabIndex = 33;
            this.btnAdjustmentClear.Text = "&Clear";
            this.btnAdjustmentClear.UseVisualStyleBackColor = true;
            this.btnAdjustmentClear.Click += new System.EventHandler(this.btnAdjustmentClear_Click);
            // 
            // btnAdjustmentSearchItem
            // 
            this.btnAdjustmentSearchItem.Location = new System.Drawing.Point(10, 70);
            this.btnAdjustmentSearchItem.Name = "btnAdjustmentSearchItem";
            this.btnAdjustmentSearchItem.Size = new System.Drawing.Size(130, 25);
            this.btnAdjustmentSearchItem.TabIndex = 28;
            this.btnAdjustmentSearchItem.Text = "Select &Item";
            this.btnAdjustmentSearchItem.UseVisualStyleBackColor = true;
            this.btnAdjustmentSearchItem.Click += new System.EventHandler(this.btnAdjustmentSearch_Click);
            // 
            // lblAdjustmentStockName
            // 
            this.lblAdjustmentStockName.AutoSize = true;
            this.lblAdjustmentStockName.Location = new System.Drawing.Point(10, 10);
            this.lblAdjustmentStockName.Name = "lblAdjustmentStockName";
            this.lblAdjustmentStockName.Size = new System.Drawing.Size(82, 16);
            this.lblAdjustmentStockName.TabIndex = 2;
            this.lblAdjustmentStockName.Text = "Stock Name";
            // 
            // txtAdjustmentStockName
            // 
            this.txtAdjustmentStockName.Location = new System.Drawing.Point(10, 40);
            this.txtAdjustmentStockName.MaxLength = 255;
            this.txtAdjustmentStockName.Name = "txtAdjustmentStockName";
            this.txtAdjustmentStockName.Size = new System.Drawing.Size(281, 22);
            this.txtAdjustmentStockName.TabIndex = 23;
            this.txtAdjustmentStockName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAdjustmentStockName_KeyDown);
            this.txtAdjustmentStockName.Leave += new System.EventHandler(this.txtAdjustmentStockName_Leave);
            // 
            // lblAdjustmentNewBalance
            // 
            this.lblAdjustmentNewBalance.AutoSize = true;
            this.lblAdjustmentNewBalance.Location = new System.Drawing.Point(515, 10);
            this.lblAdjustmentNewBalance.Name = "lblAdjustmentNewBalance";
            this.lblAdjustmentNewBalance.Size = new System.Drawing.Size(88, 16);
            this.lblAdjustmentNewBalance.TabIndex = 5;
            this.lblAdjustmentNewBalance.Text = "New Balance";
            // 
            // lblAdjustmentQuantity
            // 
            this.lblAdjustmentQuantity.AutoSize = true;
            this.lblAdjustmentQuantity.Location = new System.Drawing.Point(635, 10);
            this.lblAdjustmentQuantity.Name = "lblAdjustmentQuantity";
            this.lblAdjustmentQuantity.Size = new System.Drawing.Size(125, 16);
            this.lblAdjustmentQuantity.TabIndex = 7;
            this.lblAdjustmentQuantity.Text = "Adjustment Quantity";
            // 
            // btnAdjustmentAdd
            // 
            this.btnAdjustmentAdd.Location = new System.Drawing.Point(773, 40);
            this.btnAdjustmentAdd.Name = "btnAdjustmentAdd";
            this.btnAdjustmentAdd.Size = new System.Drawing.Size(75, 25);
            this.btnAdjustmentAdd.TabIndex = 27;
            this.btnAdjustmentAdd.Text = "&Add";
            this.btnAdjustmentAdd.UseVisualStyleBackColor = true;
            this.btnAdjustmentAdd.Click += new System.EventHandler(this.btnAdjustmentAdd_Click);
            // 
            // lstAdjustmentListing
            // 
            this.lstAdjustmentListing.FullRowSelect = true;
            this.lstAdjustmentListing.GridLines = true;
            this.lstAdjustmentListing.HideSelection = false;
            this.lstAdjustmentListing.Location = new System.Drawing.Point(10, 134);
            this.lstAdjustmentListing.MultiSelect = false;
            this.lstAdjustmentListing.Name = "lstAdjustmentListing";
            this.lstAdjustmentListing.Size = new System.Drawing.Size(838, 336);
            this.lstAdjustmentListing.TabIndex = 0;
            this.lstAdjustmentListing.UseCompatibleStateImageBehavior = false;
            this.lstAdjustmentListing.View = System.Windows.Forms.View.Details;
            this.lstAdjustmentListing.SelectedIndexChanged += new System.EventHandler(this.lstAdjustmentListing_SelectedIndexChanged);
            this.lstAdjustmentListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstAdjustmentList_ColumnClick);
            // 
            // lblAdjustmentOldBalance
            // 
            this.lblAdjustmentOldBalance.AutoSize = true;
            this.lblAdjustmentOldBalance.Location = new System.Drawing.Point(395, 10);
            this.lblAdjustmentOldBalance.Name = "lblAdjustmentOldBalance";
            this.lblAdjustmentOldBalance.Size = new System.Drawing.Size(82, 16);
            this.lblAdjustmentOldBalance.TabIndex = 9;
            this.lblAdjustmentOldBalance.Text = "Old Balance";
            // 
            // txtAdjustmentQuantity
            // 
            this.txtAdjustmentQuantity.Enabled = false;
            this.txtAdjustmentQuantity.Location = new System.Drawing.Point(635, 40);
            this.txtAdjustmentQuantity.Name = "txtAdjustmentQuantity";
            this.txtAdjustmentQuantity.Size = new System.Drawing.Size(80, 22);
            this.txtAdjustmentQuantity.TabIndex = 26;
            this.txtAdjustmentQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAdjustmentNewBalance
            // 
            this.txtAdjustmentNewBalance.Location = new System.Drawing.Point(515, 40);
            this.txtAdjustmentNewBalance.MaxLength = 7;
            this.txtAdjustmentNewBalance.Name = "txtAdjustmentNewBalance";
            this.txtAdjustmentNewBalance.Size = new System.Drawing.Size(80, 22);
            this.txtAdjustmentNewBalance.TabIndex = 25;
            this.txtAdjustmentNewBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAdjustmentNewBalance.TextChanged += new System.EventHandler(this.txtAdjustmentNewBalance_TextChanged);
            this.txtAdjustmentNewBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAdjustmentNewBalance_KeyDown);
            // 
            // txtAdjustmentOldBalance
            // 
            this.txtAdjustmentOldBalance.Enabled = false;
            this.txtAdjustmentOldBalance.Location = new System.Drawing.Point(395, 40);
            this.txtAdjustmentOldBalance.Name = "txtAdjustmentOldBalance";
            this.txtAdjustmentOldBalance.Size = new System.Drawing.Size(80, 22);
            this.txtAdjustmentOldBalance.TabIndex = 24;
            this.txtAdjustmentOldBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAdjustmentStockID
            // 
            this.txtAdjustmentStockID.Enabled = false;
            this.txtAdjustmentStockID.Location = new System.Drawing.Point(297, 73);
            this.txtAdjustmentStockID.Name = "txtAdjustmentStockID";
            this.txtAdjustmentStockID.Size = new System.Drawing.Size(92, 22);
            this.txtAdjustmentStockID.TabIndex = 29;
            this.txtAdjustmentStockID.Visible = false;
            this.txtAdjustmentStockID.TextChanged += new System.EventHandler(this.txtAdjustmentStockID_TextChanged);
            // 
            // tpCategory
            // 
            this.tpCategory.BackColor = System.Drawing.Color.Transparent;
            this.tpCategory.Controls.Add(this.pnlCategory);
            this.tpCategory.Location = new System.Drawing.Point(4, 29);
            this.tpCategory.Name = "tpCategory";
            this.tpCategory.Padding = new System.Windows.Forms.Padding(3);
            this.tpCategory.Size = new System.Drawing.Size(880, 597);
            this.tpCategory.TabIndex = 3;
            this.tpCategory.Text = "Category / Sub Category";
            this.tpCategory.UseVisualStyleBackColor = true;
            // 
            // pnlCategory
            // 
            this.pnlCategory.BackColor = System.Drawing.SystemColors.Control;
            this.pnlCategory.Controls.Add(this.grpSubCategory);
            this.pnlCategory.Controls.Add(this.btnCategoryClose);
            this.pnlCategory.Controls.Add(this.grpCategory);
            this.pnlCategory.Location = new System.Drawing.Point(0, 0);
            this.pnlCategory.Name = "pnlCategory";
            this.pnlCategory.Size = new System.Drawing.Size(888, 586);
            this.pnlCategory.TabIndex = 0;
            // 
            // grpSubCategory
            // 
            this.grpSubCategory.Controls.Add(this.lblSubCategoryCategory);
            this.grpSubCategory.Controls.Add(this.cmbSubCategoryCategory);
            this.grpSubCategory.Controls.Add(this.txtSubCategoryShortcut);
            this.grpSubCategory.Controls.Add(this.lblSubCategoryShortcut);
            this.grpSubCategory.Controls.Add(this.txtSubCategoryName);
            this.grpSubCategory.Controls.Add(this.lblSubCategoryName);
            this.grpSubCategory.Controls.Add(this.txtSubCategoryID);
            this.grpSubCategory.Controls.Add(this.lblSubCategoryID);
            this.grpSubCategory.Controls.Add(this.btnSubCategoryCancel);
            this.grpSubCategory.Controls.Add(this.btnSubCategorySave);
            this.grpSubCategory.Controls.Add(this.btnSubCategoryDelete);
            this.grpSubCategory.Controls.Add(this.btnSubCategoryEdit);
            this.grpSubCategory.Controls.Add(this.btnSubCategoryNew);
            this.grpSubCategory.Controls.Add(this.lstCategorySubCategory);
            this.grpSubCategory.Location = new System.Drawing.Point(8, 252);
            this.grpSubCategory.Name = "grpSubCategory";
            this.grpSubCategory.Size = new System.Drawing.Size(850, 240);
            this.grpSubCategory.TabIndex = 10;
            this.grpSubCategory.TabStop = false;
            this.grpSubCategory.Text = "Sub Category";
            // 
            // lblSubCategoryCategory
            // 
            this.lblSubCategoryCategory.AutoSize = true;
            this.lblSubCategoryCategory.Location = new System.Drawing.Point(210, 51);
            this.lblSubCategoryCategory.Name = "lblSubCategoryCategory";
            this.lblSubCategoryCategory.Size = new System.Drawing.Size(63, 16);
            this.lblSubCategoryCategory.TabIndex = 24;
            this.lblSubCategoryCategory.Text = "Category";
            // 
            // cmbSubCategoryCategory
            // 
            this.cmbSubCategoryCategory.Enabled = false;
            this.cmbSubCategoryCategory.FormattingEnabled = true;
            this.cmbSubCategoryCategory.Location = new System.Drawing.Point(300, 51);
            this.cmbSubCategoryCategory.Name = "cmbSubCategoryCategory";
            this.cmbSubCategoryCategory.Size = new System.Drawing.Size(300, 24);
            this.cmbSubCategoryCategory.TabIndex = 23;
            // 
            // txtSubCategoryShortcut
            // 
            this.txtSubCategoryShortcut.Enabled = false;
            this.txtSubCategoryShortcut.Location = new System.Drawing.Point(300, 111);
            this.txtSubCategoryShortcut.Name = "txtSubCategoryShortcut";
            this.txtSubCategoryShortcut.Size = new System.Drawing.Size(100, 22);
            this.txtSubCategoryShortcut.TabIndex = 22;
            // 
            // lblSubCategoryShortcut
            // 
            this.lblSubCategoryShortcut.AutoSize = true;
            this.lblSubCategoryShortcut.Location = new System.Drawing.Point(210, 111);
            this.lblSubCategoryShortcut.Name = "lblSubCategoryShortcut";
            this.lblSubCategoryShortcut.Size = new System.Drawing.Size(41, 16);
            this.lblSubCategoryShortcut.TabIndex = 21;
            this.lblSubCategoryShortcut.Text = "Code";
            // 
            // txtSubCategoryName
            // 
            this.txtSubCategoryName.Enabled = false;
            this.txtSubCategoryName.Location = new System.Drawing.Point(300, 81);
            this.txtSubCategoryName.MaxLength = 50;
            this.txtSubCategoryName.Name = "txtSubCategoryName";
            this.txtSubCategoryName.Size = new System.Drawing.Size(300, 22);
            this.txtSubCategoryName.TabIndex = 20;
            // 
            // lblSubCategoryName
            // 
            this.lblSubCategoryName.AutoSize = true;
            this.lblSubCategoryName.Location = new System.Drawing.Point(210, 81);
            this.lblSubCategoryName.Name = "lblSubCategoryName";
            this.lblSubCategoryName.Size = new System.Drawing.Size(45, 16);
            this.lblSubCategoryName.TabIndex = 19;
            this.lblSubCategoryName.Text = "Name";
            // 
            // txtSubCategoryID
            // 
            this.txtSubCategoryID.Enabled = false;
            this.txtSubCategoryID.Location = new System.Drawing.Point(300, 21);
            this.txtSubCategoryID.Name = "txtSubCategoryID";
            this.txtSubCategoryID.Size = new System.Drawing.Size(100, 22);
            this.txtSubCategoryID.TabIndex = 18;
            // 
            // lblSubCategoryID
            // 
            this.lblSubCategoryID.AutoSize = true;
            this.lblSubCategoryID.Location = new System.Drawing.Point(210, 21);
            this.lblSubCategoryID.Name = "lblSubCategoryID";
            this.lblSubCategoryID.Size = new System.Drawing.Size(21, 16);
            this.lblSubCategoryID.TabIndex = 17;
            this.lblSubCategoryID.Text = "ID";
            // 
            // btnSubCategoryCancel
            // 
            this.btnSubCategoryCancel.Enabled = false;
            this.btnSubCategoryCancel.Location = new System.Drawing.Point(722, 191);
            this.btnSubCategoryCancel.Name = "btnSubCategoryCancel";
            this.btnSubCategoryCancel.Size = new System.Drawing.Size(120, 43);
            this.btnSubCategoryCancel.TabIndex = 15;
            this.btnSubCategoryCancel.Text = "&Cancel";
            this.btnSubCategoryCancel.UseVisualStyleBackColor = true;
            this.btnSubCategoryCancel.Click += new System.EventHandler(this.btnSubCategoryCancel_Click);
            // 
            // btnSubCategorySave
            // 
            this.btnSubCategorySave.Enabled = false;
            this.btnSubCategorySave.Location = new System.Drawing.Point(596, 191);
            this.btnSubCategorySave.Name = "btnSubCategorySave";
            this.btnSubCategorySave.Size = new System.Drawing.Size(120, 43);
            this.btnSubCategorySave.TabIndex = 14;
            this.btnSubCategorySave.Text = "&Save";
            this.btnSubCategorySave.UseVisualStyleBackColor = true;
            this.btnSubCategorySave.Click += new System.EventHandler(this.btnSubCategorySave_Click);
            // 
            // btnSubCategoryDelete
            // 
            this.btnSubCategoryDelete.Location = new System.Drawing.Point(458, 191);
            this.btnSubCategoryDelete.Name = "btnSubCategoryDelete";
            this.btnSubCategoryDelete.Size = new System.Drawing.Size(120, 43);
            this.btnSubCategoryDelete.TabIndex = 13;
            this.btnSubCategoryDelete.Text = "&Delete";
            this.btnSubCategoryDelete.UseVisualStyleBackColor = true;
            this.btnSubCategoryDelete.Click += new System.EventHandler(this.btnSubCategoryDelete_Click);
            // 
            // btnSubCategoryEdit
            // 
            this.btnSubCategoryEdit.Location = new System.Drawing.Point(332, 191);
            this.btnSubCategoryEdit.Name = "btnSubCategoryEdit";
            this.btnSubCategoryEdit.Size = new System.Drawing.Size(120, 43);
            this.btnSubCategoryEdit.TabIndex = 12;
            this.btnSubCategoryEdit.Text = "&Edit";
            this.btnSubCategoryEdit.UseVisualStyleBackColor = true;
            this.btnSubCategoryEdit.Click += new System.EventHandler(this.btnSubCategoryEdit_Click);
            // 
            // btnSubCategoryNew
            // 
            this.btnSubCategoryNew.Location = new System.Drawing.Point(206, 191);
            this.btnSubCategoryNew.Name = "btnSubCategoryNew";
            this.btnSubCategoryNew.Size = new System.Drawing.Size(120, 43);
            this.btnSubCategoryNew.TabIndex = 11;
            this.btnSubCategoryNew.Text = "&Add New";
            this.btnSubCategoryNew.UseVisualStyleBackColor = true;
            this.btnSubCategoryNew.Click += new System.EventHandler(this.btnSubCategoryNew_Click);
            // 
            // lstCategorySubCategory
            // 
            this.lstCategorySubCategory.FullRowSelect = true;
            this.lstCategorySubCategory.GridLines = true;
            this.lstCategorySubCategory.HideSelection = false;
            this.lstCategorySubCategory.Location = new System.Drawing.Point(8, 21);
            this.lstCategorySubCategory.MultiSelect = false;
            this.lstCategorySubCategory.Name = "lstCategorySubCategory";
            this.lstCategorySubCategory.Size = new System.Drawing.Size(192, 213);
            this.lstCategorySubCategory.TabIndex = 8;
            this.lstCategorySubCategory.UseCompatibleStateImageBehavior = false;
            this.lstCategorySubCategory.View = System.Windows.Forms.View.Details;
            this.lstCategorySubCategory.SelectedIndexChanged += new System.EventHandler(this.lstCategorySubCategory_SelectedIndexChanged);
            this.lstCategorySubCategory.DoubleClick += new System.EventHandler(this.lstCategorySubCategory_DoubleClick);
            this.lstCategorySubCategory.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCategorySubCategory_ColumnClick);
            // 
            // btnCategoryClose
            // 
            this.btnCategoryClose.Location = new System.Drawing.Point(730, 530);
            this.btnCategoryClose.Name = "btnCategoryClose";
            this.btnCategoryClose.Size = new System.Drawing.Size(120, 43);
            this.btnCategoryClose.TabIndex = 9;
            this.btnCategoryClose.Text = "&Close";
            this.btnCategoryClose.UseVisualStyleBackColor = true;
            this.btnCategoryClose.Click += new System.EventHandler(this.btnCategoryClose_Click);
            // 
            // grpCategory
            // 
            this.grpCategory.Controls.Add(this.txtCategoryShortcut);
            this.grpCategory.Controls.Add(this.lblCategoryShortcut);
            this.grpCategory.Controls.Add(this.txtCategoryName);
            this.grpCategory.Controls.Add(this.lblCategoryName);
            this.grpCategory.Controls.Add(this.txtCategoryID);
            this.grpCategory.Controls.Add(this.lblCategoryID);
            this.grpCategory.Controls.Add(this.btnCategoryCancel);
            this.grpCategory.Controls.Add(this.btnCategorySave);
            this.grpCategory.Controls.Add(this.btnCategoryDelete);
            this.grpCategory.Controls.Add(this.btnCategoryEdit);
            this.grpCategory.Controls.Add(this.btnCategoryNew);
            this.grpCategory.Controls.Add(this.lstCategoryCategory);
            this.grpCategory.Location = new System.Drawing.Point(6, 6);
            this.grpCategory.Name = "grpCategory";
            this.grpCategory.Size = new System.Drawing.Size(848, 240);
            this.grpCategory.TabIndex = 9;
            this.grpCategory.TabStop = false;
            this.grpCategory.Text = "Category";
            // 
            // txtCategoryShortcut
            // 
            this.txtCategoryShortcut.Enabled = false;
            this.txtCategoryShortcut.Location = new System.Drawing.Point(300, 81);
            this.txtCategoryShortcut.Name = "txtCategoryShortcut";
            this.txtCategoryShortcut.Size = new System.Drawing.Size(100, 22);
            this.txtCategoryShortcut.TabIndex = 16;
            // 
            // lblCategoryShortcut
            // 
            this.lblCategoryShortcut.AutoSize = true;
            this.lblCategoryShortcut.Location = new System.Drawing.Point(210, 81);
            this.lblCategoryShortcut.Name = "lblCategoryShortcut";
            this.lblCategoryShortcut.Size = new System.Drawing.Size(41, 16);
            this.lblCategoryShortcut.TabIndex = 15;
            this.lblCategoryShortcut.Text = "Code";
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.Enabled = false;
            this.txtCategoryName.Location = new System.Drawing.Point(300, 51);
            this.txtCategoryName.MaxLength = 50;
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Size = new System.Drawing.Size(300, 22);
            this.txtCategoryName.TabIndex = 14;
            // 
            // lblCategoryName
            // 
            this.lblCategoryName.AutoSize = true;
            this.lblCategoryName.Location = new System.Drawing.Point(210, 51);
            this.lblCategoryName.Name = "lblCategoryName";
            this.lblCategoryName.Size = new System.Drawing.Size(45, 16);
            this.lblCategoryName.TabIndex = 13;
            this.lblCategoryName.Text = "Name";
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.Enabled = false;
            this.txtCategoryID.Location = new System.Drawing.Point(300, 21);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.Size = new System.Drawing.Size(100, 22);
            this.txtCategoryID.TabIndex = 12;
            // 
            // lblCategoryID
            // 
            this.lblCategoryID.AutoSize = true;
            this.lblCategoryID.Location = new System.Drawing.Point(210, 21);
            this.lblCategoryID.Name = "lblCategoryID";
            this.lblCategoryID.Size = new System.Drawing.Size(21, 16);
            this.lblCategoryID.TabIndex = 11;
            this.lblCategoryID.Text = "ID";
            // 
            // btnCategoryCancel
            // 
            this.btnCategoryCancel.Enabled = false;
            this.btnCategoryCancel.Location = new System.Drawing.Point(722, 191);
            this.btnCategoryCancel.Name = "btnCategoryCancel";
            this.btnCategoryCancel.Size = new System.Drawing.Size(120, 43);
            this.btnCategoryCancel.TabIndex = 10;
            this.btnCategoryCancel.Text = "&Cancel";
            this.btnCategoryCancel.UseVisualStyleBackColor = true;
            this.btnCategoryCancel.Click += new System.EventHandler(this.btnCategoryCancel_Click);
            // 
            // btnCategorySave
            // 
            this.btnCategorySave.Enabled = false;
            this.btnCategorySave.Location = new System.Drawing.Point(596, 191);
            this.btnCategorySave.Name = "btnCategorySave";
            this.btnCategorySave.Size = new System.Drawing.Size(120, 43);
            this.btnCategorySave.TabIndex = 9;
            this.btnCategorySave.Text = "&Save";
            this.btnCategorySave.UseVisualStyleBackColor = true;
            this.btnCategorySave.Click += new System.EventHandler(this.btnCategorySave_Click);
            // 
            // btnCategoryDelete
            // 
            this.btnCategoryDelete.Enabled = false;
            this.btnCategoryDelete.Location = new System.Drawing.Point(458, 191);
            this.btnCategoryDelete.Name = "btnCategoryDelete";
            this.btnCategoryDelete.Size = new System.Drawing.Size(120, 43);
            this.btnCategoryDelete.TabIndex = 8;
            this.btnCategoryDelete.Text = "&Delete";
            this.btnCategoryDelete.UseVisualStyleBackColor = true;
            this.btnCategoryDelete.Click += new System.EventHandler(this.btnCategoryDelete_Click);
            // 
            // btnCategoryEdit
            // 
            this.btnCategoryEdit.Location = new System.Drawing.Point(332, 191);
            this.btnCategoryEdit.Name = "btnCategoryEdit";
            this.btnCategoryEdit.Size = new System.Drawing.Size(120, 43);
            this.btnCategoryEdit.TabIndex = 7;
            this.btnCategoryEdit.Text = "&Edit";
            this.btnCategoryEdit.UseVisualStyleBackColor = true;
            this.btnCategoryEdit.Click += new System.EventHandler(this.btnCategoryEdit_Click);
            // 
            // btnCategoryNew
            // 
            this.btnCategoryNew.Enabled = false;
            this.btnCategoryNew.Location = new System.Drawing.Point(206, 191);
            this.btnCategoryNew.Name = "btnCategoryNew";
            this.btnCategoryNew.Size = new System.Drawing.Size(120, 43);
            this.btnCategoryNew.TabIndex = 6;
            this.btnCategoryNew.Text = "&Add New";
            this.btnCategoryNew.UseVisualStyleBackColor = true;
            this.btnCategoryNew.Click += new System.EventHandler(this.btnCategoryNew_Click);
            // 
            // lstCategoryCategory
            // 
            this.lstCategoryCategory.FullRowSelect = true;
            this.lstCategoryCategory.GridLines = true;
            this.lstCategoryCategory.HideSelection = false;
            this.lstCategoryCategory.Location = new System.Drawing.Point(8, 21);
            this.lstCategoryCategory.MultiSelect = false;
            this.lstCategoryCategory.Name = "lstCategoryCategory";
            this.lstCategoryCategory.Size = new System.Drawing.Size(192, 213);
            this.lstCategoryCategory.TabIndex = 7;
            this.lstCategoryCategory.UseCompatibleStateImageBehavior = false;
            this.lstCategoryCategory.View = System.Windows.Forms.View.Details;
            this.lstCategoryCategory.SelectedIndexChanged += new System.EventHandler(this.lstCategoryCategory_SelectedIndexChanged);
            this.lstCategoryCategory.DoubleClick += new System.EventHandler(this.lstCategoryCategory_DoubleClick);
            this.lstCategoryCategory.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCategoryCategory_ColumnClick);
            // 
            // erpStock
            // 
            this.erpStock.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpStock.ContainerControl = this;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(722, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 43);
            this.button1.TabIndex = 9;
            this.button1.Text = "&Close";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(258, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 43);
            this.button2.TabIndex = 8;
            this.button2.Text = "&Delete";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(132, 21);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 43);
            this.button3.TabIndex = 7;
            this.button3.Text = "&Edit";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 21);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 43);
            this.button4.TabIndex = 6;
            this.button4.Text = "&Add New";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // frmStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 614);
            this.ControlBox = false;
            this.Controls.Add(this.tabMain);
            this.Name = "frmStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmStock_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmStock_FormClosing);
            this.tabMain.ResumeLayout(false);
            this.tpListing.ResumeLayout(false);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.grpControlDetail.ResumeLayout(false);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.tpReceive.ResumeLayout(false);
            this.pnlReceive.ResumeLayout(false);
            this.pnlReceive.PerformLayout();
            this.grpControl.ResumeLayout(false);
            this.tpAdjustment.ResumeLayout(false);
            this.pnlAdjustment.ResumeLayout(false);
            this.pnlAdjustment.PerformLayout();
            this.grpAdjustmentControl.ResumeLayout(false);
            this.grpAdjustmentControl.PerformLayout();
            this.tpCategory.ResumeLayout(false);
            this.pnlCategory.ResumeLayout(false);
            this.grpSubCategory.ResumeLayout(false);
            this.grpSubCategory.PerformLayout();
            this.grpCategory.ResumeLayout(false);
            this.grpCategory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpStock)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tabMain;
        public System.Windows.Forms.TabPage tpListing;
        public System.Windows.Forms.TabPage tpAdjustment;
        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.ListView lstStockListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtStockSearch;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.Button btnEdit;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.ErrorProvider erpStock;
        public System.Windows.Forms.Button btnAdjustmentAdd;
        public System.Windows.Forms.TextBox txtAdjustmentStockName;
        public System.Windows.Forms.Label lblAdjustmentStockName;
        public System.Windows.Forms.Button btnAdjustmentSearchItem;
        public System.Windows.Forms.ListView lstAdjustmentListing;
        public System.Windows.Forms.TextBox txtAdjustmentNewBalance;
        public System.Windows.Forms.Label lblAdjustmentNewBalance;
        public System.Windows.Forms.TextBox txtAdjustmentOldBalance;
        public System.Windows.Forms.Label lblAdjustmentOldBalance;
        public System.Windows.Forms.TextBox txtAdjustmentQuantity;
        public System.Windows.Forms.Label lblAdjustmentQuantity;
        public System.Windows.Forms.Label lblAdjustmentList;
        public System.Windows.Forms.Button btnAdjustmentRemove;
        public System.Windows.Forms.TextBox txtAdjustmentStockID;
        public System.Windows.Forms.TabPage tpReceive;
        public System.Windows.Forms.Panel pnlReceive;
        public System.Windows.Forms.Panel pnlAdjustment;
        public System.Windows.Forms.GroupBox grpAdjustmentControl;
        public System.Windows.Forms.TextBox txtAdjustmentRemark;
        public System.Windows.Forms.Label lblAdjustmentRemark;
        public System.Windows.Forms.Button btnAdjustmentSave;
        public System.Windows.Forms.Button btnAdjustmentClose;
        public System.Windows.Forms.Button btnAdjustmentClear;
        public System.Windows.Forms.Button btnReceiveSelect;
        public System.Windows.Forms.RadioButton rbtReceiveEdit;
        public System.Windows.Forms.RadioButton rbtReceiveNew;
        public System.Windows.Forms.TextBox txtReceiveDocumentNo;
        public System.Windows.Forms.Label lblReceiveTransactionDate;
        public System.Windows.Forms.Label lblReceiveDocumentNo;
        public System.Windows.Forms.DateTimePicker dtpReceiveTransactionDate;
        public System.Windows.Forms.Label lblReceiveTransactionNo;
        public System.Windows.Forms.TextBox txtReceiveTransactionNo;
        public System.Windows.Forms.TextBox txtReceiveRemark;
        public System.Windows.Forms.Label lblReceiveRemark;
        public System.Windows.Forms.TextBox txtReceiveStockID;
        public System.Windows.Forms.Button btnReceiveAdd;
        public System.Windows.Forms.Label lblReceiveUnit;
        public System.Windows.Forms.ComboBox cmbReceiveUnit;
        public System.Windows.Forms.TextBox txtReceiveQuantity;
        public System.Windows.Forms.Label lblReceiveQuantity;
        public System.Windows.Forms.TextBox txtReceiveStockName;
        public System.Windows.Forms.Label lblReceiveStockName;
        public System.Windows.Forms.Button btnReceiveSearchItem;
        public System.Windows.Forms.ListView lstReceiveListing;
        public System.Windows.Forms.Button btnReceiveRemove;
        public System.Windows.Forms.GroupBox grpControl;
        public System.Windows.Forms.Button btnReceiveClose;
        public System.Windows.Forms.Button btnReceiveSave;
        public System.Windows.Forms.ListView lstCategory;
        public System.Windows.Forms.Button btnReceiveClear;
        public System.Windows.Forms.TextBox txtReceiveStockCategory;
        public System.Windows.Forms.TextBox txtAdjustmentStockCategory;
        public System.Windows.Forms.Panel pnlDetail;
        public System.Windows.Forms.ComboBox cmbType;
        public System.Windows.Forms.Label lblType;
        public System.Windows.Forms.Label lblPackageSize_E1;
        public System.Windows.Forms.Label lblStockPackageSize_PackageUOM1;
        public System.Windows.Forms.Label lblStockPackageSize_UOM1;
        public System.Windows.Forms.Label lblStockPackageSize_1;
        public System.Windows.Forms.TextBox txtStockPackageUOM1;
        public System.Windows.Forms.Label lblPackageUnit1;
        public System.Windows.Forms.ComboBox cmbCategory;
        public System.Windows.Forms.Label lblCategory;
        public System.Windows.Forms.GroupBox grpControlDetail;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.TextBox txtStockUOM;
        public System.Windows.Forms.TextBox txtStockPackageSize1;
        public System.Windows.Forms.TextBox txtStockPrice;
        public System.Windows.Forms.TextBox txtStockDesc2;
        public System.Windows.Forms.TextBox txtStockDesc1;
        public System.Windows.Forms.TextBox txtStockName;
        public System.Windows.Forms.TextBox txtStockCode;
        public System.Windows.Forms.TextBox txtStockID;
        public System.Windows.Forms.Label lblStockUOM;
        public System.Windows.Forms.Label lblPackageSize1;
        public System.Windows.Forms.Label lblStockDesc2;
        public System.Windows.Forms.Label lblStockDesc1;
        public System.Windows.Forms.Label lblStockName;
        public System.Windows.Forms.Label lblStockCode;
        public System.Windows.Forms.Label lblID;
        public System.Windows.Forms.Label lblPriceLabel;
        public System.Windows.Forms.Label lblReceiveFrom;
        public System.Windows.Forms.TextBox txtReceiveFrom;
        public System.Windows.Forms.Label lblReceiveStockCode;
        public System.Windows.Forms.TextBox txtReceiveStockCode;
        public System.Windows.Forms.TextBox txtAdjustmentStockCode;
        public System.Windows.Forms.Label lblAdjustmentStockCode;
        public System.Windows.Forms.Button btnReceiveDelete;
        public System.Windows.Forms.TextBox txtReceiveSubCat;
        public System.Windows.Forms.TextBox txtAdjustmentSubCat;
        public System.Windows.Forms.Label lblPackageSize_E2;
        public System.Windows.Forms.Label lblStockPackageSize_PackageUOM2;
        public System.Windows.Forms.Label lblStockPackageSize_UOM2;
        public System.Windows.Forms.Label lblStockPackageSize_2;
        public System.Windows.Forms.TextBox txtStockPackageUOM2;
        public System.Windows.Forms.Label lblPackageUnit2;
        public System.Windows.Forms.TextBox txtStockPackageSize2;
        public System.Windows.Forms.Label lblPackageSize2;
        public System.Windows.Forms.ComboBox cmbSubCategory;
        public System.Windows.Forms.Label lblSubCategory;
        public System.Windows.Forms.ListView lstSubCategory;
        private System.Windows.Forms.Panel pnlCategory;
        private System.Windows.Forms.TextBox txtGUID;
        public System.Windows.Forms.GroupBox grpCategory;
        public System.Windows.Forms.Button btnCategoryClose;
        public System.Windows.Forms.Button btnCategoryDelete;
        public System.Windows.Forms.Button btnCategoryEdit;
        public System.Windows.Forms.Button btnCategoryNew;
        public System.Windows.Forms.ListView lstCategorySubCategory;
        public System.Windows.Forms.ListView lstCategoryCategory;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox grpSubCategory;
        private System.Windows.Forms.TextBox txtCategoryShortcut;
        private System.Windows.Forms.Label lblCategoryShortcut;
        private System.Windows.Forms.TextBox txtCategoryName;
        private System.Windows.Forms.Label lblCategoryName;
        private System.Windows.Forms.TextBox txtCategoryID;
        private System.Windows.Forms.Label lblCategoryID;
        public System.Windows.Forms.Button btnCategoryCancel;
        public System.Windows.Forms.Button btnCategorySave;
        public System.Windows.Forms.TabPage tpCategory;
        public System.Windows.Forms.Button btnSubCategoryCancel;
        public System.Windows.Forms.Button btnSubCategorySave;
        public System.Windows.Forms.Button btnSubCategoryDelete;
        public System.Windows.Forms.Button btnSubCategoryEdit;
        public System.Windows.Forms.Button btnSubCategoryNew;
        private System.Windows.Forms.TextBox txtSubCategoryShortcut;
        private System.Windows.Forms.Label lblSubCategoryShortcut;
        private System.Windows.Forms.TextBox txtSubCategoryName;
        private System.Windows.Forms.Label lblSubCategoryName;
        private System.Windows.Forms.TextBox txtSubCategoryID;
        private System.Windows.Forms.Label lblSubCategoryID;
        private System.Windows.Forms.Label lblSubCategoryCategory;
        private System.Windows.Forms.ComboBox cmbSubCategoryCategory;
        public System.Windows.Forms.Label lblStockPackagePrice2;
        public System.Windows.Forms.TextBox txtStockPackagePrice2;
        public System.Windows.Forms.Label lblStockPackagePrice1;
        public System.Windows.Forms.TextBox txtStockPackagePrice1;
        private System.Windows.Forms.Label lblAdjustmentUOM3;
        private System.Windows.Forms.Label lblAdjustmentUOM2;
        private System.Windows.Forms.Label lblAdjustmentUOM1;
    }
}