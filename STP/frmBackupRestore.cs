﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmBackupRestore : Form
    {
        private ND.Data.SQL.BackupRestore _BackupRestore;

        public frmBackupRestore()
        {
            try
            {
                this._BackupRestore = new ND.Data.SQL.BackupRestore();
                this.InitializeComponent();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean result = false;
                if (this.rbtBackup.Checked == true)
                {
                    Global.ShowWaiting(this, true);
                    this.ButtonEnable(false);
                    result = this._BackupRestore.Backup(DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".bak");
                    this.ButtonEnable(true);
                    Global.ShowWaiting(this, false);
                }
                else
                {
                    if (this.lstBackupDevice.SelectedItems.Count > 0)
                    {
                        Global.ShowWaiting(this, true);
                        this.ButtonEnable(false);
                        result = this._BackupRestore.Restore(this.lstBackupDevice.SelectedItems[0].SubItems[0].Text);
                        this.ButtonEnable(true);
                        Global.ShowWaiting(this, false);
                    }
                    else
                    {
                        return;
                    }
                }
                if (result == true)
                {
                    if (this.rbtBackup.Checked == true)
                    {
                        MessageBox.Show("Backup Success.", "Backup", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        MessageBox.Show("Restore Success. Click OK to restart program.", "Restore", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        Global.ForceClose = true;
                        Application.Restart();
                    }
                }
                else
                {
                    if (this.rbtBackup.Checked == true)
                    {
                        MessageBox.Show("Backup Failed.", "Backup", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        MessageBox.Show("Restore Failed.", "Restore", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                }
                this.Loadlist();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void frmBackupRestore_Load(object sender, EventArgs e)
        {
            try
            {
                this.rbtBackup.Checked = true;
                this.Loadlist();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void Loadlist()
        {
            try
            {
                this.lstBackupDevice.Columns.Clear();
                this.lstBackupDevice.Columns.Add("FileName", "File Name");
                this.lstBackupDevice.Columns.Add("DateTime", "Date Time");
                ND.UI.Winform.Form.PopulateListView(ref this.lstBackupDevice, this._BackupRestore.LoadList(), false, false, ND.Data.DataObject.SortType.Descending, "FileName");
                this.lstBackupDevice.Columns["FileName"].Width = 150;
                this.lstBackupDevice.Columns["DateTime"].Width = 150;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void ButtonEnable(Boolean enable)
        {
            try
            {
                this.btnStart.Enabled = enable;
                this.btnClose.Enabled = enable;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean result = false;
                result = this._BackupRestore.Delete(this.lstBackupDevice.SelectedItems[0].SubItems[0].Text);
                if (result == true)
                {
                    MessageBox.Show("Delete Success.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    MessageBox.Show("Delete Failed.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);                   
                }
                this.Loadlist();
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }
    }
}
