﻿namespace STP
{
    partial class frmSelectStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlListing = new System.Windows.Forms.Panel();
            this.lstSubCategory = new System.Windows.Forms.ListView();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lstStockListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtStockSearch = new System.Windows.Forms.TextBox();
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.Color.Transparent;
            this.pnlListing.Controls.Add(this.lstSubCategory);
            this.pnlListing.Controls.Add(this.lstCategory);
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstStockListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtStockSearch);
            this.pnlListing.Location = new System.Drawing.Point(0, 0);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 636);
            this.pnlListing.TabIndex = 1;
            // 
            // lstSubCategory
            // 
            this.lstSubCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSubCategory.FullRowSelect = true;
            this.lstSubCategory.GridLines = true;
            this.lstSubCategory.HideSelection = false;
            this.lstSubCategory.Location = new System.Drawing.Point(204, 34);
            this.lstSubCategory.MultiSelect = false;
            this.lstSubCategory.Name = "lstSubCategory";
            this.lstSubCategory.Size = new System.Drawing.Size(192, 516);
            this.lstSubCategory.TabIndex = 7;
            this.lstSubCategory.UseCompatibleStateImageBehavior = false;
            this.lstSubCategory.View = System.Windows.Forms.View.Details;
            this.lstSubCategory.SelectedIndexChanged += new System.EventHandler(this.lstSubCategory_SelectedIndexChanged);
            // 
            // lstCategory
            // 
            this.lstCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCategory.FullRowSelect = true;
            this.lstCategory.GridLines = true;
            this.lstCategory.HideSelection = false;
            this.lstCategory.Location = new System.Drawing.Point(6, 34);
            this.lstCategory.MultiSelect = false;
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(192, 516);
            this.lstCategory.TabIndex = 5;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            this.lstCategory.View = System.Windows.Forms.View.Details;
            this.lstCategory.SelectedIndexChanged += new System.EventHandler(this.lstCategory_SelectedIndexChanged);
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnSelect);
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 556);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Location = new System.Drawing.Point(6, 21);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(120, 43);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "&Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lstStockListing
            // 
            this.lstStockListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstStockListing.FullRowSelect = true;
            this.lstStockListing.GridLines = true;
            this.lstStockListing.Location = new System.Drawing.Point(402, 34);
            this.lstStockListing.MultiSelect = false;
            this.lstStockListing.Name = "lstStockListing";
            this.lstStockListing.Size = new System.Drawing.Size(452, 516);
            this.lstStockListing.TabIndex = 0;
            this.lstStockListing.UseCompatibleStateImageBehavior = false;
            this.lstStockListing.View = System.Windows.Forms.View.Details;
            this.lstStockListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstStockListing_MouseDoubleClick);
            this.lstStockListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstStockListing_ColumnClick);
            this.lstStockListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstStockListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(720, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(130, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search &Item";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtStockSearch
            // 
            this.txtStockSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStockSearch.Location = new System.Drawing.Point(6, 6);
            this.txtStockSearch.MaxLength = 255;
            this.txtStockSearch.Name = "txtStockSearch";
            this.txtStockSearch.Size = new System.Drawing.Size(700, 22);
            this.txtStockSearch.TabIndex = 1;
            this.txtStockSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStockSearch_KeyDown);
            // 
            // frmSelectStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 625);
            this.ControlBox = false;
            this.Controls.Add(this.pnlListing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSelectStock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmSelectStock_Load);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.ListView lstStockListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtStockSearch;
        public System.Windows.Forms.Button btnSelect;
        public System.Windows.Forms.ListView lstCategory;
        public System.Windows.Forms.ListView lstSubCategory;
    }
}