﻿namespace STP
{
    partial class frmSelectVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlListing = new System.Windows.Forms.Panel();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lstVendorListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtVendorSearch = new System.Windows.Forms.TextBox();
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.Color.Transparent;
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstVendorListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtVendorSearch);
            this.pnlListing.Location = new System.Drawing.Point(0, 0);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 636);
            this.pnlListing.TabIndex = 2;
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnSelect);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 556);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Location = new System.Drawing.Point(6, 21);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(120, 43);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "&Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lstVendorListing
            // 
            this.lstVendorListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstVendorListing.FullRowSelect = true;
            this.lstVendorListing.GridLines = true;
            this.lstVendorListing.HideSelection = false;
            this.lstVendorListing.Location = new System.Drawing.Point(6, 34);
            this.lstVendorListing.MultiSelect = false;
            this.lstVendorListing.Name = "lstVendorListing";
            this.lstVendorListing.Size = new System.Drawing.Size(848, 516);
            this.lstVendorListing.TabIndex = 0;
            this.lstVendorListing.UseCompatibleStateImageBehavior = false;
            this.lstVendorListing.View = System.Windows.Forms.View.Details;
            this.lstVendorListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstVendorListing_MouseDoubleClick);
            this.lstVendorListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstVendorListing_ColumnClick);
            this.lstVendorListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstVendorListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(700, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search &Vendor";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtVendorSearch
            // 
            this.txtVendorSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorSearch.Location = new System.Drawing.Point(6, 6);
            this.txtVendorSearch.MaxLength = 255;
            this.txtVendorSearch.Name = "txtVendorSearch";
            this.txtVendorSearch.Size = new System.Drawing.Size(680, 22);
            this.txtVendorSearch.TabIndex = 1;
            this.txtVendorSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVendorSearch_KeyDown);
            // 
            // frmSelectVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 625);
            this.ControlBox = false;
            this.Controls.Add(this.pnlListing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSelectVendor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmSelectVendor_Load);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnSelect;
        public System.Windows.Forms.ListView lstVendorListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtVendorSearch;
    }
}