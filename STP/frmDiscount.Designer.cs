﻿namespace STP
{
    partial class frmDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDiscount = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lstListing = new System.Windows.Forms.ListView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.rbtLine = new System.Windows.Forms.RadioButton();
            this.rbtGeneral = new System.Windows.Forms.RadioButton();
            this.erpDiscount = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpDiscount)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDiscount
            // 
            this.pnlDiscount.Controls.Add(this.lblName);
            this.pnlDiscount.Controls.Add(this.lblAmount);
            this.pnlDiscount.Controls.Add(this.txtName);
            this.pnlDiscount.Controls.Add(this.lstListing);
            this.pnlDiscount.Controls.Add(this.btnCancel);
            this.pnlDiscount.Controls.Add(this.btnOK);
            this.pnlDiscount.Controls.Add(this.txtAmount);
            this.pnlDiscount.Controls.Add(this.rbtLine);
            this.pnlDiscount.Controls.Add(this.rbtGeneral);
            this.pnlDiscount.Location = new System.Drawing.Point(0, 0);
            this.pnlDiscount.Name = "pnlDiscount";
            this.pnlDiscount.Size = new System.Drawing.Size(500, 300);
            this.pnlDiscount.TabIndex = 0;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(12, 225);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(76, 16);
            this.lblName.TabIndex = 9;
            this.lblName.Text = "Description";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(12, 199);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(53, 16);
            this.lblAmount.TabIndex = 8;
            this.lblAmount.Text = "Amount";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(106, 225);
            this.txtName.MaxLength = 255;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(380, 22);
            this.txtName.TabIndex = 7;
            // 
            // lstListing
            // 
            this.lstListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstListing.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lstListing.FullRowSelect = true;
            this.lstListing.GridLines = true;
            this.lstListing.HideSelection = false;
            this.lstListing.Location = new System.Drawing.Point(12, 35);
            this.lstListing.MultiSelect = false;
            this.lstListing.Name = "lstListing";
            this.lstListing.Size = new System.Drawing.Size(474, 158);
            this.lstListing.TabIndex = 6;
            this.lstListing.UseCompatibleStateImageBehavior = false;
            this.lstListing.View = System.Windows.Forms.View.Details;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(386, 261);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(12, 261);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 25);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.Location = new System.Drawing.Point(106, 199);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 22);
            this.txtAmount.TabIndex = 2;
            // 
            // rbtLine
            // 
            this.rbtLine.AutoSize = true;
            this.rbtLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtLine.Location = new System.Drawing.Point(150, 12);
            this.rbtLine.Name = "rbtLine";
            this.rbtLine.Size = new System.Drawing.Size(106, 20);
            this.rbtLine.TabIndex = 1;
            this.rbtLine.TabStop = true;
            this.rbtLine.Text = "Line Discount";
            this.rbtLine.UseVisualStyleBackColor = true;
            this.rbtLine.CheckedChanged += new System.EventHandler(this.rbtLine_CheckedChanged);
            // 
            // rbtGeneral
            // 
            this.rbtGeneral.AutoSize = true;
            this.rbtGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtGeneral.Location = new System.Drawing.Point(12, 12);
            this.rbtGeneral.Name = "rbtGeneral";
            this.rbtGeneral.Size = new System.Drawing.Size(129, 20);
            this.rbtGeneral.TabIndex = 0;
            this.rbtGeneral.TabStop = true;
            this.rbtGeneral.Text = "General Discount";
            this.rbtGeneral.UseVisualStyleBackColor = true;
            this.rbtGeneral.CheckedChanged += new System.EventHandler(this.rbtGeneral_CheckedChanged);
            // 
            // erpDiscount
            // 
            this.erpDiscount.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpDiscount.ContainerControl = this;
            // 
            // frmDiscount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 298);
            this.ControlBox = false;
            this.Controls.Add(this.pnlDiscount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmDiscount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmDiscount_Load);
            this.pnlDiscount.ResumeLayout(false);
            this.pnlDiscount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpDiscount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlDiscount;
        public System.Windows.Forms.TextBox txtAmount;
        public System.Windows.Forms.RadioButton rbtLine;
        public System.Windows.Forms.RadioButton rbtGeneral;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.ListView lstListing;
        public System.Windows.Forms.Label lblAmount;
        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.ErrorProvider erpDiscount;
    }
}