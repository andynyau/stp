﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmSelectTransaction : Form
    {
        #region Declarations

            private STP.Logic.Transaction _Logic_Transaction;

            private int _SearchType;
            private String _Current_Sort;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildTransactionList;

            private STP.Logic.Common.TransType _TransType;
            private int _Mode = 0;

            public STP.Logic.Common.TransType TransType
            {
                get { return this._TransType; }
                set { this._TransType = value; }
            }

            public int Mode
            {
                get { return this._Mode; }
                set { this._Mode = value; }
            }

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmSelectTransaction_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.SearchTransaction();
                        this.BuildAutoComplete();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstTransactionListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstTransactionListing, ref this._Current_Sort, e.Column, ND.Data.DataObject.SortType.Descending, "transactionno", this.ldm_BuildTransactionList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstTransactionListing_DoubleClick(object sender, EventArgs e)
                {
                    try
                    {
                        this.SelectTransaction();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstTransactionListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.SelectTransaction();
                            this.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtDocumentNoSearch_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this._SearchType = 1;
                            //SearchTransaction();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstTransactionListing, ref this._Current_Sort, columnIndex, this.ldm_BuildTransactionList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SearchType = 1;
                        this.SearchTransaction();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSelect_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.SelectTransaction();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.CancelSelect();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Functions and Procedures

            public frmSelectTransaction()
            {
                this.InitializeComponent();
                this.Text = "Select Transaction";
                this._Logic_Transaction = new STP.Logic.Transaction();
                this.ldm_BuildTransactionList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchTransaction);
            }

            private void BuildButton()
            {
                try
                {
                    this.btnSearch.Image = Image.FromFile("image/Search.png");
                    this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnSelect.Image = Image.FromFile("image/Confirm.png");
                    this.btnSelect.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnClose.Image = Image.FromFile("image/Cancel.png");
                    this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchTransaction()
            {
                try
                {
                    this.SearchTransaction(ND.Data.DataObject.SortType.Descending, "transactionno");
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchTransaction(ND.Data.DataObject.SortType sortType, String sortField)
            {
                try
                {
                    Global.ShowWaiting(this, true);
                    this.lstTransactionListing.Visible = false;
                    this.lstTransactionListing.Clear();
                    this.BuildTransactionListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    switch (this._SearchType)
                    {
                        case 0:
                            {
                                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                                conditionFieldFlag.Field = "flag";
                                conditionFieldFlag.Value = 1;
                                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                conditions.Add(conditionFieldFlag);
                                ND.Data.ConditionField conditionFieldTransactionType = new ND.Data.ConditionField();
                                conditionFieldTransactionType.Field = "transactiontype";
                                conditionFieldTransactionType.Value = (int)this._TransType;
                                conditionFieldTransactionType.DataType = ND.Data.DataObject.DataType.Numeric;
                                conditionFieldTransactionType.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                conditions.Add(conditionFieldTransactionType);
                                break;
                            }
                        case 1:
                            {
                                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                                conditionFieldFlag.Field = "flag";
                                conditionFieldFlag.Value = 1;
                                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                conditions.Add(conditionFieldFlag);
                                ND.Data.ConditionField conditionFieldDocumentNo = new ND.Data.ConditionField();
                                conditionFieldDocumentNo.Field = "documentno";
                                conditionFieldDocumentNo.Value = "%" + this.txtDocumentNoSearch.Text + "%";
                                conditionFieldDocumentNo.DataType = ND.Data.DataObject.DataType.String;
                                conditionFieldDocumentNo.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                conditions.Add(conditionFieldDocumentNo);
                                ND.Data.ConditionField conditionFieldTransactionType = new ND.Data.ConditionField();
                                conditionFieldTransactionType.Field = "transactiontype";
                                conditionFieldTransactionType.Value = (int)this._TransType;
                                conditionFieldTransactionType.DataType = ND.Data.DataObject.DataType.Numeric;
                                conditionFieldTransactionType.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                conditions.Add(conditionFieldTransactionType);
                                break;
                            }
                    }
                    this._Logic_Transaction.BuildTransactionList(this.lstTransactionListing, conditions, sortType, sortField);
                    this.FormatTransactionListColumn();
                    this.lstTransactionListing.Visible = true;
                    this.lstTransactionListing.Focus();
                    Global.ShowWaiting(this, false);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildTransactionListColumn()
            {
                try
                {
                    this.lstTransactionListing.Columns.Clear();
                    this.lstTransactionListing.Columns.Add("transactionno", "Trans. No.");
                    this.lstTransactionListing.Columns.Add("transactiontype", "Type");
                    if ((this.Owner.GetType() == typeof(frmSales)) || (this.Owner.GetType() == typeof(frmStock)))
                    {
                        this.lstTransactionListing.Columns.Add("customer", "Customer");
                        this.lstTransactionListing.Columns.Add("customercode", "Customer Code");
                        this.lstTransactionListing.Columns.Add("customername", "Customer Name");
                    }
                    else if (this.Owner.GetType() == typeof(frmPurchase))
                    {
                        this.lstTransactionListing.Columns.Add("customer", "Vendor");
                        this.lstTransactionListing.Columns.Add("customercode", "Vendor Code");
                        this.lstTransactionListing.Columns.Add("customername", "Vendor Name");
                    }
                    this.lstTransactionListing.Columns.Add("documentno", "Doc. No.");
                    this.lstTransactionListing.Columns.Add("transactiondatetime", "Date");
                    this.lstTransactionListing.Columns.Add("totalamount", "Total Amount");
                    this.lstTransactionListing.Columns.Add("paidamount", "Paid Amount");
                    this.lstTransactionListing.Columns.Add("remark", "Remark");
                    this.lstTransactionListing.Columns.Add("createddate", "Created Date");
                    this.lstTransactionListing.Columns.Add("flag", "Flag");
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void FormatTransactionListColumn()
            {
                try
                {
                    ND.UI.Winform.Form.FormatListView(ref this.lstTransactionListing, this.lstTransactionListing.Columns["transactiondatetime"].Index, typeof(System.DateTime), "{0:dd/MMM/yyyy}");

                    if (this.Owner.GetType() == typeof(frmStock))
                    {
                        frmStock form = (frmStock)this.Owner;
                        switch (form.Working_Mode)
                        {
                            case frmStock.WorkingMode.Receive:
                                {
                                    this.lstTransactionListing.Columns["transactionno"].Width = 150;
                                    this.lstTransactionListing.Columns["transactionno"].TextAlign = HorizontalAlignment.Left;
                                    this.lstTransactionListing.Columns["documentno"].Width = 100;
                                    this.lstTransactionListing.Columns["documentno"].TextAlign = HorizontalAlignment.Left;
                                    this.lstTransactionListing.Columns["transactiondatetime"].Width = 100;
                                    this.lstTransactionListing.Columns["transactiondatetime"].TextAlign = HorizontalAlignment.Left;
                                    this.lstTransactionListing.Columns["remark"].Width = 150;
                                    this.lstTransactionListing.Columns["remark"].TextAlign = HorizontalAlignment.Left;

                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["customername"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["transactiontype"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["customer"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["customercode"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["totalamount"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["paidamount"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["createddate"]);
                                    this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["flag"]);
                                    break;
                                }
                            case frmStock.WorkingMode.Transfer:
                                {
                                    break;
                                }
                        }
                    }
                    else if ((this.Owner.GetType() == typeof(frmSales)) || (this.Owner.GetType() == typeof(frmPurchase)))
                    {
                        this.lstTransactionListing.Columns["transactionno"].Width = 150;
                        this.lstTransactionListing.Columns["transactionno"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["customername"].Width = 150;
                        this.lstTransactionListing.Columns["customername"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["documentno"].Width = 100;
                        this.lstTransactionListing.Columns["documentno"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["transactiondatetime"].Width = 100;
                        this.lstTransactionListing.Columns["transactiondatetime"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["totalamount"].Width = 100;
                        this.lstTransactionListing.Columns["totalamount"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["paidamount"].Width = 100;
                        this.lstTransactionListing.Columns["paidamount"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["remark"].Width = 150;
                        this.lstTransactionListing.Columns["remark"].TextAlign = HorizontalAlignment.Left;

                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["transactiontype"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["customer"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["customercode"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["createddate"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["flag"]);
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildAutoComplete()
            {
                try
                {
                    this.txtDocumentNoSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtDocumentNoSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtDocumentNoSearch.AutoCompleteCustomSource = this._Logic_Transaction.TransactionStringCollection();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void CancelSelect()
            {
                try
                {
                    if (this.Owner.GetType() == typeof(frmStock))
                    {
                        frmStock form = (frmStock)this.Owner;
                        switch (form.Working_Mode)
                        {
                            case frmStock.WorkingMode.Receive:
                                {
                                    form.rbtReceiveEdit.Checked = false;
                                    form.rbtReceiveNew.Checked = true;
                                    break;
                                }
                            case frmStock.WorkingMode.Transfer:
                                {
                                    break;
                                }
                        }
                    }
                    else if (this.Owner.GetType() == typeof(frmSales))
                    {
                        frmSales form = (frmSales)this.Owner;
                        form.rbtEdit.Checked = false;
                        form.rbtNew.Checked = true;
                    }
                    else if (this.Owner.GetType() == typeof(frmPurchase))
                    {
                        frmPurchase form = (frmPurchase)this.Owner;
                        form.rbtEdit.Checked = false;
                        form.rbtNew.Checked = true;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SelectTransaction()
            {
                try
                {
                    if (this.lstTransactionListing.SelectedItems.Count > 0)
                    {
                        if (this.Owner.GetType() == typeof(frmStock))
                        {
                            frmStock form = (frmStock)this.Owner;
                            switch (form.Working_Mode)
                            {
                                case frmStock.WorkingMode.Receive:
                                    {
                                        form.txtReceiveTransactionNo.Text = String.Empty;
                                        form.txtReceiveTransactionNo.Text = this.lstTransactionListing.SelectedItems[0].SubItems[0].Text;
                                        form.txtReceiveDocumentNo.Focus();
                                        break;
                                    }
                                case frmStock.WorkingMode.Transfer:
                                    {
                                        break;
                                    }
                            }
                        }
                        else if (this.Owner.GetType() == typeof(frmSales))
                        {
                            frmSales form = (frmSales)this.Owner;
                            if (this._Mode == 0)
                            {
                                form.txtTransactionNo.Text = String.Empty;
                                form.txtTransactionNo.Text = this.lstTransactionListing.SelectedItems[0].SubItems[0].Text;
                                form.txtBillNo.Focus();
                            }
                            else
                            {
                                form.txtReturn.Text = String.Empty;
                                form.txtReturn.Text = this.lstTransactionListing.SelectedItems[0].SubItems[0].Text;
                            }
                        }
                        else if (this.Owner.GetType() == typeof(frmPurchase))
                        {
                            frmPurchase form = (frmPurchase)this.Owner;
                            form.txtTransactionNo.Text = String.Empty;
                            form.txtTransactionNo.Text = this.lstTransactionListing.SelectedItems[0].SubItems[0].Text;
                            form.txtBillNo.Focus();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

        #endregion
    }
}
