﻿namespace STP
{
    partial class frmSelectCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlListing = new System.Windows.Forms.Panel();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lstCustomerListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCustomerSearch = new System.Windows.Forms.TextBox();
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.Color.Transparent;
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstCustomerListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtCustomerSearch);
            this.pnlListing.Location = new System.Drawing.Point(0, 0);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 636);
            this.pnlListing.TabIndex = 2;
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnSelect);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 556);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Location = new System.Drawing.Point(6, 21);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(120, 43);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "&Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lstCustomerListing
            // 
            this.lstCustomerListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCustomerListing.FullRowSelect = true;
            this.lstCustomerListing.GridLines = true;
            this.lstCustomerListing.HideSelection = false;
            this.lstCustomerListing.Location = new System.Drawing.Point(6, 34);
            this.lstCustomerListing.MultiSelect = false;
            this.lstCustomerListing.Name = "lstCustomerListing";
            this.lstCustomerListing.Size = new System.Drawing.Size(848, 516);
            this.lstCustomerListing.TabIndex = 0;
            this.lstCustomerListing.UseCompatibleStateImageBehavior = false;
            this.lstCustomerListing.View = System.Windows.Forms.View.Details;
            this.lstCustomerListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstCustomerListing_MouseDoubleClick);
            this.lstCustomerListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCustomerListing_ColumnClick);
            this.lstCustomerListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstCustomerListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(700, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search &Customer";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCustomerSearch
            // 
            this.txtCustomerSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerSearch.Location = new System.Drawing.Point(6, 6);
            this.txtCustomerSearch.MaxLength = 255;
            this.txtCustomerSearch.Name = "txtCustomerSearch";
            this.txtCustomerSearch.Size = new System.Drawing.Size(680, 22);
            this.txtCustomerSearch.TabIndex = 1;
            this.txtCustomerSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerSearch_KeyDown);
            // 
            // frmSelectCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 625);
            this.ControlBox = false;
            this.Controls.Add(this.pnlListing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSelectCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmSelectCustomer_Load);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnSelect;
        public System.Windows.Forms.ListView lstCustomerListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtCustomerSearch;
    }
}