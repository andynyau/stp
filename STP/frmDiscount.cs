﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmDiscount : Form
    {
        #region Declarations

            private ND.Standard.Validation.Winform.Form _Validator;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstListing;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtAmount;
            private STP.Logic.Transaction _Logic_Transaction;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmDiscount_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this.BuildList();
                        this.SetValidation();
                        this.rbtGeneral.Checked = true;
                        this.lstListing.Enabled = false;
                        this.txtAmount.Text = "0.00";
                        this.txtName.Text = String.Empty;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region RadioButton Events

                private void rbtGeneral_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.rbtGeneral.Checked == true)
                        {
                            this.lstListing.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void rbtLine_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.rbtLine.Checked == true)
                        {
                            this.lstListing.Enabled = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnOK_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._Validator.Validate() == true)
                        {
                            if (this.rbtGeneral.Checked == true)
                            {
                                STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                                objectTransactionDetail.Transaction_No = String.Empty;
                                objectTransactionDetail.Line_No = this._Logic_Transaction.Detail_Table == null ? 1 : this._Logic_Transaction.Detail_Table.Rows.Count + 1;
                                objectTransactionDetail.Reference_Line = 0;
                                objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Discount;
                                objectTransactionDetail.Stock_ID = 0;
                                objectTransactionDetail.Stock_Category = 0;
                                objectTransactionDetail.Stock_Category_Name = String.Empty;
                                objectTransactionDetail.Stock_Sub_Category = 0;
                                objectTransactionDetail.Stock_Sub_Category_Name = String.Empty;
                                objectTransactionDetail.Stock_Type = (int)STP.Logic.Common.StockType.NonInventory;
                                objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectTransactionDetail.Stock_Type.ToString());
                                objectTransactionDetail.Stock_Code = String.Empty;
                                objectTransactionDetail.Stock_Name = this.txtName.Text.Trim();
                                objectTransactionDetail.Stock_Description_1 = String.Empty;
                                objectTransactionDetail.Stock_Description_2 = String.Empty;

                                decimal amount = decimal.Parse(String.Format("{0:0.00}", decimal.Parse(this.txtAmount.Text.Trim()) * -1));
                                long quantity = 1;

                                objectTransactionDetail.Stock_Price = amount;
                                objectTransactionDetail.Stock_Cost = 0;
                                objectTransactionDetail.Stock_UOM = String.Empty;
                                objectTransactionDetail.Actual_Quantity = quantity;
                                objectTransactionDetail.Billing_Quantity = quantity;
                                objectTransactionDetail.Billing_UOM = String.Empty;
                                objectTransactionDetail.Billing_Price = amount;
                                objectTransactionDetail.Conversion_Factor = 1;
                                objectTransactionDetail.Returned_Quantity = 0;
                                objectTransactionDetail.Reference_Transaction_No = String.Empty;
                                objectTransactionDetail.Reference_Line_No = 0;
                                objectTransactionDetail.Reference_Document_No = String.Empty;
                                objectTransactionDetail.Total_Price = amount * quantity;
                                objectTransactionDetail.Old_Balance = 0;
                                objectTransactionDetail.New_Balance = 0;
                                objectTransactionDetail.CreatedDate = DateTime.Now;
                                objectTransactionDetail.Flag = true;
                                this._Logic_Transaction.AddDetailDataRow(objectTransactionDetail);
                            }
                            else
                            {
                                STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                                objectTransactionDetail.Transaction_No = String.Empty;
                                objectTransactionDetail.Line_No = this._Logic_Transaction.Detail_Table == null ? 1 : this._Logic_Transaction.Detail_Table.Rows.Count + 1;

                                int lineNo = int.Parse(this.lstListing.SelectedItems[0].SubItems[1].Text);

                                objectTransactionDetail.Reference_Line = lineNo;
                                objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Discount;
                                objectTransactionDetail.Stock_ID = 0;
                                objectTransactionDetail.Stock_Category = 0;
                                objectTransactionDetail.Stock_Category_Name = String.Empty;
                                objectTransactionDetail.Stock_Sub_Category = 0;
                                objectTransactionDetail.Stock_Sub_Category_Name = String.Empty;
                                objectTransactionDetail.Stock_Type = (int)STP.Logic.Common.StockType.NonInventory;
                                objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectTransactionDetail.Stock_Type.ToString());
                                objectTransactionDetail.Stock_Code = String.Empty;
                                objectTransactionDetail.Stock_Name = this.txtName.Text.Trim();
                                objectTransactionDetail.Stock_Description_1 = String.Empty;
                                objectTransactionDetail.Stock_Description_2 = String.Empty;

                                decimal amount = decimal.Parse(String.Format("{0:0.00}", decimal.Parse(this.txtAmount.Text.Trim()) * -1));
                                long quantity = 1;

                                objectTransactionDetail.Stock_Price = amount;
                                objectTransactionDetail.Stock_Cost = 0;
                                objectTransactionDetail.Stock_UOM = String.Empty;
                                objectTransactionDetail.Actual_Quantity = quantity;
                                objectTransactionDetail.Billing_Quantity = quantity;
                                objectTransactionDetail.Billing_UOM = String.Empty;
                                objectTransactionDetail.Billing_Price = amount;
                                objectTransactionDetail.Conversion_Factor = 1;
                                objectTransactionDetail.Returned_Quantity = 0;
                                objectTransactionDetail.Reference_Transaction_No = String.Empty;
                                objectTransactionDetail.Reference_Line_No = 0;
                                objectTransactionDetail.Reference_Document_No = String.Empty;
                                objectTransactionDetail.Total_Price = amount * quantity;
                                objectTransactionDetail.Old_Balance = 0;
                                objectTransactionDetail.New_Balance = 0;
                                objectTransactionDetail.CreatedDate = DateTime.Now;
                                objectTransactionDetail.Flag = true;
                                this._Logic_Transaction.AddDetailDataRow(objectTransactionDetail);
                            }
                            this.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnCancel_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Procedures and Functions

            #region Constructor

                public frmDiscount(STP.Logic.Transaction logicTransaction)
                {
                    this.InitializeComponent();
                    this.Text = "Discount";
                    this._Logic_Transaction = logicTransaction;
                    this._Validator = new ND.Standard.Validation.Winform.Form();
                    this.ct_lstListing = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstListing_Validate);
                    this.ct_txtAmount = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtAmount_Validate);
                }

            #endregion

            #region Validations

                private Boolean ct_lstListing_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        if (this.rbtLine.Checked == true)
                        {
                            if (this.lstListing.SelectedItems.Count > 0)
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private Boolean ct_txtAmount_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        decimal num = 0;
                        if (decimal.TryParse(this.txtAmount.Text.Trim(), out num) == true)
                        {
                            if (decimal.Parse(this.txtAmount.Text) > 0)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void BuildButton()
                {
                    try
                    {
                        this.btnOK.Image = Image.FromFile("image/Confirm.png");
                        this.btnOK.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnCancel.Image = Image.FromFile("image/Cancel.png");
                        this.btnCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildList()
                {
                    try
                    {
                        this.BuildList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstListing.Visible = false;
                        this.lstListing.Clear();
                        this._Logic_Transaction.BuildDetailListColumn(this.lstListing);
                        this._Logic_Transaction.BuildDiscountList(this.lstListing, sortType, sortField);
                        this.FormatListColumn();
                        this.lstListing.Visible = true;
                        if (this.lstListing.Items.Count > 0)
                        {
                            this.lstListing.EnsureVisible(this.lstListing.Items.Count - 1);
                        }
                        this.lstListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatListColumn()
                {
                    try
                    {
                        this.lstListing.Columns["stockcategoryname"].Width = 80;
                        this.lstListing.Columns["stockcategoryname"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["stocksubcategoryname"].Width = 80;
                        this.lstListing.Columns["stocksubcategoryname"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["stockcode"].Width = 80;
                        this.lstListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["stockname"].Width = 200;
                        this.lstListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["billingqty"].Width = 80;
                        this.lstListing.Columns["billingqty"].TextAlign = HorizontalAlignment.Right;
                        this.lstListing.Columns["billinguom"].Width = 80;
                        this.lstListing.Columns["billinguom"].TextAlign = HorizontalAlignment.Left;

                        this.lstListing.Columns.Remove(this.lstListing.Columns["transactionno"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["transactionline"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["transactionlinetype"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["refline"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockid"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockcategory"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocksubcategory"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocktype"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocktypename"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockdesc1"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockdesc2"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockprice"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockcost"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["conversionfactor"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockuom"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["actualqty"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["billingprice"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["returnedqty"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["referencetransactionno"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["referenceline"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["referencedocumentno"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["totalprice"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["oldbalance"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["newbalance"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["createddate"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._Validator.AddValidator(this.txtAmount, this.erpDiscount, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._Validator.AddValidator(this.txtName, this.erpDiscount, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._Validator.AddValidator(this.txtAmount, this.erpDiscount, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                        this._Validator.AddValidator(this.lstListing, this.erpDiscount, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstListing);
                        this._Validator.AddValidator(this.txtAmount, this.erpDiscount, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtAmount);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion
    }
}
