﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;
using System.Reflection;
using System.IO;

namespace STP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Check Config
            ND.Standard.Configuration config = new ND.Standard.Configuration();
            config.ConfigFile = Assembly.GetExecutingAssembly().Location + ".config";
            if (config.CheckConfigFileExist() == false)
            {
                config.CreateConfigFile();
            }

            Hashtable configurationElements = new Hashtable();
            configurationElements.Add("SET:CONNSTRING", String.Empty);
            configurationElements.Add("SET:COMPANY", String.Empty);
            configurationElements.Add("SET:LOGPATH", String.Empty);
            configurationElements.Add("CR:SERVER", String.Empty);
            configurationElements.Add("CR:DATABASE", String.Empty);
            configurationElements.Add("CR:USER", String.Empty);
            configurationElements.Add("CR:PASSWORD", String.Empty);
            configurationElements.Add("CR:PATH", String.Empty);
            configurationElements.Add("TRANS:FORMAT", String.Empty);
            configurationElements.Add("TRANS:LENGTH", String.Empty);

            config.CheckConfigElement(configurationElements);

            //Check DB
            ND.Data.SQL.DataAccess dataAccess = new ND.Data.SQL.DataAccess();
            if (dataAccess.TryConnect() == true)
            {
                Application.Run(new frmMain());
            }
            else
            {
                String title = "Error";
                String message = "Unable to Connect to Database.";
                MessageBox.Show(message,title,MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1);
                Application.Exit();
            }
        }
    }
}
