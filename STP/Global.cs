﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public class Global
    {
        public static Boolean ForceClose = false;

        public static void ShowWaiting(Form form, Boolean visible)
        {
            try
            {
                frmMain mainForm = null;
                if (form.GetType() == typeof(frmMain))
                {
                    mainForm = ((frmMain)form);
                }
                else if (form.IsMdiChild == true)
                {
                    if (form.MdiParent.GetType() == typeof(frmMain))
                    {
                        mainForm = ((frmMain)form.MdiParent);
                    }
                }
                else
                {
                    if (form.Owner.GetType() == typeof(frmMain))
                    {
                        mainForm = ((frmMain)form.Owner);
                    }
                    else if (form.Owner.MdiParent.GetType() == typeof(frmMain))
                    {
                        mainForm = ((frmMain)form.Owner.MdiParent);
                    }
                }

                if (visible == true)
                {
                    mainForm.pgbLoading.Visible = true;
                    mainForm.pgbLoading.Value = 100;
                    Application.DoEvents();
                }
                else
                {
                    mainForm.pgbLoading.Visible = false;
                    mainForm.pgbLoading.Value = 0;
                    Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }
    }
}
