﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmReport : Form
    {
        #region Declaration

            private STP.Logic.Report _Logic_Report;
            private STP.Logic.Stock _Logic_Stock;
            private STP.Logic.General _Logic_General;

            private String _Current_Sort;
            private String _Current_Sort_Customer;
            private String _Current_Sort_Category;
            private String _Current_Sort_Stock;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildReportList;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCustomerList;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCategoryList;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildStockList;

            private ND.Standard.Validation.Winform.Form _ValidatorReport;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstCustomer;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstCategory;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstStock;

            private Boolean _StockParam;
            private Boolean _CategoryParam;
            private Boolean _SubCategoryParam;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmReport_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildParameterPanel();
                        this.BuildButton();
                        this._Current_Sort = String.Empty;
                        this._Current_Sort_Customer = String.Empty;
                        this._Current_Sort_Category = String.Empty;
                        this._Current_Sort_Stock = String.Empty;
                        this._StockParam = false;
                        this._CategoryParam = false;
                        this.LoadReportList();
                        this._Logic_Report.SetParameterPanel(0);
                        this.SetValidation();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void frmReport_FormClosing(object sender, FormClosingEventArgs e)
                {
                    this.CloseForm(e);
                }

            #endregion

            #region ListView Events

                private void lstReport_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstReport, ref this._Current_Sort, e.Column, this.ldm_BuildReportList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstReport_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.lstReport.SelectedItems.Count > 0)
                        {
                            Global.ShowWaiting(this, true);
                            this._Logic_Report.SetParameterPanel(int.Parse(this.lstReport.SelectedItems[0].SubItems[0].Text));
                            this.BuildParameterValues();
                            Global.ShowWaiting(this, false);
                        }
                        else
                        {
                            this._Logic_Report.SetParameterPanel(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomer_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstCustomer, ref this._Current_Sort_Customer, e.Column, this.ldm_BuildCustomerList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCategory_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstCategory, ref this._Current_Sort_Category, e.Column, this.ldm_BuildCategoryList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCategory_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._SubCategoryParam == true)
                        {
                            if (this.lstCategory.SelectedItems.Count > 0)
                            {
                                if ((this.chkSubCategory.Checked == true) || (this.chkStock.Checked == true))
                                {
                                    this.LoadSubCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                                }
                            }
                        }
                        if (this._StockParam == true)
                        {
                            if (this.lstCategory.SelectedItems.Count > 0)
                            {
                                if (this.chkStock.Checked == true)
                                {
                                    this.LoadStockList(ND.Data.DataObject.SortType.None, String.Empty);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstSubCategory_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._StockParam == true)
                        {
                            if (this.lstSubCategory.SelectedItems.Count > 0)
                            {
                                this.LoadStockList(ND.Data.DataObject.SortType.None, String.Empty);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstStock_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstStock, ref this._Current_Sort_Stock, e.Column, this.ldm_BuildStockList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region CheckBox Events

                private void chkYear_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.cmbYear.Enabled = this.chkYear.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkYearRange_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.cmbYearFrom.Enabled = this.chkYearRange.Checked;
                        this.cmbYearTo.Enabled = this.chkYearRange.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkMonth_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.cmbMonth.Enabled = this.chkMonth.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkMonthRange_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.cmbMonthFrom.Enabled = this.chkMonthRange.Checked;
                        this.cmbMonthTo.Enabled = this.chkMonthRange.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkDate_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.dtpDate.Enabled = this.chkDate.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkDateRange_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.dtpDateFrom.Enabled = this.chkDateRange.Checked;
                        this.dtpDateTo.Enabled = this.chkDateRange.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkCategory_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.lstCategory.Enabled = this.chkCategory.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkSubCategory_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._SubCategoryParam == true)
                        {
                            this.chkCategory.Checked = false;
                            this.lstCategory.Enabled = this.chkSubCategory.Checked;
                        }
                        this.lstSubCategory.Enabled = this.chkSubCategory.Checked;

                        if (this._CategoryParam == true)
                        {
                            if (this.chkSubCategory.Checked == false)
                            {
                                this.chkCategory.Visible = true;
                            }
                            else
                            {
                                this.chkCategory.Visible = false;
                            }
                        }
                        this.LoadCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkCustomer_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.lstCustomer.Enabled = this.chkCustomer.Checked;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void chkStock_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._StockParam == true)
                        {
                            this.chkCategory.Checked = false;
                            this.chkSubCategory.Checked = false;
                            this.lstCategory.Enabled = this.chkStock.Checked;
                            this.lstSubCategory.Enabled = this.chkStock.Checked;
                        }
                        this.lstStock.Enabled = chkStock.Checked;

                        if (this._CategoryParam == true)
                        {
                            if (this.chkStock.Checked == false)
                            {
                                this.chkCategory.Visible = true;
                                this.chkSubCategory.Visible = true;
                            }
                            else
                            {
                                this.chkCategory.Visible = false;
                                this.chkSubCategory.Visible = false;
                            }
                        }
                        this.LoadCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                        this.LoadSubCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnGenerate_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.ValidationForm() == true)
                        {
                            frmReportViewer reportViewer = new frmReportViewer();
                            ND.Reporting.Crystal crystal = new ND.Reporting.Crystal();
                            crystal.CrystalReportViewer = reportViewer.crvMain;
                            crystal.ReportFile = this.lstReport.SelectedItems[0].SubItems[3].Text;
                            this.BuildReportFormula(crystal);
                            this.BuildReportParameter(crystal);
                            crystal.ShowFormReport();
                            reportViewer.Text = "Report - " + this.lstReport.SelectedItems[0].SubItems[2].Text;
                            reportViewer.ShowDialog();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Procedures and Functions

            #region Constructor

                public frmReport()
                {
                    try
                    {
                        this.InitializeComponent();
                        this.Text = "Report";

                        this._Logic_Report = new STP.Logic.Report();
                        this._Logic_Stock = new STP.Logic.Stock();
                        this._Logic_General = new STP.Logic.General();

                        this.ldm_BuildReportList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.LoadReportList);
                        this.ldm_BuildCustomerList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.LoadCustomerList);
                        this.ldm_BuildCategoryList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.LoadCategoryList);
                        this.ldm_BuildStockList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.LoadStockList);

                        this._ValidatorReport = new ND.Standard.Validation.Winform.Form();
                        this.ct_lstCustomer = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstCustomer_Validate);
                        this.ct_lstCategory = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstCategory_Validate);
                        this.ct_lstStock = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstStock_Validate);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Validations

                private Boolean ct_lstCustomer_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        if (this.chkCustomer.Checked == true)
                        {
                            if (this.lstCustomer.CheckedItems.Count == 0)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private Boolean ct_lstCategory_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        if (this.chkCategory.Checked == true)
                        {
                            if (this.lstCategory.CheckedItems.Count == 0)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private Boolean ct_lstStock_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        if (this.chkStock.Checked == true)
                        {
                            if (this.lstStock.CheckedItems.Count == 0)
                            {
                                result = false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void LoadReportList()
                {
                    try
                    {
                        this.LoadReportList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadReportList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstReport.Visible = false;
                        this.lstReport.Clear();
                        this.BuildReportListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                        conditionFieldFlag.Field = "flag";
                        conditionFieldFlag.Value = 1;
                        conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                        conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                        conditions.Add(conditionFieldFlag);
                        this._Logic_Report.BuildReportList(this.lstReport, conditions, sortType, sortField);
                        this.FormatReportListColumn();
                        this.lstReport.Visible = true;
                        this.lstReport.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        Global.ShowWaiting(this, true);
                        this.lstCategory.Visible = false;
                        this.lstCategory.Clear();
                        this.BuildCategoryListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        Boolean checkBox = this._CategoryParam;
                        if ((this._StockParam == true) && (this._CategoryParam == true))
                        {
                            checkBox = !this.chkStock.Checked;
                        }
                        if ((this._SubCategoryParam == true) && (this._CategoryParam == true))
                        {
                            if (this.chkStock.Checked != true)
                            {
                                checkBox = !this.chkSubCategory.Checked;
                            }
                        }
                        this._Logic_Stock.BuildCategoryList(this.lstCategory, conditions, sortType, sortField, checkBox, false);
                        this.FormatCategoryListColumn();
                        this.lstCategory.Visible = true;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadSubCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        Global.ShowWaiting(this, true);
                        this.lstSubCategory.Visible = false;
                        this.lstSubCategory.Clear();
                        this.BuildSubCategoryListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        if (this.lstCategory.SelectedItems.Count > 0)
                        {
                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                            conditionFieldStockCategory.Field = "stockcategory";
                            if (this.chkCategory.Checked == false)
                            {
                                conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                            }
                            else
                            {
                                conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[1].Text;
                            }
                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockCategory);
                        }
                        Boolean checkBox = this._SubCategoryParam;
                        if ((this._StockParam == true) && (this._SubCategoryParam == true))
                        {
                            checkBox = !this.chkStock.Checked;
                        }
                        this._Logic_Stock.BuildSubCategoryList(this.lstSubCategory, conditions, sortType, sortField, checkBox, false);
                        this.FormatSubCategoryListColumn();
                        this.lstSubCategory.Visible = true;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadStockList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        Global.ShowWaiting(this, true);
                        this.lstStock.Visible = false;
                        this.lstStock.Clear();
                        this.BuildStockListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        if (this.lstCategory.SelectedItems.Count > 0)
                        {
                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                            conditionFieldStockCategory.Field = "stockcategory";
                            if (this.chkCategory.Checked == false)
                            {
                                conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                            }
                            else
                            {
                                conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[1].Text;
                            }
                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockCategory);
                        }
                        if (this.lstSubCategory.SelectedItems.Count > 0)
                        {
                            ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
                            conditionFieldStockSubCategory.Field = "stocksubcategory";
                            if (this.chkSubCategory.Checked == false)
                            {
                                conditionFieldStockSubCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                            }
                            else
                            {
                                conditionFieldStockSubCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[2].Text;
                            }
                            conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockSubCategory);
                        }
                        this._Logic_Stock.BuildStockList(this.lstStock, conditions, sortType, sortField, true);
                        this.FormatStockListColumn();
                        this.lstStock.Visible = true;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadCustomerList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        Global.ShowWaiting(this, true);
                        this.lstCustomer.Visible = false;
                        this.lstCustomer.Clear();
                        this.BuildCustomerListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        this._Logic_General.BuildCustomerList(this.lstCustomer, conditions, sortType, sortField, true);
                        this.FormatCustomerListColumn();
                        this.lstCustomer.Visible = true;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildReportListColumn()
                {
                    try
                    {
                        this.lstReport.Columns.Clear();
                        this.lstReport.Columns.Add("id", "ID");
                        this.lstReport.Columns.Add("reportcode", "Code");
                        this.lstReport.Columns.Add("reportname", "Report");
                        this.lstReport.Columns.Add("reportfile", "File");
                        this.lstReport.Columns.Add("pr_year", "Param Year");
                        this.lstReport.Columns.Add("pr_year_range", "Param Year Range");
                        this.lstReport.Columns.Add("pr_month", "Param Month");
                        this.lstReport.Columns.Add("pr_month_range", "Param Month Range");
                        this.lstReport.Columns.Add("pr_date", "Param Date");
                        this.lstReport.Columns.Add("pr_date_range", "Param Date Range");
                        this.lstReport.Columns.Add("pr_customer", "Param Customer");
                        this.lstReport.Columns.Add("pr_category", "Param Category");
                        this.lstReport.Columns.Add("pr_subcategory", "Param Sub Category");
                        this.lstReport.Columns.Add("pr_stock", "Param Stock");
                        this.lstReport.Columns.Add("createddate", "Created Date");
                        this.lstReport.Columns.Add("flag", "Flag");
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatReportListColumn()
                {
                    try
                    {
                        this.lstReport.Columns["reportname"].Width = 150;
                        this.lstReport.Columns["reportname"].TextAlign = HorizontalAlignment.Left;

                        this.lstReport.Columns.Remove(this.lstReport.Columns["id"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["reportcode"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["reportfile"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_year"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_year_range"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_month"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_month_range"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_date"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_date_range"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_customer"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_category"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_subcategory"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["pr_stock"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["createddate"]);
                        this.lstReport.Columns.Remove(this.lstReport.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildButton()
                {
                    try
                    {
                        this.btnGenerate.Image = Image.FromFile("image/Preview.png");
                        this.btnGenerate.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnClose.Image = Image.FromFile("image/Cancel.png");
                        this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildParameterPanel()
                {
                    try
                    {
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_year", this.pnlYear, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_year_range", this.pnlYearRange, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_month", this.pnlMonth, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_month_range", this.pnlMonthRange, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_date", this.pnlDate, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_date_range", this.pnlDateRange, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_customer", this.pnlCustomer, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_category", this.pnlCategory, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_subcategory", this.pnlSubCategory, false));
                        this._Logic_Report.Report_Parameter.Add(new STP.Logic.Report.Parameter("pr_stock", this.pnlStock, false));
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildParameterValues()
                {
                    try
                    {
                        this._CategoryParam = false;
                        this._StockParam = false;
                        this.ResetCheckBox();
                        foreach (STP.Logic.Report.Parameter parameter in this._Logic_Report.Report_Parameter)
                        {
                            if (parameter.Panel.Visible == true)
                            {
                                switch (parameter.Name)
                                {
                                    case "pr_year":
                                        {
                                            DataTable dataTable = new DataTable();
                                            dataTable.Columns.Add("Year");
                                            int year = 0;
                                            for (year = 2012; year <= DateTime.Now.Year; year++)
                                            {
                                                DataRow dataRow = dataTable.NewRow();
                                                dataRow["Year"] = year;
                                                dataTable.Rows.Add(dataRow);
                                                dataTable.AcceptChanges();
                                            }
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbYear, dataTable, "Year", "Year");
                                            break;
                                        }
                                    case "pr_year_range":
                                        {
                                            DataTable dataTable = new DataTable();
                                            dataTable.Columns.Add("Year");
                                            int year = 0;
                                            for (year = 2012; year <= DateTime.Now.Year; year++)
                                            {
                                                DataRow dataRow = dataTable.NewRow();
                                                dataRow["Year"] = year;
                                                dataTable.Rows.Add(dataRow);
                                                dataTable.AcceptChanges();
                                            }
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbYearFrom, dataTable, "Year", "Year");
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbYearTo, dataTable, "Year", "Year");
                                            break;
                                        }
                                    case "pr_month":
                                        {
                                            DataTable dataTable = new DataTable();
                                            dataTable.Columns.Add("Month_Value");
                                            dataTable.Columns.Add("Month_Name");

                                            int month = 0;
                                            for (month = 1; month <= 12; month++)
                                            {
                                                DataRow dataRow = dataTable.NewRow();
                                                dataRow["Month_Value"] = month;
                                                dataRow["Month_Name"] = new DateTime(DateTime.Now.Year, month, DateTime.Now.Day).ToString("MMM");
                                                dataTable.Rows.Add(dataRow);
                                                dataTable.AcceptChanges();
                                            }
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbMonth, dataTable, "Month_Name", "Month_Value");
                                            break;
                                        }
                                    case "pr_month_range":
                                        {
                                            DataTable dataTable = new DataTable();
                                            dataTable.Columns.Add("Month_Value");
                                            dataTable.Columns.Add("Month_Name");

                                            int month = 0;
                                            for (month = 1; month <= 12; month++)
                                            {
                                                DataRow dataRow = dataTable.NewRow();
                                                dataRow["Month_Value"] = month;
                                                dataRow["Month_Name"] = new DateTime(DateTime.Now.Year, month, DateTime.Now.Day).ToString("MMM");
                                                dataTable.Rows.Add(dataRow);
                                                dataTable.AcceptChanges();
                                            }
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbMonthFrom, dataTable, "Month_Name", "Month_Value");
                                            ND.UI.Winform.Form.PopulateComboBox(ref this.cmbMonthTo, dataTable, "Month_Name", "Month_Value");
                                            break;
                                        }
                                    case "pr_date":
                                        {
                                            this.dtpDate.Value = DateTime.Now;
                                            break;
                                        }
                                    case "pr_date_range":
                                        {
                                            this.dtpDateFrom.Value = DateTime.Now;
                                            this.dtpDateTo.Value = DateTime.Now;
                                            break;
                                        }
                                    case "pr_customer":
                                        {
                                            this.LoadCustomerList(ND.Data.DataObject.SortType.None, String.Empty);
                                            break;
                                        }
                                    case "pr_category":
                                        {
                                            this.chkCategory.Visible = true;
                                            this._CategoryParam = true;
                                            this.LoadCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                                            break;
                                        }
                                    case "pr_subcategory":
                                        {
                                            this.chkSubCategory.Visible = true;
                                            this._SubCategoryParam = true;
                                            //LoadSubCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                                            break;
                                        }
                                    case "pr_stock":
                                        {
                                            if (this._CategoryParam == false)
                                            {
                                                this.pnlCategory.Visible = true;
                                                this.pnlCategory.Top = this.pnlStock.Top;
                                                this.pnlStock.Top += this.pnlStock.Height;
                                            }
                                            this._StockParam = true;
                                            this.LoadCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                                            this.BuildStockListColumn();
                                            this.lstStock.Columns.Insert(0, String.Empty);
                                            this.lstStock.Columns[0].Width = 20;
                                            ND.UI.Winform.Control.AddHeaderCheckBox(ref this.lstStock);
                                            this.FormatStockListColumn();
                                            Global.ShowWaiting(this, false);
                                            break;
                                        }
                                }
                            }
                        }
                        if (this._StockParam == true)
                        {
                            if (this._CategoryParam == true)
                            {
                                this.chkCategory.Visible = true;
                            }
                            else
                            {
                                this.chkCategory.Visible = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildCategoryListColumn()
                {
                    this.lstCategory.Columns.Clear();
                    this.lstCategory.Columns.Add("id", "ID");
                    this.lstCategory.Columns.Add("categoryname", "Category");
                    this.lstCategory.Columns.Add("categoryshortcut", "Code");
                    this.lstCategory.Columns.Add("createddate", "Created Date");
                    this.lstCategory.Columns.Add("flag", "Flag");
                }

                private void FormatCategoryListColumn()
                {
                    this.lstCategory.Columns["categoryname"].Width = 200;
                    this.lstCategory.Columns["categoryname"].TextAlign = HorizontalAlignment.Left;
                    this.lstCategory.Columns["categoryshortcut"].Width = 50;
                    this.lstCategory.Columns["categoryshortcut"].TextAlign = HorizontalAlignment.Left;

                    this.lstCategory.Columns.Remove(this.lstCategory.Columns["id"]);
                    this.lstCategory.Columns.Remove(this.lstCategory.Columns["createddate"]);
                    this.lstCategory.Columns.Remove(this.lstCategory.Columns["flag"]);
                }

                private void BuildSubCategoryListColumn()
                {
                    this.lstSubCategory.Columns.Clear();
                    this.lstSubCategory.Columns.Add("stockcategory", "Category");
                    this.lstSubCategory.Columns.Add("id", "ID");
                    this.lstSubCategory.Columns.Add("subcategoryname", "Sub Category");
                    this.lstSubCategory.Columns.Add("subcategoryshortcut", "Code");
                    this.lstSubCategory.Columns.Add("createddate", "Created Date");
                    this.lstSubCategory.Columns.Add("flag", "Flag");
                }

                private void FormatSubCategoryListColumn()
                {
                    this.lstSubCategory.Columns["subcategoryname"].Width = 200;
                    this.lstSubCategory.Columns["subcategoryname"].TextAlign = HorizontalAlignment.Left;
                    this.lstSubCategory.Columns["subcategoryshortcut"].Width = 50;
                    this.lstSubCategory.Columns["subcategoryshortcut"].TextAlign = HorizontalAlignment.Left;

                    this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["stockcategory"]);
                    this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["id"]);
                    this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["createddate"]);
                    this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["flag"]);
                }

                private void BuildCustomerListColumn()
                {
                    try
                    {
                        this.lstCustomer.Columns.Clear();
                        this.lstCustomer.Columns.Add("id", "ID");
                        this.lstCustomer.Columns.Add("customercode", "Code");
                        this.lstCustomer.Columns.Add("customername", "Name");
                        this.lstCustomer.Columns.Add("address1", "Address 1");
                        this.lstCustomer.Columns.Add("address2", "Address 2");
                        this.lstCustomer.Columns.Add("postcode", "Post Code");
                        this.lstCustomer.Columns.Add("city", "City");
                        this.lstCustomer.Columns.Add("area", "Area");
                        this.lstCustomer.Columns.Add("state", "State");
                        this.lstCustomer.Columns.Add("country", "Country");
                        this.lstCustomer.Columns.Add("tel", "Tel");
                        this.lstCustomer.Columns.Add("fax", "Fax");
                        this.lstCustomer.Columns.Add("contact", "Contact");
                        this.lstCustomer.Columns.Add("createddate", "Created Date");
                        this.lstCustomer.Columns.Add("flag", "Flag");
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatCustomerListColumn()
                {
                    try
                    {
                        this.lstCustomer.Columns["customername"].Width = 250;
                        this.lstCustomer.Columns["customername"].TextAlign = HorizontalAlignment.Left;

                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["id"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["customercode"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["address1"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["address2"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["postcode"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["city"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["area"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["state"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["country"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["tel"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["fax"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["contact"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["createddate"]);
                        this.lstCustomer.Columns.Remove(this.lstCustomer.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildStockListColumn()
                {
                    try
                    {
                        this.lstStock.Columns.Clear();
                        this.lstStock.Columns.Add("stockcategory", "Category");
                        this.lstStock.Columns.Add("stocksubcategory", "Sub Category");
                        this.lstStock.Columns.Add("id", "ID");
                        this.lstStock.Columns.Add("stockcode", "Code");
                        this.lstStock.Columns.Add("stocktype", "Stock Type");
                        this.lstStock.Columns.Add("stockname", "Stock Name");
                        this.lstStock.Columns.Add("stockdesc1", "Stock Description 1");
                        this.lstStock.Columns.Add("stockdesc2", "Stock Description 2");
                        this.lstStock.Columns.Add("stockcost", "Stock Cost");
                        this.lstStock.Columns.Add("stockbalance", "Balance");
                        this.lstStock.Columns.Add("stockuom", "Unit");
                        this.lstStock.Columns.Add("packagesize1", "Size (A)");
                        this.lstStock.Columns.Add("packageuom1", "Pkg (A)");
                        this.lstStock.Columns.Add("packagesize2", "Size (B)");
                        this.lstStock.Columns.Add("packageuom2", "Pkg (B)");
                        this.lstStock.Columns.Add("stockprice1", "SO Unit");
                        this.lstStock.Columns.Add("packageprice1", "SO Pkg");
                        this.lstStock.Columns.Add("stockprice2", "Cash Unit");
                        this.lstStock.Columns.Add("packageprice2", "Cash Pkg");
                        this.lstStock.Columns.Add("stockprice3", "Retail Unit");
                        this.lstStock.Columns.Add("packageprice3", "Retail Pkg");
                        this.lstStock.Columns.Add("stockprice4", "Special Unit");
                        this.lstStock.Columns.Add("packageprice4", "Special Pkg");
                        this.lstStock.Columns.Add("stockprice5", "Reserved Unit");
                        this.lstStock.Columns.Add("packageprice5", "Reserved Pkg");
                        this.lstStock.Columns.Add("guid", "GUID");
                        this.lstStock.Columns.Add("createddate", "Created Date");
                        this.lstStock.Columns.Add("flag", "Flag");
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatStockListColumn()
                {
                    try
                    {
                        this.lstStock.Columns["stockcode"].Width = 100;
                        this.lstStock.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                        this.lstStock.Columns["stockname"].Width = 150;
                        this.lstStock.Columns["stockname"].TextAlign = HorizontalAlignment.Left;

                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockcategory"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stocksubcategory"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["id"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stocktype"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockdesc1"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockdesc2"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockcost"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockbalance"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockuom"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packagesize1"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageuom1"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packagesize2"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageuom2"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockprice1"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageprice1"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockprice2"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageprice2"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockprice3"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageprice3"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockprice4"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageprice4"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["stockprice5"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["packageprice5"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["guid"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["createddate"]);
                        this.lstStock.Columns.Remove(this.lstStock.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._ValidatorReport.AddValidator(this.lstCustomer, this.erpReport, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstCustomer);
                        this._ValidatorReport.AddValidator(this.lstCategory, this.erpReport, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstCategory);
                        this._ValidatorReport.AddValidator(this.lstStock, this.erpReport, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstStock);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean ValidationForm()
                {
                    Boolean result = false;
                    try
                    {
                        result = this._ValidatorReport.Validate();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean BuildReportFormula(ND.Reporting.Crystal crystal)
                {
                    Boolean result = false;
                    try
                    {
                        crystal.ReportFormula += crystal.GetReportFormula();
                        if (this.chkYear.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Year}", crystal, new object[] { this.cmbYear }, STP.Logic.Common.ReportParameterType.Single);
                        }
                        if (this.chkYearRange.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Year}", crystal, new object[] { this.cmbYearFrom, this.cmbYearTo }, STP.Logic.Common.ReportParameterType.Range);
                        }
                        if (this.chkMonth.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Month}", crystal, new object[] { this.cmbMonth }, STP.Logic.Common.ReportParameterType.Single);
                        }
                        if (this.chkMonthRange.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Month}", crystal, new object[] { this.cmbMonthFrom, this.cmbMonthTo }, STP.Logic.Common.ReportParameterType.Range);
                        }
                        if (this.chkDate.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Date}", crystal, new object[] { this.dtpDate }, STP.Logic.Common.ReportParameterType.Single);
                        }
                        if (this.chkDateRange.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Date}", crystal, new object[] { this.dtpDateFrom, this.dtpDateTo }, STP.Logic.Common.ReportParameterType.Range);
                            crystal.ReportFormula += this.BuildFormula("{@DateFrom}", crystal, new object[] { this.dtpDateFrom }, STP.Logic.Common.ReportParameterType.Single);
                            crystal.ReportFormula += this.BuildFormula("{@DateTo}", crystal, new object[] { this.dtpDateTo }, STP.Logic.Common.ReportParameterType.Single);
                        }
                        if (this.chkCustomer.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Customer}", crystal, new object[] { this.lstCustomer }, STP.Logic.Common.ReportParameterType.Multi, 1);
                        }
                        if (this.chkCategory.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Category}", crystal, new object[] { this.lstCategory }, STP.Logic.Common.ReportParameterType.Multi, 1);
                        }
                        if (this.chkSubCategory.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@SubCategory}", crystal, new object[] { this.lstSubCategory }, STP.Logic.Common.ReportParameterType.Multi, 2);
                        }
                        if (this.chkStock.Checked == true)
                        {
                            crystal.ReportFormula += this.BuildFormula("{@Stock}", crystal, new object[] { this.lstStock }, STP.Logic.Common.ReportParameterType.MultiString, 3);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private String BuildFormula(String formulaField, ND.Reporting.Crystal crystal, object[] control, STP.Logic.Common.ReportParameterType reportParameterType)
                {
                    return this.BuildFormula(formulaField, crystal, control, reportParameterType, 0);
                }

                private String BuildFormula(String formulaField, ND.Reporting.Crystal crystal, object[] control, STP.Logic.Common.ReportParameterType reportParameterType, int index)
                {
                    String result = String.Empty;
                    try
                    {
                        if (crystal.CheckFormulaFieldExist(formulaField) == true)
                        {
                            if (crystal.ReportFormula != String.Empty)
                            {
                                result += " AND ";
                            }
                            switch (reportParameterType)
                            {
                                case STP.Logic.Common.ReportParameterType.Single:
                                    {
                                        if (control[0].GetType() == typeof(ComboBox))
                                        {
                                            ComboBox comboBox = (ComboBox)control[0];
                                            result +=
                                                "("
                                                    + formulaField + " = " + comboBox.SelectedValue
                                                + ")";
                                        }
                                        else if (control[0].GetType() == typeof(DateTimePicker))
                                        {
                                            DateTimePicker dateTimePicker = (DateTimePicker)control[0];
                                            result +=
                                                "("
                                                    + formulaField + " = " + "CDate(" + dateTimePicker.Value.Year + ", " + dateTimePicker.Value.Month + ", " + dateTimePicker.Value.Day + ")"
                                                +")";
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.ReportParameterType.SingleString:
                                    {
                                        if (control[0].GetType() == typeof(ComboBox))
                                        {
                                            ComboBox comboBox = (ComboBox)control[0];
                                            result +=
                                                "("
                                                    + formulaField + " = \"" + comboBox.SelectedValue + "\""
                                                + ")";
                                        }
                                        else if (control[0].GetType() == typeof(DateTimePicker))
                                        {
                                            DateTimePicker dateTimePicker = (DateTimePicker)control[0];
                                            result +=
                                                "("
                                                    + formulaField + " = " + "CDate(" + dateTimePicker.Value.Year + ", " + dateTimePicker.Value.Month + ", " + dateTimePicker.Value.Day + ")"
                                                + ")";
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.ReportParameterType.Range:
                                    {
                                        if (control[0].GetType() == typeof(ComboBox))
                                        {
                                            ComboBox comboBoxFrom = (ComboBox)control[0];
                                            ComboBox comboBoxTo = (ComboBox)control[1];
                                            result += 
                                                "(" 
                                                    + formulaField + " >= " + comboBoxFrom.SelectedValue 
                                                    + " AND " 
                                                    + formulaField + " <= " + comboBoxTo.SelectedValue 
                                                + ")";
                                        }
                                        else if (control[0].GetType() == typeof(DateTimePicker))
                                        {
                                            DateTimePicker dateTimePickerFrom = (DateTimePicker)control[0];
                                            DateTimePicker dateTimePickerTo = (DateTimePicker)control[1];
                                            result += 
                                                "(" 
                                                    + formulaField + " >= " + "CDate(" + dateTimePickerFrom.Value.Year + ", " + dateTimePickerFrom.Value.Month + ", " + dateTimePickerFrom.Value.Day + ")" 
                                                    + " AND " 
                                                    + formulaField + " <= " + "CDate(" + dateTimePickerTo.Value.Year + ", " + dateTimePickerTo.Value.Month + ", " + dateTimePickerTo.Value.Day + ")" 
                                                + ")";
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.ReportParameterType.RangeString:
                                    {
                                        if (control[0].GetType() == typeof(ComboBox))
                                        {
                                            ComboBox comboBoxFrom = (ComboBox)control[0];
                                            ComboBox comboBoxTo = (ComboBox)control[1];
                                            result +=
                                                "("
                                                    + formulaField + " >= \"" + comboBoxFrom.SelectedValue + "\""
                                                    + " AND "
                                                    + formulaField + " <= \"" + comboBoxTo.SelectedValue + "\""
                                                + ")";
                                        }
                                        else if (control[0].GetType() == typeof(DateTimePicker))
                                        {
                                            DateTimePicker dateTimePickerFrom = (DateTimePicker)control[0];
                                            DateTimePicker dateTimePickerTo = (DateTimePicker)control[1];
                                            result +=
                                                "("
                                                    + formulaField + " >= " + "CDate(" + dateTimePickerFrom.Value.Year + ", " + dateTimePickerFrom.Value.Month + ", " + dateTimePickerFrom.Value.Day + ")"
                                                    + " AND "
                                                    + formulaField + " <= " + "CDate(" + dateTimePickerTo.Value.Year + ", " + dateTimePickerTo.Value.Month + ", " + dateTimePickerTo.Value.Day + ")"
                                                + ")";
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.ReportParameterType.Multi:
                                    {
                                        if (control[0].GetType() == typeof(ListView))
                                        {
                                            ListView listView = (ListView)control[0];
                                            String items = String.Empty;
                                            foreach (ListViewItem item in listView.CheckedItems)
                                            {
                                                if (items != String.Empty)
                                                {
                                                    items += ", ";
                                                }
                                                items += item.SubItems[index].Text;
                                            }
                                            result +=
                                                "("
                                                    + formulaField + " IN " + "[" + items + "]"
                                                + ")";
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.ReportParameterType.MultiString:
                                    {
                                        if (control[0].GetType() == typeof(ListView))
                                        {
                                            ListView listView = (ListView)control[0];
                                            String items = String.Empty;
                                            foreach (ListViewItem item in listView.CheckedItems)
                                            {
                                                if (items != String.Empty)
                                                {
                                                    items += ", ";
                                                }
                                                items += "\"" +item.SubItems[index].Text + "\"";
                                            }
                                            result +=
                                                "("
                                                    + formulaField + " IN " + "[" + items + "]"
                                                + ")";
                                        }
                                        break;
                                    }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private Boolean BuildReportParameter(ND.Reporting.Crystal crystal)
                {
                    Boolean result = false;
                    try
                    {
                        if (this.chkYear.Checked == true)
                        {
                            String parameterField = "@Year";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbYear.SelectedValue);
                            }
                        }
                        if (this.chkYearRange.Checked == true)
                        {
                            String parameterField = "@YearFrom";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbYearFrom.SelectedValue);
                            }
                            parameterField = "@YearTo";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbYearTo.SelectedValue);
                            }
                        }
                        if (this.chkMonth.Checked == true)
                        {
                            String parameterField = "@Month";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbMonth.SelectedValue);
                            }
                        }
                        if (this.chkMonthRange.Checked == true)
                        {
                            String parameterField = "@MonthFrom";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbMonthFrom.SelectedValue);
                            }
                            parameterField = "@MonthTo";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.cmbMonthTo.SelectedValue);
                            }
                        }
                        if (this.chkDate.Checked == true)
                        {
                            String parameterField = "@Date";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.dtpDate.Value);
                            }
                        }
                        if (this.chkDateRange.Checked == true)
                        {
                            String parameterField = "@DateFrom";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.dtpDateFrom.Value);
                            }
                            parameterField = "@DateTo";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                crystal.HastTable.Add(parameterField, this.dtpDateTo.Value);
                            }
                        }
                        if (this.chkCustomer.Checked == true)
                        {
                            String parameterField = "@Customer";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                String value = String.Empty;
                                foreach (ListViewItem item in this.lstCustomer.CheckedItems)
                                {
                                    if (value == String.Empty)
                                    {
                                        value += item.SubItems[1].Text;
                                    }
                                    else
                                    {
                                        value += ", " + item.SubItems[1].Text;
                                    }
                                }
                                crystal.HastTable.Add(parameterField, value);
                            }
                        }
                        if (this.chkCategory.Checked == true)
                        {
                            String parameterField = "@Category";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                String value = String.Empty;
                                foreach (ListViewItem item in this.lstCategory.CheckedItems)
                                {
                                    if (value == String.Empty)
                                    {
                                        value += item.SubItems[1].Text;
                                    }
                                    else
                                    {
                                        value += ", " + item.SubItems[1].Text;
                                    }
                                }
                                crystal.HastTable.Add(parameterField, value);
                            }
                        }
                        if (this.chkSubCategory.Checked == true)
                        {
                            String parameterField = "@SubCategory";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                String value = String.Empty;
                                foreach (ListViewItem item in this.lstSubCategory.CheckedItems)
                                {
                                    if (value == String.Empty)
                                    {
                                        value += item.SubItems[1].Text;
                                    }
                                    else
                                    {
                                        value += ", " + item.SubItems[1].Text;
                                    }
                                }
                                crystal.HastTable.Add(parameterField, value);
                            }
                        }
                        if (this.chkStock.Checked == true)
                        {
                            String parameterField = "@Stock";
                            if (crystal.CheckParameterFieldExist(parameterField) == true)
                            {
                                String value = String.Empty;
                                foreach (ListViewItem item in this.lstStock.CheckedItems)
                                {
                                    if (value == String.Empty)
                                    {
                                        value += item.SubItems[3].Text;
                                    }
                                    else
                                    {
                                        value += ", " + item.SubItems[3].Text;
                                    }
                                }
                                crystal.HastTable.Add(parameterField, value);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

                private void ResetCheckBox()
                {
                    try
                    {
                        this.chkYear.Checked = false;
                        this.chkYearRange.Checked = false;
                        this.chkMonth.Checked = false;
                        this.chkMonthRange.Checked = false;
                        this.chkDate.Checked = false;
                        this.chkDateRange.Checked = false;
                        this.chkCustomer.Checked = false;
                        this.chkCategory.Checked = false;
                        this.chkStock.Checked = false;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void CloseForm(FormClosingEventArgs e)
                {
                    try
                    {
                        ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion
    }
}
