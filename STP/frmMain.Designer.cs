﻿namespace STP
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.stsMain = new System.Windows.Forms.StatusStrip();
            this.pgbLoading = new System.Windows.Forms.ToolStripProgressBar();
            this.tlsMain = new System.Windows.Forms.ToolStrip();
            this.mnsMain = new System.Windows.Forms.MenuStrip();
            this.stsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsMain
            // 
            this.stsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pgbLoading});
            this.stsMain.Location = new System.Drawing.Point(0, 650);
            this.stsMain.Name = "stsMain";
            this.stsMain.Size = new System.Drawing.Size(994, 22);
            this.stsMain.TabIndex = 1;
            this.stsMain.Text = "StatusStrip";
            // 
            // pgbLoading
            // 
            this.pgbLoading.Name = "pgbLoading";
            this.pgbLoading.Size = new System.Drawing.Size(200, 16);
            this.pgbLoading.Visible = false;
            // 
            // tlsMain
            // 
            this.tlsMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlsMain.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlsMain.Location = new System.Drawing.Point(0, 24);
            this.tlsMain.Name = "tlsMain";
            this.tlsMain.Size = new System.Drawing.Size(32, 626);
            this.tlsMain.TabIndex = 2;
            this.tlsMain.Text = "ToolStrip";
            // 
            // mnsMain
            // 
            this.mnsMain.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnsMain.Location = new System.Drawing.Point(0, 0);
            this.mnsMain.Name = "mnsMain";
            this.mnsMain.Size = new System.Drawing.Size(994, 24);
            this.mnsMain.TabIndex = 3;
            this.mnsMain.Text = "MenuStrip";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 672);
            this.Controls.Add(this.tlsMain);
            this.Controls.Add(this.stsMain);
            this.Controls.Add(this.mnsMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnsMain;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.stsMain.ResumeLayout(false);
            this.stsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.StatusStrip stsMain;
        public System.Windows.Forms.ToolStrip tlsMain;
        public System.Windows.Forms.MenuStrip mnsMain;
        public System.Windows.Forms.ToolStripProgressBar pgbLoading;
    }
}

