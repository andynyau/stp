﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace STP
{
    partial class frmAbout : Form
    {
        public frmAbout()
        {
            try
            {
                this.InitializeComponent();
                this.Text = String.Format("About {0}", AssemblyTitle);
                this.labelProductName.Text = AssemblyProduct.Replace("&", "&&");
                this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
                this.labelCopyright.Text = AssemblyCopyright;
                this.labelCompanyName.Text = System.Configuration.ConfigurationManager.AppSettings["SET:COMPANY"].ToString();//AssemblyCompany;
                this.textBoxDescription.Text = AssemblyDescription;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        #region Assembly Attribute Accessors

        public String AssemblyTitle
        {
            get
            {
                String result = String.Empty;
                try
                {
                    object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                    if (attributes.Length > 0)
                    {
                        AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                        if (titleAttribute.Title != "")
                        {
                            result = titleAttribute.Title;
                        }
                    }
                    else
                    {
                        result = System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        public String AssemblyVersion
        {
            get
            {
                String result = String.Empty;
                try
                {
                    result = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        public String AssemblyDescription
        {
            get
            {
                String result = String.Empty;
                try
                {
                    object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                    if (attributes.Length == 0)
                    {
                        result = String.Empty;
                    }
                    else
                    {
                        result = ((AssemblyDescriptionAttribute)attributes[0]).Description;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        public String AssemblyProduct
        {
            get
            {
                String result = String.Empty;
                try
                {
                    object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                    if (attributes.Length == 0)
                    {
                        result = String.Empty;
                    }
                    else
                    {
                        result = ((AssemblyProductAttribute)attributes[0]).Product;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        public String AssemblyCopyright
        {
            get
            {
                String result = String.Empty;
                try
                {
                    object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                    if (attributes.Length == 0)
                    {
                        result = String.Empty;
                    }
                    else
                    {
                        result = ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                String result = String.Empty;
                try
                {
                    object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                    if (attributes.Length == 0)
                    {
                        result = String.Empty;
                    }
                    else
                    {
                        result = ((AssemblyCompanyAttribute)attributes[0]).Company;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = String.Empty;
                }
                return result;
            }
        }

        #endregion
    }
}
