﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmCustomer : Form
    {
        #region Declarations

            private STP.Logic.General _Logic_General;
            private STP.Logic.Common.SaveType _SaveType;

            private int _SearchType;
            private String _Current_Sort;
            private ND.Standard.Validation.Winform.Form _ValidatorNew;
            private ND.Standard.Validation.Winform.Form _ValidatorEdit;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtCustomerName;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCustomerList;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmCustomer_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this.EditDeleteEnable(false);
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.SearchCustomer();
                        this.BuildAutoComplete();
                        this._SaveType = STP.Logic.Common.SaveType.None;
                        this.SetValidation();
                        this.lstCustomerListing.Focus();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void frmCustomer_FormClosing(object sender, FormClosingEventArgs e)
                {
                    try
                    {
                        this.CloseForm(e);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstCustomerListing_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.EditDeleteEnable(this.lstCustomerListing.SelectedItems.Count > 0);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomerListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, e.Column, this.ldm_BuildCustomerList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomerListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.ShowDetail(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomerListing_MouseDoubleClick(object sender, MouseEventArgs e)
                {
                    try
                    {
                        this.ShowDetail(true);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtCustomerName_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this._SearchType = 1;
                            //SearchCustomer();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, columnIndex, this.ldm_BuildCustomerList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnDelete_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SaveType = STP.Logic.Common.SaveType.Delete;
                        if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.lstCustomerListing.SelectedItems.Count > 0)
                            {
                                this.DeleteCustomer();
                                this._SearchType = 0;
                                if (this.txtCustomerSearch.Text.Trim() != String.Empty)
                                {
                                    this._SearchType = 1;
                                }
                                else
                                {
                                    this._SearchType = 0;
                                }
                                String[] sortInfo = null;
                                int columnIndex = 0;
                                if (this._Current_Sort != String.Empty)
                                {
                                    sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                    columnIndex = int.Parse(sortInfo[2]);
                                    if (sortInfo[1] == "ASC")
                                    {
                                        this._Current_Sort = String.Empty;
                                    }
                                    else
                                    {
                                        this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                    }
                                }
                                ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, columnIndex, this.ldm_BuildCustomerList);
                                //SearchCustomer();
                                this.BuildAutoComplete();
                            }
                        }
                        this.lstCustomerListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnEdit_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowDetail(true);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnAdd_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowDetail(false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SearchType = 1;
                        this.SearchCustomer();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSave_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.ValidateForm() == true)
                            {
                                if (this.SaveCustomer() == true)
                                {
                                    this.HideDetail();
                                    this._SearchType = 0;
                                    if (this.txtCustomerSearch.Text.Trim() != String.Empty)
                                    {
                                        this._SearchType = 1;
                                    }
                                    else
                                    {
                                        this._SearchType = 0;
                                    }
                                    String[] sortInfo = null;
                                    int columnIndex = 0;
                                    if (this._Current_Sort != String.Empty)
                                    {
                                        sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                        columnIndex = int.Parse(sortInfo[2]);
                                        if (sortInfo[1] == "ASC")
                                        {
                                            this._Current_Sort = String.Empty;
                                        }
                                        else
                                        {
                                            this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                        }
                                    }
                                    else
                                    {
                                        this._Current_Sort = this.lstCustomerListing.Columns[0].Name + "[..]DESC[..]" + this.lstCustomerListing.Columns[0].Index.ToString();
                                    }
                                    ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, columnIndex, this.ldm_BuildCustomerList);
                                    //SearchCustomer();
                                    this.BuildAutoComplete();
                                    this.lstCustomerListing.Focus();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnCancel_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.HideDetail();
                        this.lstCustomerListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Procedures and Functions

            #region Constructor

                public frmCustomer()
                {
                    try
                    {
                        this.InitializeComponent();
                        this.Text = "Customer";

                        this._Logic_General = new STP.Logic.General();

                        this._ValidatorNew = new ND.Standard.Validation.Winform.Form();
                        this._ValidatorEdit = new ND.Standard.Validation.Winform.Form();

                        this.ct_txtCustomerName = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtCustomerName_Validate);
                        this.ldm_BuildCustomerList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchCustomer);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Validations

                private Boolean ct_txtCustomerName_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        ND.Data.ConditionField conditionFieldCustomerName = new ND.Data.ConditionField();
                        conditionFieldCustomerName.Field = "customername";
                        conditionFieldCustomerName.Value = this.txtCustomerName.Text.Trim();
                        conditionFieldCustomerName.DataType = ND.Data.DataObject.DataType.NString;
                        conditionFieldCustomerName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                        conditions.Add(conditionFieldCustomerName);
                        result = !this._Logic_General.CheckCustomerExist(conditions);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void ShowDetail(Boolean isEdit)
                {
                    try
                    {
                        this.ClearField();
                        if (isEdit == true)
                        {
                            this._SaveType = STP.Logic.Common.SaveType.Update;
                            this.LoadCustomer();
                        }
                        else
                        {
                            this._SaveType = STP.Logic.Common.SaveType.Insert;
                        }
                        this.pnlDetail.Visible = true;
                        this.pnlDetail.BringToFront();
                        this.txtCustomerName.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void HideDetail()
                {
                    try
                    {
                        this.ClearField();
                        this._SaveType = STP.Logic.Common.SaveType.None;
                        this.pnlDetail.Visible = false;
                        this.pnlDetail.SendToBack();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildButton()
                {
                    try
                    {
                        this.btnSearch.Image = Image.FromFile("image/Search.png");
                        this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnAdd.Image = Image.FromFile("image/New.png");
                        this.btnAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnEdit.Image = Image.FromFile("image/Edit.png");
                        this.btnEdit.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnDelete.Image = Image.FromFile("image/Delete.png");
                        this.btnDelete.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnClose.Image = Image.FromFile("image/Cancel.png");
                        this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnSave.Image = Image.FromFile("image/Confirm.png");
                        this.btnSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnCancel.Image = Image.FromFile("image/Cancel.png");
                        this.btnCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void EditDeleteEnable(Boolean enable)
                {
                    try
                    {
                        this.btnEdit.Enabled = enable;
                        this.btnDelete.Enabled = enable;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SearchCustomer()
                {
                    try
                    {
                        this.SearchCustomer(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SearchCustomer(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstCustomerListing.Visible = false;
                        this.lstCustomerListing.Clear();
                        this.BuildCustomerListColumn();
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        switch (this._SearchType)
                        {
                            case 0:
                                {
                                    break;
                                }
                            case 1:
                                {
                                    ND.Data.ConditionField conditionFieldCustomerName = new ND.Data.ConditionField();
                                    conditionFieldCustomerName.Field = "customername";
                                    conditionFieldCustomerName.Value = "%" + this.txtCustomerSearch.Text + "%";
                                    conditionFieldCustomerName.DataType = ND.Data.DataObject.DataType.String;
                                    conditionFieldCustomerName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                    conditions.Add(conditionFieldCustomerName);
                                    break;
                                }
                        }
                        this._Logic_General.BuildCustomerList(this.lstCustomerListing, conditions, sortType, sortField);
                        this.FormatCustomerListColumn();
                        this.lstCustomerListing.Visible = true;
                        if (this.lstCustomerListing.Items.Count > 0)
                        {
                            this.lstCustomerListing.Items[0].Selected = true;
                            this.EditDeleteEnable(true);
                        }
                        else
                        {
                            this.EditDeleteEnable(false);
                        }
                        this.lstCustomerListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildCustomerListColumn()
                {
                    try
                    {
                        this.lstCustomerListing.Columns.Clear();
                        this.lstCustomerListing.Columns.Add("id", "ID");
                        this.lstCustomerListing.Columns.Add("customercode", "Code");
                        this.lstCustomerListing.Columns.Add("customername", "Name");
                        this.lstCustomerListing.Columns.Add("address1", "Address 1");
                        this.lstCustomerListing.Columns.Add("address2", "Address 2");
                        this.lstCustomerListing.Columns.Add("postcode", "Post Code");
                        this.lstCustomerListing.Columns.Add("city", "City");
                        this.lstCustomerListing.Columns.Add("area", "Area");
                        this.lstCustomerListing.Columns.Add("state", "State");
                        this.lstCustomerListing.Columns.Add("country", "Country");
                        this.lstCustomerListing.Columns.Add("tel", "Tel");
                        this.lstCustomerListing.Columns.Add("fax", "Fax");
                        this.lstCustomerListing.Columns.Add("contact", "Contact");
                        this.lstCustomerListing.Columns.Add("createddate", "Created Date");
                        this.lstCustomerListing.Columns.Add("flag", "Flag");
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatCustomerListColumn()
                {
                    try
                    {
                        this.lstCustomerListing.Columns["customername"].Width = 150;
                        this.lstCustomerListing.Columns["customername"].TextAlign = HorizontalAlignment.Left;
                        this.lstCustomerListing.Columns["city"].Width = 100;
                        this.lstCustomerListing.Columns["city"].TextAlign = HorizontalAlignment.Left;
                        this.lstCustomerListing.Columns["area"].Width = 100;
                        this.lstCustomerListing.Columns["area"].TextAlign = HorizontalAlignment.Left;
                        this.lstCustomerListing.Columns["tel"].Width = 70;
                        this.lstCustomerListing.Columns["tel"].TextAlign = HorizontalAlignment.Left;
                        this.lstCustomerListing.Columns["fax"].Width = 70;
                        this.lstCustomerListing.Columns["fax"].TextAlign = HorizontalAlignment.Left;
                        this.lstCustomerListing.Columns["contact"].Width = 70;
                        this.lstCustomerListing.Columns["contact"].TextAlign = HorizontalAlignment.Left;

                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["id"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["customercode"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["address1"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["address2"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["postcode"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["state"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["country"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["createddate"]);
                        this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildAutoComplete()
                {
                    try
                    {
                        this.txtCustomerSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        this.txtCustomerSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        this.txtCustomerSearch.AutoCompleteCustomSource = this._Logic_General.CustomerStringCollection();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._ValidatorNew.AddValidator(this.txtCustomerName, this.erpCustomer, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorNew.AddValidator(this.txtCustomerName, this.erpCustomer, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtCustomerName);

                        this._ValidatorEdit.AddValidator(this.txtCustomerName, this.erpCustomer, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void CloseForm(FormClosingEventArgs e)
                {
                    try
                    {
                        ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void DeleteCustomer()
                {
                    try
                    {
                        if (this._Logic_General.DeleteCustomer(int.Parse(this.lstCustomerListing.SelectedItems[0].SubItems[0].Text)) == true)
                        {
                            MessageBox.Show("Delete Success", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            MessageBox.Show("Delete Failed", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean ClearField()
                {
                    Boolean result = false;
                    try
                    {
                        this.txtCustomerID.Text = String.Empty;
                        this.txtCustomerCode.Text = String.Empty;
                        this.txtCustomerName.Text = String.Empty;
                        this.txtCustomerAddress1.Text = String.Empty;
                        this.txtCustomerAddress2.Text = String.Empty;
                        this.txtCustomerPostCode.Text = String.Empty;
                        this.txtCustomerCity.Text = String.Empty;
                        this.txtCustomerState.Text = String.Empty;
                        this.txtCustomerCountry.Text = String.Empty;
                        this.txtCustomerTelephone.Text = String.Empty;
                        this.txtCustomerFax.Text = String.Empty;
                        this.txtCustomerContactPerson.Text = String.Empty;
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private void LoadCustomer()
                {
                    try
                    {
                        STP.Base.Customer_Object objectCustomer = new STP.Base.Customer_Object();
                        if (this.lstCustomerListing.SelectedItems.Count > 0)
                        {
                            objectCustomer = this._Logic_General.GetCustomer(int.Parse(this.lstCustomerListing.SelectedItems[0].SubItems[0].Text));
                            if (objectCustomer != null)
                            {
                                this.txtCustomerID.Text = objectCustomer.ID.ToString();
                                this.txtCustomerCode.Text = objectCustomer.Customer_Code;
                                this.txtCustomerName.Text = objectCustomer.Customer_Name;
                                this.txtCustomerAddress1.Text = objectCustomer.Address_1;
                                this.txtCustomerAddress2.Text = objectCustomer.Address_2;
                                this.txtCustomerPostCode.Text = objectCustomer.Post_Code;
                                this.txtCustomerCity.Text = objectCustomer.City;
                                this.txtCustomerArea.Text = objectCustomer.Area;
                                this.txtCustomerState.Text = objectCustomer.State;
                                this.txtCustomerCountry.Text = objectCustomer.Country;
                                this.txtCustomerTelephone.Text = objectCustomer.Telephone;
                                this.txtCustomerFax.Text = objectCustomer.Fax;
                                this.txtCustomerContactPerson.Text = objectCustomer.Contact_Person;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean SaveCustomer()
                {
                    Boolean result = false;
                    try
                    {
                        STP.Base.Customer_Object objectCustomer = new STP.Base.Customer_Object();
                        objectCustomer.ID = this.txtCustomerID.Text.Trim() == String.Empty ? (int?)null : int.Parse(this.txtCustomerID.Text.Trim());
                        objectCustomer.Customer_Code = this.txtCustomerCode.Text.Trim();
                        objectCustomer.Customer_Name = this.txtCustomerName.Text.Trim();
                        objectCustomer.Address_1 = this.txtCustomerAddress1.Text.Trim();
                        objectCustomer.Address_2 = this.txtCustomerAddress2.Text.Trim();
                        objectCustomer.Post_Code = this.txtCustomerPostCode.Text.Trim();
                        objectCustomer.Area = this.txtCustomerArea.Text.Trim();
                        objectCustomer.City = this.txtCustomerCity.Text.Trim();
                        objectCustomer.State = this.txtCustomerState.Text.Trim();
                        objectCustomer.Country = this.txtCustomerCountry.Text.Trim();
                        objectCustomer.Telephone = this.txtCustomerTelephone.Text.Trim();
                        objectCustomer.Fax = this.txtCustomerFax.Text.Trim();
                        objectCustomer.Contact_Person = this.txtCustomerContactPerson.Text.Trim();

                        if (this._Logic_General.SaveCustomer(objectCustomer, this._SaveType) == true)
                        {
                            switch (this._SaveType)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        result = true;
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        result = true;
                                        break;
                                    }
                            }
                        }
                        else
                        {
                            switch (this._SaveType)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        result = false;
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                        result = false;
                                        break;
                                    }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ValidateForm()
                {
                    Boolean result = false;
                    try
                    {
                        switch (this._SaveType)
                        {
                            case STP.Logic.Common.SaveType.Insert:
                                {
                                    result = this._ValidatorNew.Validate();
                                    break;
                                }
                            case STP.Logic.Common.SaveType.Update:
                                {
                                    result = this._ValidatorEdit.Validate();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

        #endregion
    }
}
