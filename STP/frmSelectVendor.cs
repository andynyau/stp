﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmSelectVendor : Form
    {
        #region Declarations

            private STP.Logic.General _Logic_General;

            private int _SearchType;
            private String _Current_Sort;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildVendorList;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmSelectVendor_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.SearchVendor();
                        this.BuildAutoComplete();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstVendorListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, e.Column, this.ldm_BuildVendorList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstVendorListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.SelectVendor();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstVendorListing_MouseDoubleClick(object sender, MouseEventArgs e)
                {
                    try
                    {
                        this.SelectVendor();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtVendorSearch_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this._SearchType = 1;
                            //this.SearchVendor();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstVendorListing, ref this._Current_Sort, columnIndex, this.ldm_BuildVendorList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SearchType = 1;
                        this.SearchVendor();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSelect_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.SelectVendor();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Functions and Procedures

            public frmSelectVendor()
            {
                try
                {
                    this.InitializeComponent();
                    this.Text = "Select Vendor";
                    this._Logic_General = new STP.Logic.General();
                    this.ldm_BuildVendorList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchVendor);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildButton()
            {
                try
                {
                    this.btnSearch.Image = Image.FromFile("image/Search.png");
                    this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnSelect.Image = Image.FromFile("image/Confirm.png");
                    this.btnSelect.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnClose.Image = Image.FromFile("image/Cancel.png");
                    this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchVendor()
            {
                try
                {
                    this.SearchVendor(ND.Data.DataObject.SortType.None, String.Empty);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchVendor(ND.Data.DataObject.SortType sortType, String sortField)
            {
                try
                {
                    this.lstVendorListing.Visible = false;
                    this.lstVendorListing.Clear();
                    this.BuildVendorListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    switch (this._SearchType)
                    {
                        case 0:
                            {
                                break;
                            }
                        case 1:
                            {
                                ND.Data.ConditionField conditionFieldVendorName = new ND.Data.ConditionField();
                                conditionFieldVendorName.Field = "Vendorname";
                                conditionFieldVendorName.Value = "%" + this.txtVendorSearch.Text + "%";
                                conditionFieldVendorName.DataType = ND.Data.DataObject.DataType.String;
                                conditionFieldVendorName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                conditions.Add(conditionFieldVendorName);
                                break;
                            }
                    }
                    this._Logic_General.BuildVendorList(this.lstVendorListing, conditions, sortType, sortField);
                    this.FormatVendorListColumn();
                    this.lstVendorListing.Visible = true;
                    this.lstVendorListing.Focus();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildVendorListColumn()
            {
                try
                {
                    this.lstVendorListing.Columns.Clear();
                    this.lstVendorListing.Columns.Add("id", "ID");
                    this.lstVendorListing.Columns.Add("Vendorcode", "Code");
                    this.lstVendorListing.Columns.Add("Vendorname", "Name");
                    this.lstVendorListing.Columns.Add("address1", "Address 1");
                    this.lstVendorListing.Columns.Add("address2", "Address 2");
                    this.lstVendorListing.Columns.Add("postcode", "Post Code");
                    this.lstVendorListing.Columns.Add("city", "City");
                    this.lstVendorListing.Columns.Add("area", "Area");
                    this.lstVendorListing.Columns.Add("state", "State");
                    this.lstVendorListing.Columns.Add("country", "Country");
                    this.lstVendorListing.Columns.Add("tel", "Tel");
                    this.lstVendorListing.Columns.Add("fax", "Fax");
                    this.lstVendorListing.Columns.Add("contact", "Contact");
                    this.lstVendorListing.Columns.Add("createddate", "Created Date");
                    this.lstVendorListing.Columns.Add("flag", "Flag");
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void FormatVendorListColumn()
            {
                try
                {
                    this.lstVendorListing.Columns["Vendorname"].Width = 150;
                    this.lstVendorListing.Columns["Vendorname"].TextAlign = HorizontalAlignment.Left;
                    this.lstVendorListing.Columns["city"].Width = 100;
                    this.lstVendorListing.Columns["city"].TextAlign = HorizontalAlignment.Left;
                    this.lstVendorListing.Columns["area"].Width = 100;
                    this.lstVendorListing.Columns["area"].TextAlign = HorizontalAlignment.Left;
                    this.lstVendorListing.Columns["tel"].Width = 70;
                    this.lstVendorListing.Columns["tel"].TextAlign = HorizontalAlignment.Left;
                    this.lstVendorListing.Columns["fax"].Width = 70;
                    this.lstVendorListing.Columns["fax"].TextAlign = HorizontalAlignment.Left;
                    this.lstVendorListing.Columns["contact"].Width = 70;
                    this.lstVendorListing.Columns["contact"].TextAlign = HorizontalAlignment.Left;

                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["id"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["Vendorcode"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["address1"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["address2"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["postcode"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["state"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["country"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["createddate"]);
                    this.lstVendorListing.Columns.Remove(this.lstVendorListing.Columns["flag"]);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildAutoComplete()
            {
                try
                {
                    this.txtVendorSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtVendorSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtVendorSearch.AutoCompleteCustomSource = this._Logic_General.VendorStringCollection();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SelectVendor()
            {
                try
                {
                    if (this.lstVendorListing.SelectedItems.Count > 0)
                    {
                        if (this.Owner.GetType() == typeof(frmPurchase))
                        {
                            frmPurchase form = (frmPurchase)this.Owner;
                            form.txtVendorID.Text = String.Empty;
                            form.txtVendorID.Text = this.lstVendorListing.SelectedItems[0].SubItems[0].Text;
                            form.txtBillNo.Focus();
                        }
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

        #endregion
    }
}
