﻿namespace STP
{
    partial class frmReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.pnlReport = new System.Windows.Forms.Panel();
            this.grpParameter = new System.Windows.Forms.GroupBox();
            this.pnlStock = new System.Windows.Forms.Panel();
            this.chkStock = new System.Windows.Forms.CheckBox();
            this.lstStock = new System.Windows.Forms.ListView();
            this.lblStock = new System.Windows.Forms.Label();
            this.pnlCustomer = new System.Windows.Forms.Panel();
            this.chkCustomer = new System.Windows.Forms.CheckBox();
            this.lstCustomer = new System.Windows.Forms.ListView();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.pnlCategory = new System.Windows.Forms.Panel();
            this.chkCategory = new System.Windows.Forms.CheckBox();
            this.lstCategory = new System.Windows.Forms.ListView();
            this.lblCategory = new System.Windows.Forms.Label();
            this.pnlDateRange = new System.Windows.Forms.Panel();
            this.chkDateRange = new System.Windows.Forms.CheckBox();
            this.dtpDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateFrom = new System.Windows.Forms.Label();
            this.pnlDate = new System.Windows.Forms.Panel();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.pnlMonthRange = new System.Windows.Forms.Panel();
            this.chkMonthRange = new System.Windows.Forms.CheckBox();
            this.cmbMonthTo = new System.Windows.Forms.ComboBox();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.cmbMonthFrom = new System.Windows.Forms.ComboBox();
            this.lblMonthFrom = new System.Windows.Forms.Label();
            this.pnlMonth = new System.Windows.Forms.Panel();
            this.chkMonth = new System.Windows.Forms.CheckBox();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.pnlYearRange = new System.Windows.Forms.Panel();
            this.chkYearRange = new System.Windows.Forms.CheckBox();
            this.cmbYearTo = new System.Windows.Forms.ComboBox();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.cmbYearFrom = new System.Windows.Forms.ComboBox();
            this.lblYearFrom = new System.Windows.Forms.Label();
            this.pnlYear = new System.Windows.Forms.Panel();
            this.chkYear = new System.Windows.Forms.CheckBox();
            this.cmbYear = new System.Windows.Forms.ComboBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.lstReport = new System.Windows.Forms.ListView();
            this.erpReport = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlSubCategory = new System.Windows.Forms.Panel();
            this.chkSubCategory = new System.Windows.Forms.CheckBox();
            this.lstSubCategory = new System.Windows.Forms.ListView();
            this.lblSubCategory = new System.Windows.Forms.Label();
            this.gpbControlListing.SuspendLayout();
            this.pnlReport.SuspendLayout();
            this.grpParameter.SuspendLayout();
            this.pnlStock.SuspendLayout();
            this.pnlCustomer.SuspendLayout();
            this.pnlCategory.SuspendLayout();
            this.pnlDateRange.SuspendLayout();
            this.pnlDate.SuspendLayout();
            this.pnlMonthRange.SuspendLayout();
            this.pnlMonth.SuspendLayout();
            this.pnlYearRange.SuspendLayout();
            this.pnlYear.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpReport)).BeginInit();
            this.pnlSubCategory.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnGenerate);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 532);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 7;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerate.Location = new System.Drawing.Point(596, 21);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(120, 43);
            this.btnGenerate.TabIndex = 6;
            this.btnGenerate.Text = "&Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // pnlReport
            // 
            this.pnlReport.Controls.Add(this.grpParameter);
            this.pnlReport.Controls.Add(this.lstReport);
            this.pnlReport.Controls.Add(this.gpbControlListing);
            this.pnlReport.Location = new System.Drawing.Point(0, 0);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(888, 630);
            this.pnlReport.TabIndex = 8;
            // 
            // grpParameter
            // 
            this.grpParameter.Controls.Add(this.pnlStock);
            this.grpParameter.Controls.Add(this.pnlSubCategory);
            this.grpParameter.Controls.Add(this.pnlCustomer);
            this.grpParameter.Controls.Add(this.pnlCategory);
            this.grpParameter.Controls.Add(this.pnlDateRange);
            this.grpParameter.Controls.Add(this.pnlDate);
            this.grpParameter.Controls.Add(this.pnlMonthRange);
            this.grpParameter.Controls.Add(this.pnlMonth);
            this.grpParameter.Controls.Add(this.pnlYearRange);
            this.grpParameter.Controls.Add(this.pnlYear);
            this.grpParameter.Location = new System.Drawing.Point(172, 12);
            this.grpParameter.Name = "grpParameter";
            this.grpParameter.Size = new System.Drawing.Size(682, 514);
            this.grpParameter.TabIndex = 9;
            this.grpParameter.TabStop = false;
            // 
            // pnlStock
            // 
            this.pnlStock.Controls.Add(this.chkStock);
            this.pnlStock.Controls.Add(this.lstStock);
            this.pnlStock.Controls.Add(this.lblStock);
            this.pnlStock.Location = new System.Drawing.Point(6, 307);
            this.pnlStock.Name = "pnlStock";
            this.pnlStock.Size = new System.Drawing.Size(640, 150);
            this.pnlStock.TabIndex = 9;
            // 
            // chkStock
            // 
            this.chkStock.AutoSize = true;
            this.chkStock.Location = new System.Drawing.Point(20, 6);
            this.chkStock.Name = "chkStock";
            this.chkStock.Size = new System.Drawing.Size(15, 14);
            this.chkStock.TabIndex = 3;
            this.chkStock.UseVisualStyleBackColor = true;
            this.chkStock.CheckedChanged += new System.EventHandler(this.chkStock_CheckedChanged);
            // 
            // lstStock
            // 
            this.lstStock.CheckBoxes = true;
            this.lstStock.Enabled = false;
            this.lstStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstStock.FullRowSelect = true;
            this.lstStock.GridLines = true;
            this.lstStock.HideSelection = false;
            this.lstStock.Location = new System.Drawing.Point(140, 6);
            this.lstStock.Name = "lstStock";
            this.lstStock.Size = new System.Drawing.Size(300, 130);
            this.lstStock.TabIndex = 2;
            this.lstStock.UseCompatibleStateImageBehavior = false;
            this.lstStock.View = System.Windows.Forms.View.Details;
            this.lstStock.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstStock_ColumnClick);
            // 
            // lblStock
            // 
            this.lblStock.AutoSize = true;
            this.lblStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStock.Location = new System.Drawing.Point(50, 6);
            this.lblStock.Name = "lblStock";
            this.lblStock.Size = new System.Drawing.Size(42, 16);
            this.lblStock.TabIndex = 1;
            this.lblStock.Text = "Stock";
            // 
            // pnlCustomer
            // 
            this.pnlCustomer.Controls.Add(this.chkCustomer);
            this.pnlCustomer.Controls.Add(this.lstCustomer);
            this.pnlCustomer.Controls.Add(this.lblCustomer);
            this.pnlCustomer.Location = new System.Drawing.Point(6, 241);
            this.pnlCustomer.Name = "pnlCustomer";
            this.pnlCustomer.Size = new System.Drawing.Size(640, 100);
            this.pnlCustomer.TabIndex = 8;
            // 
            // chkCustomer
            // 
            this.chkCustomer.AutoSize = true;
            this.chkCustomer.Location = new System.Drawing.Point(20, 6);
            this.chkCustomer.Name = "chkCustomer";
            this.chkCustomer.Size = new System.Drawing.Size(15, 14);
            this.chkCustomer.TabIndex = 3;
            this.chkCustomer.UseVisualStyleBackColor = true;
            this.chkCustomer.CheckedChanged += new System.EventHandler(this.chkCustomer_CheckedChanged);
            // 
            // lstCustomer
            // 
            this.lstCustomer.CheckBoxes = true;
            this.lstCustomer.Enabled = false;
            this.lstCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCustomer.FullRowSelect = true;
            this.lstCustomer.GridLines = true;
            this.lstCustomer.HideSelection = false;
            this.lstCustomer.Location = new System.Drawing.Point(140, 6);
            this.lstCustomer.Name = "lstCustomer";
            this.lstCustomer.Size = new System.Drawing.Size(300, 80);
            this.lstCustomer.TabIndex = 2;
            this.lstCustomer.UseCompatibleStateImageBehavior = false;
            this.lstCustomer.View = System.Windows.Forms.View.Details;
            this.lstCustomer.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCustomer_ColumnClick);
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(50, 6);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(65, 16);
            this.lblCustomer.TabIndex = 1;
            this.lblCustomer.Text = "Customer";
            // 
            // pnlCategory
            // 
            this.pnlCategory.Controls.Add(this.chkCategory);
            this.pnlCategory.Controls.Add(this.lstCategory);
            this.pnlCategory.Controls.Add(this.lblCategory);
            this.pnlCategory.Location = new System.Drawing.Point(6, 208);
            this.pnlCategory.Name = "pnlCategory";
            this.pnlCategory.Size = new System.Drawing.Size(640, 100);
            this.pnlCategory.TabIndex = 6;
            // 
            // chkCategory
            // 
            this.chkCategory.AutoSize = true;
            this.chkCategory.Location = new System.Drawing.Point(20, 6);
            this.chkCategory.Name = "chkCategory";
            this.chkCategory.Size = new System.Drawing.Size(15, 14);
            this.chkCategory.TabIndex = 3;
            this.chkCategory.UseVisualStyleBackColor = true;
            this.chkCategory.CheckedChanged += new System.EventHandler(this.chkCategory_CheckedChanged);
            // 
            // lstCategory
            // 
            this.lstCategory.CheckBoxes = true;
            this.lstCategory.Enabled = false;
            this.lstCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCategory.FullRowSelect = true;
            this.lstCategory.GridLines = true;
            this.lstCategory.HideSelection = false;
            this.lstCategory.Location = new System.Drawing.Point(140, 6);
            this.lstCategory.Name = "lstCategory";
            this.lstCategory.Size = new System.Drawing.Size(300, 80);
            this.lstCategory.TabIndex = 2;
            this.lstCategory.UseCompatibleStateImageBehavior = false;
            this.lstCategory.View = System.Windows.Forms.View.Details;
            this.lstCategory.SelectedIndexChanged += new System.EventHandler(this.lstCategory_SelectedIndexChanged);
            this.lstCategory.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCategory_ColumnClick);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(50, 6);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(63, 16);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Category";
            // 
            // pnlDateRange
            // 
            this.pnlDateRange.Controls.Add(this.chkDateRange);
            this.pnlDateRange.Controls.Add(this.dtpDateTo);
            this.pnlDateRange.Controls.Add(this.dtpDateFrom);
            this.pnlDateRange.Controls.Add(this.lblDateTo);
            this.pnlDateRange.Controls.Add(this.lblDateFrom);
            this.pnlDateRange.Location = new System.Drawing.Point(6, 175);
            this.pnlDateRange.Name = "pnlDateRange";
            this.pnlDateRange.Size = new System.Drawing.Size(640, 33);
            this.pnlDateRange.TabIndex = 5;
            // 
            // chkDateRange
            // 
            this.chkDateRange.AutoSize = true;
            this.chkDateRange.Location = new System.Drawing.Point(20, 6);
            this.chkDateRange.Name = "chkDateRange";
            this.chkDateRange.Size = new System.Drawing.Size(15, 14);
            this.chkDateRange.TabIndex = 54;
            this.chkDateRange.UseVisualStyleBackColor = true;
            this.chkDateRange.CheckedChanged += new System.EventHandler(this.chkDateRange_CheckedChanged);
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpDateTo.Enabled = false;
            this.dtpDateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTo.Location = new System.Drawing.Point(440, 6);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.Size = new System.Drawing.Size(150, 22);
            this.dtpDateTo.TabIndex = 53;
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpDateFrom.Enabled = false;
            this.dtpDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateFrom.Location = new System.Drawing.Point(140, 6);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(150, 22);
            this.dtpDateFrom.TabIndex = 52;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateTo.Location = new System.Drawing.Point(350, 6);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(57, 16);
            this.lblDateTo.TabIndex = 2;
            this.lblDateTo.Text = "Date To";
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.AutoSize = true;
            this.lblDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateFrom.Location = new System.Drawing.Point(50, 6);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Size = new System.Drawing.Size(71, 16);
            this.lblDateFrom.TabIndex = 0;
            this.lblDateFrom.Text = "Date From";
            // 
            // pnlDate
            // 
            this.pnlDate.Controls.Add(this.chkDate);
            this.pnlDate.Controls.Add(this.dtpDate);
            this.pnlDate.Controls.Add(this.lblDate);
            this.pnlDate.Location = new System.Drawing.Point(6, 142);
            this.pnlDate.Name = "pnlDate";
            this.pnlDate.Size = new System.Drawing.Size(640, 33);
            this.pnlDate.TabIndex = 4;
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Location = new System.Drawing.Point(20, 6);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(15, 14);
            this.chkDate.TabIndex = 52;
            this.chkDate.UseVisualStyleBackColor = true;
            this.chkDate.CheckedChanged += new System.EventHandler(this.chkDate_CheckedChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpDate.Enabled = false;
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(140, 6);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(150, 22);
            this.dtpDate.TabIndex = 51;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(50, 6);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(37, 16);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Date";
            // 
            // pnlMonthRange
            // 
            this.pnlMonthRange.Controls.Add(this.chkMonthRange);
            this.pnlMonthRange.Controls.Add(this.cmbMonthTo);
            this.pnlMonthRange.Controls.Add(this.lblMonthTo);
            this.pnlMonthRange.Controls.Add(this.cmbMonthFrom);
            this.pnlMonthRange.Controls.Add(this.lblMonthFrom);
            this.pnlMonthRange.Location = new System.Drawing.Point(6, 109);
            this.pnlMonthRange.Name = "pnlMonthRange";
            this.pnlMonthRange.Size = new System.Drawing.Size(640, 33);
            this.pnlMonthRange.TabIndex = 3;
            // 
            // chkMonthRange
            // 
            this.chkMonthRange.AutoSize = true;
            this.chkMonthRange.Location = new System.Drawing.Point(20, 6);
            this.chkMonthRange.Name = "chkMonthRange";
            this.chkMonthRange.Size = new System.Drawing.Size(15, 14);
            this.chkMonthRange.TabIndex = 4;
            this.chkMonthRange.UseVisualStyleBackColor = true;
            this.chkMonthRange.CheckedChanged += new System.EventHandler(this.chkMonthRange_CheckedChanged);
            // 
            // cmbMonthTo
            // 
            this.cmbMonthTo.Enabled = false;
            this.cmbMonthTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMonthTo.FormattingEnabled = true;
            this.cmbMonthTo.Location = new System.Drawing.Point(440, 6);
            this.cmbMonthTo.Name = "cmbMonthTo";
            this.cmbMonthTo.Size = new System.Drawing.Size(150, 24);
            this.cmbMonthTo.TabIndex = 3;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.AutoSize = true;
            this.lblMonthTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthTo.Location = new System.Drawing.Point(350, 6);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(64, 16);
            this.lblMonthTo.TabIndex = 2;
            this.lblMonthTo.Text = "Month To";
            // 
            // cmbMonthFrom
            // 
            this.cmbMonthFrom.Enabled = false;
            this.cmbMonthFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMonthFrom.FormattingEnabled = true;
            this.cmbMonthFrom.Location = new System.Drawing.Point(140, 6);
            this.cmbMonthFrom.Name = "cmbMonthFrom";
            this.cmbMonthFrom.Size = new System.Drawing.Size(150, 24);
            this.cmbMonthFrom.TabIndex = 1;
            // 
            // lblMonthFrom
            // 
            this.lblMonthFrom.AutoSize = true;
            this.lblMonthFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonthFrom.Location = new System.Drawing.Point(50, 6);
            this.lblMonthFrom.Name = "lblMonthFrom";
            this.lblMonthFrom.Size = new System.Drawing.Size(78, 16);
            this.lblMonthFrom.TabIndex = 0;
            this.lblMonthFrom.Text = "Month From";
            // 
            // pnlMonth
            // 
            this.pnlMonth.Controls.Add(this.chkMonth);
            this.pnlMonth.Controls.Add(this.cmbMonth);
            this.pnlMonth.Controls.Add(this.lblMonth);
            this.pnlMonth.Location = new System.Drawing.Point(6, 76);
            this.pnlMonth.Name = "pnlMonth";
            this.pnlMonth.Size = new System.Drawing.Size(640, 33);
            this.pnlMonth.TabIndex = 2;
            // 
            // chkMonth
            // 
            this.chkMonth.AutoSize = true;
            this.chkMonth.Location = new System.Drawing.Point(20, 6);
            this.chkMonth.Name = "chkMonth";
            this.chkMonth.Size = new System.Drawing.Size(15, 14);
            this.chkMonth.TabIndex = 3;
            this.chkMonth.UseVisualStyleBackColor = true;
            this.chkMonth.CheckedChanged += new System.EventHandler(this.chkMonth_CheckedChanged);
            // 
            // cmbMonth
            // 
            this.cmbMonth.Enabled = false;
            this.cmbMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Location = new System.Drawing.Point(140, 6);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(150, 24);
            this.cmbMonth.TabIndex = 1;
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonth.Location = new System.Drawing.Point(50, 6);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(44, 16);
            this.lblMonth.TabIndex = 0;
            this.lblMonth.Text = "Month";
            // 
            // pnlYearRange
            // 
            this.pnlYearRange.Controls.Add(this.chkYearRange);
            this.pnlYearRange.Controls.Add(this.cmbYearTo);
            this.pnlYearRange.Controls.Add(this.lblYearTo);
            this.pnlYearRange.Controls.Add(this.cmbYearFrom);
            this.pnlYearRange.Controls.Add(this.lblYearFrom);
            this.pnlYearRange.Location = new System.Drawing.Point(6, 43);
            this.pnlYearRange.Name = "pnlYearRange";
            this.pnlYearRange.Size = new System.Drawing.Size(640, 33);
            this.pnlYearRange.TabIndex = 1;
            // 
            // chkYearRange
            // 
            this.chkYearRange.AutoSize = true;
            this.chkYearRange.Location = new System.Drawing.Point(20, 6);
            this.chkYearRange.Name = "chkYearRange";
            this.chkYearRange.Size = new System.Drawing.Size(15, 14);
            this.chkYearRange.TabIndex = 4;
            this.chkYearRange.UseVisualStyleBackColor = true;
            this.chkYearRange.CheckedChanged += new System.EventHandler(this.chkYearRange_CheckedChanged);
            // 
            // cmbYearTo
            // 
            this.cmbYearTo.Enabled = false;
            this.cmbYearTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbYearTo.FormattingEnabled = true;
            this.cmbYearTo.Location = new System.Drawing.Point(440, 6);
            this.cmbYearTo.Name = "cmbYearTo";
            this.cmbYearTo.Size = new System.Drawing.Size(150, 24);
            this.cmbYearTo.TabIndex = 3;
            // 
            // lblYearTo
            // 
            this.lblYearTo.AutoSize = true;
            this.lblYearTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYearTo.Location = new System.Drawing.Point(350, 6);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(57, 16);
            this.lblYearTo.TabIndex = 2;
            this.lblYearTo.Text = "Year To";
            // 
            // cmbYearFrom
            // 
            this.cmbYearFrom.Enabled = false;
            this.cmbYearFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbYearFrom.FormattingEnabled = true;
            this.cmbYearFrom.Location = new System.Drawing.Point(140, 6);
            this.cmbYearFrom.Name = "cmbYearFrom";
            this.cmbYearFrom.Size = new System.Drawing.Size(150, 24);
            this.cmbYearFrom.TabIndex = 1;
            // 
            // lblYearFrom
            // 
            this.lblYearFrom.AutoSize = true;
            this.lblYearFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYearFrom.Location = new System.Drawing.Point(50, 6);
            this.lblYearFrom.Name = "lblYearFrom";
            this.lblYearFrom.Size = new System.Drawing.Size(71, 16);
            this.lblYearFrom.TabIndex = 0;
            this.lblYearFrom.Text = "Year From";
            // 
            // pnlYear
            // 
            this.pnlYear.Controls.Add(this.chkYear);
            this.pnlYear.Controls.Add(this.cmbYear);
            this.pnlYear.Controls.Add(this.lblYear);
            this.pnlYear.Location = new System.Drawing.Point(6, 10);
            this.pnlYear.Name = "pnlYear";
            this.pnlYear.Size = new System.Drawing.Size(640, 33);
            this.pnlYear.TabIndex = 0;
            // 
            // chkYear
            // 
            this.chkYear.AutoSize = true;
            this.chkYear.Location = new System.Drawing.Point(20, 6);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(15, 14);
            this.chkYear.TabIndex = 2;
            this.chkYear.UseVisualStyleBackColor = true;
            this.chkYear.CheckedChanged += new System.EventHandler(this.chkYear_CheckedChanged);
            // 
            // cmbYear
            // 
            this.cmbYear.Enabled = false;
            this.cmbYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.Location = new System.Drawing.Point(140, 6);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(150, 24);
            this.cmbYear.TabIndex = 1;
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYear.Location = new System.Drawing.Point(50, 6);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(37, 16);
            this.lblYear.TabIndex = 0;
            this.lblYear.Text = "Year";
            // 
            // lstReport
            // 
            this.lstReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReport.FullRowSelect = true;
            this.lstReport.GridLines = true;
            this.lstReport.HideSelection = false;
            this.lstReport.Location = new System.Drawing.Point(11, 12);
            this.lstReport.MultiSelect = false;
            this.lstReport.Name = "lstReport";
            this.lstReport.Size = new System.Drawing.Size(155, 514);
            this.lstReport.TabIndex = 8;
            this.lstReport.UseCompatibleStateImageBehavior = false;
            this.lstReport.View = System.Windows.Forms.View.Details;
            this.lstReport.SelectedIndexChanged += new System.EventHandler(this.lstReport_SelectedIndexChanged);
            this.lstReport.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstReport_ColumnClick);
            // 
            // erpReport
            // 
            this.erpReport.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpReport.ContainerControl = this;
            // 
            // pnlSubCategory
            // 
            this.pnlSubCategory.Controls.Add(this.chkSubCategory);
            this.pnlSubCategory.Controls.Add(this.lstSubCategory);
            this.pnlSubCategory.Controls.Add(this.lblSubCategory);
            this.pnlSubCategory.Location = new System.Drawing.Point(6, 274);
            this.pnlSubCategory.Name = "pnlSubCategory";
            this.pnlSubCategory.Size = new System.Drawing.Size(640, 100);
            this.pnlSubCategory.TabIndex = 10;
            // 
            // chkSubCategory
            // 
            this.chkSubCategory.AutoSize = true;
            this.chkSubCategory.Location = new System.Drawing.Point(20, 6);
            this.chkSubCategory.Name = "chkSubCategory";
            this.chkSubCategory.Size = new System.Drawing.Size(15, 14);
            this.chkSubCategory.TabIndex = 3;
            this.chkSubCategory.UseVisualStyleBackColor = true;
            this.chkSubCategory.CheckedChanged += new System.EventHandler(this.chkSubCategory_CheckedChanged);
            // 
            // lstSubCategory
            // 
            this.lstSubCategory.CheckBoxes = true;
            this.lstSubCategory.Enabled = false;
            this.lstSubCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSubCategory.FullRowSelect = true;
            this.lstSubCategory.GridLines = true;
            this.lstSubCategory.HideSelection = false;
            this.lstSubCategory.Location = new System.Drawing.Point(140, 6);
            this.lstSubCategory.Name = "lstSubCategory";
            this.lstSubCategory.Size = new System.Drawing.Size(300, 80);
            this.lstSubCategory.TabIndex = 2;
            this.lstSubCategory.UseCompatibleStateImageBehavior = false;
            this.lstSubCategory.View = System.Windows.Forms.View.Details;
            this.lstSubCategory.SelectedIndexChanged += new System.EventHandler(this.lstSubCategory_SelectedIndexChanged);
            // 
            // lblSubCategory
            // 
            this.lblSubCategory.AutoSize = true;
            this.lblSubCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubCategory.Location = new System.Drawing.Point(50, 6);
            this.lblSubCategory.Name = "lblSubCategory";
            this.lblSubCategory.Size = new System.Drawing.Size(90, 16);
            this.lblSubCategory.TabIndex = 1;
            this.lblSubCategory.Text = "Sub Category";
            // 
            // frmReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 614);
            this.ControlBox = false;
            this.Controls.Add(this.pnlReport);
            this.Name = "frmReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmReport_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmReport_FormClosing);
            this.gpbControlListing.ResumeLayout(false);
            this.pnlReport.ResumeLayout(false);
            this.grpParameter.ResumeLayout(false);
            this.pnlStock.ResumeLayout(false);
            this.pnlStock.PerformLayout();
            this.pnlCustomer.ResumeLayout(false);
            this.pnlCustomer.PerformLayout();
            this.pnlCategory.ResumeLayout(false);
            this.pnlCategory.PerformLayout();
            this.pnlDateRange.ResumeLayout(false);
            this.pnlDateRange.PerformLayout();
            this.pnlDate.ResumeLayout(false);
            this.pnlDate.PerformLayout();
            this.pnlMonthRange.ResumeLayout(false);
            this.pnlMonthRange.PerformLayout();
            this.pnlMonth.ResumeLayout(false);
            this.pnlMonth.PerformLayout();
            this.pnlYearRange.ResumeLayout(false);
            this.pnlYearRange.PerformLayout();
            this.pnlYear.ResumeLayout(false);
            this.pnlYear.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpReport)).EndInit();
            this.pnlSubCategory.ResumeLayout(false);
            this.pnlSubCategory.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnGenerate;
        public System.Windows.Forms.Panel pnlReport;
        public System.Windows.Forms.GroupBox grpParameter;
        public System.Windows.Forms.ListView lstReport;
        public System.Windows.Forms.Panel pnlYear;
        public System.Windows.Forms.ComboBox cmbYear;
        public System.Windows.Forms.Label lblYear;
        public System.Windows.Forms.Panel pnlYearRange;
        public System.Windows.Forms.ComboBox cmbYearFrom;
        public System.Windows.Forms.Label lblYearFrom;
        public System.Windows.Forms.ComboBox cmbYearTo;
        public System.Windows.Forms.Label lblYearTo;
        public System.Windows.Forms.Panel pnlMonthRange;
        public System.Windows.Forms.ComboBox cmbMonthTo;
        public System.Windows.Forms.Label lblMonthTo;
        public System.Windows.Forms.ComboBox cmbMonthFrom;
        public System.Windows.Forms.Label lblMonthFrom;
        public System.Windows.Forms.Panel pnlMonth;
        public System.Windows.Forms.ComboBox cmbMonth;
        public System.Windows.Forms.Label lblMonth;
        public System.Windows.Forms.Panel pnlDateRange;
        public System.Windows.Forms.Label lblDateTo;
        public System.Windows.Forms.Label lblDateFrom;
        public System.Windows.Forms.Panel pnlDate;
        public System.Windows.Forms.Label lblDate;
        public System.Windows.Forms.DateTimePicker dtpDate;
        public System.Windows.Forms.DateTimePicker dtpDateTo;
        public System.Windows.Forms.DateTimePicker dtpDateFrom;
        public System.Windows.Forms.Panel pnlCategory;
        public System.Windows.Forms.Label lblCategory;
        public System.Windows.Forms.ListView lstCategory;
        public System.Windows.Forms.Panel pnlCustomer;
        public System.Windows.Forms.ListView lstCustomer;
        public System.Windows.Forms.Label lblCustomer;
        public System.Windows.Forms.Panel pnlStock;
        public System.Windows.Forms.ListView lstStock;
        public System.Windows.Forms.Label lblStock;
        public System.Windows.Forms.CheckBox chkStock;
        public System.Windows.Forms.CheckBox chkCustomer;
        public System.Windows.Forms.CheckBox chkCategory;
        public System.Windows.Forms.CheckBox chkDateRange;
        public System.Windows.Forms.CheckBox chkDate;
        public System.Windows.Forms.CheckBox chkMonthRange;
        public System.Windows.Forms.CheckBox chkMonth;
        public System.Windows.Forms.CheckBox chkYearRange;
        public System.Windows.Forms.CheckBox chkYear;
        public System.Windows.Forms.ErrorProvider erpReport;
        public System.Windows.Forms.Panel pnlSubCategory;
        public System.Windows.Forms.CheckBox chkSubCategory;
        public System.Windows.Forms.ListView lstSubCategory;
        public System.Windows.Forms.Label lblSubCategory;
    }
}