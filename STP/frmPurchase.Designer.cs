﻿namespace STP
{
    partial class frmPurchase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.txtVendorID = new System.Windows.Forms.TextBox();
            this.btnSearchVendor = new System.Windows.Forms.Button();
            this.lblVendor = new System.Windows.Forms.Label();
            this.txtVendorCode = new System.Windows.Forms.TextBox();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.grpControl = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtTotalOutstanding = new System.Windows.Forms.TextBox();
            this.lblTotalOutstanding = new System.Windows.Forms.Label();
            this.lblTotalPaid = new System.Windows.Forms.Label();
            this.txtBillNo = new System.Windows.Forms.TextBox();
            this.txtTotalPaid = new System.Windows.Forms.TextBox();
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblTransactionDate = new System.Windows.Forms.Label();
            this.lblBillNo = new System.Windows.Forms.Label();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.lblTransactionNo = new System.Windows.Forms.Label();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpItems = new System.Windows.Forms.TabPage();
            this.txtSubCat = new System.Windows.Forms.TextBox();
            this.lblStockCode = new System.Windows.Forms.Label();
            this.txtStockCode = new System.Windows.Forms.TextBox();
            this.txtStockCategory = new System.Windows.Forms.TextBox();
            this.btnDiscount = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.txtStockID = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblUnit = new System.Windows.Forms.Label();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.txtStockName = new System.Windows.Forms.TextBox();
            this.lblAdjustmentStockName = new System.Windows.Forms.Label();
            this.btnSearchItem = new System.Windows.Forms.Button();
            this.lstTransactionListing = new System.Windows.Forms.ListView();
            this.tpPayments = new System.Windows.Forms.TabPage();
            this.lblPaymentDate = new System.Windows.Forms.Label();
            this.dtpPaymentDate = new System.Windows.Forms.DateTimePicker();
            this.btnPaymentAdd = new System.Windows.Forms.Button();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.txtPaymentAmount = new System.Windows.Forms.TextBox();
            this.lblPaymentAmount = new System.Windows.Forms.Label();
            this.txtPaymentReference = new System.Windows.Forms.TextBox();
            this.lblPaymentReference = new System.Windows.Forms.Label();
            this.btnPaymentRemove = new System.Windows.Forms.Button();
            this.lblPaymentType = new System.Windows.Forms.Label();
            this.lstPaymentListing = new System.Windows.Forms.ListView();
            this.txtTransactionNo = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.rbtEdit = new System.Windows.Forms.RadioButton();
            this.rbtNew = new System.Windows.Forms.RadioButton();
            this.lblRemark = new System.Windows.Forms.Label();
            this.erpPurchase = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlMain.SuspendLayout();
            this.grpControl.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tpItems.SuspendLayout();
            this.tpPayments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpPurchase)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.txtVendorID);
            this.pnlMain.Controls.Add(this.btnSearchVendor);
            this.pnlMain.Controls.Add(this.lblVendor);
            this.pnlMain.Controls.Add(this.txtVendorCode);
            this.pnlMain.Controls.Add(this.txtRemark);
            this.pnlMain.Controls.Add(this.txtVendorName);
            this.pnlMain.Controls.Add(this.grpControl);
            this.pnlMain.Controls.Add(this.txtTotalOutstanding);
            this.pnlMain.Controls.Add(this.lblTotalOutstanding);
            this.pnlMain.Controls.Add(this.lblTotalPaid);
            this.pnlMain.Controls.Add(this.txtBillNo);
            this.pnlMain.Controls.Add(this.txtTotalPaid);
            this.pnlMain.Controls.Add(this.lblTotalAmount);
            this.pnlMain.Controls.Add(this.txtTotalAmount);
            this.pnlMain.Controls.Add(this.lblTransactionDate);
            this.pnlMain.Controls.Add(this.lblBillNo);
            this.pnlMain.Controls.Add(this.dtpTransactionDate);
            this.pnlMain.Controls.Add(this.lblTransactionNo);
            this.pnlMain.Controls.Add(this.tabMain);
            this.pnlMain.Controls.Add(this.txtTransactionNo);
            this.pnlMain.Controls.Add(this.btnSearch);
            this.pnlMain.Controls.Add(this.rbtEdit);
            this.pnlMain.Controls.Add(this.rbtNew);
            this.pnlMain.Controls.Add(this.lblRemark);
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(888, 688);
            this.pnlMain.TabIndex = 35;
            // 
            // txtVendorID
            // 
            this.txtVendorID.Enabled = false;
            this.txtVendorID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorID.Location = new System.Drawing.Point(432, 116);
            this.txtVendorID.Name = "txtVendorID";
            this.txtVendorID.Size = new System.Drawing.Size(100, 22);
            this.txtVendorID.TabIndex = 54;
            this.txtVendorID.Visible = false;
            this.txtVendorID.TextChanged += new System.EventHandler(this.txtVendorID_TextChanged);
            // 
            // btnSearchVendor
            // 
            this.btnSearchVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchVendor.Location = new System.Drawing.Point(290, 57);
            this.btnSearchVendor.Name = "btnSearchVendor";
            this.btnSearchVendor.Size = new System.Drawing.Size(130, 25);
            this.btnSearchVendor.TabIndex = 40;
            this.btnSearchVendor.Text = "Select &Vendor";
            this.btnSearchVendor.UseVisualStyleBackColor = true;
            this.btnSearchVendor.Click += new System.EventHandler(this.btnSearchVendor_Click);
            // 
            // lblVendor
            // 
            this.lblVendor.AutoSize = true;
            this.lblVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendor.Location = new System.Drawing.Point(150, 40);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(52, 16);
            this.lblVendor.TabIndex = 53;
            this.lblVendor.Text = "Vendor";
            // 
            // txtVendorCode
            // 
            this.txtVendorCode.Enabled = false;
            this.txtVendorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorCode.Location = new System.Drawing.Point(546, 116);
            this.txtVendorCode.Name = "txtVendorCode";
            this.txtVendorCode.Size = new System.Drawing.Size(100, 22);
            this.txtVendorCode.TabIndex = 55;
            this.txtVendorCode.Visible = false;
            // 
            // txtRemark
            // 
            this.txtRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemark.Location = new System.Drawing.Point(10, 172);
            this.txtRemark.MaxLength = 255;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(692, 22);
            this.txtRemark.TabIndex = 57;
            // 
            // txtVendorName
            // 
            this.txtVendorName.Enabled = false;
            this.txtVendorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorName.Location = new System.Drawing.Point(150, 60);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(130, 22);
            this.txtVendorName.TabIndex = 38;
            // 
            // grpControl
            // 
            this.grpControl.Controls.Add(this.btnDelete);
            this.grpControl.Controls.Add(this.btnClose);
            this.grpControl.Controls.Add(this.btnSave);
            this.grpControl.Location = new System.Drawing.Point(6, 532);
            this.grpControl.Name = "grpControl";
            this.grpControl.Size = new System.Drawing.Size(848, 70);
            this.grpControl.TabIndex = 52;
            this.grpControl.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(11, 21);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 43);
            this.btnDelete.TabIndex = 29;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(596, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 43);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtTotalOutstanding
            // 
            this.txtTotalOutstanding.Enabled = false;
            this.txtTotalOutstanding.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalOutstanding.Location = new System.Drawing.Point(290, 116);
            this.txtTotalOutstanding.Name = "txtTotalOutstanding";
            this.txtTotalOutstanding.Size = new System.Drawing.Size(130, 22);
            this.txtTotalOutstanding.TabIndex = 48;
            this.txtTotalOutstanding.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalOutstanding
            // 
            this.lblTotalOutstanding.AutoSize = true;
            this.lblTotalOutstanding.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalOutstanding.Location = new System.Drawing.Point(290, 96);
            this.lblTotalOutstanding.Name = "lblTotalOutstanding";
            this.lblTotalOutstanding.Size = new System.Drawing.Size(113, 16);
            this.lblTotalOutstanding.TabIndex = 51;
            this.lblTotalOutstanding.Text = "Total Outstanding";
            // 
            // lblTotalPaid
            // 
            this.lblTotalPaid.AutoSize = true;
            this.lblTotalPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPaid.Location = new System.Drawing.Point(150, 96);
            this.lblTotalPaid.Name = "lblTotalPaid";
            this.lblTotalPaid.Size = new System.Drawing.Size(70, 16);
            this.lblTotalPaid.TabIndex = 50;
            this.lblTotalPaid.Text = "Total Paid";
            // 
            // txtBillNo
            // 
            this.txtBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBillNo.Location = new System.Drawing.Point(432, 60);
            this.txtBillNo.MaxLength = 50;
            this.txtBillNo.Name = "txtBillNo";
            this.txtBillNo.Size = new System.Drawing.Size(130, 22);
            this.txtBillNo.TabIndex = 41;
            // 
            // txtTotalPaid
            // 
            this.txtTotalPaid.Enabled = false;
            this.txtTotalPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPaid.Location = new System.Drawing.Point(150, 116);
            this.txtTotalPaid.Name = "txtTotalPaid";
            this.txtTotalPaid.Size = new System.Drawing.Size(130, 22);
            this.txtTotalPaid.TabIndex = 47;
            this.txtTotalPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAmount.Location = new System.Drawing.Point(10, 96);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(87, 16);
            this.lblTotalAmount.TabIndex = 46;
            this.lblTotalAmount.Text = "Total Amount";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Enabled = false;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAmount.Location = new System.Drawing.Point(10, 116);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(130, 22);
            this.txtTotalAmount.TabIndex = 45;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTransactionDate
            // 
            this.lblTransactionDate.AutoSize = true;
            this.lblTransactionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionDate.Location = new System.Drawing.Point(572, 40);
            this.lblTransactionDate.Name = "lblTransactionDate";
            this.lblTransactionDate.Size = new System.Drawing.Size(111, 16);
            this.lblTransactionDate.TabIndex = 43;
            this.lblTransactionDate.Text = "Transaction Date";
            // 
            // lblBillNo
            // 
            this.lblBillNo.AutoSize = true;
            this.lblBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillNo.Location = new System.Drawing.Point(432, 40);
            this.lblBillNo.Name = "lblBillNo";
            this.lblBillNo.Size = new System.Drawing.Size(50, 16);
            this.lblBillNo.TabIndex = 42;
            this.lblBillNo.Text = "Bill No.";
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpTransactionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(572, 60);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.ShowCheckBox = true;
            this.dtpTransactionDate.Size = new System.Drawing.Size(130, 22);
            this.dtpTransactionDate.TabIndex = 44;
            // 
            // lblTransactionNo
            // 
            this.lblTransactionNo.AutoSize = true;
            this.lblTransactionNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionNo.Location = new System.Drawing.Point(10, 40);
            this.lblTransactionNo.Name = "lblTransactionNo";
            this.lblTransactionNo.Size = new System.Drawing.Size(103, 16);
            this.lblTransactionNo.TabIndex = 39;
            this.lblTransactionNo.Text = "Transaction No.";
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tpItems);
            this.tabMain.Controls.Add(this.tpPayments);
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMain.Location = new System.Drawing.Point(2, 209);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(870, 317);
            this.tabMain.TabIndex = 49;
            // 
            // tpItems
            // 
            this.tpItems.BackColor = System.Drawing.SystemColors.Control;
            this.tpItems.Controls.Add(this.txtSubCat);
            this.tpItems.Controls.Add(this.lblStockCode);
            this.tpItems.Controls.Add(this.txtStockCode);
            this.tpItems.Controls.Add(this.txtStockCategory);
            this.tpItems.Controls.Add(this.btnDiscount);
            this.tpItems.Controls.Add(this.btnReturn);
            this.tpItems.Controls.Add(this.txtStockID);
            this.tpItems.Controls.Add(this.btnAdd);
            this.tpItems.Controls.Add(this.lblUnit);
            this.tpItems.Controls.Add(this.cmbUnit);
            this.tpItems.Controls.Add(this.txtUnitPrice);
            this.tpItems.Controls.Add(this.lblUnitPrice);
            this.tpItems.Controls.Add(this.txtQuantity);
            this.tpItems.Controls.Add(this.lblQuantity);
            this.tpItems.Controls.Add(this.btnRemove);
            this.tpItems.Controls.Add(this.txtStockName);
            this.tpItems.Controls.Add(this.lblAdjustmentStockName);
            this.tpItems.Controls.Add(this.btnSearchItem);
            this.tpItems.Controls.Add(this.lstTransactionListing);
            this.tpItems.Location = new System.Drawing.Point(4, 25);
            this.tpItems.Name = "tpItems";
            this.tpItems.Padding = new System.Windows.Forms.Padding(3);
            this.tpItems.Size = new System.Drawing.Size(862, 288);
            this.tpItems.TabIndex = 0;
            this.tpItems.Text = "Items";
            // 
            // txtSubCat
            // 
            this.txtSubCat.Enabled = false;
            this.txtSubCat.Location = new System.Drawing.Point(543, 62);
            this.txtSubCat.Name = "txtSubCat";
            this.txtSubCat.Size = new System.Drawing.Size(100, 22);
            this.txtSubCat.TabIndex = 35;
            this.txtSubCat.Visible = false;
            // 
            // lblStockCode
            // 
            this.lblStockCode.AutoSize = true;
            this.lblStockCode.Location = new System.Drawing.Point(331, 4);
            this.lblStockCode.Name = "lblStockCode";
            this.lblStockCode.Size = new System.Drawing.Size(41, 16);
            this.lblStockCode.TabIndex = 34;
            this.lblStockCode.Text = "Code";
            // 
            // txtStockCode
            // 
            this.txtStockCode.Enabled = false;
            this.txtStockCode.Location = new System.Drawing.Point(331, 34);
            this.txtStockCode.Name = "txtStockCode";
            this.txtStockCode.Size = new System.Drawing.Size(100, 22);
            this.txtStockCode.TabIndex = 33;
            // 
            // txtStockCategory
            // 
            this.txtStockCategory.Enabled = false;
            this.txtStockCategory.Location = new System.Drawing.Point(437, 62);
            this.txtStockCategory.Name = "txtStockCategory";
            this.txtStockCategory.Size = new System.Drawing.Size(100, 22);
            this.txtStockCategory.TabIndex = 32;
            this.txtStockCategory.Visible = false;
            // 
            // btnDiscount
            // 
            this.btnDiscount.Enabled = false;
            this.btnDiscount.Location = new System.Drawing.Point(486, 257);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(180, 25);
            this.btnDiscount.TabIndex = 19;
            this.btnDiscount.Text = "&Discount";
            this.btnDiscount.UseVisualStyleBackColor = true;
            this.btnDiscount.Visible = false;
            // 
            // btnReturn
            // 
            this.btnReturn.Enabled = false;
            this.btnReturn.Location = new System.Drawing.Point(672, 257);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(180, 25);
            this.btnReturn.TabIndex = 20;
            this.btnReturn.Text = "Customer &Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Visible = false;
            // 
            // txtStockID
            // 
            this.txtStockID.Enabled = false;
            this.txtStockID.Location = new System.Drawing.Point(331, 62);
            this.txtStockID.Name = "txtStockID";
            this.txtStockID.Size = new System.Drawing.Size(100, 22);
            this.txtStockID.TabIndex = 19;
            this.txtStockID.Visible = false;
            this.txtStockID.TextChanged += new System.EventHandler(this.txtStockID_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(755, 34);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 25);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(649, 4);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(31, 16);
            this.lblUnit.TabIndex = 31;
            this.lblUnit.Text = "Unit";
            // 
            // cmbUnit
            // 
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Location = new System.Drawing.Point(649, 34);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(100, 24);
            this.cmbUnit.TabIndex = 16;
            this.cmbUnit.SelectedIndexChanged += new System.EventHandler(this.cmbUnit_SelectedIndexChanged);
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(437, 34);
            this.txtUnitPrice.MaxLength = 7;
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(100, 22);
            this.txtUnitPrice.TabIndex = 14;
            this.txtUnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(437, 4);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(65, 16);
            this.lblUnitPrice.TabIndex = 28;
            this.lblUnitPrice.Text = "Unit Price";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(543, 34);
            this.txtQuantity.MaxLength = 7;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(100, 22);
            this.txtQuantity.TabIndex = 15;
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantity_KeyDown);
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(543, 4);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(56, 16);
            this.lblQuantity.TabIndex = 26;
            this.lblQuantity.Text = "Quantity";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(11, 257);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(180, 25);
            this.btnRemove.TabIndex = 18;
            this.btnRemove.Text = "&Remove From List";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // txtStockName
            // 
            this.txtStockName.Location = new System.Drawing.Point(11, 34);
            this.txtStockName.MaxLength = 255;
            this.txtStockName.Name = "txtStockName";
            this.txtStockName.Size = new System.Drawing.Size(314, 22);
            this.txtStockName.TabIndex = 12;
            this.txtStockName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStockName_KeyDown);
            this.txtStockName.Leave += new System.EventHandler(this.txtStockName_Leave);
            // 
            // lblAdjustmentStockName
            // 
            this.lblAdjustmentStockName.AutoSize = true;
            this.lblAdjustmentStockName.Location = new System.Drawing.Point(11, 4);
            this.lblAdjustmentStockName.Name = "lblAdjustmentStockName";
            this.lblAdjustmentStockName.Size = new System.Drawing.Size(82, 16);
            this.lblAdjustmentStockName.TabIndex = 5;
            this.lblAdjustmentStockName.Text = "Stock Name";
            // 
            // btnSearchItem
            // 
            this.btnSearchItem.Location = new System.Drawing.Point(11, 62);
            this.btnSearchItem.Name = "btnSearchItem";
            this.btnSearchItem.Size = new System.Drawing.Size(130, 25);
            this.btnSearchItem.TabIndex = 13;
            this.btnSearchItem.Text = "Select &Item";
            this.btnSearchItem.UseVisualStyleBackColor = true;
            this.btnSearchItem.Click += new System.EventHandler(this.btnSearchItem_Click);
            // 
            // lstTransactionListing
            // 
            this.lstTransactionListing.FullRowSelect = true;
            this.lstTransactionListing.GridLines = true;
            this.lstTransactionListing.HideSelection = false;
            this.lstTransactionListing.Location = new System.Drawing.Point(14, 93);
            this.lstTransactionListing.MultiSelect = false;
            this.lstTransactionListing.Name = "lstTransactionListing";
            this.lstTransactionListing.Size = new System.Drawing.Size(838, 158);
            this.lstTransactionListing.TabIndex = 0;
            this.lstTransactionListing.UseCompatibleStateImageBehavior = false;
            this.lstTransactionListing.View = System.Windows.Forms.View.Details;
            this.lstTransactionListing.SelectedIndexChanged += new System.EventHandler(this.lstTransactionListing_SelectedIndexChanged);
            this.lstTransactionListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstTransactionListing_ColumnClick);
            // 
            // tpPayments
            // 
            this.tpPayments.BackColor = System.Drawing.SystemColors.Control;
            this.tpPayments.Controls.Add(this.lblPaymentDate);
            this.tpPayments.Controls.Add(this.dtpPaymentDate);
            this.tpPayments.Controls.Add(this.btnPaymentAdd);
            this.tpPayments.Controls.Add(this.cmbPaymentType);
            this.tpPayments.Controls.Add(this.txtPaymentAmount);
            this.tpPayments.Controls.Add(this.lblPaymentAmount);
            this.tpPayments.Controls.Add(this.txtPaymentReference);
            this.tpPayments.Controls.Add(this.lblPaymentReference);
            this.tpPayments.Controls.Add(this.btnPaymentRemove);
            this.tpPayments.Controls.Add(this.lblPaymentType);
            this.tpPayments.Controls.Add(this.lstPaymentListing);
            this.tpPayments.Location = new System.Drawing.Point(4, 25);
            this.tpPayments.Name = "tpPayments";
            this.tpPayments.Padding = new System.Windows.Forms.Padding(3);
            this.tpPayments.Size = new System.Drawing.Size(862, 288);
            this.tpPayments.TabIndex = 1;
            this.tpPayments.Text = "Payments";
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(378, 4);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(37, 16);
            this.lblPaymentDate.TabIndex = 43;
            this.lblPaymentDate.Text = "Date";
            // 
            // dtpPaymentDate
            // 
            this.dtpPaymentDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPaymentDate.Location = new System.Drawing.Point(378, 34);
            this.dtpPaymentDate.Name = "dtpPaymentDate";
            this.dtpPaymentDate.ShowCheckBox = true;
            this.dtpPaymentDate.Size = new System.Drawing.Size(130, 22);
            this.dtpPaymentDate.TabIndex = 42;
            // 
            // btnPaymentAdd
            // 
            this.btnPaymentAdd.Location = new System.Drawing.Point(514, 31);
            this.btnPaymentAdd.Name = "btnPaymentAdd";
            this.btnPaymentAdd.Size = new System.Drawing.Size(75, 25);
            this.btnPaymentAdd.TabIndex = 24;
            this.btnPaymentAdd.Text = "&Add";
            this.btnPaymentAdd.UseVisualStyleBackColor = true;
            this.btnPaymentAdd.Click += new System.EventHandler(this.btnPaymentAdd_Click);
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Location = new System.Drawing.Point(11, 34);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(200, 24);
            this.cmbPaymentType.TabIndex = 21;
            this.cmbPaymentType.SelectedIndexChanged += new System.EventHandler(this.cmbPaymentType_SelectedIndexChanged);
            // 
            // txtPaymentAmount
            // 
            this.txtPaymentAmount.Location = new System.Drawing.Point(221, 34);
            this.txtPaymentAmount.MaxLength = 7;
            this.txtPaymentAmount.Name = "txtPaymentAmount";
            this.txtPaymentAmount.Size = new System.Drawing.Size(150, 22);
            this.txtPaymentAmount.TabIndex = 22;
            this.txtPaymentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPaymentAmount
            // 
            this.lblPaymentAmount.AutoSize = true;
            this.lblPaymentAmount.Location = new System.Drawing.Point(221, 4);
            this.lblPaymentAmount.Name = "lblPaymentAmount";
            this.lblPaymentAmount.Size = new System.Drawing.Size(53, 16);
            this.lblPaymentAmount.TabIndex = 41;
            this.lblPaymentAmount.Text = "Amount";
            // 
            // txtPaymentReference
            // 
            this.txtPaymentReference.Location = new System.Drawing.Point(144, 62);
            this.txtPaymentReference.MaxLength = 255;
            this.txtPaymentReference.Name = "txtPaymentReference";
            this.txtPaymentReference.Size = new System.Drawing.Size(364, 22);
            this.txtPaymentReference.TabIndex = 23;
            // 
            // lblPaymentReference
            // 
            this.lblPaymentReference.AutoSize = true;
            this.lblPaymentReference.Location = new System.Drawing.Point(11, 62);
            this.lblPaymentReference.Name = "lblPaymentReference";
            this.lblPaymentReference.Size = new System.Drawing.Size(71, 16);
            this.lblPaymentReference.TabIndex = 40;
            this.lblPaymentReference.Text = "Reference";
            // 
            // btnPaymentRemove
            // 
            this.btnPaymentRemove.Location = new System.Drawing.Point(11, 257);
            this.btnPaymentRemove.Name = "btnPaymentRemove";
            this.btnPaymentRemove.Size = new System.Drawing.Size(180, 25);
            this.btnPaymentRemove.TabIndex = 25;
            this.btnPaymentRemove.Text = "&Remove From List";
            this.btnPaymentRemove.UseVisualStyleBackColor = true;
            this.btnPaymentRemove.Click += new System.EventHandler(this.btnPaymentRemove_Click);
            // 
            // lblPaymentType
            // 
            this.lblPaymentType.AutoSize = true;
            this.lblPaymentType.Location = new System.Drawing.Point(11, 4);
            this.lblPaymentType.Name = "lblPaymentType";
            this.lblPaymentType.Size = new System.Drawing.Size(40, 16);
            this.lblPaymentType.TabIndex = 33;
            this.lblPaymentType.Text = "Type";
            // 
            // lstPaymentListing
            // 
            this.lstPaymentListing.FullRowSelect = true;
            this.lstPaymentListing.GridLines = true;
            this.lstPaymentListing.HideSelection = false;
            this.lstPaymentListing.Location = new System.Drawing.Point(11, 93);
            this.lstPaymentListing.MultiSelect = false;
            this.lstPaymentListing.Name = "lstPaymentListing";
            this.lstPaymentListing.Size = new System.Drawing.Size(838, 158);
            this.lstPaymentListing.TabIndex = 0;
            this.lstPaymentListing.UseCompatibleStateImageBehavior = false;
            this.lstPaymentListing.View = System.Windows.Forms.View.Details;
            this.lstPaymentListing.SelectedIndexChanged += new System.EventHandler(this.lstPaymentListing_SelectedIndexChanged);
            this.lstPaymentListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstPaymentListing_ColumnClick);
            // 
            // txtTransactionNo
            // 
            this.txtTransactionNo.Enabled = false;
            this.txtTransactionNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransactionNo.Location = new System.Drawing.Point(10, 60);
            this.txtTransactionNo.Name = "txtTransactionNo";
            this.txtTransactionNo.Size = new System.Drawing.Size(130, 22);
            this.txtTransactionNo.TabIndex = 37;
            this.txtTransactionNo.TextChanged += new System.EventHandler(this.txtTransactionNo_TextChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(200, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 25);
            this.btnSearch.TabIndex = 36;
            this.btnSearch.Text = "Select &Transaction";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // rbtEdit
            // 
            this.rbtEdit.AutoSize = true;
            this.rbtEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtEdit.Location = new System.Drawing.Point(100, 10);
            this.rbtEdit.Name = "rbtEdit";
            this.rbtEdit.Size = new System.Drawing.Size(49, 20);
            this.rbtEdit.TabIndex = 35;
            this.rbtEdit.Text = "Edit";
            this.rbtEdit.UseVisualStyleBackColor = true;
            this.rbtEdit.CheckedChanged += new System.EventHandler(this.rbtEdit_CheckedChanged);
            // 
            // rbtNew
            // 
            this.rbtNew.AutoSize = true;
            this.rbtNew.Checked = true;
            this.rbtNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtNew.Location = new System.Drawing.Point(10, 10);
            this.rbtNew.Name = "rbtNew";
            this.rbtNew.Size = new System.Drawing.Size(53, 20);
            this.rbtNew.TabIndex = 34;
            this.rbtNew.TabStop = true;
            this.rbtNew.Text = "New";
            this.rbtNew.UseVisualStyleBackColor = true;
            this.rbtNew.CheckedChanged += new System.EventHandler(this.rbtNew_CheckedChanged);
            // 
            // lblRemark
            // 
            this.lblRemark.AutoSize = true;
            this.lblRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemark.Location = new System.Drawing.Point(10, 152);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(56, 16);
            this.lblRemark.TabIndex = 56;
            this.lblRemark.Text = "Remark";
            // 
            // erpPurchase
            // 
            this.erpPurchase.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpPurchase.ContainerControl = this;
            // 
            // frmPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 614);
            this.ControlBox = false;
            this.Controls.Add(this.pnlMain);
            this.Name = "frmPurchase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmPurchase_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPurchase_FormClosing);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.grpControl.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tpItems.ResumeLayout(false);
            this.tpItems.PerformLayout();
            this.tpPayments.ResumeLayout(false);
            this.tpPayments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpPurchase)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlMain;
        public System.Windows.Forms.TextBox txtVendorID;
        public System.Windows.Forms.Button btnSearchVendor;
        public System.Windows.Forms.Label lblVendor;
        public System.Windows.Forms.TextBox txtVendorCode;
        public System.Windows.Forms.TextBox txtRemark;
        public System.Windows.Forms.TextBox txtVendorName;
        public System.Windows.Forms.GroupBox grpControl;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.TextBox txtTotalOutstanding;
        public System.Windows.Forms.Label lblTotalOutstanding;
        public System.Windows.Forms.Label lblTotalPaid;
        public System.Windows.Forms.TextBox txtBillNo;
        public System.Windows.Forms.TextBox txtTotalPaid;
        public System.Windows.Forms.Label lblTotalAmount;
        public System.Windows.Forms.TextBox txtTotalAmount;
        public System.Windows.Forms.Label lblTransactionDate;
        public System.Windows.Forms.Label lblBillNo;
        public System.Windows.Forms.DateTimePicker dtpTransactionDate;
        public System.Windows.Forms.Label lblTransactionNo;
        public System.Windows.Forms.TabControl tabMain;
        public System.Windows.Forms.TabPage tpItems;
        public System.Windows.Forms.TextBox txtStockCategory;
        public System.Windows.Forms.Button btnDiscount;
        public System.Windows.Forms.Button btnReturn;
        public System.Windows.Forms.TextBox txtStockID;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.Label lblUnit;
        public System.Windows.Forms.ComboBox cmbUnit;
        public System.Windows.Forms.TextBox txtUnitPrice;
        public System.Windows.Forms.Label lblUnitPrice;
        public System.Windows.Forms.TextBox txtQuantity;
        public System.Windows.Forms.Label lblQuantity;
        public System.Windows.Forms.Button btnRemove;
        public System.Windows.Forms.TextBox txtStockName;
        public System.Windows.Forms.Label lblAdjustmentStockName;
        public System.Windows.Forms.Button btnSearchItem;
        public System.Windows.Forms.ListView lstTransactionListing;
        public System.Windows.Forms.TabPage tpPayments;
        public System.Windows.Forms.Label lblPaymentDate;
        public System.Windows.Forms.DateTimePicker dtpPaymentDate;
        public System.Windows.Forms.Button btnPaymentAdd;
        public System.Windows.Forms.ComboBox cmbPaymentType;
        public System.Windows.Forms.TextBox txtPaymentAmount;
        public System.Windows.Forms.Label lblPaymentAmount;
        public System.Windows.Forms.TextBox txtPaymentReference;
        public System.Windows.Forms.Label lblPaymentReference;
        public System.Windows.Forms.Button btnPaymentRemove;
        public System.Windows.Forms.Label lblPaymentType;
        public System.Windows.Forms.ListView lstPaymentListing;
        public System.Windows.Forms.TextBox txtTransactionNo;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.RadioButton rbtEdit;
        public System.Windows.Forms.RadioButton rbtNew;
        public System.Windows.Forms.Label lblRemark;
        public System.Windows.Forms.ErrorProvider erpPurchase;
        public System.Windows.Forms.TextBox txtStockCode;
        public System.Windows.Forms.Label lblStockCode;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.TextBox txtSubCat;
    }
}