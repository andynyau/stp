﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmSelectStock : Form
    {
        #region Declarations

            private STP.Logic.Stock _Logic_Stock;

            private int _SearchType;
            private String _Current_Sort;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildStockList;
            private Boolean _CancelSearch;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmSelectStock_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this.BuildCategoryList();
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.BuildAutoComplete();
                        this._CancelSearch = true;
                        this.lstCategory.Items[0].Selected = true;
                        this.lstSubCategory.Items[0].Selected = true;
                        this.lstStockListing.Items[0].Selected = true;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstCategory_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                            String category, subCategory;
                            if (this.lstCategory.SelectedItems.Count > 0)
                            {
                                this.BuildSubCategoryList();
                                this._CancelSearch = true;
                                this.lstSubCategory.Items[0].Selected = true;
                                category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                                {
                                    this._SearchType = 0;
                                }
                                else
                                {
                                    this._SearchType = 1;
                                }
                                this.SearchStock();
                            }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstSubCategory_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        String category, subCategory;
                        if (this.lstSubCategory.SelectedItems.Count > 0)
                        {
                            category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                            subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                            if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                            {
                                this._SearchType = 0;
                            }
                            else
                            {
                                this._SearchType = 1;
                            }
                            if (this._CancelSearch == false)
                            {
                                this.SearchStock();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstStockListing_MouseDoubleClick(object sender, MouseEventArgs e)
                {
                    try
                    {
                        this.SelectStock();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstStockListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.SelectStock();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstStockListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, e.Column, this.ldm_BuildStockList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtStockSearch_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            if (this.txtStockSearch.Text.Contains(" ") == true)
                            {
                                this.txtStockSearch.Text = this.txtStockSearch.Text.Remove(0, this.txtStockSearch.Text.IndexOf(" ") + 1);
                            }
                            this._SearchType = 1;
                            //this.SearchStock();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.txtStockSearch.Text.Contains(" ") == true)
                        {
                            this.txtStockSearch.Text = this.txtStockSearch.Text.Remove(0, this.txtStockSearch.Text.IndexOf(" ") + 1);
                        }
                        this._SearchType = 1;
                        //this.SearchStock();
                        String[] sortInfo = null;
                        int columnIndex = 0;
                        if (this._Current_Sort != String.Empty)
                        {
                            sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                            columnIndex = int.Parse(sortInfo[2]);
                            if (sortInfo[1] == "ASC")
                            {
                                this._Current_Sort = String.Empty;
                            }
                            else
                            {
                                this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                            }
                        }
                        ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSelect_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.SelectStock();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Functions and Procedures

            public frmSelectStock()
            {
                try
                {
                    this.InitializeComponent();
                    this.Text = "Select Stock";
                    this._Logic_Stock = new STP.Logic.Stock();
                    this.ldm_BuildStockList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchStock);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildButton()
            {
                try
                {
                    this.btnSearch.Image = Image.FromFile("image/Search.png");
                    this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnSelect.Image = Image.FromFile("image/Confirm.png");
                    this.btnSelect.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnClose.Image = Image.FromFile("image/Cancel.png");
                    this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildCategoryList()
            {
                try
                {
                    Global.ShowWaiting(this, true);
                    this.lstCategory.Visible = false;
                    this.lstCategory.Clear();
                    this.BuildCategoryListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    this._Logic_Stock.BuildCategoryList(this.lstCategory, conditions, false, true);
                    this.FormatCategoryListColumn();
                    this.lstCategory.Visible = true;
                    Global.ShowWaiting(this, false);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildCategoryListColumn()
            {
                this.lstCategory.Columns.Clear();
                this.lstCategory.Columns.Add("id", "ID");
                this.lstCategory.Columns.Add("categoryname", "Category");
                this.lstCategory.Columns.Add("categoryshortcut", "Code");
                this.lstCategory.Columns.Add("createddate", "Created Date");
                this.lstCategory.Columns.Add("flag", "Flag");
            }

            private void FormatCategoryListColumn()
            {
                this.lstCategory.Columns["categoryname"].Width = 117;
                this.lstCategory.Columns["categoryname"].TextAlign = HorizontalAlignment.Left;
                this.lstCategory.Columns["categoryshortcut"].Width = 50;
                this.lstCategory.Columns["categoryshortcut"].TextAlign = HorizontalAlignment.Left;

                this.lstCategory.Columns.Remove(this.lstCategory.Columns["id"]);
                this.lstCategory.Columns.Remove(this.lstCategory.Columns["createddate"]);
                this.lstCategory.Columns.Remove(this.lstCategory.Columns["flag"]);
            }

            private void BuildSubCategoryList()
            {
                try
                {
                    Global.ShowWaiting(this, true);
                    this.lstSubCategory.Visible = false;
                    this.lstSubCategory.Clear();
                    this.BuildSubCategoryListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    if (this.lstCategory.SelectedItems.Count > 0)
                    {
                        if (this.lstCategory.SelectedItems[0].SubItems[0].Text != "0")
                        {
                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                            conditionFieldStockCategory.Field = "stockcategory";
                            conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockCategory);
                        }
                    }
                    this._Logic_Stock.BuildSubCategoryList(this.lstSubCategory, conditions, false, true);
                    this.FormatSubCategoryListColumn();
                    this.lstSubCategory.Visible = true;
                    Global.ShowWaiting(this, false);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildSubCategoryListColumn()
            {
                this.lstSubCategory.Columns.Clear();
                this.lstSubCategory.Columns.Add("stockcategory", "Category");
                this.lstSubCategory.Columns.Add("id", "ID");
                this.lstSubCategory.Columns.Add("subcategoryname", "Sub Category");
                this.lstSubCategory.Columns.Add("subcategoryshortcut", "Code");
                this.lstSubCategory.Columns.Add("createddate", "Created Date");
                this.lstSubCategory.Columns.Add("flag", "Flag");
            }

            private void FormatSubCategoryListColumn()
            {
                this.lstSubCategory.Columns["subcategoryname"].Width = 117;
                this.lstSubCategory.Columns["subcategoryname"].TextAlign = HorizontalAlignment.Left;
                this.lstSubCategory.Columns["subcategoryshortcut"].Width = 50;
                this.lstSubCategory.Columns["subcategoryshortcut"].TextAlign = HorizontalAlignment.Left;

                this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["stockcategory"]);
                this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["id"]);
                this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["createddate"]);
                this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["flag"]);
            }

            private void SearchStock()
            {
                try
                {
                    this.SearchStock(ND.Data.DataObject.SortType.None, String.Empty);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchStock(ND.Data.DataObject.SortType sortType, String sortField)
            {
                try
                {
                    Global.ShowWaiting(this, true);
                    this.lstStockListing.Visible = false;
                    this.lstStockListing.Clear();
                    this.BuildStockListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    switch (this._SearchType)
                    {
                        case 0:
                            {
                                break;
                            }
                        case 1:
                            {
                                if (this.lstCategory.SelectedItems[0].SubItems[0].Text != "0")
                                {
                                    ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                                    conditionFieldStockCategory.Field = "stockcategory";
                                    conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                    conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                    conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                    conditions.Add(conditionFieldStockCategory);
                                }
                                if (this.lstSubCategory.SelectedItems[0].SubItems[1].Text != "0")
                                {
                                    ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                                    conditionFieldStockCategory.Field = "stockcategory";
                                    conditionFieldStockCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[0].Text;
                                    conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                    conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                    conditions.Add(conditionFieldStockCategory);
                                    ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
                                    conditionFieldStockSubCategory.Field = "stocksubcategory";
                                    conditionFieldStockSubCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                    conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                    conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                    conditions.Add(conditionFieldStockSubCategory);
                                }
                                ND.Data.ConditionField conditionFieldStockName = new ND.Data.ConditionField();
                                conditionFieldStockName.Field = "stockname";
                                conditionFieldStockName.Value = "%" + this.txtStockSearch.Text + "%";
                                conditionFieldStockName.DataType = ND.Data.DataObject.DataType.NString;
                                conditionFieldStockName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                conditions.Add(conditionFieldStockName);
                                break;
                            }
                    }
                    this._Logic_Stock.BuildStockList(this.lstStockListing, conditions, sortType, sortField);
                    this.FormatStockListColumn();
                    this.lstStockListing.Visible = true;
                    this.lstStockListing.Focus();
                    Global.ShowWaiting(this, false);
                    this._CancelSearch = false;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildAutoComplete()
            {
                try
                {
                    this.txtStockSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtStockSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtStockSearch.AutoCompleteCustomSource = this._Logic_Stock.StringCollection();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildStockListColumn()
            {
                try
                {
                    this.lstStockListing.Columns.Clear();
                    this.lstStockListing.Columns.Add("stockcategory", "Stock Category");
                    this.lstStockListing.Columns.Add("stocksubcategory", "Sub Category");
                    this.lstStockListing.Columns.Add("id", "ID");
                    this.lstStockListing.Columns.Add("stockcode", "Code");
                    this.lstStockListing.Columns.Add("stocktype", "Stock Type");
                    this.lstStockListing.Columns.Add("stockname", "Stock Name");
                    this.lstStockListing.Columns.Add("stockdesc1", "Stock Description 1");
                    this.lstStockListing.Columns.Add("stockdesc2", "Stock Description 2");
                    this.lstStockListing.Columns.Add("stockcost", "Stock Cost");
                    this.lstStockListing.Columns.Add("stockbalance", "Balance");
                    this.lstStockListing.Columns.Add("stockuom", "Unit");
                    this.lstStockListing.Columns.Add("packagesize1", "Size (A)");
                    this.lstStockListing.Columns.Add("packageuom1", "Pkg (A)");
                    this.lstStockListing.Columns.Add("packagesize2", "Size (B)");
                    this.lstStockListing.Columns.Add("packageuom2", "Pkg Unit (B)");
                    this.lstStockListing.Columns.Add("stockprice1", "Unit Price");
                    this.lstStockListing.Columns.Add("packageprice1", "SO Pkg");
                    this.lstStockListing.Columns.Add("stockprice2", "Cash Unit");
                    this.lstStockListing.Columns.Add("packageprice2", "Cash Pkg");
                    this.lstStockListing.Columns.Add("stockprice3", "Retail Unit");
                    this.lstStockListing.Columns.Add("packageprice3", "Retail Pkg");
                    this.lstStockListing.Columns.Add("stockprice4", "Special Unit");
                    this.lstStockListing.Columns.Add("packageprice4", "Special Pkg");
                    this.lstStockListing.Columns.Add("stockprice5", "Reserved Unit");
                    this.lstStockListing.Columns.Add("packageprice5", "Reserved Pkg");
                    this.lstStockListing.Columns.Add("guid", "GUID");
                    this.lstStockListing.Columns.Add("createddate", "Created Date");
                    this.lstStockListing.Columns.Add("flag", "Flag");
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void FormatStockListColumn()
            {
                try
                {
                    this.lstStockListing.Columns["stockcode"].Width = 50;
                    this.lstStockListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["stockname"].Width = 200;
                    this.lstStockListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["packagesize1"].Width = 50;
                    this.lstStockListing.Columns["packagesize1"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["packageuom1"].Width = 50;
                    this.lstStockListing.Columns["packageuom1"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["packagesize2"].Width = 50;
                    this.lstStockListing.Columns["packagesize2"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["packageuom2"].Width = 50;
                    this.lstStockListing.Columns["packageuom2"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["stockuom"].Width = 50;
                    this.lstStockListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Left;
                    this.lstStockListing.Columns["stockprice1"].Width = 70;
                    this.lstStockListing.Columns["stockprice1"].TextAlign = HorizontalAlignment.Right;
                    this.lstStockListing.Columns["stockbalance"].Width = 70;
                    this.lstStockListing.Columns["stockbalance"].TextAlign = HorizontalAlignment.Right;

                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["id"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockcategory"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stocksubcategory"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stocktype"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockdesc1"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockdesc2"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockcost"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice1"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice2"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice2"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice3"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice3"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice4"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice4"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice5"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice5"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["guid"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["createddate"]);
                    this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["flag"]);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SelectStock()
            {
                try
                {
                    if (this.lstStockListing.SelectedItems.Count > 0)
                    {
                        if (this.Owner.GetType() == typeof(frmStock))
                        {
                            frmStock form = (frmStock)this.Owner;
                            switch (form.Working_Mode)
                            {
                                case frmStock.WorkingMode.Receive:
                                    {
                                        form.txtReceiveStockCategory.Text = String.Empty;
                                        form.txtReceiveStockCategory.Text = this.lstStockListing.SelectedItems[0].SubItems[1].Text;
                                        form.txtReceiveSubCat.Text = String.Empty;
                                        form.txtReceiveSubCat.Text = this.lstStockListing.SelectedItems[0].SubItems[2].Text;
                                        form.txtReceiveStockID.Text = String.Empty;
                                        form.txtReceiveStockID.Text = this.lstStockListing.SelectedItems[0].SubItems[0].Text;
                                        form.txtReceiveStockCode.Text = String.Empty;
                                        form.txtReceiveStockCode.Text = this.lstStockListing.SelectedItems[0].SubItems[3].Text;
                                        break;
                                    }
                                case frmStock.WorkingMode.Transfer:
                                    {
                                        break;
                                    }
                                case frmStock.WorkingMode.Adjustment:
                                    {
                                        form.txtAdjustmentStockCategory.Text = String.Empty;
                                        form.txtAdjustmentStockCategory.Text = this.lstStockListing.SelectedItems[0].SubItems[1].Text;
                                        form.txtAdjustmentSubCat.Text = String.Empty;
                                        form.txtAdjustmentSubCat.Text = this.lstStockListing.SelectedItems[0].SubItems[2].Text;
                                        form.txtAdjustmentStockID.Text = String.Empty;
                                        form.txtAdjustmentStockID.Text = this.lstStockListing.SelectedItems[0].SubItems[0].Text;
                                        form.txtAdjustmentStockCode.Text = String.Empty;
                                        form.txtAdjustmentStockCode.Text = this.lstStockListing.SelectedItems[0].SubItems[3].Text;
                                        break;
                                    }
                            }
                        }
                        else if (this.Owner.GetType() == typeof(frmSales))
                        {
                            frmSales form = (frmSales)this.Owner;
                            form.txtStockCategory.Text = String.Empty;
                            form.txtStockCategory.Text = this.lstStockListing.SelectedItems[0].SubItems[1].Text;
                            form.txtSubCat.Text = String.Empty;
                            form.txtSubCat.Text = this.lstStockListing.SelectedItems[0].SubItems[2].Text;
                            form.txtStockID.Text = String.Empty;
                            form.txtStockID.Text = this.lstStockListing.SelectedItems[0].SubItems[0].Text;
                            form.txtStockCode.Text = String.Empty;
                            form.txtStockCode.Text = this.lstStockListing.SelectedItems[0].SubItems[3].Text;
                        }
                        else if (this.Owner.GetType() == typeof(frmPurchase))
                        {
                            frmPurchase form = (frmPurchase)this.Owner;
                            form.txtStockCategory.Text = String.Empty;
                            form.txtStockCategory.Text = this.lstStockListing.SelectedItems[0].SubItems[1].Text;
                            form.txtSubCat.Text = String.Empty;
                            form.txtSubCat.Text = this.lstStockListing.SelectedItems[0].SubItems[2].Text;
                            form.txtStockID.Text = String.Empty;
                            form.txtStockID.Text = this.lstStockListing.SelectedItems[0].SubItems[0].Text;
                            form.txtStockCode.Text = String.Empty;
                            form.txtStockCode.Text = this.lstStockListing.SelectedItems[0].SubItems[3].Text;
                        }
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

        #endregion
    }
}
