﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmStock : Form
    {
        #region Declarations

            #region General

                public enum WorkingMode
                {
                    Stock = 0,
                    Receive = 1,
                    Transfer = 3,
                    Adjustment = 2
                }

                public WorkingMode Working_Mode
                {
                    get { return this._Working_Mode; }
                    set { this._Working_Mode = value; }
                }
                public DataTable DataTable_Transaction_Receive
                {
                    get { return this._Logic_Transaction_Receive.Detail_Table; }
                }
                public DataTable DataTable_Transaction_Transfer
                {
                    get { return this._Logic_Transaction_Transfer.Detail_Table; }
                }
                public DataTable DataTable_Transaction_Adjustment
                {
                    get { return this._Logic_Transaction_Adjustment.Detail_Table; }
                }

                private WorkingMode _Working_Mode;

                private STP.Logic.Stock _Logic_Stock;
                private STP.Logic.Transaction _Logic_Transaction_Receive;
                private STP.Logic.Transaction _Logic_Transaction_Transfer;
                private STP.Logic.Transaction _Logic_Transaction_Adjustment;

                private STP.Logic.Common.SaveType _SaveType_Stock;
                private STP.Logic.Common.SaveType _SaveType_Receive;
                //private STP.Logic.Common.SaveType _SaveType_Transfer;
                private STP.Logic.Common.SaveType _SaveType_Adjustment;
                private STP.Logic.Common.SaveType _SaveType_Category;
                private STP.Logic.Common.SaveType _SaveType_Sub_Category;

            #endregion

            #region Listing

                private int _SearchType;
                private String _Current_Sort;
                private String _Current_Category_Sort;
                private String _Current_SubCategory_Sort;
                private ND.Standard.Validation.Winform.Form _ValidatorNew;
                private ND.Standard.Validation.Winform.Form _ValidatorEdit;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtStockName;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildStockList;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCategoryList;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildSubCategoryList;
                private Boolean _CancelSearch;

            #endregion

            #region Receive

                private String _Current_Receive_Sort;
                private ND.Standard.Validation.Winform.Form _ValidatorReceive;
                private ND.Standard.Validation.Winform.Form _ValidatorReceiveTransactionNew;
                private ND.Standard.Validation.Winform.Form _ValidatorReceiveTransactionEdit;
                private ND.Standard.Validation.Winform.Form _ValidatorReceiveTransactionDelete;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstReceiveList;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_dtpReceiveTransactionDate;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildReceiveList;

            #endregion

            #region Adjustment

                private String _Current_Adjustment_Sort;
                private ND.Standard.Validation.Winform.Form _ValidatorAdjustment;
                private ND.Standard.Validation.Winform.Form _ValidatorAdjustmentTransaction;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstAdjustmentList;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstAdjustmentListExist;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildAdjustmentList;

            #endregion

            #region Category

                private String _Current_Category_Category_Sort;
                private String _Current_Category_SubCategory_Sort;
                private ND.Standard.Validation.Winform.Form _Validator_Category_New;
                private ND.Standard.Validation.Winform.Form _Validator_Category_Edit;
                private ND.Standard.Validation.Winform.Form _Validator_Sub_Category_New;
                private ND.Standard.Validation.Winform.Form _Validator_Sub_Category_Edit;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtCategoryName;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtSubCategoryName;
                private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtSubCategoryShortcut;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCategoryCategoryList;
                private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCategorySubCategoryList;

            #endregion

        #endregion

        #region Event Handlings

            #region General

                #region Form Events

                    private void frmStock_Load(object sender, EventArgs e)
                    {
                        try
                        {
                            this.LoopTab();
                            this.BuildButton();
                            this.rbtReceiveNew.Checked = true;
                            this.btnReceiveSelect.Enabled = false;
                            this.txtReceiveFrom.Text = "HQ";
                            this.dtpReceiveTransactionDate.Checked = false;
                            this.EditDeleteEnable(false);
                            this.BuildCategoryList();
                            this._SearchType = 0;
                            this._Current_Sort = String.Empty;
                            this._Current_Category_Sort = String.Empty;
                            this._Current_SubCategory_Sort = String.Empty;
                            this._Current_Receive_Sort = String.Empty;
                            this._Current_Adjustment_Sort = String.Empty;
                            this.BuildAutoComplete();
                            this.BuildReceiveList();
                            this.BuildAdjustmentList();
                            this.SetValidation();
                            this.RemoveAdjustmentEnable(false);
                            this.RemoveReceiveEnable(false);
                            this.lstStockListing.Focus();
                            this._SaveType_Stock = STP.Logic.Common.SaveType.None;
                            this._SaveType_Receive = STP.Logic.Common.SaveType.Insert;
                            //this._SaveType_Transfer = STP.Logic.Common.SaveType.Insert;
                            this._SaveType_Adjustment = STP.Logic.Common.SaveType.Insert;
                            this._CancelSearch = true;
                            if (this.lstCategory.Items.Count > 0) this.lstCategory.Items[0].Selected = true;
                            if (this.lstSubCategory.Items.Count > 0) this.lstSubCategory.Items[0].Selected = true;
                            if (this.lstStockListing.Items.Count > 0) this.lstStockListing.Items[0].Selected = true;
                            this._Current_Category_Category_Sort = String.Empty;
                            this._Current_Category_SubCategory_Sort = String.Empty;
                            this.BuildCategoryCategoryList();
                            this.BuildCategorySubCategoryList();
                            this.BuildCategoryCategoryCombo();
                            this._SaveType_Category = STP.Logic.Common.SaveType.None;
                            this._SaveType_Sub_Category = STP.Logic.Common.SaveType.None;
                            Global.ShowWaiting(this, false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void frmStock_FormClosing(object sender, FormClosingEventArgs e)
                    {
                        try
                        {
                            this.CloseForm(e);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region TabControl Events

                    private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this._Working_Mode = (WorkingMode)this.tabMain.SelectedIndex;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

            #region Listing

                #region ListView Events

                    private void lstCategory_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstCategory, ref this._Current_Category_Sort, e.Column, this.ldm_BuildCategoryList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategory_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            String category, subCategory;
                            if (this.lstCategory.SelectedItems.Count > 0)
                            {
                                this.BuildSubCategoryList();
                                this._CancelSearch = true;
                                this.lstSubCategory.Items[0].Selected = true;
                                category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                                {
                                    this._SearchType = 0;
                                }
                                else
                                {
                                    this._SearchType = 1;
                                }
                                this.SearchStock();
                                //SearchStock();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstSubCategory_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstSubCategory, ref this._Current_SubCategory_Sort, e.Column, this.ldm_BuildSubCategoryList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstSubCategory_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            String category, subCategory;
                            if (this.lstSubCategory.SelectedItems.Count > 0)
                            {
                                category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                                {
                                    this._SearchType = 0;
                                }
                                else
                                {
                                    this._SearchType = 1;
                                }
                                if (this._CancelSearch == false)
                                {
                                    this.SearchStock();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstStockListing_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.EditDeleteEnable(this.lstStockListing.SelectedItems.Count > 0);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstStockListing_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                this.ShowDetail(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstStockListing_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, e.Column, this.ldm_BuildStockList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstStockListing_MouseDoubleClick(object sender, MouseEventArgs e)
                    {
                        try
                        {
                            this.ShowDetail(true);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region ComboBox Events

                    private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.BuildSubCategoryCombo();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region TextBox Events

                    private void txtStockSearch_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                if (this.txtStockSearch.Text.Contains(" ") == true)
                                {
                                    this.txtStockSearch.Text = this.txtStockSearch.Text.Remove(0, this.txtStockSearch.Text.IndexOf(" ") + 1);
                                }
                                this._SearchType = 1;
                                //SearchStock();
                                String[] sortInfo = null;
                                int columnIndex = 0;
                                if (this._Current_Sort != String.Empty)
                                {
                                    sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                    columnIndex = int.Parse(sortInfo[2]);
                                    if (sortInfo[1] == "ASC")
                                    {
                                        this._Current_Sort = String.Empty;
                                    }
                                    else
                                    {
                                        this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                    }
                                }
                                ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtStockUOM_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.lblStockPackageSize_UOM1.Text = this.txtStockUOM.Text.Trim();
                            this.lblStockPackageSize_UOM2.Text = this.txtStockUOM.Text.Trim();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtStockPackageUOM1_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.lblStockPackageSize_PackageUOM1.Text = this.txtStockPackageUOM1.Text.Trim();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtStockPackageUOM2_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.lblStockPackageSize_PackageUOM2.Text = this.txtStockPackageUOM2.Text.Trim();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region Button Events

                    private void btnSearch_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtStockSearch.Text.Contains(" ") == true)
                            {
                                this.txtStockSearch.Text = this.txtStockSearch.Text.Remove(0, this.txtStockSearch.Text.IndexOf(" ") + 1);
                            }
                            this._SearchType = 1;
                            //SearchStock();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnClose_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdd_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.ShowDetail(false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnEdit_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.ShowDetail(true);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnDelete_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstStockListing.SelectedItems.Count > 0)
                            {
                                this._SaveType_Stock = STP.Logic.Common.SaveType.Delete;
                                if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    Global.ShowWaiting(this, true);
                                    if (this.DeleteStock() == true)
                                    {
                                        MessageBox.Show("Delete Success", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        //SearchStock();
                                        if (this.txtStockSearch.Text.Trim() != String.Empty)
                                        {
                                            this._SearchType = 1;
                                        }
                                        else
                                        {
                                            String category, subCategory;
                                            if (this.lstCategory.SelectedItems.Count > 0)
                                            {
                                                category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                                subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                                if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                                                {
                                                    this._SearchType = 0;
                                                }
                                                else
                                                {
                                                    this._SearchType = 1;
                                                }
                                            }
                                            else
                                            {
                                                this._SearchType = 0;
                                            }
                                        }
                                        String[] sortInfo = null;
                                        int columnIndex = 0;
                                        if (this._Current_Sort != String.Empty)
                                        {
                                            sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                            columnIndex = int.Parse(sortInfo[2]);
                                            if (sortInfo[1] == "ASC")
                                            {
                                                this._Current_Sort = String.Empty;
                                            }
                                            else
                                            {
                                                this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                            }
                                        }
                                        ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                                        this.BuildAutoComplete();
                                        this.lstStockListing.Focus();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Delete Failed", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    }
                                    Global.ShowWaiting(this, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSave_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                if (this.ValidationForm() == true)
                                {
                                    Global.ShowWaiting(this, true);
                                    Boolean updateTransaction = false;
                                    if (this._SaveType_Stock == STP.Logic.Common.SaveType.Update)
                                    {
                                        if (DialogResult.Yes == MessageBox.Show("Apply Changes to Existing Transaction?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                        {
                                            updateTransaction = true;
                                        }
                                    }
                                    if (this.SaveStock(updateTransaction) == true)
                                    {
                                        switch (this._SaveType_Stock)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                        this.HideDetail();
                                        //SearchStock();
                                        if (this.txtStockSearch.Text.Trim() != String.Empty)
                                        {
                                            this._SearchType = 1;
                                        }
                                        else
                                        {
                                            String category, subCategory;
                                            if (this.lstCategory.SelectedItems.Count > 0)
                                            {
                                                category = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                                subCategory = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                                if ((category == "0") && (subCategory == "0") && (this.txtStockSearch.Text.Trim() == String.Empty))
                                                {
                                                    this._SearchType = 0;
                                                }
                                                else
                                                {
                                                    this._SearchType = 1;
                                                }
                                            }
                                            else
                                            {
                                                this._SearchType = 0;
                                            }
                                        }
                                        String[] sortInfo = null;
                                        int columnIndex = 0;
                                        if (this._Current_Sort != String.Empty)
                                        {
                                            sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                            columnIndex = int.Parse(sortInfo[2]);
                                            if (sortInfo[1] == "ASC")
                                            {
                                                this._Current_Sort = String.Empty;
                                            }
                                            else
                                            {
                                                this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                            }
                                        }
                                        else
                                        {
                                            this._Current_Sort = this.lstStockListing.Columns[0].Name + "[..]DESC[..]" + this.lstStockListing.Columns[0].Index.ToString();
                                        }
                                        ND.UI.Winform.Form.SortListView(ref this.lstStockListing, ref this._Current_Sort, columnIndex, this.ldm_BuildStockList);
                                        this.BuildAutoComplete();
                                        this.lstStockListing.Focus();
                                    }
                                    else
                                    {
                                        switch (this._SaveType_Stock)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                    }
                                    Global.ShowWaiting(this, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnCancel_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.HideDetail();
                            this.lstStockListing.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

            #region Receive

                #region RadioButton Events

                    private void rbtReceiveNew_CheckedChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.rbtReceiveNew.Checked == true)
                            {
                                this.btnReceiveSelect.Enabled = false;
                                this._SaveType_Receive = STP.Logic.Common.SaveType.Insert;
                                if (!this._Logic_Transaction_Receive.CheckDetailDataRowEmpty())
                                {
                                    if (DialogResult.Yes == MessageBox.Show("Clear Data?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                    {
                                        this.ClearReceiveForm();
                                    }
                                }
                                else
                                {
                                    this.ClearReceiveForm();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void rbtReceiveEdit_CheckedChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.rbtReceiveEdit.Checked == true)
                            {
                                this.btnReceiveSelect.Enabled = true;
                                this._SaveType_Receive = STP.Logic.Common.SaveType.Update;
                                if (!this._Logic_Transaction_Receive.CheckDetailDataRowEmpty())
                                {
                                    if (DialogResult.Yes == MessageBox.Show("Clear Data?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                    {
                                        this.ClearReceiveForm();
                                    }
                                }
                                else
                                {
                                    this.ClearReceiveForm();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region TextBox Events

                    private void txtReceiveStockName_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                this.txtReceiveQuantity.Focus();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtReceiveStockName_Leave(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtReceiveStockName.Text.Trim() != String.Empty)
                            {
                                if (this.txtReceiveStockName.Text.Contains("---") == true)
                                {
                                    this.txtReceiveStockName.Text = this.txtReceiveStockName.Text.Split(new String[] { "---" }, StringSplitOptions.None)[1];
                                }
                                this.GetStockID(this.txtReceiveStockCategory, this.txtReceiveSubCat, this.txtReceiveStockID, this.txtReceiveStockCode);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtReceiveTransactionNo_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtReceiveTransactionNo.Text != String.Empty)
                            {
                                this.ShowReceiveTransaction();
                            }
                            else
                            {
                                this.ClearReceiveForm();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtReceiveStockID_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtReceiveStockID.Text != String.Empty)
                            {
                                this.ShowReceive();
                            }
                            else
                            {
                                //ClearReceiveSelection();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtReceiveQuantity_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                if (this.ValidationFormReceive() == true)
                                {
                                    this.AddtoReceive();
                                    this.ClearReceiveSelection();
                                    this.BuildReceiveList();
                                    this.txtReceiveStockName.Focus();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region ListView Events

                    private void lstReceiveListing_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.RemoveReceiveEnable(this.lstReceiveListing.SelectedItems.Count > 0);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstReceiveListing_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstReceiveListing, ref this._Current_Receive_Sort, e.Column, this.ldm_BuildReceiveList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region Button Events

                    private void btnReceiveSelect_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            frmSelectTransaction selectTransactionForm = new frmSelectTransaction();
                            selectTransactionForm.Owner = this;
                            selectTransactionForm.TransType = STP.Logic.Common.TransType.Receive;
                            Global.ShowWaiting(this, true);
                            Application.DoEvents();
                            selectTransactionForm.ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveSearchItem_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            frmSelectStock selectStockForm = new frmSelectStock();
                            selectStockForm.Owner = this;
                            Global.ShowWaiting(this, true);
                            selectStockForm.ShowDialog();
                            this.txtReceiveQuantity.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveAdd_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.ValidationFormReceive() == true)
                            {
                                this.AddtoReceive();
                                this.ClearReceiveSelection();
                                this.BuildReceiveList();
                                this.txtReceiveStockName.Focus();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveRemove_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstReceiveListing.SelectedItems.Count > 0)
                            {
                                this._Logic_Transaction_Receive.RemoveDetailDataRow(int.Parse(this.lstReceiveListing.SelectedItems[0].SubItems[1].Text));
                            }
                            this.BuildReceiveList();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveSave_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                if (this.ValidationFormReceiveTransaction() == true)
                                {
                                    Global.ShowWaiting(this, true);
                                    Application.DoEvents();
                                    this._Logic_Transaction_Receive.Save_Type = this._SaveType_Receive;
                                    this._Logic_Transaction_Receive.Trans_Type = STP.Logic.Common.TransType.Receive;

                                    this._Logic_Transaction_Receive.Object_Transaction_Header = new STP.Base.Transaction_Header_Object();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Transaction_No = this.txtReceiveTransactionNo.Text.Trim();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Customer_Name = this.txtReceiveFrom.Text.Trim();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Transaction_DateTime = DateTime.Parse(String.Format("{0:dd/MMM/yyyy}", this.dtpReceiveTransactionDate.Value));
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Document_No = this.txtReceiveDocumentNo.Text.Trim();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Remark = this.txtReceiveRemark.Text.Trim();
                                    if (this._Logic_Transaction_Receive.SaveTransaction() == true)
                                    {
                                        Application.DoEvents();
                                        switch (this._SaveType_Receive)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                        this._SaveType_Receive = STP.Logic.Common.SaveType.Insert;
                                        this.ClearReceiveForm();
                                        this.rbtReceiveNew.Checked = true;
                                    }
                                    else
                                    {
                                        Application.DoEvents();
                                        switch (this._SaveType_Receive)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                    }
                                    Global.ShowWaiting(this, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveClose_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveClear_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Clear?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                this.ClearReceiveForm();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnReceiveDelete_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                if (this.ValidationFormReceiveTransactionDelete() == true)
                                {                                    
                                    Global.ShowWaiting(this, true);
                                    Application.DoEvents();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header = new STP.Base.Transaction_Header_Object();
                                    this._Logic_Transaction_Receive.Object_Transaction_Header.Transaction_No = this.txtReceiveTransactionNo.Text.Trim();

                                    if (this._Logic_Transaction_Receive.DeleteTransaction() == true)
                                    {
                                        Global.ShowWaiting(this, false);
                                        Application.DoEvents();
                                        MessageBox.Show("Delete Successfully", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                        this._SaveType_Receive = STP.Logic.Common.SaveType.Insert;
                                        this.ClearReceiveForm();
                                        this.rbtReceiveNew.Checked = true;
                                    }
                                    else
                                    {
                                        Global.ShowWaiting(this, false);
                                        Application.DoEvents();
                                        MessageBox.Show("Delete Failed", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region ComboBox Events

                    private void cmbReceiveUnit_SelectedIndexChanged(object sender, EventArgs e)
                    {

                    }

                #endregion

            #endregion

            #region Adjustment

                #region ListView Events

                    private void lstAdjustmentListing_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.RemoveAdjustmentEnable(this.lstAdjustmentListing.SelectedItems.Count > 0);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstAdjustmentList_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstAdjustmentListing, ref this._Current_Adjustment_Sort, e.Column, this.ldm_BuildAdjustmentList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region TextBox Events

                    private void txtAdjustmentStockName_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                this.txtAdjustmentNewBalance.Focus();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtAdjustmentStockName_Leave(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtAdjustmentStockName.Text.Trim() != String.Empty)
                            {
                                if (this.txtAdjustmentStockName.Text.Contains("---") == true)
                                {
                                    this.txtAdjustmentStockName.Text = this.txtAdjustmentStockName.Text.Split(new String[] { "---" }, StringSplitOptions.None)[1];
                                }
                                this.GetStockID(this.txtAdjustmentStockCategory, this.txtAdjustmentSubCat, this.txtAdjustmentStockID, this.txtAdjustmentStockCode);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtAdjustmentStockID_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.txtAdjustmentStockID.Text != String.Empty)
                            {
                                this.ShowAdjustment();
                            }
                            else
                            {
                                //ClearAdjustmentSelection();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtAdjustmentNewBalance_TextChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            this.CalculateAdjustmentQuantity();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void txtAdjustmentNewBalance_KeyDown(object sender, KeyEventArgs e)
                    {
                        try
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                e.SuppressKeyPress = true;
                                if (this.ValidationFormAdjustment() == true)
                                {
                                    this.AddtoAdjustment();
                                    this.ClearAdjustmentSelection();
                                    this.BuildAdjustmentList();
                                    this.txtAdjustmentStockName.Focus();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region Button Events

                    private void btnAdjustmentSearch_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            frmSelectStock selectStockForm = new frmSelectStock();
                            selectStockForm.Owner = this;
                            Global.ShowWaiting(this, true);
                            selectStockForm.ShowDialog();
                            this.txtAdjustmentNewBalance.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdjustmentClose_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdjustmentAdd_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.ValidationFormAdjustment() == true)
                            {
                                this.AddtoAdjustment();
                                this.ClearAdjustmentSelection();
                                this.BuildAdjustmentList();
                                this.txtAdjustmentStockName.Focus();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdjustmentRemove_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstAdjustmentListing.SelectedItems.Count > 0)
                            {
                                this._Logic_Transaction_Adjustment.RemoveDetailDataRow(int.Parse(this.lstAdjustmentListing.SelectedItems[0].SubItems[1].Text));
                            }
                            this.BuildAdjustmentList();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdjustmentSave_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                if (this.ValidationFormAdjustmentTransaction() == true)
                                {
                                    Global.ShowWaiting(this, true);
                                    Application.DoEvents();
                                    this._Logic_Transaction_Adjustment.Save_Type = this._SaveType_Adjustment;
                                    this._Logic_Transaction_Adjustment.Trans_Type = STP.Logic.Common.TransType.Adjustment;

                                    this._Logic_Transaction_Adjustment.Object_Transaction_Header = new STP.Base.Transaction_Header_Object();
                                    this._Logic_Transaction_Adjustment.Object_Transaction_Header.Transaction_DateTime = DateTime.Now;
                                    if (this._Logic_Transaction_Adjustment.SaveTransaction() == true)
                                    {                                        
                                        Application.DoEvents();
                                        switch (this._SaveType_Adjustment)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                        this._SaveType_Adjustment = STP.Logic.Common.SaveType.Insert;
                                        this.ClearAdjustmentForm();
                                    }
                                    else
                                    {
                                        Application.DoEvents();
                                        switch (this._SaveType_Adjustment)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                    }
                                    Global.ShowWaiting(this, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnAdjustmentClear_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Clear?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                this.ClearAdjustmentForm();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

            #region Category

                #region ListView Events

                    private void lstCategoryCategory_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstCategoryCategory, ref this._Current_Category_Category_Sort, e.Column, this.ldm_BuildCategoryCategoryList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategorySubCategory_ColumnClick(object sender, ColumnClickEventArgs e)
                    {
                        try
                        {
                            ND.UI.Winform.Form.SortListView(ref this.lstCategorySubCategory, ref this._Current_Category_SubCategory_Sort, e.Column, this.ldm_BuildCategorySubCategoryList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategoryCategory_DoubleClick(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategoryCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Category = STP.Logic.Common.SaveType.Update;
                                this.EnableCategoryPanel(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategorySubCategory_DoubleClick(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategorySubCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Sub_Category = STP.Logic.Common.SaveType.Update;
                                this.EnableSubCategoryPanel(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategoryCategory_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategoryCategory.SelectedItems.Count > 0)
                            {
                                this.ShowCategory();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void lstCategorySubCategory_SelectedIndexChanged(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategorySubCategory.SelectedItems.Count > 0)
                            {
                                this.ShowSubCategory();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region Button Events

                    private void btnCategoryClose_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }
                    
                    private void btnCategoryNew_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this._SaveType_Category = STP.Logic.Common.SaveType.Insert;
                            this.ClearCategoryPanel();
                            this.EnableCategoryPanel(true);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnCategoryEdit_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategoryCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Category = STP.Logic.Common.SaveType.Update;
                                this.EnableCategoryPanel(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnCategoryDelete_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategoryCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Category = STP.Logic.Common.SaveType.Delete;
                                if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    if (this.DeleteCategory() == true)
                                    {
                                        this.ClearCategoryPanel();
                                        this._SaveType_Category = STP.Logic.Common.SaveType.None;
                                        this.EnableCategoryPanel(false);
                                        String[] sortInfo = null;
                                        int columnIndex = 0;
                                        if (this._Current_Category_Sort != String.Empty)
                                        {
                                            sortInfo = this._Current_Category_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                            columnIndex = int.Parse(sortInfo[2]);
                                            if (sortInfo[1] == "ASC")
                                            {
                                                this._Current_Category_Sort = String.Empty;
                                            }
                                            else
                                            {
                                                this._Current_Category_Sort = this._Current_Category_Sort.Replace("DESC", "ASC");
                                            }
                                        }
                                        else
                                        {
                                            this._Current_Category_Sort = this.lstCategoryCategory.Columns[0].Name + "[..]DESC[..]" + this.lstCategoryCategory.Columns[0].Index.ToString();
                                        }
                                        ND.UI.Winform.Form.SortListView(ref this.lstCategoryCategory, ref this._Current_Category_Sort, columnIndex, this.ldm_BuildCategoryCategoryList);
                                        this.BuildCategoryList();
                                        this.lstCategory.Items[0].Selected = true;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnCategorySave_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                            {
                                if (this.ValidationCategoryForm() == true)
                                {
                                    Global.ShowWaiting(this, true);
                                    Boolean updateTransaction = false;
                                    if (this._SaveType_Category == STP.Logic.Common.SaveType.Update)
                                    {
                                        if (DialogResult.Yes == MessageBox.Show("Apply Changes to Existing Transaction?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                        {
                                            updateTransaction = true;
                                        }
                                    }
                                    if (this.SaveCategory(updateTransaction) == true)
                                    {
                                        switch (this._SaveType_Category)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }

                                        this.ClearCategoryPanel();
                                        this._SaveType_Category = STP.Logic.Common.SaveType.None;
                                        this.EnableCategoryPanel(false);
                                        String[] sortInfo = null;
                                        int columnIndex = 0;
                                        if (this._Current_Category_Sort != String.Empty)
                                        {
                                            sortInfo = this._Current_Category_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                            columnIndex = int.Parse(sortInfo[2]);
                                            if (sortInfo[1] == "ASC")
                                            {
                                                this._Current_Category_Sort = String.Empty;
                                            }
                                            else
                                            {
                                                this._Current_Category_Sort = this._Current_Category_Sort.Replace("DESC", "ASC");
                                            }
                                        }
                                        else
                                        {
                                            this._Current_Category_Sort = this.lstCategoryCategory.Columns[0].Name + "[..]DESC[..]" + this.lstCategoryCategory.Columns[0].Index.ToString();
                                        }
                                        ND.UI.Winform.Form.SortListView(ref this.lstCategoryCategory, ref this._Current_Category_Sort, columnIndex, this.ldm_BuildCategoryCategoryList);
                                        this.BuildCategoryList();
                                        this.lstCategory.Items[0].Selected = true;
                                    }
                                    else
                                    {
                                        switch (this._SaveType_Category)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                    }
                                    Global.ShowWaiting(this, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnCategoryCancel_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.lstCategoryCategory.Items[0].Selected = true;
                            this.lstCategoryCategory.Items[0].Selected = false;
                            this.ClearCategoryPanel();
                            this._SaveType_Category = STP.Logic.Common.SaveType.None;
                            this.EnableCategoryPanel(false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSubCategoryNew_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this._SaveType_Sub_Category = STP.Logic.Common.SaveType.Insert;
                            this.ClearSubCategoryPanel();
                            this.EnableSubCategoryPanel(true);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSubCategoryEdit_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategorySubCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Sub_Category = STP.Logic.Common.SaveType.Update;
                                this.EnableSubCategoryPanel(true);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSubCategoryDelete_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if (this.lstCategorySubCategory.SelectedItems.Count > 0)
                            {
                                this._SaveType_Sub_Category = STP.Logic.Common.SaveType.Delete;
                                if (this._Logic_Stock.CheckSubCategoryAssociate(int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[0].Text), int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[1].Text)) == 0)
                                {
                                    if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                    {
                                        if (this.DeleteSubCategory() == true)
                                        {
                                            this.ClearSubCategoryPanel();
                                            this._SaveType_Sub_Category = STP.Logic.Common.SaveType.None;
                                            this.EnableSubCategoryPanel(false);
                                            String[] sortInfo = null;
                                            int columnIndex = 0;
                                            if (this._Current_SubCategory_Sort != String.Empty)
                                            {
                                                sortInfo = this._Current_SubCategory_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                                columnIndex = int.Parse(sortInfo[2]);
                                                if (sortInfo[1] == "ASC")
                                                {
                                                    this._Current_SubCategory_Sort = String.Empty;
                                                }
                                                else
                                                {
                                                    this._Current_SubCategory_Sort = this._Current_SubCategory_Sort.Replace("DESC", "ASC");
                                                }
                                            }
                                            else
                                            {
                                                this._Current_SubCategory_Sort = this.lstCategorySubCategory.Columns[0].Name + "[..]DESC[..]" + this.lstCategorySubCategory.Columns[0].Index.ToString();
                                            }
                                            ND.UI.Winform.Form.SortListView(ref this.lstCategorySubCategory, ref this._Current_SubCategory_Sort, columnIndex, this.ldm_BuildCategorySubCategoryList);
                                            this.BuildCategoryList();
                                            this.lstCategory.Items[0].Selected = true;
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("This Sub Category is associated with 1 or more Stock, please remove them from this Sub Category before deleting it.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSubCategorySave_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            if ((this.CheckSubCategoryCategoryChanged() == false) || ((this.CheckSubCategoryCategoryChanged() == true) && (this._Logic_Stock.CheckSubCategoryAssociate(int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[0].Text), int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[1].Text)) == 0)))
                            {
                                if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    if (this.ValidationSubCategoryForm() == true)
                                    {
                                        Global.ShowWaiting(this, true);
                                        Boolean updateTransaction = false;
                                        if (this._SaveType_Sub_Category == STP.Logic.Common.SaveType.Update)
                                        {
                                            if (DialogResult.Yes == MessageBox.Show("Apply Changes to Existing Transaction?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                            {
                                                updateTransaction = true;
                                            }
                                        }
                                        if (this.SaveSubCategory(updateTransaction) == true)
                                        {

                                            switch (this._SaveType_Sub_Category)
                                            {
                                                case STP.Logic.Common.SaveType.Insert:
                                                    {
                                                        MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                case STP.Logic.Common.SaveType.Update:
                                                    {
                                                        MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                            }

                                            this.ClearSubCategoryPanel();
                                            this._SaveType_Sub_Category = STP.Logic.Common.SaveType.None;
                                            this.EnableSubCategoryPanel(false);
                                            String[] sortInfo = null;
                                            int columnIndex = 0;
                                            if (this._Current_SubCategory_Sort != String.Empty)
                                            {
                                                sortInfo = this._Current_SubCategory_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                                columnIndex = int.Parse(sortInfo[2]);
                                                if (sortInfo[1] == "ASC")
                                                {
                                                    this._Current_SubCategory_Sort = String.Empty;
                                                }
                                                else
                                                {
                                                    this._Current_SubCategory_Sort = this._Current_SubCategory_Sort.Replace("DESC", "ASC");
                                                }
                                            }
                                            else
                                            {
                                                this._Current_SubCategory_Sort = this.lstCategorySubCategory.Columns[0].Name + "[..]DESC[..]" + this.lstCategorySubCategory.Columns[0].Index.ToString();
                                            }
                                            ND.UI.Winform.Form.SortListView(ref this.lstCategorySubCategory, ref this._Current_SubCategory_Sort, columnIndex, this.ldm_BuildCategorySubCategoryList);
                                            this.BuildCategoryList();
                                            this.lstCategory.Items[0].Selected = true;
                                        }
                                        else
                                        {
                                            switch (this._SaveType_Sub_Category)
                                            {
                                                case STP.Logic.Common.SaveType.Insert:
                                                    {
                                                        MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                                case STP.Logic.Common.SaveType.Update:
                                                    {
                                                        MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                        break;
                                                    }
                                            }
                                        }
                                        Global.ShowWaiting(this, false);
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("This Sub Category is associated with 1 or more Stock, please remove them from this Sub Category before changing the Category of this Sub Category.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void btnSubCategoryCancel_Click(object sender, EventArgs e)
                    {
                        try
                        {
                            this.lstCategorySubCategory.Items[0].Selected = true;
                            this.lstCategorySubCategory.Items[0].Selected = false;
                            this.ClearSubCategoryPanel();
                            this._SaveType_Sub_Category = STP.Logic.Common.SaveType.None;
                            this.EnableSubCategoryPanel(false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

        #endregion

        #region Local Functions and Procedures

            #region General

                #region Constructor

                    public frmStock()
                    {
                        try
                        {
                            this.InitializeComponent();
                            this.Text = "Stock";
                            this._Logic_Stock = new STP.Logic.Stock();
                            this._Logic_Transaction_Receive = new STP.Logic.Transaction();
                            this._Logic_Transaction_Transfer = new STP.Logic.Transaction();
                            this._Logic_Transaction_Adjustment = new STP.Logic.Transaction();

                            this._ValidatorNew = new ND.Standard.Validation.Winform.Form();
                            this._ValidatorEdit = new ND.Standard.Validation.Winform.Form();

                            this._ValidatorReceive = new ND.Standard.Validation.Winform.Form();
                            this._ValidatorReceiveTransactionNew = new ND.Standard.Validation.Winform.Form();
                            this._ValidatorReceiveTransactionEdit = new ND.Standard.Validation.Winform.Form();
                            this._ValidatorReceiveTransactionDelete = new ND.Standard.Validation.Winform.Form();

                            this._ValidatorAdjustment = new ND.Standard.Validation.Winform.Form();
                            this._ValidatorAdjustmentTransaction = new ND.Standard.Validation.Winform.Form();

                            this._Validator_Category_New = new ND.Standard.Validation.Winform.Form();
                            this._Validator_Category_Edit = new ND.Standard.Validation.Winform.Form();
                            this._Validator_Sub_Category_New = new ND.Standard.Validation.Winform.Form();
                            this._Validator_Sub_Category_Edit = new ND.Standard.Validation.Winform.Form();

                            this.ct_txtStockName = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtStockName_Validate);
                            this.ct_lstReceiveList = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstReceiveList_Validate);
                            this.ct_lstAdjustmentList = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstAdjustmentList_Validate);
                            this.ct_lstAdjustmentListExist = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstAdjustmentListExist_Validate);
                            this.ct_dtpReceiveTransactionDate = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_dtpReceiveTransactionDate_Validate);
                            this.ct_txtCategoryName = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtCategoryName_Validate);
                            this.ct_txtSubCategoryName = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtSubCategoryName_Validate);
                            this.ct_txtSubCategoryShortcut = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtSubCategoryShortcut_Validate);

                            this.ldm_BuildStockList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchStock);
                            this.ldm_BuildCategoryList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildCategoryList);
                            this.ldm_BuildSubCategoryList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildSubCategoryList);
                            this.ldm_BuildReceiveList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildReceiveList);
                            this.ldm_BuildAdjustmentList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildAdjustmentList);
                            this.ldm_BuildCategoryCategoryList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildCategoryCategoryList);
                            this.ldm_BuildCategorySubCategoryList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildCategorySubCategoryList);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

                #region Methods

                    private void LoopTab()
                    {
                        try
                        {
                            int iCount = 0;
                            for (iCount = 0; iCount < this.tabMain.TabCount; iCount++)
                            {
                                this.tabMain.SelectedIndex = iCount;
                            }
                            this.tabMain.SelectedIndex = 0;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildAutoComplete()
                    {
                        try
                        {
                            this.txtStockSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            this.txtStockSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            this.txtStockSearch.AutoCompleteCustomSource = this._Logic_Stock.StringCollection();

                            this.txtReceiveStockName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            this.txtReceiveStockName.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            this.txtReceiveStockName.AutoCompleteCustomSource = this._Logic_Stock.StringCollection();

                            this.txtAdjustmentStockName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                            this.txtAdjustmentStockName.AutoCompleteSource = AutoCompleteSource.CustomSource;
                            this.txtAdjustmentStockName.AutoCompleteCustomSource = this._Logic_Stock.StringCollection();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void CloseForm(FormClosingEventArgs e)
                    {
                        try
                        {
                            if (Global.ForceClose == false)
                            {
                                if ((this._Logic_Transaction_Adjustment.CheckDetailDataRowEmpty() == false) || (this._Logic_Transaction_Receive.CheckDetailDataRowEmpty() == false) || (this._Logic_Transaction_Transfer.CheckDetailDataRowEmpty() == false))
                                {
                                    String title = "Close";
                                    String messageBody = "Close the Form?";

                                    if (DialogResult.Yes != MessageBox.Show(messageBody, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                    {
                                        e.Cancel = true;
                                    }
                                    else
                                    {
                                        ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                                    }
                                }
                                else
                                {
                                    ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildButton()
                    {
                        try
                        {
                            this.btnSearch.Image = Image.FromFile("image/Search.png");
                            this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdd.Image = Image.FromFile("image/New.png");
                            this.btnAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnEdit.Image = Image.FromFile("image/Edit.png");
                            this.btnEdit.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnDelete.Image = Image.FromFile("image/Delete.png");
                            this.btnDelete.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnClose.Image = Image.FromFile("image/Cancel.png");
                            this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSave.Image = Image.FromFile("image/Confirm.png");
                            this.btnSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCancel.Image = Image.FromFile("image/Cancel.png");
                            this.btnCancel.TextImageRelation = TextImageRelation.ImageBeforeText;

                            this.btnReceiveSelect.Image = Image.FromFile("image/Search.png");
                            this.btnReceiveSelect.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveSearchItem.Image = Image.FromFile("image/Search.png");
                            this.btnReceiveSearchItem.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveAdd.Image = Image.FromFile("image/New.png");
                            this.btnReceiveAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveRemove.Image = Image.FromFile("image/Delete.png");
                            this.btnReceiveRemove.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveClose.Image = Image.FromFile("image/Cancel.png");
                            this.btnReceiveClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveSave.Image = Image.FromFile("image/Confirm.png");
                            this.btnReceiveSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveClear.Image = Image.FromFile("image/Refresh.png");
                            this.btnReceiveClear.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnReceiveDelete.Image = Image.FromFile("image/Delete.png");
                            this.btnReceiveDelete.TextImageRelation = TextImageRelation.ImageBeforeText;

                            this.btnAdjustmentSearchItem.Image = Image.FromFile("image/Search.png");
                            this.btnAdjustmentSearchItem.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdjustmentAdd.Image = Image.FromFile("image/New.png");
                            this.btnAdjustmentAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdjustmentRemove.Image = Image.FromFile("image/Delete.png");
                            this.btnAdjustmentRemove.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdjustmentClose.Image = Image.FromFile("image/Cancel.png");
                            this.btnAdjustmentClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdjustmentSave.Image = Image.FromFile("image/Confirm.png");
                            this.btnAdjustmentSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnAdjustmentClear.Image = Image.FromFile("image/Refresh.png");
                            this.btnAdjustmentClear.TextImageRelation = TextImageRelation.ImageBeforeText;

                            this.btnCategoryNew.Image = Image.FromFile("image/New.png");
                            this.btnCategoryNew.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCategoryEdit.Image = Image.FromFile("image/Edit.png");
                            this.btnCategoryEdit.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCategoryDelete.Image = Image.FromFile("image/Delete.png");
                            this.btnCategoryDelete.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCategorySave.Image = Image.FromFile("image/Confirm.png");
                            this.btnCategorySave.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCategoryCancel.Image = Image.FromFile("image/Cancel.png");
                            this.btnCategoryCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSubCategoryNew.Image = Image.FromFile("image/New.png");
                            this.btnSubCategoryNew.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSubCategoryEdit.Image = Image.FromFile("image/Edit.png");
                            this.btnSubCategoryEdit.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSubCategoryDelete.Image = Image.FromFile("image/Delete.png");
                            this.btnSubCategoryDelete.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSubCategorySave.Image = Image.FromFile("image/Confirm.png");
                            this.btnSubCategorySave.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnSubCategoryCancel.Image = Image.FromFile("image/Cancel.png");
                            this.btnSubCategoryCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                            this.btnCategoryClose.Image = Image.FromFile("image/Cancel.png");
                            this.btnCategoryClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void SetValidation()
                    {
                        try
                        {
                            this._ValidatorNew.AddValidator(this.cmbCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorNew.AddValidator(this.cmbSubCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorNew.AddValidator(this.txtStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorNew.AddValidator(this.txtStockPrice, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorNew.AddValidator(this.txtStockUOM, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorNew.AddValidator(this.txtStockPackageSize1, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                            this._ValidatorNew.AddValidator(this.txtStockPackageSize2, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                            this._ValidatorNew.AddValidator(this.txtStockPackagePrice1, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorNew.AddValidator(this.txtStockPackagePrice2, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorNew.AddValidator(this.txtStockPrice, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorNew.AddValidator(this.txtStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtStockName);

                            this._ValidatorEdit.AddValidator(this.cmbCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorEdit.AddValidator(this.cmbSubCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorEdit.AddValidator(this.txtStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorEdit.AddValidator(this.txtStockPrice, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorEdit.AddValidator(this.txtStockUOM, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorEdit.AddValidator(this.txtStockPackageSize1, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                            this._ValidatorEdit.AddValidator(this.txtStockPackageSize2, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                            this._ValidatorEdit.AddValidator(this.txtStockPackagePrice1, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorEdit.AddValidator(this.txtStockPackagePrice2, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorEdit.AddValidator(this.txtStockPrice, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                            this._ValidatorEdit.AddValidator(this.txtStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtStockName);

                            this._ValidatorAdjustment.AddValidator(this.txtAdjustmentStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorAdjustment.AddValidator(this.txtAdjustmentNewBalance, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorAdjustment.AddValidator(this.txtAdjustmentNewBalance, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                            this._ValidatorAdjustment.AddValidator(this.lstAdjustmentListing, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstAdjustmentListExist);

                            this._ValidatorAdjustmentTransaction.AddValidator(this.lstAdjustmentListing, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstAdjustmentList);

                            this._ValidatorReceive.AddValidator(this.txtReceiveStockName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorReceive.AddValidator(this.txtReceiveQuantity, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorReceive.AddValidator(this.txtReceiveQuantity, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);

                            this._ValidatorReceiveTransactionNew.AddValidator(this.lstReceiveListing, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstReceiveList);
                            this._ValidatorReceiveTransactionNew.AddValidator(this.dtpReceiveTransactionDate, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_dtpReceiveTransactionDate);

                            this._ValidatorReceiveTransactionEdit.AddValidator(this.txtReceiveTransactionNo, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._ValidatorReceiveTransactionEdit.AddValidator(this.lstReceiveListing, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstReceiveList);
                            this._ValidatorReceiveTransactionEdit.AddValidator(this.dtpReceiveTransactionDate, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_dtpReceiveTransactionDate);

                            this._ValidatorReceiveTransactionDelete.AddValidator(this.txtReceiveTransactionNo, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);

                            this._Validator_Category_New.AddValidator(this.txtCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Category_New.AddValidator(this.txtCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtCategoryName);

                            this._Validator_Category_Edit.AddValidator(this.txtCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Category_Edit.AddValidator(this.txtCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtCategoryName);

                            this._Validator_Sub_Category_New.AddValidator(this.txtSubCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Sub_Category_New.AddValidator(this.cmbSubCategoryCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Sub_Category_New.AddValidator(this.txtSubCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtSubCategoryName);

                            this._Validator_Sub_Category_Edit.AddValidator(this.txtSubCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Sub_Category_Edit.AddValidator(this.cmbSubCategoryCategory, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                            this._Validator_Sub_Category_Edit.AddValidator(this.txtSubCategoryName, this.erpStock, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtSubCategoryName);

                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    public void GetStockID(TextBox textCategory, TextBox textSubCat, TextBox textID, TextBox textCode)
                    {
                        try
                        {
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            ND.Data.ConditionField conditionFieldStockName = new ND.Data.ConditionField();
                            conditionFieldStockName.Field = "stockname";
                            if (textID == this.txtAdjustmentStockID)
                            {
                                conditionFieldStockName.Value = this.txtAdjustmentStockName.Text.Trim();
                            }
                            else if (textID == this.txtReceiveStockID)
                            {
                                conditionFieldStockName.Value = this.txtReceiveStockName.Text.Trim();
                            }
                            conditionFieldStockName.DataType = ND.Data.DataObject.DataType.NString;
                            conditionFieldStockName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockName);

                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock = this._Logic_Stock.GetStock(conditions);

                            if (objectStock != null)
                            {
                                textCategory.Text = objectStock.Stock_Category.ToString();
                                textSubCat.Text = objectStock.Stock_Sub_Category.ToString();
                                textID.Text = objectStock.ID.ToString();
                                textCode.Text = objectStock.Stock_Code.ToString();
                            }
                            else
                            {
                                textCategory.Text = String.Empty;
                                textSubCat.Text = String.Empty;
                                textID.Text = String.Empty;
                                textCode.Text = String.Empty;
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

            #region Listing

                #region Validations

                    private Boolean ct_txtStockName_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            ND.Data.ConditionField conditionFieldStockName = new ND.Data.ConditionField();
                            conditionFieldStockName.Field = "stockname";
                            conditionFieldStockName.Value = this.txtStockName.Text.Trim().Replace(" ", String.Empty);
                            conditionFieldStockName.DataType = ND.Data.DataObject.DataType.NString;
                            conditionFieldStockName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldStockName);
                            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
                            conditionFieldId.Field = "id";
                            conditionFieldId.Value = this.txtStockID.Text.Trim();
                            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.NotEqual;
                            conditions.Add(conditionFieldId);
                            result = !this._Logic_Stock.CheckExist(conditions);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

                #region Methods

                    private void ShowDetail(Boolean isEdit)
                    {
                        try
                        {
                            this.BuildTypeCombo();
                            this.BuildCategoryCombo();
                            this.ClearField();
                            if (isEdit == true)
                            {
                                this._SaveType_Stock = STP.Logic.Common.SaveType.Update;
                                this.LoadStock();
                            }
                            else
                            {
                                this._SaveType_Stock = STP.Logic.Common.SaveType.Insert;
                            }
                            this.pnlDetail.Visible = true;
                            this.pnlDetail.BringToFront();
                            this.txtStockName.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void HideDetail()
                    {
                        try
                        {
                            this.ClearField();
                            this._SaveType_Stock = STP.Logic.Common.SaveType.None;
                            this.pnlDetail.Visible = false;
                            this.pnlDetail.SendToBack();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void EditDeleteEnable(Boolean enable)
                    {
                        try
                        {
                            this.btnEdit.Enabled = enable;
                            this.btnDelete.Enabled = enable;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void SearchStock()
                    {
                        try
                        {
                            this.SearchStock(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void SearchStock(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            Global.ShowWaiting(this, true);
                            this.lstStockListing.Visible = false;
                            this.lstStockListing.Clear();
                            this.BuildStockListColumn();
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            switch (this._SearchType)
                            {
                                case 0:
                                    {
                                        break;
                                    }
                                case 1:
                                    {
                                        if (this.lstCategory.SelectedItems[0].SubItems[0].Text != "0")
                                        {
                                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                                            conditionFieldStockCategory.Field = "stockcategory";
                                            conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                            conditions.Add(conditionFieldStockCategory);
                                        }
                                        if (this.lstSubCategory.SelectedItems[0].SubItems[1].Text != "0")
                                        {
                                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                                            conditionFieldStockCategory.Field = "stockcategory";
                                            conditionFieldStockCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[0].Text;
                                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                            conditions.Add(conditionFieldStockCategory);
                                            ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
                                            conditionFieldStockSubCategory.Field = "stocksubcategory";
                                            conditionFieldStockSubCategory.Value = this.lstSubCategory.SelectedItems[0].SubItems[1].Text;
                                            conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                            conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                            conditions.Add(conditionFieldStockSubCategory);
                                        }
                                        ND.Data.ConditionField conditionFieldStockName = new ND.Data.ConditionField();
                                        conditionFieldStockName.Field = "stockname";
                                        conditionFieldStockName.Value = "%" + this.txtStockSearch.Text + "%";
                                        conditionFieldStockName.DataType = ND.Data.DataObject.DataType.NString;
                                        conditionFieldStockName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                        conditions.Add(conditionFieldStockName);
                                        break;
                                    }
                            }
                            this._Logic_Stock.BuildStockList(this.lstStockListing, conditions, sortType, sortField);
                            this.FormatStockListColumn();
                            this.lstStockListing.Visible = true;
                            if (this.lstStockListing.Items.Count > 0)
                            {
                                this.lstStockListing.Items[0].Selected = true;
                                this.EditDeleteEnable(true);
                            }
                            else
                            {
                                this.EditDeleteEnable(false);
                            }
                            this.lstStockListing.Focus();
                            Global.ShowWaiting(this, false);
                            this._CancelSearch = false;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildStockListColumn()
                    {
                        try
                        {
                            this.lstStockListing.Columns.Clear();
                            this.lstStockListing.Columns.Add("id", "ID");
                            this.lstStockListing.Columns.Add("stockcategory", "Stock Category");
                            this.lstStockListing.Columns.Add("stocksubcategory", "Sub Category");
                            this.lstStockListing.Columns.Add("stocktype", "Stock Type");
                            this.lstStockListing.Columns.Add("stockcode", "Code");
                            this.lstStockListing.Columns.Add("stockname", "Stock Name");
                            this.lstStockListing.Columns.Add("stockdesc1", "Stock Description 1");
                            this.lstStockListing.Columns.Add("stockdesc2", "Stock Description 2");
                            this.lstStockListing.Columns.Add("stockcost", "Stock Cost");
                            this.lstStockListing.Columns.Add("stockuom", "Unit");                                                        
                            this.lstStockListing.Columns.Add("packagesize1", "Size (A)");
                            this.lstStockListing.Columns.Add("packageuom1", "Pkg (A)");
                            this.lstStockListing.Columns.Add("packagesize2", "Size (B)");
                            this.lstStockListing.Columns.Add("packageuom2", "Pkg Unit (B)");
                            this.lstStockListing.Columns.Add("stockprice1", "Unit Price");
                            this.lstStockListing.Columns.Add("packageprice1", "SO Pkg");
                            this.lstStockListing.Columns.Add("stockprice2", "Cash Unit");
                            this.lstStockListing.Columns.Add("packageprice2", "Cash Pkg");
                            this.lstStockListing.Columns.Add("stockprice3", "Retail Unit");
                            this.lstStockListing.Columns.Add("packageprice3", "Retail Pkg");
                            this.lstStockListing.Columns.Add("stockprice4", "Special Unit");
                            this.lstStockListing.Columns.Add("packageprice4", "Special Pkg");
                            this.lstStockListing.Columns.Add("stockprice5", "Reserved Unit");
                            this.lstStockListing.Columns.Add("packageprice5", "Reserved Pkg");
                            this.lstStockListing.Columns.Add("stockbalance", "Balance");
                            this.lstStockListing.Columns.Add("guid", "GUID");
                            this.lstStockListing.Columns.Add("createddate", "Created Date");
                            this.lstStockListing.Columns.Add("flag", "Flag");
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatStockListColumn()
                    {
                        try
                        {
                            this.lstStockListing.Columns["stockcode"].Width = 50;
                            this.lstStockListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["stockname"].Width = 200;
                            this.lstStockListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["packagesize1"].Width = 50;
                            this.lstStockListing.Columns["packagesize1"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["packageuom1"].Width = 50;
                            this.lstStockListing.Columns["packageuom1"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["packagesize2"].Width = 50;
                            this.lstStockListing.Columns["packagesize2"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["packageuom2"].Width = 50;
                            this.lstStockListing.Columns["packageuom2"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["stockuom"].Width = 50;
                            this.lstStockListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Left;
                            this.lstStockListing.Columns["stockprice1"].Width = 70;
                            this.lstStockListing.Columns["stockprice1"].TextAlign = HorizontalAlignment.Right;
                            this.lstStockListing.Columns["stockbalance"].Width = 70;
                            this.lstStockListing.Columns["stockbalance"].TextAlign = HorizontalAlignment.Right;

                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["id"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockcategory"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stocksubcategory"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stocktype"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockdesc1"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockdesc2"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockcost"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice1"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice2"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice2"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice3"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice3"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice4"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice4"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["stockprice5"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["packageprice5"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["guid"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["createddate"]);
                            this.lstStockListing.Columns.Remove(this.lstStockListing.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryList()
                    {
                        try
                        {
                            this.BuildCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            Global.ShowWaiting(this, true);
                            this.lstCategory.Visible = false;
                            this.lstCategory.Clear();
                            this.BuildCategoryListColumn();
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            this._Logic_Stock.BuildCategoryList(this.lstCategory, conditions, sortType, sortField, false, true);
                            this.FormatCategoryListColumn();
                            this.lstCategory.Visible = true;
                            Global.ShowWaiting(this, false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategory.Columns.Clear();
                            this.lstCategory.Columns.Add("id", "ID");
                            this.lstCategory.Columns.Add("categoryname", "Category");
                            this.lstCategory.Columns.Add("categoryshortcut", "Code");
                            this.lstCategory.Columns.Add("createddate", "Created Date");
                            this.lstCategory.Columns.Add("flag", "Flag");
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategory.Columns["categoryname"].Width = 117;
                            this.lstCategory.Columns["categoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstCategory.Columns["categoryshortcut"].Width = 50;
                            this.lstCategory.Columns["categoryshortcut"].TextAlign = HorizontalAlignment.Left;

                            this.lstCategory.Columns.Remove(this.lstCategory.Columns["id"]);
                            this.lstCategory.Columns.Remove(this.lstCategory.Columns["createddate"]);
                            this.lstCategory.Columns.Remove(this.lstCategory.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildSubCategoryList()
                    {
                        try
                        {
                            this.BuildSubCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildSubCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            Global.ShowWaiting(this, true);
                            this.lstSubCategory.Visible = false;
                            this.lstSubCategory.Clear();
                            this.BuildSubCategoryListColumn();
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            if (this.lstCategory.SelectedItems.Count > 0)
                            {
                                if (this.lstCategory.SelectedItems[0].SubItems[0].Text != "0")
                                {
                                    ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                                    conditionFieldStockCategory.Field = "stockcategory";
                                    conditionFieldStockCategory.Value = this.lstCategory.SelectedItems[0].SubItems[0].Text;
                                    conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                                    conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                                    conditions.Add(conditionFieldStockCategory);
                                }
                            }
                            this._Logic_Stock.BuildSubCategoryList(this.lstSubCategory, conditions, sortType, sortField, false, true);
                            this.FormatSubCategoryListColumn();
                            this.lstSubCategory.Visible = true;
                            Global.ShowWaiting(this, false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildSubCategoryListColumn()
                    {
                        try
                        {
                            this.lstSubCategory.Columns.Clear();
                            this.lstSubCategory.Columns.Add("stockcategory", "Category");
                            this.lstSubCategory.Columns.Add("id", "ID");
                            this.lstSubCategory.Columns.Add("subcategoryname", "Sub Category");
                            this.lstSubCategory.Columns.Add("subcategoryshortcut", "Code");
                            this.lstSubCategory.Columns.Add("createddate", "Created Date");
                            this.lstSubCategory.Columns.Add("flag", "Flag");
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatSubCategoryListColumn()
                    {
                        try
                        {
                            this.lstSubCategory.Columns["subcategoryname"].Width = 117;
                            this.lstSubCategory.Columns["subcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstSubCategory.Columns["subcategoryshortcut"].Width = 50;
                            this.lstSubCategory.Columns["subcategoryshortcut"].TextAlign = HorizontalAlignment.Left;

                            this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["stockcategory"]);
                            this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["id"]);
                            this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["createddate"]);
                            this.lstSubCategory.Columns.Remove(this.lstSubCategory.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildSubCategoryCombo()
                    {
                        try
                        {
                            if ((this.cmbCategory.SelectedValue != null) && (ND.Data.DataObject.IsNumeric(this.cmbCategory.SelectedValue.ToString()) == true))
                            {
                                this._Logic_Stock.BuildSubCategoryCombo(this.cmbSubCategory, int.Parse(this.cmbCategory.SelectedValue.ToString()));
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryCombo()
                    {
                        try
                        {
                            this._Logic_Stock.BuildCategoryCombo(this.cmbCategory);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildTypeCombo()
                    {
                        try
                        {
                            this._Logic_Stock.BuildTypeCombo(this.cmbType);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean ValidationForm()
                    {
                        Boolean result = false;
                        try
                        {
                            switch (this._SaveType_Stock)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        result = this._ValidatorNew.Validate();
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        result = this._ValidatorEdit.Validate();
                                        break;
                                    }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ClearField()
                    {
                        Boolean result = false;
                        try
                        {
                            this.txtStockID.Text = String.Empty;
                            this.txtStockCode.Text = String.Empty;
                            this.txtStockName.Text = String.Empty;
                            this.txtStockDesc1.Text = String.Empty;
                            this.txtStockDesc2.Text = String.Empty;
                            this.txtStockUOM.Text = String.Empty;
                            this.txtStockPrice.Text = "0.00";
                            this.txtStockPackageSize1.Text = "0";
                            this.txtStockPackageUOM1.Text = String.Empty;
                            this.txtStockPackagePrice1.Text = "0.00";
                            this.txtStockPackageSize2.Text = "0";
                            this.txtStockPackageUOM2.Text = String.Empty;
                            this.txtStockPackagePrice2.Text = "0.00";
                            result = true;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private void LoadStock()
                    {
                        try
                        {
                            if (this.lstStockListing.SelectedItems.Count > 0)
                            {
                                STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                                objectStock = this._Logic_Stock.GetStock(int.Parse(this.lstStockListing.SelectedItems[0].SubItems[1].Text), int.Parse(this.lstStockListing.SelectedItems[0].SubItems[2].Text), int.Parse(this.lstStockListing.SelectedItems[0].SubItems[0].Text));
                                if (objectStock != null)
                                {
                                    this.cmbCategory.SelectedValue = objectStock.Stock_Category;
                                    this.cmbSubCategory.SelectedValue = objectStock.Stock_Sub_Category;
                                    this.cmbType.SelectedValue = objectStock.Stock_Type;
                                    this.txtStockID.Text = objectStock.ID.ToString();
                                    this.txtStockCode.Text = objectStock.Stock_Code;
                                    this.txtStockName.Text = objectStock.Stock_Name;
                                    this.txtStockDesc1.Text = objectStock.Stock_Description_1;
                                    this.txtStockDesc2.Text = objectStock.Stock_Description_2;
                                    this.txtStockUOM.Text = objectStock.Stock_UOM;
                                    this.txtStockPrice.Text = objectStock.Stock_Price_1.ToString();
                                    this.txtStockPackageSize1.Text = objectStock.Package_Size_1.ToString();
                                    this.txtStockPackageUOM1.Text = objectStock.Package_UOM_1;
                                    this.txtStockPackagePrice1.Text = objectStock.Package_Price_1.ToString();
                                    this.txtStockPackageSize2.Text = objectStock.Package_Size_2.ToString();
                                    this.txtStockPackageUOM2.Text = objectStock.Package_UOM_2;
                                    this.txtStockPackagePrice2.Text = objectStock.Package_Price_2.ToString();
                                    this.txtGUID.Text = objectStock.GUID.ToString();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean SaveStock(Boolean updateTransaction)
                    {
                        Boolean result = false;
                        try
                        {
                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock.ID = this.txtStockID.Text.Trim() == String.Empty ? (int?)null : int.Parse(this.txtStockID.Text.Trim());
                            objectStock.Stock_Category = int.Parse(this.cmbCategory.SelectedValue.ToString());
                            objectStock.Stock_Sub_Category = int.Parse(this.cmbSubCategory.SelectedValue.ToString());
                            objectStock.Stock_Type = int.Parse(this.cmbType.SelectedValue.ToString());
                            objectStock.Stock_Code = this.txtStockCode.Text.Trim();
                            objectStock.Stock_Name = this.txtStockName.Text.Trim().Replace(" ", String.Empty);
                            objectStock.Stock_Description_1 = this.txtStockDesc1.Text.Trim();
                            objectStock.Stock_Description_2 = this.txtStockDesc2.Text.Trim();
                            objectStock.Stock_UOM = this.txtStockUOM.Text.Trim();
                            objectStock.Stock_Price_1 = decimal.Parse(this.txtStockPrice.Text.Trim());
                            objectStock.Package_Size_1 = long.Parse(this.txtStockPackageSize1.Text.Trim());
                            objectStock.Package_UOM_1 = this.txtStockPackageUOM1.Text.Trim();
                            objectStock.Package_Price_1 = decimal.Parse(this.txtStockPackagePrice1.Text.Trim());
                            objectStock.Package_Size_2 = long.Parse(this.txtStockPackageSize2.Text.Trim());
                            objectStock.Package_UOM_2 = this.txtStockPackageUOM2.Text.Trim();
                            objectStock.Package_Price_2 = decimal.Parse(this.txtStockPackagePrice2.Text.Trim());
                            if (this.txtGUID.Text.Trim() != String.Empty)
                            {
                                objectStock.GUID = new Guid(this.txtGUID.Text.Trim());
                            }

                            result = this._Logic_Stock.SaveStock(objectStock, this._SaveType_Stock, updateTransaction);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean DeleteStock()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._Logic_Stock.DeleteStock(int.Parse(this.lstStockListing.SelectedItems[0].SubItems[0].Text), int.Parse(this.lstStockListing.SelectedItems[0].SubItems[1].Text), int.Parse(this.lstStockListing.SelectedItems[0].SubItems[2].Text));
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                        return result;
                    }

                #endregion

            #endregion

            #region Receive

                #region Validations

                    private Boolean ct_lstReceiveList_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            result = !this._Logic_Transaction_Receive.CheckDetailDataRowEmpty();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ct_dtpReceiveTransactionDate_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            if (this.dtpReceiveTransactionDate.Checked == true)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

                #region Methods

                    private void BuildReceiveList()
                    {
                        try
                        {
                            this.BuildReceiveList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildReceiveList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            this.lstReceiveListing.Visible = false;
                            this.lstReceiveListing.Clear();
                            this._Logic_Transaction_Receive.BuildDetailListColumn(this.lstReceiveListing);
                            this._Logic_Transaction_Receive.BuildDetailList(this.lstReceiveListing, sortType, sortField);
                            this.FormatReceiveListColumn();
                            this.lstReceiveListing.Visible = true;
                            if (this.lstReceiveListing.Items.Count > 0)
                            {
                                this.lstReceiveListing.Items[0].Selected = true;
                                this.RemoveReceiveEnable(true);
                            }
                            else
                            {
                                this.RemoveReceiveEnable(false);
                            }
                            if (this.lstReceiveListing.Items.Count > 0)
                            {
                                this.lstReceiveListing.EnsureVisible(this.lstReceiveListing.Items.Count - 1);
                            }
                            this.lstReceiveListing.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatReceiveListColumn()
                    {
                        try
                        {
                            this.lstReceiveListing.Columns["stockcategoryname"].Width = 80;
                            this.lstReceiveListing.Columns["stockcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstReceiveListing.Columns["stocksubcategoryname"].Width = 80;
                            this.lstReceiveListing.Columns["stocksubcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstReceiveListing.Columns["stockcode"].Width = 80;
                            this.lstReceiveListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                            this.lstReceiveListing.Columns["stockname"].Width = 200;
                            this.lstReceiveListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                            this.lstReceiveListing.Columns["billingqty"].Width = 80;
                            this.lstReceiveListing.Columns["billingqty"].TextAlign = HorizontalAlignment.Right;
                            this.lstReceiveListing.Columns["billinguom"].Width = 80;
                            this.lstReceiveListing.Columns["billinguom"].TextAlign = HorizontalAlignment.Left;
                            this.lstReceiveListing.Columns["stockuom"].Width = 80;
                            this.lstReceiveListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Right;
                            this.lstReceiveListing.Columns["conversionfactor"].Width = 50;
                            this.lstReceiveListing.Columns["conversionfactor"].TextAlign = HorizontalAlignment.Right;

                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["transactionno"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["transactionline"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["transactionlinetype"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["refline"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockid"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockcategory"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stocksubcategory"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stocktype"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stocktypename"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockdesc1"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockdesc2"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockprice"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["stockcost"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["actualqty"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["billingprice"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["returnedqty"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["referencetransactionno"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["referenceline"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["referencedocumentno"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["totalprice"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["oldbalance"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["newbalance"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["createddate"]);
                            this.lstReceiveListing.Columns.Remove(this.lstReceiveListing.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ClearReceiveSelection()
                    {
                        try
                        {
                            this.txtReceiveStockCategory.Text = String.Empty;
                            this.txtReceiveStockID.Text = String.Empty;
                            this.txtReceiveStockCode.Text = String.Empty;
                            this.txtReceiveStockName.Text = String.Empty;
                            this.txtReceiveQuantity.Text = String.Empty;
                            if (this.cmbReceiveUnit.Items.Count > 0)
                            {
                                this.cmbReceiveUnit.DataSource = null;
                                this.cmbReceiveUnit.Items.Clear();
                            }
                            if (this._Logic_Stock.UOM_Table != null)
                            {
                                this._Logic_Stock.UOM_Table.Clear();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void AddtoReceive()
                    {
                        try
                        {
                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtReceiveStockCategory.Text), int.Parse(this.txtReceiveSubCat.Text), int.Parse(this.txtReceiveStockID.Text));
                            if (objectStock != null)
                            {
                                STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                                objectTransactionDetail.Transaction_No = String.Empty;
                                objectTransactionDetail.Line_No = this._Logic_Transaction_Receive.Detail_Table == null ? 1 : this._Logic_Transaction_Receive.Detail_Table.Rows.Count + 1;
                                objectTransactionDetail.Reference_Line = 0;
                                objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Stock;
                                objectTransactionDetail.Stock_ID = objectStock.ID;
                                objectTransactionDetail.Stock_Category = objectStock.Stock_Category;
                                objectTransactionDetail.Stock_Category_Name = new STP.Logic.Stock().GetCategory((int)objectStock.Stock_Category).Category_Name;
                                objectTransactionDetail.Stock_Sub_Category = objectStock.Stock_Sub_Category;
                                objectTransactionDetail.Stock_Sub_Category_Name = new STP.Logic.Stock().GetSubCategory((int)objectStock.Stock_Sub_Category).Sub_Category_Name;
                                objectTransactionDetail.Stock_Type = objectStock.Stock_Type;
                                objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectStock.Stock_Type.ToString());
                                objectTransactionDetail.Stock_Code = objectStock.Stock_Code;
                                objectTransactionDetail.Stock_Name = objectStock.Stock_Name;
                                objectTransactionDetail.Stock_Description_1 = objectStock.Stock_Description_1;
                                objectTransactionDetail.Stock_Description_2 = objectStock.Stock_Description_2;

                                long conversionFactor = this._Logic_Stock.GetUOMConFactor(this.cmbReceiveUnit.SelectedValue.ToString());
                                decimal billingPrice = decimal.Parse(String.Format("{0:0.00}", decimal.Parse("0.00")));
                                String billingUOM = this.cmbReceiveUnit.SelectedValue.ToString();
                                long billingQuantity = long.Parse(this.txtReceiveQuantity.Text.Trim());
                                decimal stockPrice = billingPrice / conversionFactor;
                                String stockUOM = objectStock.Stock_UOM;
                                long actualQuantity = billingQuantity * conversionFactor;

                                objectTransactionDetail.Stock_Price = stockPrice;
                                objectTransactionDetail.Stock_Cost = objectStock.Stock_Cost;
                                objectTransactionDetail.Stock_UOM = stockUOM;
                                objectTransactionDetail.Actual_Quantity = actualQuantity;
                                objectTransactionDetail.Billing_Quantity = billingQuantity;
                                objectTransactionDetail.Billing_UOM = billingUOM;
                                objectTransactionDetail.Billing_Price = billingPrice;
                                objectTransactionDetail.Conversion_Factor = conversionFactor;
                                objectTransactionDetail.Returned_Quantity = 0;
                                objectTransactionDetail.Reference_Transaction_No = String.Empty;
                                objectTransactionDetail.Reference_Line_No = 0;
                                objectTransactionDetail.Reference_Document_No = String.Empty;
                                objectTransactionDetail.Total_Price = billingPrice * long.Parse(this.txtReceiveQuantity.Text);
                                objectTransactionDetail.Old_Balance = 0;
                                objectTransactionDetail.New_Balance = 0;
                                objectTransactionDetail.CreatedDate = DateTime.Now;
                                objectTransactionDetail.Flag = true;
                                this._Logic_Transaction_Receive.AddDetailDataRow(objectTransactionDetail);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean ValidationFormReceive()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._ValidatorReceive.Validate();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private void ShowReceive()
                    {
                        try
                        {
                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtReceiveStockCategory.Text), int.Parse(this.txtReceiveSubCat.Text), int.Parse(this.txtReceiveStockID.Text));
                            if (objectStock != null)
                            {
                                this.txtReceiveStockCode.Text = objectStock.Stock_Code;
                                this.txtReceiveStockName.Text = objectStock.Stock_Name;
                                DataTable uomTable = this._Logic_Stock.UOM_Table;
                                this._Logic_Stock.BuildUOMComboBox((int)objectStock.Stock_Category, (int)objectStock.Stock_Sub_Category, (int)objectStock.ID, ref this.cmbReceiveUnit, ref uomTable);
                                this.txtReceiveQuantity.Text = "1";
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ClearReceiveForm()
                    {
                        try
                        {
                            this.txtReceiveTransactionNo.Text = String.Empty;
                            this.txtReceiveFrom.Text = "HQ";
                            this.txtReceiveDocumentNo.Text = String.Empty;
                            this.dtpReceiveTransactionDate.Checked = false;
                            this._Logic_Transaction_Receive.ClearDetailDataRow();
                            this.ClearReceiveSelection();
                            this.txtReceiveRemark.Text = String.Empty;
                            this.BuildReceiveList();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ShowReceiveTransaction()
                    {
                        try
                        {
                            STP.Base.Transaction_Header_Object objectTransactionHeader = new STP.Base.Transaction_Header_Object();
                            objectTransactionHeader = this._Logic_Transaction_Receive.GetTransaction(this.txtReceiveTransactionNo.Text);
                            if (objectTransactionHeader != null)
                            {
                                this.txtReceiveDocumentNo.Text = objectTransactionHeader.Document_No;
                                this.dtpReceiveTransactionDate.Value = (DateTime)objectTransactionHeader.Transaction_DateTime;
                                this.dtpReceiveTransactionDate.Checked = true;
                                this.txtReceiveRemark.Text = objectTransactionHeader.Remark;
                                this.BuildReceiveList();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean ValidationFormReceiveTransaction()
                    {
                        Boolean result = false;
                        try
                        {
                            if (this._SaveType_Receive == STP.Logic.Common.SaveType.Insert)
                            {
                                result = this._ValidatorReceiveTransactionNew.Validate();
                            }
                            else
                            {
                                result = this._ValidatorReceiveTransactionEdit.Validate();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ValidationFormReceiveTransactionDelete()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._ValidatorReceiveTransactionDelete.Validate();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private void RemoveReceiveEnable(Boolean enable)
                    {
                        try
                        {
                            this.btnReceiveRemove.Enabled = enable;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                #endregion

            #endregion

            #region Adjustment

                #region Validations

                    private Boolean ct_lstAdjustmentList_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            result = !this._Logic_Transaction_Adjustment.CheckDetailDataRowEmpty();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ct_lstAdjustmentListExist_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            result = !this._Logic_Transaction_Adjustment.CheckDetailDataRowExist(this.txtAdjustmentStockCode.Text);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

                #region Methods

                    private void ShowAdjustment()
                    {
                        try
                        {
                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtAdjustmentStockCategory.Text), int.Parse(this.txtAdjustmentSubCat.Text), int.Parse(this.txtAdjustmentStockID.Text));
                            if (objectStock != null)
                            {
                                this.txtAdjustmentStockCode.Text = objectStock.Stock_Code;
                                this.txtAdjustmentStockName.Text = objectStock.Stock_Name;
                                this.txtAdjustmentOldBalance.Text = objectStock.Stock_Balance.ToString();
                                this.txtAdjustmentNewBalance.Text = objectStock.Stock_Balance.ToString();
                                this.lblAdjustmentUOM1.Text = objectStock.Stock_UOM;
                                this.lblAdjustmentUOM2.Text = objectStock.Stock_UOM;
                                this.lblAdjustmentUOM3.Text = objectStock.Stock_UOM;
                                this.CalculateAdjustmentQuantity();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void CalculateAdjustmentQuantity()
                    {
                        try
                        {
                            if (ND.Data.DataObject.IsNumeric(this.txtAdjustmentNewBalance.Text) && ND.Data.DataObject.IsNumeric(this.txtAdjustmentOldBalance.Text))
                            {
                                this.txtAdjustmentQuantity.Text = (decimal.Parse(this.txtAdjustmentNewBalance.Text) - decimal.Parse(this.txtAdjustmentOldBalance.Text)).ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean ValidationFormAdjustment()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._ValidatorAdjustment.Validate();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private void AddtoAdjustment()
                    {
                        try
                        {
                            STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                            objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtAdjustmentStockCategory.Text), int.Parse(this.txtAdjustmentSubCat.Text), int.Parse(this.txtAdjustmentStockID.Text));
                            if (objectStock != null)
                            {
                                STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                                objectTransactionDetail.Transaction_No = String.Empty;
                                objectTransactionDetail.Line_No = this._Logic_Transaction_Adjustment.Detail_Table == null ? 1 : this._Logic_Transaction_Adjustment.Detail_Table.Rows.Count + 1;
                                objectTransactionDetail.Reference_Line = 0;
                                objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Stock;
                                objectTransactionDetail.Stock_ID = objectStock.ID;
                                objectTransactionDetail.Stock_Category = objectStock.Stock_Category;
                                objectTransactionDetail.Stock_Category_Name = new STP.Logic.Stock().GetCategory((int)objectStock.Stock_Category).Category_Name;
                                objectTransactionDetail.Stock_Sub_Category = objectStock.Stock_Sub_Category;
                                objectTransactionDetail.Stock_Sub_Category_Name = new STP.Logic.Stock().GetSubCategory((int)objectStock.Stock_Sub_Category).Sub_Category_Name;
                                objectTransactionDetail.Stock_Type = objectStock.Stock_Type;
                                objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectStock.Stock_Type.ToString());
                                objectTransactionDetail.Stock_Code = objectStock.Stock_Code;
                                objectTransactionDetail.Stock_Name = objectStock.Stock_Name;
                                objectTransactionDetail.Stock_Description_1 = objectStock.Stock_Description_1;
                                objectTransactionDetail.Stock_Description_2 = objectStock.Stock_Description_2;
                                objectTransactionDetail.Stock_Price = 0;
                                objectTransactionDetail.Stock_Cost = 0;
                                objectTransactionDetail.Stock_UOM = objectStock.Stock_UOM;
                                objectTransactionDetail.Actual_Quantity = long.Parse(this.txtAdjustmentQuantity.Text.Trim());
                                objectTransactionDetail.Billing_Quantity = long.Parse(this.txtAdjustmentQuantity.Text.Trim());
                                objectTransactionDetail.Billing_UOM = objectStock.Stock_UOM;
                                objectTransactionDetail.Billing_Price = objectStock.Stock_Price_1;
                                objectTransactionDetail.Conversion_Factor = 1;
                                objectTransactionDetail.Returned_Quantity = 0;
                                objectTransactionDetail.Reference_Transaction_No = String.Empty;
                                objectTransactionDetail.Reference_Line_No = 0;
                                objectTransactionDetail.Reference_Document_No = String.Empty;
                                objectTransactionDetail.Total_Price = 0;
                                objectTransactionDetail.Old_Balance = long.Parse(this.txtAdjustmentOldBalance.Text.Trim());
                                objectTransactionDetail.New_Balance = long.Parse(this.txtAdjustmentNewBalance.Text.Trim());
                                objectTransactionDetail.CreatedDate = DateTime.Now;
                                objectTransactionDetail.Flag = true;
                                this._Logic_Transaction_Adjustment.AddDetailDataRow(objectTransactionDetail);
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildAdjustmentList()
                    {
                        try
                        {
                            this.BuildAdjustmentList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildAdjustmentList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            this.lstAdjustmentListing.Visible = false;
                            this.lstAdjustmentListing.Clear();
                            this._Logic_Transaction_Adjustment.BuildDetailListColumn(this.lstAdjustmentListing);
                            this._Logic_Transaction_Adjustment.BuildDetailList(this.lstAdjustmentListing, sortType, sortField);
                            this.FormatAdjustmentListColumn();
                            this.lstAdjustmentListing.Visible = true;
                            if (this.lstAdjustmentListing.Items.Count > 0)
                            {
                                this.lstAdjustmentListing.Items[0].Selected = true;
                                this.RemoveAdjustmentEnable(true);
                            }
                            else
                            {
                                this.RemoveAdjustmentEnable(false);
                            }
                            if (this.lstAdjustmentListing.Items.Count > 0)
                            {
                                this.lstAdjustmentListing.EnsureVisible(this.lstAdjustmentListing.Items.Count - 1);
                            }
                            this.lstAdjustmentListing.Focus();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatAdjustmentListColumn()
                    {
                        try
                        {
                            this.lstAdjustmentListing.Columns["stockcategoryname"].Width = 100;
                            this.lstAdjustmentListing.Columns["stockcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstAdjustmentListing.Columns["stocksubcategoryname"].Width = 100;
                            this.lstAdjustmentListing.Columns["stocksubcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstAdjustmentListing.Columns["stockcode"].Width = 80;
                            this.lstAdjustmentListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                            this.lstAdjustmentListing.Columns["stockname"].Width = 200;
                            this.lstAdjustmentListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                            this.lstAdjustmentListing.Columns["actualqty"].Width = 100;
                            this.lstAdjustmentListing.Columns["actualqty"].TextAlign = HorizontalAlignment.Right;
                            this.lstAdjustmentListing.Columns["newbalance"].Width = 120;
                            this.lstAdjustmentListing.Columns["newbalance"].TextAlign = HorizontalAlignment.Right;
                            this.lstAdjustmentListing.Columns["stockuom"].Width = 80;
                            this.lstAdjustmentListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Left;

                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["transactionno"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["transactionline"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["transactionlinetype"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["refline"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockid"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockcategory"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stocksubcategory"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stocktype"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stocktypename"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockdesc1"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockdesc2"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockprice"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["stockcost"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["billingqty"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["billinguom"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["conversionfactor"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["billingprice"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["returnedqty"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["referencetransactionno"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["referenceline"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["referencedocumentno"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["totalprice"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["oldbalance"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["createddate"]);
                            this.lstAdjustmentListing.Columns.Remove(this.lstAdjustmentListing.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ClearAdjustmentForm()
                    {
                        try
                        {
                            this._Logic_Transaction_Adjustment.ClearDetailDataRow();
                            this.ClearAdjustmentSelection();
                            this.txtAdjustmentRemark.Text = String.Empty;
                            this.BuildAdjustmentList();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ClearAdjustmentSelection()
                    {
                        try
                        {
                            this.txtAdjustmentStockCategory.Text = String.Empty;
                            this.txtAdjustmentStockID.Text = String.Empty;
                            this.txtAdjustmentStockCode.Text = String.Empty;
                            this.txtAdjustmentStockName.Text = String.Empty;
                            this.txtAdjustmentOldBalance.Text = String.Empty;
                            this.txtAdjustmentNewBalance.Text = String.Empty;
                            this.txtAdjustmentQuantity.Text = String.Empty;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void RemoveAdjustmentEnable(Boolean enable)
                    {
                        try
                        {
                            this.btnAdjustmentRemove.Enabled = enable;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean ValidationFormAdjustmentTransaction()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._ValidatorAdjustmentTransaction.Validate();
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

            #endregion

            #region Category

                #region Validations

                    private Boolean ct_txtCategoryName_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            ND.Data.ConditionField conditionFieldCategoryName = new ND.Data.ConditionField();
                            conditionFieldCategoryName.Field = "categoryname";
                            conditionFieldCategoryName.Value = this.txtCategoryName.Text.Trim().Replace(" ", String.Empty);
                            conditionFieldCategoryName.DataType = ND.Data.DataObject.DataType.NString;
                            conditionFieldCategoryName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldCategoryName);
                            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
                            conditionFieldId.Field = "id";
                            conditionFieldId.Value = this.txtCategoryID.Text.Trim();
                            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.NotEqual;
                            conditions.Add(conditionFieldId);
                            result = !this._Logic_Stock.CheckExistCategory(conditions);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ct_txtSubCategoryName_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            ND.Data.ConditionField conditionFieldSubCategoryName = new ND.Data.ConditionField();
                            conditionFieldSubCategoryName.Field = "subcategoryname";
                            conditionFieldSubCategoryName.Value = this.txtSubCategoryName.Text.Trim().Replace(" ", String.Empty);
                            conditionFieldSubCategoryName.DataType = ND.Data.DataObject.DataType.NString;
                            conditionFieldSubCategoryName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldSubCategoryName);
                            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
                            conditionFieldId.Field = "id";
                            conditionFieldId.Value = this.txtSubCategoryID.Text.Trim();
                            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.NotEqual;
                            conditions.Add(conditionFieldId);
                            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                            conditionFieldStockCategory.Field = "stockcategory";
                            conditionFieldStockCategory.Value = int.Parse(this.cmbSubCategoryCategory.SelectedValue.ToString());
                            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.NotEqual;
                            conditions.Add(conditionFieldStockCategory);
                            result = !this._Logic_Stock.CheckExistSubCategory(conditions);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ct_txtSubCategoryShortcut_Validate()
                    {
                        Boolean result = false;
                        try
                        {
                            switch (this._SaveType_Sub_Category)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        if (this._Logic_Stock.GenerateSubCategoryShortCut(int.Parse(this.cmbSubCategoryCategory.SelectedValue.ToString())) == String.Empty)
                                        {
                                            result = false;
                                        }
                                        else
                                        {
                                            result = true;
                                        }
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        result = true;
                                        break;
                                    }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

                #region Methods

                    private void BuildCategoryCategoryCombo()
                    {
                        try
                        {
                            this._Logic_Stock.BuildCategoryCombo(this.cmbSubCategoryCategory);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ShowCategory()
                    {
                        try
                        {
                            STP.Base.Category_Object objectCategory = new STP.Base.Category_Object();
                            objectCategory = this._Logic_Stock.GetCategory(int.Parse(this.lstCategoryCategory.SelectedItems[0].SubItems[0].Text));
                            if (objectCategory != null)
                            {
                                this.txtCategoryID.Text = objectCategory.ID.ToString();
                                this.txtCategoryName.Text = objectCategory.Category_Name;
                                this.txtCategoryShortcut.Text = objectCategory.Category_Shortcut;
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ShowSubCategory()
                    {
                        try
                        {
                            STP.Base.Sub_Category_Object objectSubCategory = new STP.Base.Sub_Category_Object();
                            objectSubCategory = this._Logic_Stock.GetSubCategory(int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[1].Text));
                            if (objectSubCategory != null)
                            {
                                this.txtSubCategoryID.Text = objectSubCategory.ID.ToString();
                                this.cmbSubCategoryCategory.SelectedValue = objectSubCategory.Category;
                                this.txtSubCategoryName.Text = objectSubCategory.Sub_Category_Name;
                                this.txtSubCategoryShortcut.Text = objectSubCategory.Sub_Category_Shortcut;
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryCategoryList()
                    {
                        try
                        {
                            this.BuildCategoryCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            Global.ShowWaiting(this, true);
                            this.lstCategoryCategory.Visible = false;
                            this.lstCategoryCategory.Clear();
                            this.BuildCategoryCategoryListColumn();
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            this._Logic_Stock.BuildCategoryList(this.lstCategoryCategory, conditions, sortType, sortField, false, false);
                            this.FormatCategoryCategoryListColumn();
                            this.lstCategoryCategory.Visible = true;
                            Global.ShowWaiting(this, false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategoryCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategoryCategory.Columns.Clear();
                            this.lstCategoryCategory.Columns.Add("id", "ID");
                            this.lstCategoryCategory.Columns.Add("categoryname", "Category");
                            this.lstCategoryCategory.Columns.Add("categoryshortcut", "Code");
                            this.lstCategoryCategory.Columns.Add("createddate", "Created Date");
                            this.lstCategoryCategory.Columns.Add("flag", "Flag");
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatCategoryCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategoryCategory.Columns["categoryname"].Width = 117;
                            this.lstCategoryCategory.Columns["categoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstCategoryCategory.Columns["categoryshortcut"].Width = 50;
                            this.lstCategoryCategory.Columns["categoryshortcut"].TextAlign = HorizontalAlignment.Left;

                            this.lstCategoryCategory.Columns.Remove(this.lstCategoryCategory.Columns["id"]);
                            this.lstCategoryCategory.Columns.Remove(this.lstCategoryCategory.Columns["createddate"]);
                            this.lstCategoryCategory.Columns.Remove(this.lstCategoryCategory.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategorySubCategoryList()
                    {
                        try
                        {
                            this.BuildCategorySubCategoryList(ND.Data.DataObject.SortType.None, String.Empty);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategorySubCategoryList(ND.Data.DataObject.SortType sortType, String sortField)
                    {
                        try
                        {
                            Global.ShowWaiting(this, true);
                            this.lstCategorySubCategory.Visible = false;
                            this.lstCategorySubCategory.Clear();
                            this.BuildCategorySubCategoryListColumn();
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            this._Logic_Stock.BuildSubCategoryList(this.lstCategorySubCategory, conditions, sortType, sortField, false, false);
                            this.FormatCategorySubCategoryListColumn();
                            this.lstCategorySubCategory.Visible = true;
                            Global.ShowWaiting(this, false);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void BuildCategorySubCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategorySubCategory.Columns.Clear();
                            this.lstCategorySubCategory.Columns.Add("stockcategory", "Category");
                            this.lstCategorySubCategory.Columns.Add("id", "ID");
                            this.lstCategorySubCategory.Columns.Add("subcategoryname", "Sub Category");
                            this.lstCategorySubCategory.Columns.Add("subcategoryshortcut", "Code");
                            this.lstCategorySubCategory.Columns.Add("createddate", "Created Date");
                            this.lstCategorySubCategory.Columns.Add("flag", "Flag");
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void FormatCategorySubCategoryListColumn()
                    {
                        try
                        {
                            this.lstCategorySubCategory.Columns["subcategoryname"].Width = 117;
                            this.lstCategorySubCategory.Columns["subcategoryname"].TextAlign = HorizontalAlignment.Left;
                            this.lstCategorySubCategory.Columns["subcategoryshortcut"].Width = 50;
                            this.lstCategorySubCategory.Columns["subcategoryshortcut"].TextAlign = HorizontalAlignment.Left;

                            this.lstCategorySubCategory.Columns.Remove(this.lstCategorySubCategory.Columns["stockcategory"]);
                            this.lstCategorySubCategory.Columns.Remove(this.lstCategorySubCategory.Columns["id"]);
                            this.lstCategorySubCategory.Columns.Remove(this.lstCategorySubCategory.Columns["createddate"]);
                            this.lstCategorySubCategory.Columns.Remove(this.lstCategorySubCategory.Columns["flag"]);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void ClearCategoryPanel()
                    {
                        try
                        {
                            this.txtCategoryID.Text = String.Empty;
                            this.txtCategoryName.Text = String.Empty;
                            this.txtCategoryShortcut.Text = String.Empty;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void EnableCategoryPanel(Boolean enable)
                    {
                        try
                        {
                            this.lstCategoryCategory.Enabled = !enable;
                            this.txtCategoryName.Enabled = enable;                            
                            this.btnCategoryEdit.Enabled = !enable;
                            this.btnCategorySave.Enabled = enable;
                            this.btnCategoryCancel.Enabled = enable;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean DeleteCategory()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._Logic_Stock.DeleteCategory(int.Parse(this.lstCategoryCategory.SelectedItems[0].SubItems[0].Text));
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                        return result;
                    }

                    private Boolean SaveCategory(Boolean updateTransaction)
                    {
                        Boolean result = false;
                        try
                        {
                            STP.Base.Category_Object objectCategory = new STP.Base.Category_Object();
                            objectCategory.ID = this.txtCategoryID.Text.Trim() == String.Empty ? (int?)null : int.Parse(this.txtCategoryID.Text.Trim());
                            objectCategory.Category_Name = this.txtCategoryName.Text.Trim();
                            objectCategory.Category_Shortcut = this.txtCategoryShortcut.Text.Trim();

                            result = this._Logic_Stock.SaveCategory(objectCategory, this._SaveType_Category, updateTransaction);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private void ClearSubCategoryPanel()
                    {
                        try
                        {
                            this.txtSubCategoryID.Text = String.Empty;
                            this.cmbSubCategoryCategory.SelectedIndex = 0;
                            this.txtSubCategoryName.Text = String.Empty;
                            this.txtSubCategoryShortcut.Text = String.Empty;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private void EnableSubCategoryPanel(Boolean enable)
                    {
                        try
                        {
                            this.lstCategorySubCategory.Enabled = !enable;
                            this.cmbSubCategoryCategory.Enabled = enable;
                            this.txtSubCategoryName.Enabled = enable;
                            this.btnSubCategoryNew.Enabled = !enable;
                            this.btnSubCategoryEdit.Enabled = !enable;
                            this.btnSubCategoryDelete.Enabled = !enable;
                            this.btnSubCategorySave.Enabled = enable;
                            this.btnSubCategoryCancel.Enabled = enable;
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                    }

                    private Boolean DeleteSubCategory()
                    {
                        Boolean result = false;
                        try
                        {
                            result = this._Logic_Stock.DeleteSubCategory(int.Parse(this.lstCategorySubCategory.SelectedItems[0].SubItems[1].Text));
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                        }
                        return result;
                    }

                    private Boolean SaveSubCategory(Boolean updateTransaction)
                    {
                        Boolean result = false;
                        try
                        {
                            STP.Base.Sub_Category_Object objectSubCategory = new STP.Base.Sub_Category_Object();
                            objectSubCategory.ID = this.txtSubCategoryID.Text.Trim() == String.Empty ? (int?)null : int.Parse(this.txtSubCategoryID.Text.Trim());
                            objectSubCategory.Category = int.Parse(this.cmbSubCategoryCategory.SelectedValue.ToString());
                            objectSubCategory.Sub_Category_Name = this.txtSubCategoryName.Text.Trim();
                            objectSubCategory.Sub_Category_Shortcut = this.txtSubCategoryShortcut.Text.Trim();

                            result = this._Logic_Stock.SaveSubCategory(objectSubCategory, this._SaveType_Sub_Category, updateTransaction);
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ValidationCategoryForm()
                    {
                        Boolean result = false;
                        try
                        {
                            switch (this._SaveType_Category)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        result = this._Validator_Category_New.Validate();
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        result = this._Validator_Category_Edit.Validate();
                                        break;
                                    }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean ValidationSubCategoryForm()
                    {
                        Boolean result = false;
                        try
                        {
                            switch (this._SaveType_Sub_Category)
                            {
                                case STP.Logic.Common.SaveType.Insert:
                                    {
                                        result = this._Validator_Sub_Category_New.Validate();
                                        break;
                                    }
                                case STP.Logic.Common.SaveType.Update:
                                    {
                                        result = this._Validator_Sub_Category_Edit.Validate();
                                        break;
                                    }
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                    private Boolean CheckSubCategoryCategoryChanged()
                    {
                        Boolean result = false;
                        try
                        {
                            if (this.lstCategorySubCategory.SelectedItems.Count > 0)
                            {
                                result = !(this.cmbSubCategoryCategory.SelectedValue.ToString() == this.lstCategorySubCategory.SelectedItems[0].SubItems[0].Text);
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            ND.Log.LogWriter.WriteLog(ex.ToString());
                            result = false;
                        }
                        return result;
                    }

                #endregion

            #endregion

        #endregion
    }
}