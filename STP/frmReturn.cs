﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmReturn : Form
    {
        #region Declarations

            public String TransactionNo
            {
                get { return this._TransactionNo; }
                set { this._TransactionNo = value; }
            }

            private String _TransactionNo = String.Empty;
            private ND.Standard.Validation.Winform.Form _Validator;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstListing;
            private STP.Logic.Transaction _Logic_Transaction;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmDiscount_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this.LoadList();
                        this.SetValidation();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstListing_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.lstListing.SelectedItems.Count > 0)
                        {
                            this.txtQty.Text = this.lstListing.SelectedItems[0].SubItems[13].Text;
                            this.txtAmount.Text = this.lstListing.SelectedItems[0].SubItems[14].Text;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnOK_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this._Validator.Validate() == true)
                        {
                            STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                            objectTransactionDetail.Transaction_No = String.Empty;
                            objectTransactionDetail.Line_No = this._Logic_Transaction.Detail_Table == null ? 1 : this._Logic_Transaction.Detail_Table.Rows.Count + 1;
                            objectTransactionDetail.Reference_Line = 0;
                            objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Return;
                            objectTransactionDetail.Stock_ID = int.Parse(this.lstListing.SelectedItems[0].SubItems[2].Text);
                            objectTransactionDetail.Stock_Category = int.Parse(this.lstListing.SelectedItems[0].SubItems[3].Text);
                            objectTransactionDetail.Stock_Category_Name = this.lstListing.SelectedItems[0].SubItems[4].Text;
                            objectTransactionDetail.Stock_Sub_Category = int.Parse(this.lstListing.SelectedItems[0].SubItems[5].Text);
                            objectTransactionDetail.Stock_Sub_Category_Name = this.lstListing.SelectedItems[0].SubItems[6].Text;
                            objectTransactionDetail.Stock_Type = int.Parse(this.lstListing.SelectedItems[0].SubItems[7].Text);
                            objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectTransactionDetail.Stock_Type.ToString());
                            objectTransactionDetail.Stock_Code = this.lstListing.SelectedItems[0].SubItems[8].Text;
                            objectTransactionDetail.Stock_Name = this.lstListing.SelectedItems[0].SubItems[9].Text;
                            objectTransactionDetail.Stock_Description_1 = String.Empty;
                            objectTransactionDetail.Stock_Description_2 = String.Empty;

                            long quantity = long.Parse(this.txtQty.Text);
                            decimal amount = decimal.Parse(this.txtAmount.Text) * -1 / quantity;

                            objectTransactionDetail.Stock_Price = amount;
                            objectTransactionDetail.Stock_Cost = 0;
                            objectTransactionDetail.Stock_UOM = this.lstListing.SelectedItems[0].SubItems[10].Text;
                            objectTransactionDetail.Actual_Quantity = quantity * -1;
                            objectTransactionDetail.Billing_Quantity = quantity * -1;
                            objectTransactionDetail.Billing_UOM = this.lstListing.SelectedItems[0].SubItems[10].Text;
                            objectTransactionDetail.Billing_Price = amount;
                            objectTransactionDetail.Conversion_Factor = 1;
                            objectTransactionDetail.Returned_Quantity = 0;
                            objectTransactionDetail.Reference_Transaction_No = this.lstListing.SelectedItems[0].SubItems[0].Text;
                            objectTransactionDetail.Reference_Line_No = int.Parse(this.lstListing.SelectedItems[0].SubItems[1].Text);
                            objectTransactionDetail.Reference_Document_No = new STP.Base.Transaction_Header_Base().GetSingleObject(this.lstListing.SelectedItems[0].SubItems[0].Text).Document_No;
                            objectTransactionDetail.Total_Price = decimal.Parse(this.txtAmount.Text) * -1;
                            objectTransactionDetail.Old_Balance = 0;
                            objectTransactionDetail.New_Balance = 0;
                            objectTransactionDetail.CreatedDate = DateTime.Now;
                            objectTransactionDetail.Flag = true;
                            this._Logic_Transaction.AddDetailDataRow(objectTransactionDetail);
                            this.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnCancel_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Procedures and Functions

            #region Constructor

                public frmReturn(STP.Logic.Transaction logicTransaction)
                {
                    this.InitializeComponent();
                    this.Text = "Return";
                    this._Logic_Transaction = logicTransaction;
                    this._Validator = new ND.Standard.Validation.Winform.Form();
                    this.ct_lstListing = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstListing_Validate);
                }

            #endregion

            #region Validations

                private Boolean ct_lstListing_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        if (this.lstListing.SelectedItems.Count > 0)
                        {
                            result = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void BuildButton()
                {
                    try
                    {
                        this.btnOK.Image = Image.FromFile("image/Confirm.png");
                        this.btnOK.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnCancel.Image = Image.FromFile("image/Cancel.png");
                        this.btnCancel.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void LoadList()
                {
                    try
                    {
                        if (this._TransactionNo != String.Empty)
                        {
                            this._Logic_Transaction.BuildReturnListColumn(this.lstListing);
                            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                            ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
                            conditionFieldTransactionNo.Field = "transactionno";
                            conditionFieldTransactionNo.Value = this._TransactionNo;
                            conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
                            conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                            conditions.Add(conditionFieldTransactionNo);
                            this._Logic_Transaction.BuildReturnList(this.lstListing, conditions, ND.Data.DataObject.SortType.None, String.Empty);
                            this.FormatListColumn();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatListColumn()
                {
                    try
                    {
                        this.lstListing.Columns["stockcode"].Width = 80;
                        this.lstListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["stockname"].Width = 200;
                        this.lstListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["stockprice"].Width = 100;
                        this.lstListing.Columns["stockprice"].TextAlign = HorizontalAlignment.Right;
                        this.lstListing.Columns["stockuom"].Width = 80;
                        this.lstListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Left;
                        this.lstListing.Columns["actualqty"].Width = 100;
                        this.lstListing.Columns["actualqty"].TextAlign = HorizontalAlignment.Right;
                        this.lstListing.Columns["totalprice"].Width = 100;
                        this.lstListing.Columns["totalprice"].TextAlign = HorizontalAlignment.Right;

                        this.lstListing.Columns.Remove(this.lstListing.Columns["transactionno"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["transactionline"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockid"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockcategory"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stockcategoryname"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocksubcategory"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocksubcategoryname"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocktype"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocktypename"]);
                        this.lstListing.Columns.Remove(this.lstListing.Columns["stocktype"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._Validator.AddValidator(this.txtAmount, this.erpReturn, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._Validator.AddValidator(this.txtQty, this.erpReturn, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._Validator.AddValidator(this.txtAmount, this.erpReturn, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                        this._Validator.AddValidator(this.txtQty, this.erpReturn, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);
                        this._Validator.AddValidator(this.lstListing, this.erpReturn, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstListing);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion
    }
}
