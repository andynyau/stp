﻿namespace STP
{
    partial class frmBackupRestore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBackup = new System.Windows.Forms.Panel();
            this.rbtRestore = new System.Windows.Forms.RadioButton();
            this.rbtBackup = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.lstBackupDevice = new System.Windows.Forms.ListView();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pnlBackup.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBackup
            // 
            this.pnlBackup.Controls.Add(this.btnDelete);
            this.pnlBackup.Controls.Add(this.rbtRestore);
            this.pnlBackup.Controls.Add(this.rbtBackup);
            this.pnlBackup.Controls.Add(this.btnClose);
            this.pnlBackup.Controls.Add(this.lstBackupDevice);
            this.pnlBackup.Controls.Add(this.btnStart);
            this.pnlBackup.Location = new System.Drawing.Point(0, 0);
            this.pnlBackup.Name = "pnlBackup";
            this.pnlBackup.Size = new System.Drawing.Size(400, 300);
            this.pnlBackup.TabIndex = 2;
            // 
            // rbtRestore
            // 
            this.rbtRestore.AutoSize = true;
            this.rbtRestore.Location = new System.Drawing.Point(99, 12);
            this.rbtRestore.Name = "rbtRestore";
            this.rbtRestore.Size = new System.Drawing.Size(62, 17);
            this.rbtRestore.TabIndex = 4;
            this.rbtRestore.TabStop = true;
            this.rbtRestore.Text = "Restore";
            this.rbtRestore.UseVisualStyleBackColor = true;
            // 
            // rbtBackup
            // 
            this.rbtBackup.AutoSize = true;
            this.rbtBackup.Location = new System.Drawing.Point(8, 12);
            this.rbtBackup.Name = "rbtBackup";
            this.rbtBackup.Size = new System.Drawing.Size(62, 17);
            this.rbtBackup.TabIndex = 3;
            this.rbtBackup.TabStop = true;
            this.rbtBackup.Text = "Backup";
            this.rbtBackup.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(311, 263);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lstBackupDevice
            // 
            this.lstBackupDevice.FullRowSelect = true;
            this.lstBackupDevice.GridLines = true;
            this.lstBackupDevice.HideSelection = false;
            this.lstBackupDevice.Location = new System.Drawing.Point(8, 35);
            this.lstBackupDevice.MultiSelect = false;
            this.lstBackupDevice.Name = "lstBackupDevice";
            this.lstBackupDevice.Size = new System.Drawing.Size(378, 222);
            this.lstBackupDevice.TabIndex = 1;
            this.lstBackupDevice.UseCompatibleStateImageBehavior = false;
            this.lstBackupDevice.View = System.Windows.Forms.View.Details;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(8, 263);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(99, 263);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmBackupRestore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 298);
            this.ControlBox = false;
            this.Controls.Add(this.pnlBackup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmBackupRestore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmBackupRestore_Load);
            this.pnlBackup.ResumeLayout(false);
            this.pnlBackup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlBackup;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.ListView lstBackupDevice;
        public System.Windows.Forms.Button btnStart;
        public System.Windows.Forms.RadioButton rbtRestore;
        public System.Windows.Forms.RadioButton rbtBackup;
        public System.Windows.Forms.Button btnDelete;

    }
}