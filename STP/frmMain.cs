﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace STP
{
    public partial class frmMain : Form
    {
        #region Declarations

            public FormName CurrentForm
            {
                get { return this._CurrentForm; }
                set { this._CurrentForm = value; }
            }

            public enum FormName
            {
                None = 0,
                Stock = 1,
                Sales = 2,
                Purchase = 3,
                Customer = 4,
                Vendor = 5,
                Report = 6
            }

            private frmStock _StockForm;
            private frmSales _SalesForm;
            private frmPurchase _PurchaseForm;
            private frmCustomer _CustomerForm;
            private frmVendor _VendorForm;
            private frmReport _ReportForm;

            private FormName _CurrentForm;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmMain_Load(object sender, EventArgs e)
                {
                    try 
                    { 
                        this.InitializeForm();
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
                {
                    try 
                    { 
                        this.CloseApplication(e); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void _ReportForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Report;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void _VendorForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Vendor;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void _CustomerForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Customer;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void _PurchaseForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Purchase;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void _SalesForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Sales;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void _StockForm_GotFocus(object sender, EventArgs e)
                {
                    try
                    {
                        this._CurrentForm = FormName.Stock;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region MenuStrip Events

                private void mnuMainClick_Exit(object sender, EventArgs e)
                {
                    try 
                    { 
                        Application.Exit(); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void mnuMainClick_About(object sender, EventArgs e)
                {
                    try 
                    { 
                        this.ShowAboutDialog(); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void mnuMainClick_BackupRestore(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowBackupRestore();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ToolStrip Events

                private void tlsMainClick_Stock(object sender, EventArgs e)
                {
                    try 
                    { 
                        this.ShowForm(FormName.Stock); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void tlsMainClick_Sales(object sender, EventArgs e)
                {
                    try 
                    { 
                        this.ShowForm(FormName.Sales); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void tlsMainClick_Purchase(object sender, EventArgs e)
                {
                    try 
                    {
                        this.ShowForm(FormName.Purchase); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void tlsMainClick_Customer(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowForm(FormName.Customer);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void tlsMainClick_Vendor(object sender, EventArgs e)
                {
                    try
                    {
                        this.ShowForm(FormName.Vendor);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void tlsMainClick_Report(object sender, EventArgs e)
                {
                    try 
                    {
                        this.ShowForm(FormName.Report); 
                    }
                    catch (Exception ex) 
                    { 
                        ND.Log.LogWriter.WriteLog(ex.ToString()); 
                    }
                }

                private void tlsMainClick_Debug(object sender, EventArgs e)
                {
                    try
                    {
                        frmTransactionDebug debugForm = new frmTransactionDebug();
                        switch (this._CurrentForm)
                        {
                            case FormName.Stock:
                                {
                                    switch (this._StockForm.Working_Mode)
                                    {
                                        case frmStock.WorkingMode.Receive:
                                            {
                                                debugForm.Transaction_DataTable = this._StockForm.DataTable_Transaction_Receive;
                                                debugForm.Payment_DataTable = null;
                                                break;
                                            }
                                        case frmStock.WorkingMode.Transfer:
                                            {
                                                debugForm.Transaction_DataTable = this._StockForm.DataTable_Transaction_Transfer;
                                                debugForm.Payment_DataTable = null;
                                                break;
                                            }
                                        case frmStock.WorkingMode.Adjustment:
                                            {
                                                debugForm.Transaction_DataTable = this._StockForm.DataTable_Transaction_Adjustment;
                                                debugForm.Payment_DataTable = null;
                                                break;
                                            }
                                    }
                                    break;
                                }
                            case FormName.Sales:
                                {
                                    debugForm.Transaction_DataTable = this._SalesForm.Transaction_DataTable;
                                    debugForm.Payment_DataTable = this._SalesForm.Payment_DataTable;
                                    break;
                                }
                            case FormName.Purchase:
                                {
                                    debugForm.Transaction_DataTable = this._PurchaseForm.Transaction_DataTable;
                                    debugForm.Payment_DataTable = this._PurchaseForm.Payment_DataTable;
                                    break;
                                }
                        }
                        debugForm.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Functions and Procedures

            public frmMain()
            {
                try
                {
                    this.InitializeComponent();
                    this._StockForm = new frmStock();
                    this._SalesForm = new frmSales();
                    this._PurchaseForm = new frmPurchase();
                    this._CustomerForm = new frmCustomer();
                    this._VendorForm = new frmVendor();
                    this._ReportForm = new frmReport();
                    this._StockForm.GotFocus += new EventHandler(this._StockForm_GotFocus);
                    this._SalesForm.GotFocus += new EventHandler(this._SalesForm_GotFocus);
                    this._PurchaseForm.GotFocus += new EventHandler(this._PurchaseForm_GotFocus);
                    this._CustomerForm.GotFocus += new EventHandler(this._CustomerForm_GotFocus);
                    this._VendorForm.GotFocus += new EventHandler(this._VendorForm_GotFocus);
                    this._ReportForm.GotFocus += new EventHandler(this._ReportForm_GotFocus);
                    this._StockForm.Close();
                    this._SalesForm.Close();
                    this._PurchaseForm.Close();
                    this._CustomerForm.Close();
                    this._VendorForm.Close();
                    this._ReportForm.Close();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void InitializeForm()
            {
                try
                {
                    ND.UI.Winform.Dynamic dynamicBuilder = new ND.UI.Winform.Dynamic();
                    dynamicBuilder.LoadDynamicMenu(ref this.mnsMain, "xml/menu.xml", this, this.mnsMain.Name);
                    dynamicBuilder.LoadDynamicTool(ref this.tlsMain, "xml/tool.xml", this, this.tlsMain.Name);

                    #if DEBUG
                        ToolStripItem debugItem = new ToolStripButton();
                        debugItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                        debugItem.TextImageRelation = TextImageRelation.ImageBeforeText;
                        debugItem.ImageAlign = ContentAlignment.MiddleLeft;
                        debugItem.Image = Image.FromFile("image/Information.png");
                        debugItem.ImageScaling = ToolStripItemImageScaling.None;
                        debugItem.Name = "tlsDebug";
                        debugItem.Text = "DEBUG";
                        this.tlsMain.Items.Add(debugItem);
                        debugItem.Click += new EventHandler(this.tlsMainClick_Debug);
                    #endif

                    this.Text = Application.ProductName + " - [" + Application.ProductVersion + "]";
                    this._CurrentForm = FormName.None;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void ShowAboutDialog()
            {
                try
                {
                    frmAbout formDialog = new frmAbout();
                    formDialog.ShowDialog();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void ShowBackupRestore()
            {
                try
                {
                    frmBackupRestore formBackupRestore = new frmBackupRestore();
                    formBackupRestore.Owner = this;
                    formBackupRestore.ShowDialog();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void CloseApplication(FormClosingEventArgs e)
            {
                try
                {
                    if (Global.ForceClose == false)
                    {
                        String title = "Exit";
                        String messageBody = "Close the application?";

                        if (DialogResult.Yes != MessageBox.Show(messageBody, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            e.Cancel = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void ShowForm(FormName formName)
            {
                try
                {
                    switch (formName)
                    {
                        case FormName.Stock:
                            {
                                if (this._CurrentForm != FormName.Stock)
                                {
                                    this.MinimizeForm();
                                    if (this._StockForm != null)
                                    {
                                        if (this._StockForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._StockForm = new frmStock();
                                            this._StockForm.MdiParent = this;
                                            this._StockForm.WindowState = FormWindowState.Maximized;
                                            this._StockForm.Show();
                                            this._CurrentForm = FormName.Stock;
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._StockForm.WindowState = FormWindowState.Maximized;
                                            this._StockForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Stock;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._StockForm = new frmStock();
                                        this._StockForm.MdiParent = this;
                                        this._StockForm.WindowState = FormWindowState.Maximized;
                                        this._StockForm.Show();
                                        this._CurrentForm = FormName.Stock;
                                        //}
                                    }
                                }
                                break;
                            }
                        case FormName.Sales:
                            {
                                if (this._CurrentForm != FormName.Sales)
                                {
                                    this.MinimizeForm();
                                    if (this._SalesForm != null)
                                    {
                                        if (this._SalesForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._SalesForm = new frmSales();
                                            this._SalesForm.MdiParent = this;
                                            this._SalesForm.WindowState = FormWindowState.Maximized;
                                            this._SalesForm.Show();
                                            this._CurrentForm = FormName.Sales;
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._SalesForm.WindowState = FormWindowState.Maximized;

                                            this._SalesForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Sales;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._SalesForm = new frmSales();
                                        this._SalesForm.MdiParent = this;
                                        this._SalesForm.WindowState = FormWindowState.Maximized;
                                        this._SalesForm.Show();
                                        this._CurrentForm = FormName.Sales;
                                        //}
                                    }
                                }
                                break;
                            }
                        case FormName.Purchase:
                            {
                                if (this._CurrentForm != FormName.Purchase)
                                {
                                    this.MinimizeForm();
                                    if (this._PurchaseForm != null)
                                    {
                                        if (this._PurchaseForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._PurchaseForm = new frmPurchase();
                                            this._PurchaseForm.MdiParent = this;
                                            this._PurchaseForm.WindowState = FormWindowState.Maximized;
                                            this._PurchaseForm.Show();
                                            this._CurrentForm = FormName.Purchase;
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._PurchaseForm.WindowState = FormWindowState.Maximized;
                                            this._PurchaseForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Purchase;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._PurchaseForm = new frmPurchase();
                                        this._PurchaseForm.MdiParent = this;
                                        this._PurchaseForm.WindowState = FormWindowState.Maximized;
                                        this._PurchaseForm.Show();
                                        this._CurrentForm = FormName.Purchase;
                                        //}
                                    }
                                }
                                break;
                            }
                        case FormName.Customer:
                            {
                                if (this._CurrentForm != FormName.Customer)
                                {
                                    this.MinimizeForm();
                                    if (this._CustomerForm != null)
                                    {
                                        if (this._CustomerForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._CustomerForm = new frmCustomer();
                                            this._CustomerForm.MdiParent = this;
                                            this._CustomerForm.WindowState = FormWindowState.Maximized;
                                            this._CustomerForm.Show();
                                            this._CurrentForm = FormName.Customer;
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._CustomerForm.WindowState = FormWindowState.Maximized;
                                            this._CustomerForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Customer;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._CustomerForm = new frmCustomer();
                                        this._CustomerForm.MdiParent = this;
                                        this._CustomerForm.WindowState = FormWindowState.Maximized;
                                        this._CustomerForm.Show();
                                        this._CurrentForm = FormName.Customer;
                                        //}
                                    }
                                }
                                break;
                            }
                        case FormName.Vendor:
                            {
                                if (this._CurrentForm != FormName.Vendor)
                                {
                                    this.MinimizeForm();
                                    if (this._VendorForm != null)
                                    {
                                        if (this._VendorForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._VendorForm = new frmVendor();
                                            this._VendorForm.MdiParent = this;
                                            this._VendorForm.WindowState = FormWindowState.Maximized;
                                            this._VendorForm.Show();
                                            this._CurrentForm = FormName.Vendor;
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._VendorForm.WindowState = FormWindowState.Maximized;
                                            this._VendorForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Vendor;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._VendorForm = new frmVendor();
                                        this._VendorForm.MdiParent = this;
                                        this._VendorForm.WindowState = FormWindowState.Maximized;
                                        this._VendorForm.Show();
                                        this._CurrentForm = FormName.Vendor;
                                        //}
                                    }
                                }
                                break;
                            }
                        case FormName.Report:
                            {
                                if (this._CurrentForm != FormName.Report)
                                {
                                    this.MinimizeForm();
                                    if (this._ReportForm != null)
                                    {
                                        if (this._ReportForm.IsDisposed == true)
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._ReportForm = new frmReport();
                                            this._ReportForm.MdiParent = this;
                                            this._ReportForm.WindowState = FormWindowState.Maximized;
                                            this._ReportForm.Show();
                                            this._CurrentForm = FormName.Report;
                                            
                                        }
                                        else
                                        {
                                            Global.ShowWaiting(this, true);
                                            Application.DoEvents();
                                            this._ReportForm.WindowState = FormWindowState.Maximized;
                                            this._ReportForm.Focus();
                                            Global.ShowWaiting(this, false);
                                            this._CurrentForm = FormName.Report;
                                        }
                                    }
                                    else
                                    {
                                        //if (CloseForm() == true)
                                        //{
                                        Global.ShowWaiting(this, true);
                                        Application.DoEvents();
                                        this._ReportForm = new frmReport();
                                        this._ReportForm.MdiParent = this;
                                        this._ReportForm.WindowState = FormWindowState.Maximized;
                                        this._ReportForm.Show();
                                        this._CurrentForm = FormName.Report;
                                        //}
                                    }
                                }
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private Boolean MinimizeForm()
            {
                Boolean result = true;
                try
                {
                    if (this._StockForm != null)
                    {
                        if (this._StockForm.IsDisposed == false)
                        {
                            this._StockForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                    if (this._SalesForm != null)
                    {
                        if (this._SalesForm.IsDisposed == false)
                        {
                            this._SalesForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                    if (this._PurchaseForm != null)
                    {
                        if (this._PurchaseForm.IsDisposed == false)
                        {
                            this._PurchaseForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                    if (this._CustomerForm != null)
                    {
                        if (this._CustomerForm.IsDisposed == false)
                        {
                            this._CustomerForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                    if (this._VendorForm != null)
                    {
                        if (this._VendorForm.IsDisposed == false)
                        {
                            this._VendorForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                    if (this._ReportForm != null)
                    {
                        if (this._ReportForm.IsDisposed == false)
                        {
                            this._ReportForm.WindowState = FormWindowState.Minimized;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = false;
                }
                return result;
            }

            private Boolean CloseForm()
            {
                Boolean result = true;
                try
                {
                    this._StockForm.Close();
                    if (this._StockForm.IsDisposed == false)
                    {
                        result = false;
                    }
                    this._SalesForm.Close();
                    if (this._SalesForm.IsDisposed == false)
                    {
                        result = false;
                    }
                    this._PurchaseForm.Close();
                    if (this._PurchaseForm.IsDisposed == false)
                    {
                        result = false;
                    }
                    this._CustomerForm.Close();
                    if (this._CustomerForm.IsDisposed == false)
                    {
                        result = false;
                    }
                    this._VendorForm.Close();
                    if (this._VendorForm.IsDisposed == false)
                    {
                        result = false;
                    }
                    this._ReportForm.Close();
                    if (this._ReportForm.IsDisposed == false)
                    {
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                    result = false;
                }
                return result;
            }

        #endregion
    }
}
