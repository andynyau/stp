﻿namespace STP
{
    partial class frmReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlDiscount = new System.Windows.Forms.Panel();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lstListing = new System.Windows.Forms.ListView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.erpReturn = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpReturn)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlDiscount
            // 
            this.pnlDiscount.Controls.Add(this.lblUnit);
            this.pnlDiscount.Controls.Add(this.lblQuantity);
            this.pnlDiscount.Controls.Add(this.lblAmount);
            this.pnlDiscount.Controls.Add(this.txtQty);
            this.pnlDiscount.Controls.Add(this.lstListing);
            this.pnlDiscount.Controls.Add(this.btnCancel);
            this.pnlDiscount.Controls.Add(this.btnOK);
            this.pnlDiscount.Controls.Add(this.txtAmount);
            this.pnlDiscount.Location = new System.Drawing.Point(0, 0);
            this.pnlDiscount.Name = "pnlDiscount";
            this.pnlDiscount.Size = new System.Drawing.Size(800, 300);
            this.pnlDiscount.TabIndex = 0;
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.Location = new System.Drawing.Point(180, 200);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(0, 13);
            this.lblUnit.TabIndex = 10;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.Location = new System.Drawing.Point(12, 200);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(98, 16);
            this.lblQuantity.TabIndex = 9;
            this.lblQuantity.Text = "Return Quantity";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(12, 228);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(95, 16);
            this.lblAmount.TabIndex = 8;
            this.lblAmount.Text = "Return Amount";
            // 
            // txtQty
            // 
            this.txtQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQty.Location = new System.Drawing.Point(116, 200);
            this.txtQty.MaxLength = 10;
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(100, 22);
            this.txtQty.TabIndex = 7;
            // 
            // lstListing
            // 
            this.lstListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstListing.FullRowSelect = true;
            this.lstListing.GridLines = true;
            this.lstListing.HideSelection = false;
            this.lstListing.Location = new System.Drawing.Point(12, 12);
            this.lstListing.MultiSelect = false;
            this.lstListing.Name = "lstListing";
            this.lstListing.Size = new System.Drawing.Size(774, 182);
            this.lstListing.TabIndex = 6;
            this.lstListing.UseCompatibleStateImageBehavior = false;
            this.lstListing.View = System.Windows.Forms.View.Details;
            this.lstListing.SelectedIndexChanged += new System.EventHandler(this.lstListing_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(686, 261);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 25);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(12, 261);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 25);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.Location = new System.Drawing.Point(116, 228);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 22);
            this.txtAmount.TabIndex = 2;
            // 
            // erpReturn
            // 
            this.erpReturn.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpReturn.ContainerControl = this;
            // 
            // frmReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 298);
            this.ControlBox = false;
            this.Controls.Add(this.pnlDiscount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmReturn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmDiscount_Load);
            this.pnlDiscount.ResumeLayout(false);
            this.pnlDiscount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpReturn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlDiscount;
        public System.Windows.Forms.TextBox txtAmount;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.TextBox txtQty;
        public System.Windows.Forms.ListView lstListing;
        public System.Windows.Forms.Label lblAmount;
        public System.Windows.Forms.Label lblQuantity;
        public System.Windows.Forms.ErrorProvider erpReturn;
        public System.Windows.Forms.Label lblUnit;
    }
}