﻿namespace STP
{
    partial class frmTransactionDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpTransaction = new System.Windows.Forms.TabPage();
            this.dgvTransaction = new System.Windows.Forms.DataGridView();
            this.tpPayment = new System.Windows.Forms.TabPage();
            this.dgvPayment = new System.Windows.Forms.DataGridView();
            this.tabMain.SuspendLayout();
            this.tpTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransaction)).BeginInit();
            this.tpPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tpTransaction);
            this.tabMain.Controls.Add(this.tpPayment);
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(780, 560);
            this.tabMain.TabIndex = 0;
            // 
            // tpTransaction
            // 
            this.tpTransaction.Controls.Add(this.dgvTransaction);
            this.tpTransaction.Location = new System.Drawing.Point(4, 25);
            this.tpTransaction.Name = "tpTransaction";
            this.tpTransaction.Padding = new System.Windows.Forms.Padding(3);
            this.tpTransaction.Size = new System.Drawing.Size(772, 531);
            this.tpTransaction.TabIndex = 0;
            this.tpTransaction.Text = "Transaction";
            this.tpTransaction.UseVisualStyleBackColor = true;
            // 
            // dgvTransaction
            // 
            this.dgvTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTransaction.Location = new System.Drawing.Point(8, 6);
            this.dgvTransaction.Name = "dgvTransaction";
            this.dgvTransaction.Size = new System.Drawing.Size(758, 522);
            this.dgvTransaction.TabIndex = 0;
            // 
            // tpPayment
            // 
            this.tpPayment.Controls.Add(this.dgvPayment);
            this.tpPayment.Location = new System.Drawing.Point(4, 25);
            this.tpPayment.Name = "tpPayment";
            this.tpPayment.Padding = new System.Windows.Forms.Padding(3);
            this.tpPayment.Size = new System.Drawing.Size(772, 531);
            this.tpPayment.TabIndex = 1;
            this.tpPayment.Text = "Payment";
            this.tpPayment.UseVisualStyleBackColor = true;
            // 
            // dgvPayment
            // 
            this.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPayment.Location = new System.Drawing.Point(6, 6);
            this.dgvPayment.Name = "dgvPayment";
            this.dgvPayment.Size = new System.Drawing.Size(760, 522);
            this.dgvPayment.TabIndex = 0;
            // 
            // frmTransactionDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.tabMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTransactionDebug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Debug Table";
            this.Load += new System.EventHandler(this.frmTransactionDebug_Load);
            this.tabMain.ResumeLayout(false);
            this.tpTransaction.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransaction)).EndInit();
            this.tpPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tabMain;
        public System.Windows.Forms.TabPage tpTransaction;
        public System.Windows.Forms.TabPage tpPayment;
        public System.Windows.Forms.DataGridView dgvTransaction;
        public System.Windows.Forms.DataGridView dgvPayment;
    }
}