﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmSelectCustomer : Form
    {
        #region Declarations

            private STP.Logic.General _Logic_General;

            private int _SearchType;
            private String _Current_Sort;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildCustomerList;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmSelectCustomer_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.BuildButton();
                        this._SearchType = 0;
                        this._Current_Sort = String.Empty;
                        this.SearchCustomer();
                        this.BuildAutoComplete();
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstCustomerListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, e.Column, this.ldm_BuildCustomerList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomerListing_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.SelectCustomer();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstCustomerListing_MouseDoubleClick(object sender, MouseEventArgs e)
                {
                    try
                    {
                        this.SelectCustomer();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtCustomerSearch_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this._SearchType = 1;
                            //this.SearchCustomer();
                            String[] sortInfo = null;
                            int columnIndex = 0;
                            if (this._Current_Sort != String.Empty)
                            {
                                sortInfo = this._Current_Sort.Split(new String[] { "[..]" }, StringSplitOptions.None);
                                columnIndex = int.Parse(sortInfo[2]);
                                if (sortInfo[1] == "ASC")
                                {
                                    this._Current_Sort = String.Empty;
                                }
                                else
                                {
                                    this._Current_Sort = this._Current_Sort.Replace("DESC", "ASC");
                                }
                            }
                            ND.UI.Winform.Form.SortListView(ref this.lstCustomerListing, ref this._Current_Sort, columnIndex, this.ldm_BuildCustomerList);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this._SearchType = 1;
                        this.SearchCustomer();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSelect_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.SelectCustomer();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Local Functions and Procedures

            public frmSelectCustomer()
            {
                try
                {
                    this.InitializeComponent();
                    this.Text = "Select Customer";
                    this._Logic_General = new STP.Logic.General();
                    this.ldm_BuildCustomerList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.SearchCustomer);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildButton()
            {
                try
                {
                    this.btnSearch.Image = Image.FromFile("image/Search.png");
                    this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnSelect.Image = Image.FromFile("image/Confirm.png");
                    this.btnSelect.TextImageRelation = TextImageRelation.ImageBeforeText;
                    this.btnClose.Image = Image.FromFile("image/Cancel.png");
                    this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchCustomer()
            {
                try
                {
                    this.SearchCustomer(ND.Data.DataObject.SortType.None, String.Empty);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SearchCustomer(ND.Data.DataObject.SortType sortType, String sortField)
            {
                try
                {
                    this.lstCustomerListing.Visible = false;
                    this.lstCustomerListing.Clear();
                    this.BuildCustomerListColumn();
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    switch (this._SearchType)
                    {
                        case 0:
                            {
                                break;
                            }
                        case 1:
                            {
                                ND.Data.ConditionField conditionFieldCustomerName = new ND.Data.ConditionField();
                                conditionFieldCustomerName.Field = "customername";
                                conditionFieldCustomerName.Value = "%" + this.txtCustomerSearch.Text + "%";
                                conditionFieldCustomerName.DataType = ND.Data.DataObject.DataType.String;
                                conditionFieldCustomerName.ConditionType = ND.Data.DataObject.ConditionType.Like;
                                conditions.Add(conditionFieldCustomerName);
                                break;
                            }
                    }
                    this._Logic_General.BuildCustomerList(this.lstCustomerListing, conditions, sortType, sortField);
                    this.FormatCustomerListColumn();
                    this.lstCustomerListing.Visible = true;
                    this.lstCustomerListing.Focus();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildCustomerListColumn()
            {
                try
                {
                    this.lstCustomerListing.Columns.Clear();
                    this.lstCustomerListing.Columns.Add("id", "ID");
                    this.lstCustomerListing.Columns.Add("customercode", "Code");
                    this.lstCustomerListing.Columns.Add("customername", "Name");
                    this.lstCustomerListing.Columns.Add("address1", "Address 1");
                    this.lstCustomerListing.Columns.Add("address2", "Address 2");
                    this.lstCustomerListing.Columns.Add("postcode", "Post Code");
                    this.lstCustomerListing.Columns.Add("city", "City");
                    this.lstCustomerListing.Columns.Add("area", "Area");
                    this.lstCustomerListing.Columns.Add("state", "State");
                    this.lstCustomerListing.Columns.Add("country", "Country");
                    this.lstCustomerListing.Columns.Add("tel", "Tel");
                    this.lstCustomerListing.Columns.Add("fax", "Fax");
                    this.lstCustomerListing.Columns.Add("contact", "Contact");
                    this.lstCustomerListing.Columns.Add("createddate", "Created Date");
                    this.lstCustomerListing.Columns.Add("flag", "Flag");
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void FormatCustomerListColumn()
            {
                try
                {
                    this.lstCustomerListing.Columns["customername"].Width = 150;
                    this.lstCustomerListing.Columns["customername"].TextAlign = HorizontalAlignment.Left;
                    this.lstCustomerListing.Columns["city"].Width = 100;
                    this.lstCustomerListing.Columns["city"].TextAlign = HorizontalAlignment.Left;
                    this.lstCustomerListing.Columns["area"].Width = 100;
                    this.lstCustomerListing.Columns["area"].TextAlign = HorizontalAlignment.Left;
                    this.lstCustomerListing.Columns["tel"].Width = 70;
                    this.lstCustomerListing.Columns["tel"].TextAlign = HorizontalAlignment.Left;
                    this.lstCustomerListing.Columns["fax"].Width = 70;
                    this.lstCustomerListing.Columns["fax"].TextAlign = HorizontalAlignment.Left;
                    this.lstCustomerListing.Columns["contact"].Width = 70;
                    this.lstCustomerListing.Columns["contact"].TextAlign = HorizontalAlignment.Left;

                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["id"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["customercode"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["address1"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["address2"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["postcode"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["state"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["country"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["createddate"]);
                    this.lstCustomerListing.Columns.Remove(this.lstCustomerListing.Columns["flag"]);
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void BuildAutoComplete()
            {
                try
                {
                    this.txtCustomerSearch.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtCustomerSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtCustomerSearch.AutoCompleteCustomSource = this._Logic_General.CustomerStringCollection();
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

            private void SelectCustomer()
            {
                try
                {
                    if (this.lstCustomerListing.SelectedItems.Count > 0)
                    {
                        if (this.Owner.GetType() == typeof(frmSales))
                        {
                            frmSales form = (frmSales)this.Owner;
                            form.txtCustomerID.Text = String.Empty;
                            form.txtCustomerID.Text = this.lstCustomerListing.SelectedItems[0].SubItems[0].Text;
                            form.txtBillNo.Focus();
                        }
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    ND.Log.LogWriter.WriteLog(ex.ToString());
                }
            }

        #endregion
    }
}
