﻿namespace STP
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlListing = new System.Windows.Forms.Panel();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstCustomerListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCustomerSearch = new System.Windows.Forms.TextBox();
            this.pnlDetail = new System.Windows.Forms.Panel();
            this.lblCustomerArea = new System.Windows.Forms.Label();
            this.txtCustomerArea = new System.Windows.Forms.TextBox();
            this.txtCustomerContactPerson = new System.Windows.Forms.TextBox();
            this.lblCustomerContact = new System.Windows.Forms.Label();
            this.txtCustomerFax = new System.Windows.Forms.TextBox();
            this.txtCustomerTelephone = new System.Windows.Forms.TextBox();
            this.lblCustomerFax = new System.Windows.Forms.Label();
            this.lblCustomerTelephone = new System.Windows.Forms.Label();
            this.txtCustomerCountry = new System.Windows.Forms.TextBox();
            this.lblCustomerCountry = new System.Windows.Forms.Label();
            this.grpControlDetail = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtCustomerState = new System.Windows.Forms.TextBox();
            this.txtCustomerCity = new System.Windows.Forms.TextBox();
            this.txtCustomerPostCode = new System.Windows.Forms.TextBox();
            this.txtCustomerAddress2 = new System.Windows.Forms.TextBox();
            this.txtCustomerAddress1 = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lblCustomerState = new System.Windows.Forms.Label();
            this.lblCustomerCity = new System.Windows.Forms.Label();
            this.lblCustomerPostCode = new System.Windows.Forms.Label();
            this.lblCustomerAddress2 = new System.Windows.Forms.Label();
            this.lblCustomerAddress1 = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.erpCustomer = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.grpControlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.Color.Transparent;
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstCustomerListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtCustomerSearch);
            this.pnlListing.Location = new System.Drawing.Point(5, 18);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 636);
            this.pnlListing.TabIndex = 1;
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnDelete);
            this.gpbControlListing.Controls.Add(this.btnEdit);
            this.gpbControlListing.Controls.Add(this.btnAdd);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 514);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(258, 21);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 43);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(132, 21);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(120, 43);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(6, 21);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(120, 43);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstCustomerListing
            // 
            this.lstCustomerListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCustomerListing.FullRowSelect = true;
            this.lstCustomerListing.GridLines = true;
            this.lstCustomerListing.HideSelection = false;
            this.lstCustomerListing.Location = new System.Drawing.Point(6, 34);
            this.lstCustomerListing.MultiSelect = false;
            this.lstCustomerListing.Name = "lstCustomerListing";
            this.lstCustomerListing.Size = new System.Drawing.Size(848, 474);
            this.lstCustomerListing.TabIndex = 0;
            this.lstCustomerListing.UseCompatibleStateImageBehavior = false;
            this.lstCustomerListing.View = System.Windows.Forms.View.Details;
            this.lstCustomerListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstCustomerListing_MouseDoubleClick);
            this.lstCustomerListing.SelectedIndexChanged += new System.EventHandler(this.lstCustomerListing_SelectedIndexChanged);
            this.lstCustomerListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstCustomerListing_ColumnClick);
            this.lstCustomerListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstCustomerListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(700, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search &Customer";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCustomerSearch
            // 
            this.txtCustomerSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerSearch.Location = new System.Drawing.Point(6, 6);
            this.txtCustomerSearch.MaxLength = 255;
            this.txtCustomerSearch.Name = "txtCustomerSearch";
            this.txtCustomerSearch.Size = new System.Drawing.Size(680, 22);
            this.txtCustomerSearch.TabIndex = 1;
            this.txtCustomerSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerName_KeyDown);
            // 
            // pnlDetail
            // 
            this.pnlDetail.Controls.Add(this.lblCustomerArea);
            this.pnlDetail.Controls.Add(this.txtCustomerArea);
            this.pnlDetail.Controls.Add(this.txtCustomerContactPerson);
            this.pnlDetail.Controls.Add(this.lblCustomerContact);
            this.pnlDetail.Controls.Add(this.txtCustomerFax);
            this.pnlDetail.Controls.Add(this.txtCustomerTelephone);
            this.pnlDetail.Controls.Add(this.lblCustomerFax);
            this.pnlDetail.Controls.Add(this.lblCustomerTelephone);
            this.pnlDetail.Controls.Add(this.txtCustomerCountry);
            this.pnlDetail.Controls.Add(this.lblCustomerCountry);
            this.pnlDetail.Controls.Add(this.grpControlDetail);
            this.pnlDetail.Controls.Add(this.txtCustomerState);
            this.pnlDetail.Controls.Add(this.txtCustomerCity);
            this.pnlDetail.Controls.Add(this.txtCustomerPostCode);
            this.pnlDetail.Controls.Add(this.txtCustomerAddress2);
            this.pnlDetail.Controls.Add(this.txtCustomerAddress1);
            this.pnlDetail.Controls.Add(this.txtCustomerName);
            this.pnlDetail.Controls.Add(this.txtCustomerCode);
            this.pnlDetail.Controls.Add(this.txtCustomerID);
            this.pnlDetail.Controls.Add(this.lblCustomerState);
            this.pnlDetail.Controls.Add(this.lblCustomerCity);
            this.pnlDetail.Controls.Add(this.lblCustomerPostCode);
            this.pnlDetail.Controls.Add(this.lblCustomerAddress2);
            this.pnlDetail.Controls.Add(this.lblCustomerAddress1);
            this.pnlDetail.Controls.Add(this.lblCustomerName);
            this.pnlDetail.Controls.Add(this.lblCustomerCode);
            this.pnlDetail.Controls.Add(this.lblID);
            this.pnlDetail.Location = new System.Drawing.Point(5, 18);
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.Size = new System.Drawing.Size(862, 636);
            this.pnlDetail.TabIndex = 2;
            this.pnlDetail.Visible = false;
            // 
            // lblCustomerArea
            // 
            this.lblCustomerArea.AutoSize = true;
            this.lblCustomerArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerArea.Location = new System.Drawing.Point(10, 220);
            this.lblCustomerArea.Name = "lblCustomerArea";
            this.lblCustomerArea.Size = new System.Drawing.Size(37, 16);
            this.lblCustomerArea.TabIndex = 31;
            this.lblCustomerArea.Text = "Area";
            // 
            // txtCustomerArea
            // 
            this.txtCustomerArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerArea.Location = new System.Drawing.Point(130, 220);
            this.txtCustomerArea.MaxLength = 255;
            this.txtCustomerArea.Name = "txtCustomerArea";
            this.txtCustomerArea.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerArea.TabIndex = 15;
            // 
            // txtCustomerContactPerson
            // 
            this.txtCustomerContactPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerContactPerson.Location = new System.Drawing.Point(130, 370);
            this.txtCustomerContactPerson.MaxLength = 50;
            this.txtCustomerContactPerson.Name = "txtCustomerContactPerson";
            this.txtCustomerContactPerson.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerContactPerson.TabIndex = 20;
            // 
            // lblCustomerContact
            // 
            this.lblCustomerContact.AutoSize = true;
            this.lblCustomerContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerContact.Location = new System.Drawing.Point(10, 370);
            this.lblCustomerContact.Name = "lblCustomerContact";
            this.lblCustomerContact.Size = new System.Drawing.Size(99, 16);
            this.lblCustomerContact.TabIndex = 29;
            this.lblCustomerContact.Text = "Contact Person";
            // 
            // txtCustomerFax
            // 
            this.txtCustomerFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerFax.Location = new System.Drawing.Point(130, 340);
            this.txtCustomerFax.MaxLength = 255;
            this.txtCustomerFax.Name = "txtCustomerFax";
            this.txtCustomerFax.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerFax.TabIndex = 19;
            // 
            // txtCustomerTelephone
            // 
            this.txtCustomerTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerTelephone.Location = new System.Drawing.Point(130, 310);
            this.txtCustomerTelephone.MaxLength = 255;
            this.txtCustomerTelephone.Name = "txtCustomerTelephone";
            this.txtCustomerTelephone.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerTelephone.TabIndex = 18;
            // 
            // lblCustomerFax
            // 
            this.lblCustomerFax.AutoSize = true;
            this.lblCustomerFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFax.Location = new System.Drawing.Point(10, 338);
            this.lblCustomerFax.Name = "lblCustomerFax";
            this.lblCustomerFax.Size = new System.Drawing.Size(30, 16);
            this.lblCustomerFax.TabIndex = 26;
            this.lblCustomerFax.Text = "Fax";
            // 
            // lblCustomerTelephone
            // 
            this.lblCustomerTelephone.AutoSize = true;
            this.lblCustomerTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerTelephone.Location = new System.Drawing.Point(10, 310);
            this.lblCustomerTelephone.Name = "lblCustomerTelephone";
            this.lblCustomerTelephone.Size = new System.Drawing.Size(74, 16);
            this.lblCustomerTelephone.TabIndex = 25;
            this.lblCustomerTelephone.Text = "Telephone";
            // 
            // txtCustomerCountry
            // 
            this.txtCustomerCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerCountry.Location = new System.Drawing.Point(130, 280);
            this.txtCustomerCountry.MaxLength = 255;
            this.txtCustomerCountry.Name = "txtCustomerCountry";
            this.txtCustomerCountry.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerCountry.TabIndex = 17;
            // 
            // lblCustomerCountry
            // 
            this.lblCustomerCountry.AutoSize = true;
            this.lblCustomerCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerCountry.Location = new System.Drawing.Point(10, 280);
            this.lblCustomerCountry.Name = "lblCustomerCountry";
            this.lblCustomerCountry.Size = new System.Drawing.Size(53, 16);
            this.lblCustomerCountry.TabIndex = 23;
            this.lblCustomerCountry.Text = "Country";
            // 
            // grpControlDetail
            // 
            this.grpControlDetail.Controls.Add(this.btnCancel);
            this.grpControlDetail.Controls.Add(this.btnSave);
            this.grpControlDetail.Location = new System.Drawing.Point(6, 514);
            this.grpControlDetail.Name = "grpControlDetail";
            this.grpControlDetail.Size = new System.Drawing.Size(848, 70);
            this.grpControlDetail.TabIndex = 21;
            this.grpControlDetail.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(722, 21);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 43);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Ca&ncel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(596, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 43);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtCustomerState
            // 
            this.txtCustomerState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerState.Location = new System.Drawing.Point(130, 250);
            this.txtCustomerState.MaxLength = 255;
            this.txtCustomerState.Name = "txtCustomerState";
            this.txtCustomerState.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerState.TabIndex = 16;
            // 
            // txtCustomerCity
            // 
            this.txtCustomerCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerCity.Location = new System.Drawing.Point(130, 190);
            this.txtCustomerCity.MaxLength = 255;
            this.txtCustomerCity.Name = "txtCustomerCity";
            this.txtCustomerCity.Size = new System.Drawing.Size(150, 22);
            this.txtCustomerCity.TabIndex = 14;
            // 
            // txtCustomerPostCode
            // 
            this.txtCustomerPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerPostCode.Location = new System.Drawing.Point(130, 160);
            this.txtCustomerPostCode.MaxLength = 50;
            this.txtCustomerPostCode.Name = "txtCustomerPostCode";
            this.txtCustomerPostCode.Size = new System.Drawing.Size(100, 22);
            this.txtCustomerPostCode.TabIndex = 13;
            // 
            // txtCustomerAddress2
            // 
            this.txtCustomerAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerAddress2.Location = new System.Drawing.Point(130, 130);
            this.txtCustomerAddress2.MaxLength = 255;
            this.txtCustomerAddress2.Name = "txtCustomerAddress2";
            this.txtCustomerAddress2.Size = new System.Drawing.Size(378, 22);
            this.txtCustomerAddress2.TabIndex = 12;
            // 
            // txtCustomerAddress1
            // 
            this.txtCustomerAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerAddress1.Location = new System.Drawing.Point(130, 100);
            this.txtCustomerAddress1.MaxLength = 255;
            this.txtCustomerAddress1.Name = "txtCustomerAddress1";
            this.txtCustomerAddress1.Size = new System.Drawing.Size(378, 22);
            this.txtCustomerAddress1.TabIndex = 11;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerName.Location = new System.Drawing.Point(130, 70);
            this.txtCustomerName.MaxLength = 255;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(252, 22);
            this.txtCustomerName.TabIndex = 10;
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.Enabled = false;
            this.txtCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerCode.Location = new System.Drawing.Point(130, 40);
            this.txtCustomerCode.MaxLength = 50;
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.Size = new System.Drawing.Size(252, 22);
            this.txtCustomerCode.TabIndex = 8;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Enabled = false;
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerID.Location = new System.Drawing.Point(130, 10);
            this.txtCustomerID.MaxLength = 6;
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(100, 22);
            this.txtCustomerID.TabIndex = 7;
            // 
            // lblCustomerState
            // 
            this.lblCustomerState.AutoSize = true;
            this.lblCustomerState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerState.Location = new System.Drawing.Point(10, 248);
            this.lblCustomerState.Name = "lblCustomerState";
            this.lblCustomerState.Size = new System.Drawing.Size(39, 16);
            this.lblCustomerState.TabIndex = 10;
            this.lblCustomerState.Text = "State";
            // 
            // lblCustomerCity
            // 
            this.lblCustomerCity.AutoSize = true;
            this.lblCustomerCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerCity.Location = new System.Drawing.Point(10, 190);
            this.lblCustomerCity.Name = "lblCustomerCity";
            this.lblCustomerCity.Size = new System.Drawing.Size(30, 16);
            this.lblCustomerCity.TabIndex = 9;
            this.lblCustomerCity.Text = "City";
            // 
            // lblCustomerPostCode
            // 
            this.lblCustomerPostCode.AutoSize = true;
            this.lblCustomerPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerPostCode.Location = new System.Drawing.Point(10, 160);
            this.lblCustomerPostCode.Name = "lblCustomerPostCode";
            this.lblCustomerPostCode.Size = new System.Drawing.Size(71, 16);
            this.lblCustomerPostCode.TabIndex = 6;
            this.lblCustomerPostCode.Text = "Post Code";
            // 
            // lblCustomerAddress2
            // 
            this.lblCustomerAddress2.AutoSize = true;
            this.lblCustomerAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerAddress2.Location = new System.Drawing.Point(10, 130);
            this.lblCustomerAddress2.Name = "lblCustomerAddress2";
            this.lblCustomerAddress2.Size = new System.Drawing.Size(69, 16);
            this.lblCustomerAddress2.TabIndex = 5;
            this.lblCustomerAddress2.Text = "Address 2";
            // 
            // lblCustomerAddress1
            // 
            this.lblCustomerAddress1.AutoSize = true;
            this.lblCustomerAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerAddress1.Location = new System.Drawing.Point(10, 100);
            this.lblCustomerAddress1.Name = "lblCustomerAddress1";
            this.lblCustomerAddress1.Size = new System.Drawing.Size(69, 16);
            this.lblCustomerAddress1.TabIndex = 4;
            this.lblCustomerAddress1.Text = "Address 1";
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerName.Location = new System.Drawing.Point(10, 70);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(45, 16);
            this.lblCustomerName.TabIndex = 3;
            this.lblCustomerName.Text = "Name";
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.AutoSize = true;
            this.lblCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerCode.Location = new System.Drawing.Point(10, 40);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(101, 16);
            this.lblCustomerCode.TabIndex = 2;
            this.lblCustomerCode.Text = "Customer Code";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(10, 10);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(21, 16);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // erpCustomer
            // 
            this.erpCustomer.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpCustomer.ContainerControl = this;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 614);
            this.ControlBox = false;
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlListing);
            this.Name = "frmCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomer_FormClosing);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.grpControlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.erpCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button btnEdit;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.ListView lstCustomerListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtCustomerSearch;
        public System.Windows.Forms.Panel pnlDetail;
        public System.Windows.Forms.TextBox txtCustomerCountry;
        public System.Windows.Forms.Label lblCustomerCountry;
        public System.Windows.Forms.GroupBox grpControlDetail;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.TextBox txtCustomerState;
        public System.Windows.Forms.TextBox txtCustomerCity;
        public System.Windows.Forms.TextBox txtCustomerPostCode;
        public System.Windows.Forms.TextBox txtCustomerAddress2;
        public System.Windows.Forms.TextBox txtCustomerAddress1;
        public System.Windows.Forms.TextBox txtCustomerName;
        public System.Windows.Forms.TextBox txtCustomerCode;
        public System.Windows.Forms.TextBox txtCustomerID;
        public System.Windows.Forms.Label lblCustomerState;
        public System.Windows.Forms.Label lblCustomerCity;
        public System.Windows.Forms.Label lblCustomerPostCode;
        public System.Windows.Forms.Label lblCustomerAddress2;
        public System.Windows.Forms.Label lblCustomerAddress1;
        public System.Windows.Forms.Label lblCustomerName;
        public System.Windows.Forms.Label lblCustomerCode;
        public System.Windows.Forms.Label lblID;
        public System.Windows.Forms.TextBox txtCustomerContactPerson;
        public System.Windows.Forms.Label lblCustomerContact;
        public System.Windows.Forms.TextBox txtCustomerFax;
        public System.Windows.Forms.TextBox txtCustomerTelephone;
        public System.Windows.Forms.Label lblCustomerFax;
        public System.Windows.Forms.Label lblCustomerTelephone;
        public System.Windows.Forms.ErrorProvider erpCustomer;
        public System.Windows.Forms.Label lblCustomerArea;
        public System.Windows.Forms.TextBox txtCustomerArea;
    }
}