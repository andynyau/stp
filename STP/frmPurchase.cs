﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace STP
{
    public partial class frmPurchase : Form
    {
        #region Declaration

            public DataTable Transaction_DataTable
            {
                get { return this._Logic_Transaction.Detail_Table; }
            }

            public DataTable Payment_DataTable
            {
                get { return this._Logic_Transaction.Payment_Table; }
            }

            private STP.Logic.General _Logic_General;
            private STP.Logic.Stock _Logic_Stock;
            private STP.Logic.Transaction _Logic_Transaction;
            private STP.Logic.Common.SaveType _SaveType;

            private ND.Standard.Validation.Winform.Form _ValidatorDetail;
            private ND.Standard.Validation.Winform.Form _ValidatorPayment;
            private ND.Standard.Validation.Winform.Form _ValidatorNew;
            private ND.Standard.Validation.Winform.Form _ValidatorEdit;
            private ND.Standard.Validation.Winform.Form _ValidatorDelete;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtPaymentReference;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_lstTransactionListing;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtTotalOutstanding;
            private ND.Standard.Validation.Winform.Field.DelegateCustomFunction ct_txtBillNo;

            private String _Current_Sort_Detail;
            private String _Current_Sort_Payment;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildDetailList;
            private ND.UI.Winform.Form.DelegateLoadDataMethod ldm_BuildPaymentList;

        #endregion

        #region Event Handlings

            #region Form Events

                private void frmPurchase_Load(object sender, EventArgs e)
                {
                    try
                    {
                        this.LoopTab();
                        this.BuildButton();
                        this.rbtNew.Checked = true;
                        this.btnSearch.Enabled = false;
                        this.txtTotalAmount.Text = "0.00";
                        this.txtTotalPaid.Text = "0.00";
                        this.txtTotalOutstanding.Text = "0.00";
                        this.dtpTransactionDate.Checked = false;
                        this.dtpPaymentDate.Checked = false;
                        this._Current_Sort_Detail = String.Empty;
                        this._Current_Sort_Payment = String.Empty;
                        this.BuildDetailList();
                        this.BuildPaymentList();
                        this.BuildAutoComplete();
                        this.SetValidation();
                        this.RemoveTransactionEnable(false);
                        this.RemovePaymentEnable(false);
                        this._Logic_Transaction.BuildPaymentTypeComboBox(this.cmbPaymentType);
                        this.lblPaymentReference.Text = String.Empty;
                        this._SaveType = STP.Logic.Common.SaveType.Insert;
                        Global.ShowWaiting(this, false);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void frmPurchase_FormClosing(object sender, FormClosingEventArgs e)
                {
                    try
                    {
                        this.CloseForm(e);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ListView Events

                private void lstTransactionListing_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.RemoveTransactionEnable(this.lstTransactionListing.SelectedItems.Count > 0);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstPaymentListing_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        this.RemovePaymentEnable(this.lstTransactionListing.SelectedItems.Count > 0);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstTransactionListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstTransactionListing, ref this._Current_Sort_Detail, e.Column, this.ldm_BuildDetailList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void lstPaymentListing_ColumnClick(object sender, ColumnClickEventArgs e)
                {
                    try
                    {
                        ND.UI.Winform.Form.SortListView(ref this.lstPaymentListing, ref this._Current_Sort_Payment, e.Column, this.ldm_BuildPaymentList);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Radio Button Events

                private void rbtNew_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.rbtNew.Checked == true)
                        {
                            this.btnSearch.Enabled = false;
                            this._SaveType = STP.Logic.Common.SaveType.Insert;
                            if (!this._Logic_Transaction.CheckDetailDataRowEmpty())
                            {
                                if (DialogResult.Yes == MessageBox.Show("Clear Data?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    this.ClearForm();
                                }
                            }
                            else
                            {
                                this.ClearForm();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void rbtEdit_CheckedChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.rbtEdit.Checked == true)
                        {
                            this.btnSearch.Enabled = true;
                            this._SaveType = STP.Logic.Common.SaveType.Update;
                            if (!this._Logic_Transaction.CheckDetailDataRowEmpty())
                            {
                                if (DialogResult.Yes == MessageBox.Show("Clear Data?", "Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    this.ClearForm();
                                }
                            }
                            else
                            {
                                this.ClearForm();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region ComboBox Events

                private void cmbUnit_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.cmbUnit.SelectedValue != null)
                        {
                            String uom = this.cmbUnit.SelectedValue.ToString();
                            this.txtUnitPrice.Text = this._Logic_Stock.GetUOMPrice(uom) == null ? this.txtUnitPrice.Text : this._Logic_Stock.GetUOMPrice(uom).ToString();
                            this.txtUnitPrice.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void cmbPaymentType_SelectedIndexChanged(object sender, EventArgs e)
                {
                    try
                    {
                        switch (this.cmbPaymentType.SelectedValue.ToString())
                        {
                            case "1":
                                {
                                    this.lblPaymentReference.Text = String.Empty;
                                    this.txtPaymentReference.Text = String.Empty;
                                    this.txtPaymentReference.Visible = false;
                                    break;
                                }
                            case "2":
                                {
                                    this.lblPaymentReference.Text = "Cheque No.";
                                    this.txtPaymentReference.Text = String.Empty;
                                    this.txtPaymentReference.Visible = true;
                                    break;
                                }
                            case "3":
                                {
                                    this.lblPaymentReference.Text = "Credit Card No.";
                                    this.txtPaymentReference.Text = String.Empty;
                                    this.txtPaymentReference.Visible = true;
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region TextBox Events

                private void txtTransactionNo_TextChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.txtTransactionNo.Text != String.Empty)
                        {
                            this.ShowTransaction();
                        }
                        else
                        {
                            this.ClearForm();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void txtVendorID_TextChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.txtVendorID.Text != String.Empty)
                        {
                            this.ShowVendor();
                        }
                        else
                        {
                            this.ClearVendorSelection();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void txtStockID_TextChanged(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.txtStockID.Text != String.Empty)
                        {
                            this.ShowItem();
                        }
                        else
                        {
                            //ClearDetailSelection();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void txtStockName_Leave(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.txtStockName.Text.Trim() != String.Empty)
                        {
                            if (this.txtStockName.Text.Contains("---") == true)
                            {
                                this.txtStockName.Text = this.txtStockName.Text.Split(new String[] { "---" }, StringSplitOptions.None)[1];
                            }
                            GetStockID();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void txtStockName_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            this.txtQuantity.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void txtQuantity_KeyDown(object sender, KeyEventArgs e)
                {
                    try
                    {
                        if (e.KeyCode == Keys.Enter)
                        {
                            e.SuppressKeyPress = true;
                            if (this.ValidationFormDetail() == true)
                            {
                                this.AddtoDetail();
                                this.ClearDetailSelection();
                                this.BuildDetailList();
                                this.CalculateAmount();
                                this.txtStockName.Focus();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

            #region Button Events

                private void btnSearch_Click(object sender, EventArgs e)
                {
                    try
                    {
                        frmSelectTransaction selectTransactionForm = new frmSelectTransaction();
                        selectTransactionForm.Owner = this;
                        selectTransactionForm.TransType = STP.Logic.Common.TransType.Purchase;
                        Global.ShowWaiting(this, true);
                        Application.DoEvents();
                        selectTransactionForm.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearchVendor_Click(object sender, EventArgs e)
                {
                    try
                    {
                        frmSelectVendor selectVendorForm = new frmSelectVendor();
                        selectVendorForm.Owner = this;
                        Global.ShowWaiting(this, true);
                        Application.DoEvents();
                        selectVendorForm.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSearchItem_Click(object sender, EventArgs e)
                {
                    try
                    {
                        frmSelectStock selectStockForm = new frmSelectStock();
                        selectStockForm.Owner = this;
                        Global.ShowWaiting(this, true);
                        Application.DoEvents();
                        selectStockForm.ShowDialog();
                        this.txtUnitPrice.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnSave_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirm Save?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.ValidationForm() == true)
                            {
                                if (((decimal.Parse(this.txtTotalOutstanding.Text.Trim()) > 0) && (DialogResult.Yes == MessageBox.Show("Save Without Full Payment?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))) || (decimal.Parse(this.txtTotalOutstanding.Text.Trim()) == 0))
                                {
                                    Global.ShowWaiting(this, true);
                                    Application.DoEvents();
                                    this._Logic_Transaction.Save_Type = this._SaveType;
                                    this._Logic_Transaction.Trans_Type = STP.Logic.Common.TransType.Purchase;

                                    this._Logic_Transaction.Object_Transaction_Header = new STP.Base.Transaction_Header_Object();
                                    this._Logic_Transaction.Object_Transaction_Header.Transaction_No = this.txtTransactionNo.Text.Trim();
                                    this._Logic_Transaction.Object_Transaction_Header.Transaction_DateTime = DateTime.Parse(String.Format("{0:dd/MMM/yyyy}", this.dtpTransactionDate.Value));
                                    this._Logic_Transaction.Object_Transaction_Header.Document_No = this.txtBillNo.Text.Trim();
                                    this._Logic_Transaction.Object_Transaction_Header.Customer_ID = int.Parse(this.txtVendorID.Text.Trim());
                                    this._Logic_Transaction.Object_Transaction_Header.Customer_Code = this.txtVendorCode.Text.Trim();
                                    this._Logic_Transaction.Object_Transaction_Header.Customer_Name = this.txtVendorName.Text.Trim();
                                    this._Logic_Transaction.Object_Transaction_Header.Remark = this.txtRemark.Text.Trim();
                                    if (this._Logic_Transaction.SaveTransaction() == true)
                                    {
                                        Global.ShowWaiting(this, false);
                                        Application.DoEvents();
                                        switch (this._SaveType)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Successfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                        this._SaveType = STP.Logic.Common.SaveType.Insert;
                                        this.ClearForm();
                                        this.rbtNew.Checked = true;
                                    }
                                    else
                                    {
                                        Global.ShowWaiting(this, false);
                                        Application.DoEvents();
                                        switch (this._SaveType)
                                        {
                                            case STP.Logic.Common.SaveType.Insert:
                                                {
                                                    MessageBox.Show("Insert Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                            case STP.Logic.Common.SaveType.Update:
                                                {
                                                    MessageBox.Show("Edit Failed", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                                    break;
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnClose_Click(object sender, EventArgs e)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnAdd_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.ValidationFormDetail() == true)
                        {
                            this.AddtoDetail();
                            this.ClearDetailSelection();
                            this.BuildDetailList();
                            this.CalculateAmount();
                            this.txtStockName.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnRemove_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.lstTransactionListing.SelectedItems.Count > 0)
                        {
                            this._Logic_Transaction.RemoveDetailDataRow(int.Parse(this.lstTransactionListing.SelectedItems[0].SubItems[1].Text));
                        }
                        this.BuildDetailList();
                        this.CalculateAmount();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnPaymentAdd_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.ValidationFormPayment() == true)
                        {
                            this.AddtoPayment();
                            this.ClearPaymentSelection();
                            this.BuildPaymentList();
                            this.CalculateAmount();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnPaymentRemove_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (this.lstPaymentListing.SelectedItems.Count > 0)
                        {
                            this._Logic_Transaction.RemovePaymentDataRow(int.Parse(this.lstPaymentListing.SelectedItems[0].SubItems[1].Text));
                        }
                        this.BuildPaymentList();
                        this.CalculateAmount();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void btnDelete_Click(object sender, EventArgs e)
                {
                    try
                    {
                        if (DialogResult.Yes == MessageBox.Show("Confirm Delete?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                        {
                            if (this.ValidationFormDelete() == true)
                            {
                                Global.ShowWaiting(this, true);
                                Application.DoEvents();
                                this._Logic_Transaction.Object_Transaction_Header = new STP.Base.Transaction_Header_Object();
                                this._Logic_Transaction.Object_Transaction_Header.Transaction_No = this.txtTransactionNo.Text.Trim();
                                if (this._Logic_Transaction.DeleteTransaction() == true)
                                {
                                    Global.ShowWaiting(this, false);
                                    Application.DoEvents();
                                    MessageBox.Show("Delete Successfully", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                    this.ClearForm();
                                    this.rbtNew.Checked = true;
                                }
                                else
                                {
                                    Global.ShowWaiting(this, false);
                                    Application.DoEvents();
                                    MessageBox.Show("Delete Failed", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

            #endregion

        #endregion

        #region Procedures and Functions

            #region Constructor

                public frmPurchase()
                {
                    this.InitializeComponent();
                    this.Text = "Purchase";
                    this._Logic_General = new STP.Logic.General();
                    this._Logic_Stock = new STP.Logic.Stock();
                    this._Logic_Transaction = new STP.Logic.Transaction();

                    this._ValidatorDetail = new ND.Standard.Validation.Winform.Form();
                    this._ValidatorPayment = new ND.Standard.Validation.Winform.Form();
                    this._ValidatorNew = new ND.Standard.Validation.Winform.Form();
                    this._ValidatorEdit = new ND.Standard.Validation.Winform.Form();
                    this._ValidatorDelete = new ND.Standard.Validation.Winform.Form();

                    this.ct_txtPaymentReference = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtPaymentReference_Validate);
                    this.ct_lstTransactionListing = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_lstTransactionListing_Validate);
                    this.ct_txtTotalOutstanding = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtTotalOutstanding_Validate);
                    this.ct_txtBillNo = new ND.Standard.Validation.Winform.Field.DelegateCustomFunction(this.ct_txtBillNo_Validate);

                    this.ldm_BuildDetailList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildDetailList);
                    this.ldm_BuildPaymentList = new ND.UI.Winform.Form.DelegateLoadDataMethod(this.BuildPaymentList);
                }

            #endregion

            #region Validations

                private Boolean ct_txtTotalOutstanding_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        result = (decimal.Parse(this.txtTotalOutstanding.Text.Trim()) >= 0);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ct_lstTransactionListing_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        result = !this._Logic_Transaction.CheckDetailDataRowEmpty();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ct_txtPaymentReference_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        switch (this.cmbPaymentType.SelectedValue.ToString())
                        {
                            case "1":
                                {
                                    result = true;
                                    break;
                                }
                            case "2":
                            case "3":
                                {
                                    if (this.txtPaymentReference.Text.Trim() == String.Empty)
                                    {
                                        result = false;
                                    }
                                    else
                                    {
                                        result = true;
                                    }
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ct_txtBillNo_Validate()
                {
                    Boolean result = false;
                    try
                    {
                        result = !this._Logic_Transaction.CheckDocumentNoExist(this.txtBillNo.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                    return result;
                }

            #endregion

            #region Methods

                private void LoopTab()
                {
                    try
                    {
                        int iCount = 0;
                        for (iCount = 0; iCount < this.tabMain.TabCount; iCount++)
                        {
                            this.tabMain.SelectedIndex = iCount;
                        }
                        this.tabMain.SelectedIndex = 0;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildAutoComplete()
                {
                    try
                    {
                        this.txtStockName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                        this.txtStockName.AutoCompleteSource = AutoCompleteSource.CustomSource;
                        this.txtStockName.AutoCompleteCustomSource = this._Logic_Stock.StringCollection();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildButton()
                {
                    try
                    {
                        this.btnSearch.Image = Image.FromFile("image/Search.png");
                        this.btnSearch.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnSearchVendor.Image = Image.FromFile("image/Search.png");
                        this.btnSearchVendor.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnSearchItem.Image = Image.FromFile("image/Search.png");
                        this.btnSearchItem.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnAdd.Image = Image.FromFile("image/New.png");
                        this.btnAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnSave.Image = Image.FromFile("image/Confirm.png");
                        this.btnSave.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnClose.Image = Image.FromFile("image/Cancel.png");
                        this.btnClose.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnRemove.Image = Image.FromFile("image/Delete.png");
                        this.btnRemove.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnDelete.Image = Image.FromFile("image/Delete.png");
                        this.btnDelete.TextImageRelation = TextImageRelation.ImageBeforeText;

                        this.btnPaymentAdd.Image = Image.FromFile("image/New.png");
                        this.btnPaymentAdd.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnPaymentRemove.Image = Image.FromFile("image/Delete.png");
                        this.btnPaymentRemove.TextImageRelation = TextImageRelation.ImageBeforeText;

                        this.btnDiscount.Image = Image.FromFile("image/Tip.png");
                        this.btnDiscount.TextImageRelation = TextImageRelation.ImageBeforeText;
                        this.btnReturn.Image = Image.FromFile("image/Return.png");
                        this.btnReturn.TextImageRelation = TextImageRelation.ImageBeforeText;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildDetailList()
                {
                    try
                    {
                        this.BuildDetailList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildDetailList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstTransactionListing.Visible = false;
                        this.lstTransactionListing.Clear();
                        this._Logic_Transaction.BuildDetailListColumn(this.lstTransactionListing);
                        this._Logic_Transaction.BuildDetailList(this.lstTransactionListing, sortType, sortField);
                        this.FormatDetailListColumn();
                        this.lstTransactionListing.Visible = true;
                        if (this.lstTransactionListing.Items.Count > 0)
                        {
                            this.lstTransactionListing.Items[0].Selected = true;
                            this.RemoveTransactionEnable(true);
                        }
                        else
                        {
                            this.RemoveTransactionEnable(false);
                        }
                        if (this.lstTransactionListing.Items.Count > 0)
                        {
                            this.lstTransactionListing.EnsureVisible(this.lstTransactionListing.Items.Count - 1);
                        }
                        this.lstTransactionListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildPaymentList()
                {
                    try
                    {
                        this.BuildPaymentList(ND.Data.DataObject.SortType.None, String.Empty);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void BuildPaymentList(ND.Data.DataObject.SortType sortType, String sortField)
                {
                    try
                    {
                        this.lstPaymentListing.Visible = false;
                        this.lstPaymentListing.Clear();
                        this._Logic_Transaction.BuildPaymentListColumn(this.lstPaymentListing);
                        this._Logic_Transaction.BuildPaymentList(this.lstPaymentListing, sortType, sortField);
                        this.FormatPaymentListColumn();
                        this.lstPaymentListing.Visible = true;
                        if (this.lstPaymentListing.Items.Count > 0)
                        {
                            this.lstPaymentListing.Items[0].Selected = true;
                            this.RemovePaymentEnable(true);
                        }
                        else
                        {
                            this.RemovePaymentEnable(false);
                        }
                        if (this.lstPaymentListing.Items.Count > 0)
                        {
                            this.lstPaymentListing.EnsureVisible(this.lstPaymentListing.Items.Count - 1);
                        }
                        this.lstPaymentListing.Focus();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatDetailListColumn()
                {
                    try
                    {
                        this.lstTransactionListing.Columns["stockcategoryname"].Width = 80;
                        this.lstTransactionListing.Columns["stockcategoryname"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["stocksubcategoryname"].Width = 80;
                        this.lstTransactionListing.Columns["stocksubcategoryname"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["stockcode"].Width = 80;
                        this.lstTransactionListing.Columns["stockcode"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["stockname"].Width = 150;
                        this.lstTransactionListing.Columns["stockname"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["billingqty"].Width = 80;
                        this.lstTransactionListing.Columns["billingqty"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["billinguom"].Width = 80;
                        this.lstTransactionListing.Columns["billinguom"].TextAlign = HorizontalAlignment.Left;
                        this.lstTransactionListing.Columns["billingprice"].Width = 80;
                        this.lstTransactionListing.Columns["billingprice"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["totalprice"].Width = 80;
                        this.lstTransactionListing.Columns["totalprice"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["stockuom"].Width = 80;
                        this.lstTransactionListing.Columns["stockuom"].TextAlign = HorizontalAlignment.Right;
                        this.lstTransactionListing.Columns["conversionfactor"].Width = 50;
                        this.lstTransactionListing.Columns["conversionfactor"].TextAlign = HorizontalAlignment.Right;

                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["transactionno"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["transactionline"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["transactionlinetype"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["refline"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockid"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockcategory"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stocksubcategory"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stocktype"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stocktypename"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockdesc1"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockdesc2"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockprice"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["stockcost"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["actualqty"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["returnedqty"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["referencetransactionno"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["referenceline"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["referencedocumentno"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["oldbalance"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["newbalance"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["createddate"]);
                        this.lstTransactionListing.Columns.Remove(this.lstTransactionListing.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void FormatPaymentListColumn()
                {
                    try
                    {
                        ND.UI.Winform.Form.FormatListView(ref this.lstPaymentListing, this.lstPaymentListing.Columns["paymentdate"].Index, typeof(System.DateTime), "{0:dd/MMM/yyyy}");

                        this.lstPaymentListing.Columns["paymenttypename"].Width = 150;
                        this.lstPaymentListing.Columns["paymenttypename"].TextAlign = HorizontalAlignment.Left;
                        this.lstPaymentListing.Columns["paymentamount"].Width = 180;
                        this.lstPaymentListing.Columns["paymentamount"].TextAlign = HorizontalAlignment.Right;
                        this.lstPaymentListing.Columns["paymentreference"].Width = 250;
                        this.lstPaymentListing.Columns["paymentreference"].TextAlign = HorizontalAlignment.Left;
                        this.lstPaymentListing.Columns["paymentdate"].Width = 150;
                        this.lstPaymentListing.Columns["paymentdate"].TextAlign = HorizontalAlignment.Left;

                        this.lstPaymentListing.Columns.Remove(this.lstPaymentListing.Columns["transactionno"]);
                        this.lstPaymentListing.Columns.Remove(this.lstPaymentListing.Columns["paymentline"]);
                        this.lstPaymentListing.Columns.Remove(this.lstPaymentListing.Columns["paymenttype"]);
                        this.lstPaymentListing.Columns.Remove(this.lstPaymentListing.Columns["createddate"]);
                        this.lstPaymentListing.Columns.Remove(this.lstPaymentListing.Columns["flag"]);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void RemoveTransactionEnable(Boolean enable)
                {
                    try
                    {
                        this.btnRemove.Enabled = enable;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void RemovePaymentEnable(Boolean enable)
                {
                    try
                    {
                        this.btnPaymentRemove.Enabled = enable;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void SetValidation()
                {
                    try
                    {
                        this._ValidatorNew.AddValidator(this.txtBillNo, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorNew.AddValidator(this.txtVendorName, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorNew.AddValidator(this.dtpTransactionDate, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorNew.AddValidator(this.lstTransactionListing, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstTransactionListing);
                        this._ValidatorNew.AddValidator(this.txtTotalOutstanding, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtTotalOutstanding);

                        this._ValidatorEdit.AddValidator(this.txtBillNo, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorEdit.AddValidator(this.txtVendorName, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorEdit.AddValidator(this.dtpTransactionDate, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorEdit.AddValidator(this.txtTransactionNo, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorEdit.AddValidator(this.lstTransactionListing, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_lstTransactionListing);
                        this._ValidatorEdit.AddValidator(this.txtTotalOutstanding, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtTotalOutstanding);

                        this._ValidatorDetail.AddValidator(this.txtStockName, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorDetail.AddValidator(this.txtUnitPrice, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorDetail.AddValidator(this.txtQuantity, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorDetail.AddValidator(this.cmbUnit, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorDetail.AddValidator(this.txtUnitPrice, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternDecimal);
                        this._ValidatorDetail.AddValidator(this.txtQuantity, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Regex, STP.Logic.Common.RegexPatternInteger);

                        this._ValidatorPayment.AddValidator(this.cmbPaymentType, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorPayment.AddValidator(this.txtPaymentAmount, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                        this._ValidatorPayment.AddValidator(this.txtPaymentReference, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Custom, this.ct_txtPaymentReference);

                        this._ValidatorDelete.AddValidator(this.txtTransactionNo, this.erpPurchase, ND.Standard.Validation.Winform.Field.FieldValidationType.Required);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ClearForm()
                {
                    this.txtTransactionNo.Text = String.Empty;
                    this.txtVendorID.Text = String.Empty;
                    this.txtVendorName.Text = String.Empty;
                    this.txtBillNo.Text = String.Empty;
                    this.dtpTransactionDate.Checked = false;
                    this.txtTotalAmount.Text = "0.00";
                    this.txtTotalPaid.Text = "0.00";
                    this.txtTotalOutstanding.Text = "0.00";
                    this.ClearDetailSelection();
                    this._Logic_Transaction.ClearDetailDataRow();
                    this.BuildDetailList();
                    this.ClearPaymentSelection();
                    this._Logic_Transaction.ClearPaymentDataRow();
                    this.BuildPaymentList();
                }

                private void AddtoDetail()
                {
                    try
                    {
                        STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                        objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtStockCategory.Text), int.Parse(this.txtSubCat.Text), int.Parse(this.txtStockID.Text));
                        if (objectStock != null)
                        {
                            STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                            objectTransactionDetail.Transaction_No = String.Empty;
                            objectTransactionDetail.Line_No = this._Logic_Transaction.Detail_Table == null ? 1 : this._Logic_Transaction.Detail_Table.Rows.Count + 1;
                            objectTransactionDetail.Line_Type = (int)Logic.Common.TransLineType.Stock;
                            objectTransactionDetail.Reference_Line = 0;
                            objectTransactionDetail.Stock_ID = objectStock.ID;
                            objectTransactionDetail.Stock_Category = objectStock.Stock_Category;
                            objectTransactionDetail.Stock_Category_Name = new STP.Logic.Stock().GetCategory((int)objectStock.Stock_Category).Category_Name;
                            objectTransactionDetail.Stock_Sub_Category = objectStock.Stock_Sub_Category;
                            objectTransactionDetail.Stock_Sub_Category_Name = new STP.Logic.Stock().GetSubCategory((int)objectStock.Stock_Sub_Category).Sub_Category_Name;
                            objectTransactionDetail.Stock_Type = objectStock.Stock_Type;
                            objectTransactionDetail.Stock_Type_Name = new STP.Logic.General().GetCodeDesc("STOCKTYPE", objectStock.Stock_Type.ToString());
                            objectTransactionDetail.Stock_Code = objectStock.Stock_Code;
                            objectTransactionDetail.Stock_Name = objectStock.Stock_Name;
                            objectTransactionDetail.Stock_Description_1 = objectStock.Stock_Description_1;
                            objectTransactionDetail.Stock_Description_2 = objectStock.Stock_Description_2;

                            long conversionFactor = this._Logic_Stock.GetUOMConFactor(this.cmbUnit.SelectedValue.ToString());
                            decimal billingPrice = decimal.Parse(String.Format("{0:0.00}", decimal.Parse(this.txtUnitPrice.Text.Trim())));
                            String billingUOM = this.cmbUnit.SelectedValue.ToString();
                            long billingQuantity = long.Parse(this.txtQuantity.Text.Trim());
                            decimal stockPrice = billingPrice / conversionFactor;
                            String stockUOM = objectStock.Stock_UOM;
                            long actualQuantity = billingQuantity * conversionFactor;

                            objectTransactionDetail.Stock_Price = stockPrice;
                            objectTransactionDetail.Stock_Cost = objectStock.Stock_Cost;
                            objectTransactionDetail.Stock_UOM = stockUOM;
                            objectTransactionDetail.Actual_Quantity = actualQuantity;
                            objectTransactionDetail.Billing_Quantity = billingQuantity;
                            objectTransactionDetail.Billing_UOM = billingUOM;
                            objectTransactionDetail.Billing_Price = billingPrice;
                            objectTransactionDetail.Conversion_Factor = conversionFactor;
                            objectTransactionDetail.Returned_Quantity = 0;
                            objectTransactionDetail.Reference_Transaction_No = String.Empty;
                            objectTransactionDetail.Reference_Line_No = 0;
                            objectTransactionDetail.Reference_Document_No = String.Empty;
                            objectTransactionDetail.Total_Price = billingPrice * long.Parse(this.txtQuantity.Text);
                            objectTransactionDetail.Old_Balance = 0;
                            objectTransactionDetail.New_Balance = 0;
                            objectTransactionDetail.CreatedDate = DateTime.Now;
                            objectTransactionDetail.Flag = true;
                            this._Logic_Transaction.AddDetailDataRow(objectTransactionDetail);
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void AddtoPayment()
                {
                    try
                    {
                        STP.Base.Transaction_Payment_Object objectTransactionPayment = new STP.Base.Transaction_Payment_Object();
                        objectTransactionPayment.Transaction_No = String.Empty;
                        objectTransactionPayment.Line_No = this._Logic_Transaction.Payment_Table == null ? 1 : this._Logic_Transaction.Payment_Table.Rows.Count + 1;
                        objectTransactionPayment.Payment_Date = DateTime.Parse(String.Format("{0:dd/MMM/yyyy}", this.dtpPaymentDate.Value));
                        objectTransactionPayment.Payment_Type = int.Parse(this.cmbPaymentType.SelectedValue.ToString());
                        objectTransactionPayment.Payment_Type_Name = this._Logic_General.GetCodeDesc("PAYMENTTYPE", this.cmbPaymentType.SelectedValue.ToString());
                        objectTransactionPayment.Payment_Amount = decimal.Parse(String.Format("{0:0.00}", decimal.Parse(this.txtPaymentAmount.Text.Trim())));
                        objectTransactionPayment.Payment_Reference = this.txtPaymentReference.Text.Trim();
                        objectTransactionPayment.CreatedDate = DateTime.Now;
                        objectTransactionPayment.Flag = true;
                        this._Logic_Transaction.AddPaymentDataRow(objectTransactionPayment);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ClearDetailSelection()
                {
                    try
                    {
                        this.txtStockCategory.Text = String.Empty;
                        this.txtStockID.Text = String.Empty;
                        this.txtStockCode.Text = String.Empty;
                        this.txtStockName.Text = String.Empty;
                        this.txtUnitPrice.Text = String.Empty;
                        this.txtQuantity.Text = String.Empty;
                        if (this.cmbUnit.Items.Count > 0)
                        {
                            this.cmbUnit.DataSource = null;
                            this.cmbUnit.Items.Clear();
                        }
                        if (this._Logic_Stock.UOM_Table != null)
                        {
                            this._Logic_Stock.UOM_Table.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ClearPaymentSelection()
                {
                    try
                    {
                        this.cmbPaymentType.SelectedIndex = 0;
                        this.txtPaymentAmount.Text = String.Empty;
                        this.txtPaymentReference.Text = String.Empty;
                        this.dtpPaymentDate.Checked = false;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void CloseForm(FormClosingEventArgs e)
                {
                    try
                    {
                        if (Global.ForceClose == false)
                        {
                            if ((this._Logic_Transaction.CheckDetailDataRowEmpty() == false) || (this._Logic_Transaction.CheckPaymentDataRowEmpty() == false))
                            {
                                String title = "Close";
                                String messageBody = "Close the Form?";

                                if (DialogResult.Yes != MessageBox.Show(messageBody, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                                {
                                    e.Cancel = true;
                                }
                                else
                                {
                                    ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                                }
                            }
                            else
                            {
                                ((frmMain)this.MdiParent).CurrentForm = frmMain.FormName.None;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ShowTransaction()
                {
                    try
                    {
                        STP.Base.Transaction_Header_Object objectTransactionHeader = new STP.Base.Transaction_Header_Object();
                        objectTransactionHeader = this._Logic_Transaction.GetTransaction(this.txtTransactionNo.Text);
                        if (objectTransactionHeader != null)
                        {
                            this.txtBillNo.Text = objectTransactionHeader.Document_No;
                            this.txtVendorID.Text = objectTransactionHeader.Customer_ID.ToString();
                            this.txtVendorCode.Text = objectTransactionHeader.Customer_Code;
                            this.txtVendorName.Text = objectTransactionHeader.Customer_Name;
                            this.dtpTransactionDate.Value = (DateTime)objectTransactionHeader.Transaction_DateTime;
                            this.dtpTransactionDate.Checked = true;
                            this.txtRemark.Text = objectTransactionHeader.Remark;
                            this.txtTotalAmount.Text = String.Format("{0:0.00}", objectTransactionHeader.Total_Amount);
                            this.txtTotalPaid.Text = String.Format("{0:0.00}", objectTransactionHeader.Paid_Amount);
                            this.CalculateAmount();
                            this.BuildDetailList();
                            this.BuildPaymentList();
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void CalculateAmount()
                {
                    try
                    {
                        decimal totalAmount = 0;
                        if (this._Logic_Transaction.Detail_Table != null)
                        {
                            DataRow[] dataRowTransaction = this._Logic_Transaction.Detail_Table.Select("flag = 1");
                            foreach (DataRow dataRow in dataRowTransaction)
                            {
                                totalAmount += (decimal)dataRow["totalprice"];
                            }
                        }
                        this.txtTotalAmount.Text = String.Format("{0:0.00}", totalAmount);

                        decimal totalPaid = 0;
                        if (this._Logic_Transaction.Payment_Table != null)
                        {
                            DataRow[] dataRowPayment = this._Logic_Transaction.Payment_Table.Select("flag = 1");
                            foreach (DataRow dataRow in dataRowPayment)
                            {
                                totalPaid += (decimal)dataRow["paymentamount"];
                            }
                        }
                        this.txtTotalPaid.Text = String.Format("{0:0.00}", totalPaid);

                        decimal totalOutstanding = totalAmount - totalPaid;
                        this.txtTotalOutstanding.Text = String.Format("{0:0.00}", totalOutstanding);
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ShowVendor()
                {
                    try
                    {
                        STP.Base.Vendor_Object objectVendor = new STP.Base.Vendor_Object();
                        objectVendor = this._Logic_General.GetVendor(int.Parse(this.txtVendorID.Text));
                        if (objectVendor != null)
                        {
                            this.txtVendorCode.Text = objectVendor.Vendor_Code;
                            this.txtVendorName.Text = objectVendor.Vendor_Name;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ClearVendorSelection()
                {
                    try
                    {
                        this.txtVendorID.Text = String.Empty;
                        this.txtVendorCode.Text = String.Empty;
                        this.txtVendorName.Text = String.Empty;
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void ShowItem()
                {
                    try
                    {
                        STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                        objectStock = this._Logic_Stock.GetStock(int.Parse(this.txtStockCategory.Text), int.Parse(this.txtSubCat.Text), int.Parse(this.txtStockID.Text));
                        if (objectStock != null)
                        {
                            this.txtStockName.Text = objectStock.Stock_Name;
                            this.txtUnitPrice.Text = objectStock.Stock_Price_1.ToString();
                            DataTable uomTable = this._Logic_Stock.UOM_Table;
                            this._Logic_Stock.BuildUOMComboBox((int)objectStock.Stock_Category, (int)objectStock.Stock_Sub_Category, (int)objectStock.ID, ref this.cmbUnit, ref uomTable);
                            this.txtQuantity.Text = "1";
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private void GetStockID()
                {
                    try
                    {
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        ND.Data.ConditionField conditionFieldStockName = new ND.Data.ConditionField();
                        conditionFieldStockName.Field = "stockname";
                        conditionFieldStockName.Value = this.txtStockName.Text.Trim();
                        conditionFieldStockName.DataType = ND.Data.DataObject.DataType.NString;
                        conditionFieldStockName.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                        conditions.Add(conditionFieldStockName);

                        STP.Base.Stock_Object objectStock = new STP.Base.Stock_Object();
                        objectStock = this._Logic_Stock.GetStock(conditions);

                        if (objectStock != null)
                        {
                            this.txtStockCategory.Text = objectStock.Stock_Category.ToString();
                            this.txtSubCat.Text = objectStock.Stock_Sub_Category.ToString();
                            this.txtStockID.Text = objectStock.ID.ToString();
                            this.txtStockCode.Text = objectStock.Stock_Code;
                        }
                        else
                        {
                            this.txtStockID.Text = String.Empty;
                            this.txtSubCat.Text = String.Empty;
                            this.txtStockCategory.Text = String.Empty;
                            this.txtStockCode.Text = String.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                    }
                }

                private Boolean ValidationFormDetail()
                {
                    Boolean result = false;
                    try
                    {
                        result = this._ValidatorDetail.Validate();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ValidationFormPayment()
                {
                    Boolean result = false;
                    try
                    {
                        result = this._ValidatorPayment.Validate();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ValidationForm()
                {
                    Boolean result = false;
                    try
                    {
                        switch (this._SaveType)
                        {
                            case STP.Logic.Common.SaveType.Insert:
                                {
                                    result = this._ValidatorNew.Validate();
                                    break;
                                }
                            case STP.Logic.Common.SaveType.Update:
                                {
                                    result = this._ValidatorEdit.Validate();
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

                private Boolean ValidationFormDelete()
                {
                    Boolean result = false;
                    try
                    {
                        result = this._ValidatorDelete.Validate();
                    }
                    catch (Exception ex)
                    {
                        ND.Log.LogWriter.WriteLog(ex.ToString());
                        result = false;
                    }
                    return result;
                }

            #endregion

        #endregion
    }
}
