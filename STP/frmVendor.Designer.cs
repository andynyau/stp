﻿namespace STP
{
    partial class frmVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlListing = new System.Windows.Forms.Panel();
            this.gpbControlListing = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstVendorListing = new System.Windows.Forms.ListView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtVendorSearch = new System.Windows.Forms.TextBox();
            this.pnlDetail = new System.Windows.Forms.Panel();
            this.lblVendorArea = new System.Windows.Forms.Label();
            this.txtVendorArea = new System.Windows.Forms.TextBox();
            this.txtVendorContactPerson = new System.Windows.Forms.TextBox();
            this.lblVendorContact = new System.Windows.Forms.Label();
            this.txtVendorFax = new System.Windows.Forms.TextBox();
            this.txtVendorTelephone = new System.Windows.Forms.TextBox();
            this.lblVendorFax = new System.Windows.Forms.Label();
            this.lblVendorTelephone = new System.Windows.Forms.Label();
            this.txtVendorCountry = new System.Windows.Forms.TextBox();
            this.lblVendorCountry = new System.Windows.Forms.Label();
            this.grpControlDetail = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtVendorState = new System.Windows.Forms.TextBox();
            this.txtVendorCity = new System.Windows.Forms.TextBox();
            this.txtVendorPostCode = new System.Windows.Forms.TextBox();
            this.txtVendorAddress2 = new System.Windows.Forms.TextBox();
            this.txtVendorAddress1 = new System.Windows.Forms.TextBox();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.txtVendorCode = new System.Windows.Forms.TextBox();
            this.txtVendorID = new System.Windows.Forms.TextBox();
            this.lblVendorState = new System.Windows.Forms.Label();
            this.lblVendorCity = new System.Windows.Forms.Label();
            this.lblVendorPostCode = new System.Windows.Forms.Label();
            this.lblVendorAddress2 = new System.Windows.Forms.Label();
            this.lblVendorAddress1 = new System.Windows.Forms.Label();
            this.lblVendorName = new System.Windows.Forms.Label();
            this.lblVendorCode = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.erpVendor = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlListing.SuspendLayout();
            this.gpbControlListing.SuspendLayout();
            this.pnlDetail.SuspendLayout();
            this.grpControlDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.erpVendor)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlListing
            // 
            this.pnlListing.BackColor = System.Drawing.Color.Transparent;
            this.pnlListing.Controls.Add(this.gpbControlListing);
            this.pnlListing.Controls.Add(this.lstVendorListing);
            this.pnlListing.Controls.Add(this.btnSearch);
            this.pnlListing.Controls.Add(this.txtVendorSearch);
            this.pnlListing.Location = new System.Drawing.Point(5, 18);
            this.pnlListing.Name = "pnlListing";
            this.pnlListing.Size = new System.Drawing.Size(862, 636);
            this.pnlListing.TabIndex = 1;
            // 
            // gpbControlListing
            // 
            this.gpbControlListing.Controls.Add(this.btnClose);
            this.gpbControlListing.Controls.Add(this.btnDelete);
            this.gpbControlListing.Controls.Add(this.btnEdit);
            this.gpbControlListing.Controls.Add(this.btnAdd);
            this.gpbControlListing.Location = new System.Drawing.Point(6, 514);
            this.gpbControlListing.Name = "gpbControlListing";
            this.gpbControlListing.Size = new System.Drawing.Size(848, 70);
            this.gpbControlListing.TabIndex = 4;
            this.gpbControlListing.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(722, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 43);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(258, 21);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 43);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(132, 21);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(120, 43);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(6, 21);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(120, 43);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstVendorListing
            // 
            this.lstVendorListing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstVendorListing.FullRowSelect = true;
            this.lstVendorListing.GridLines = true;
            this.lstVendorListing.HideSelection = false;
            this.lstVendorListing.Location = new System.Drawing.Point(6, 34);
            this.lstVendorListing.MultiSelect = false;
            this.lstVendorListing.Name = "lstVendorListing";
            this.lstVendorListing.Size = new System.Drawing.Size(848, 474);
            this.lstVendorListing.TabIndex = 0;
            this.lstVendorListing.UseCompatibleStateImageBehavior = false;
            this.lstVendorListing.View = System.Windows.Forms.View.Details;
            this.lstVendorListing.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstVendorListing_MouseDoubleClick);
            this.lstVendorListing.SelectedIndexChanged += new System.EventHandler(this.lstVendorListing_SelectedIndexChanged);
            this.lstVendorListing.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstVendorListing_ColumnClick);
            this.lstVendorListing.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstVendorListing_KeyDown);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(700, 6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(150, 25);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search &Vendor";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtVendorSearch
            // 
            this.txtVendorSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorSearch.Location = new System.Drawing.Point(6, 6);
            this.txtVendorSearch.MaxLength = 255;
            this.txtVendorSearch.Name = "txtVendorSearch";
            this.txtVendorSearch.Size = new System.Drawing.Size(680, 22);
            this.txtVendorSearch.TabIndex = 1;
            this.txtVendorSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVendorName_KeyDown);
            // 
            // pnlDetail
            // 
            this.pnlDetail.Controls.Add(this.lblVendorArea);
            this.pnlDetail.Controls.Add(this.txtVendorArea);
            this.pnlDetail.Controls.Add(this.txtVendorContactPerson);
            this.pnlDetail.Controls.Add(this.lblVendorContact);
            this.pnlDetail.Controls.Add(this.txtVendorFax);
            this.pnlDetail.Controls.Add(this.txtVendorTelephone);
            this.pnlDetail.Controls.Add(this.lblVendorFax);
            this.pnlDetail.Controls.Add(this.lblVendorTelephone);
            this.pnlDetail.Controls.Add(this.txtVendorCountry);
            this.pnlDetail.Controls.Add(this.lblVendorCountry);
            this.pnlDetail.Controls.Add(this.grpControlDetail);
            this.pnlDetail.Controls.Add(this.txtVendorState);
            this.pnlDetail.Controls.Add(this.txtVendorCity);
            this.pnlDetail.Controls.Add(this.txtVendorPostCode);
            this.pnlDetail.Controls.Add(this.txtVendorAddress2);
            this.pnlDetail.Controls.Add(this.txtVendorAddress1);
            this.pnlDetail.Controls.Add(this.txtVendorName);
            this.pnlDetail.Controls.Add(this.txtVendorCode);
            this.pnlDetail.Controls.Add(this.txtVendorID);
            this.pnlDetail.Controls.Add(this.lblVendorState);
            this.pnlDetail.Controls.Add(this.lblVendorCity);
            this.pnlDetail.Controls.Add(this.lblVendorPostCode);
            this.pnlDetail.Controls.Add(this.lblVendorAddress2);
            this.pnlDetail.Controls.Add(this.lblVendorAddress1);
            this.pnlDetail.Controls.Add(this.lblVendorName);
            this.pnlDetail.Controls.Add(this.lblVendorCode);
            this.pnlDetail.Controls.Add(this.lblID);
            this.pnlDetail.Location = new System.Drawing.Point(5, 18);
            this.pnlDetail.Name = "pnlDetail";
            this.pnlDetail.Size = new System.Drawing.Size(862, 636);
            this.pnlDetail.TabIndex = 2;
            this.pnlDetail.Visible = false;
            // 
            // lblVendorArea
            // 
            this.lblVendorArea.AutoSize = true;
            this.lblVendorArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorArea.Location = new System.Drawing.Point(10, 220);
            this.lblVendorArea.Name = "lblVendorArea";
            this.lblVendorArea.Size = new System.Drawing.Size(37, 16);
            this.lblVendorArea.TabIndex = 31;
            this.lblVendorArea.Text = "Area";
            // 
            // txtVendorArea
            // 
            this.txtVendorArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorArea.Location = new System.Drawing.Point(130, 220);
            this.txtVendorArea.MaxLength = 255;
            this.txtVendorArea.Name = "txtVendorArea";
            this.txtVendorArea.Size = new System.Drawing.Size(150, 22);
            this.txtVendorArea.TabIndex = 15;
            // 
            // txtVendorContactPerson
            // 
            this.txtVendorContactPerson.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorContactPerson.Location = new System.Drawing.Point(130, 370);
            this.txtVendorContactPerson.MaxLength = 50;
            this.txtVendorContactPerson.Name = "txtVendorContactPerson";
            this.txtVendorContactPerson.Size = new System.Drawing.Size(150, 22);
            this.txtVendorContactPerson.TabIndex = 20;
            // 
            // lblVendorContact
            // 
            this.lblVendorContact.AutoSize = true;
            this.lblVendorContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorContact.Location = new System.Drawing.Point(10, 370);
            this.lblVendorContact.Name = "lblVendorContact";
            this.lblVendorContact.Size = new System.Drawing.Size(99, 16);
            this.lblVendorContact.TabIndex = 29;
            this.lblVendorContact.Text = "Contact Person";
            // 
            // txtVendorFax
            // 
            this.txtVendorFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorFax.Location = new System.Drawing.Point(130, 340);
            this.txtVendorFax.MaxLength = 255;
            this.txtVendorFax.Name = "txtVendorFax";
            this.txtVendorFax.Size = new System.Drawing.Size(150, 22);
            this.txtVendorFax.TabIndex = 19;
            // 
            // txtVendorTelephone
            // 
            this.txtVendorTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorTelephone.Location = new System.Drawing.Point(130, 310);
            this.txtVendorTelephone.MaxLength = 255;
            this.txtVendorTelephone.Name = "txtVendorTelephone";
            this.txtVendorTelephone.Size = new System.Drawing.Size(150, 22);
            this.txtVendorTelephone.TabIndex = 18;
            // 
            // lblVendorFax
            // 
            this.lblVendorFax.AutoSize = true;
            this.lblVendorFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorFax.Location = new System.Drawing.Point(10, 338);
            this.lblVendorFax.Name = "lblVendorFax";
            this.lblVendorFax.Size = new System.Drawing.Size(30, 16);
            this.lblVendorFax.TabIndex = 26;
            this.lblVendorFax.Text = "Fax";
            // 
            // lblVendorTelephone
            // 
            this.lblVendorTelephone.AutoSize = true;
            this.lblVendorTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorTelephone.Location = new System.Drawing.Point(10, 310);
            this.lblVendorTelephone.Name = "lblVendorTelephone";
            this.lblVendorTelephone.Size = new System.Drawing.Size(74, 16);
            this.lblVendorTelephone.TabIndex = 25;
            this.lblVendorTelephone.Text = "Telephone";
            // 
            // txtVendorCountry
            // 
            this.txtVendorCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorCountry.Location = new System.Drawing.Point(130, 280);
            this.txtVendorCountry.MaxLength = 255;
            this.txtVendorCountry.Name = "txtVendorCountry";
            this.txtVendorCountry.Size = new System.Drawing.Size(150, 22);
            this.txtVendorCountry.TabIndex = 17;
            // 
            // lblVendorCountry
            // 
            this.lblVendorCountry.AutoSize = true;
            this.lblVendorCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorCountry.Location = new System.Drawing.Point(10, 280);
            this.lblVendorCountry.Name = "lblVendorCountry";
            this.lblVendorCountry.Size = new System.Drawing.Size(53, 16);
            this.lblVendorCountry.TabIndex = 23;
            this.lblVendorCountry.Text = "Country";
            // 
            // grpControlDetail
            // 
            this.grpControlDetail.Controls.Add(this.btnCancel);
            this.grpControlDetail.Controls.Add(this.btnSave);
            this.grpControlDetail.Location = new System.Drawing.Point(6, 514);
            this.grpControlDetail.Name = "grpControlDetail";
            this.grpControlDetail.Size = new System.Drawing.Size(848, 70);
            this.grpControlDetail.TabIndex = 21;
            this.grpControlDetail.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(722, 21);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 43);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "Ca&ncel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(596, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 43);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtVendorState
            // 
            this.txtVendorState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorState.Location = new System.Drawing.Point(130, 250);
            this.txtVendorState.MaxLength = 255;
            this.txtVendorState.Name = "txtVendorState";
            this.txtVendorState.Size = new System.Drawing.Size(150, 22);
            this.txtVendorState.TabIndex = 16;
            // 
            // txtVendorCity
            // 
            this.txtVendorCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorCity.Location = new System.Drawing.Point(130, 190);
            this.txtVendorCity.MaxLength = 255;
            this.txtVendorCity.Name = "txtVendorCity";
            this.txtVendorCity.Size = new System.Drawing.Size(150, 22);
            this.txtVendorCity.TabIndex = 14;
            // 
            // txtVendorPostCode
            // 
            this.txtVendorPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorPostCode.Location = new System.Drawing.Point(130, 160);
            this.txtVendorPostCode.MaxLength = 50;
            this.txtVendorPostCode.Name = "txtVendorPostCode";
            this.txtVendorPostCode.Size = new System.Drawing.Size(100, 22);
            this.txtVendorPostCode.TabIndex = 13;
            // 
            // txtVendorAddress2
            // 
            this.txtVendorAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorAddress2.Location = new System.Drawing.Point(130, 130);
            this.txtVendorAddress2.MaxLength = 255;
            this.txtVendorAddress2.Name = "txtVendorAddress2";
            this.txtVendorAddress2.Size = new System.Drawing.Size(378, 22);
            this.txtVendorAddress2.TabIndex = 12;
            // 
            // txtVendorAddress1
            // 
            this.txtVendorAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorAddress1.Location = new System.Drawing.Point(130, 100);
            this.txtVendorAddress1.MaxLength = 255;
            this.txtVendorAddress1.Name = "txtVendorAddress1";
            this.txtVendorAddress1.Size = new System.Drawing.Size(378, 22);
            this.txtVendorAddress1.TabIndex = 11;
            // 
            // txtVendorName
            // 
            this.txtVendorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorName.Location = new System.Drawing.Point(130, 70);
            this.txtVendorName.MaxLength = 255;
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(252, 22);
            this.txtVendorName.TabIndex = 10;
            // 
            // txtVendorCode
            // 
            this.txtVendorCode.Enabled = false;
            this.txtVendorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorCode.Location = new System.Drawing.Point(130, 40);
            this.txtVendorCode.MaxLength = 50;
            this.txtVendorCode.Name = "txtVendorCode";
            this.txtVendorCode.Size = new System.Drawing.Size(252, 22);
            this.txtVendorCode.TabIndex = 8;
            // 
            // txtVendorID
            // 
            this.txtVendorID.Enabled = false;
            this.txtVendorID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorID.Location = new System.Drawing.Point(130, 10);
            this.txtVendorID.MaxLength = 6;
            this.txtVendorID.Name = "txtVendorID";
            this.txtVendorID.Size = new System.Drawing.Size(100, 22);
            this.txtVendorID.TabIndex = 7;
            // 
            // lblVendorState
            // 
            this.lblVendorState.AutoSize = true;
            this.lblVendorState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorState.Location = new System.Drawing.Point(10, 248);
            this.lblVendorState.Name = "lblVendorState";
            this.lblVendorState.Size = new System.Drawing.Size(39, 16);
            this.lblVendorState.TabIndex = 10;
            this.lblVendorState.Text = "State";
            // 
            // lblVendorCity
            // 
            this.lblVendorCity.AutoSize = true;
            this.lblVendorCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorCity.Location = new System.Drawing.Point(10, 190);
            this.lblVendorCity.Name = "lblVendorCity";
            this.lblVendorCity.Size = new System.Drawing.Size(30, 16);
            this.lblVendorCity.TabIndex = 9;
            this.lblVendorCity.Text = "City";
            // 
            // lblVendorPostCode
            // 
            this.lblVendorPostCode.AutoSize = true;
            this.lblVendorPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorPostCode.Location = new System.Drawing.Point(10, 160);
            this.lblVendorPostCode.Name = "lblVendorPostCode";
            this.lblVendorPostCode.Size = new System.Drawing.Size(71, 16);
            this.lblVendorPostCode.TabIndex = 6;
            this.lblVendorPostCode.Text = "Post Code";
            // 
            // lblVendorAddress2
            // 
            this.lblVendorAddress2.AutoSize = true;
            this.lblVendorAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorAddress2.Location = new System.Drawing.Point(10, 130);
            this.lblVendorAddress2.Name = "lblVendorAddress2";
            this.lblVendorAddress2.Size = new System.Drawing.Size(69, 16);
            this.lblVendorAddress2.TabIndex = 5;
            this.lblVendorAddress2.Text = "Address 2";
            // 
            // lblVendorAddress1
            // 
            this.lblVendorAddress1.AutoSize = true;
            this.lblVendorAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorAddress1.Location = new System.Drawing.Point(10, 100);
            this.lblVendorAddress1.Name = "lblVendorAddress1";
            this.lblVendorAddress1.Size = new System.Drawing.Size(69, 16);
            this.lblVendorAddress1.TabIndex = 4;
            this.lblVendorAddress1.Text = "Address 1";
            // 
            // lblVendorName
            // 
            this.lblVendorName.AutoSize = true;
            this.lblVendorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorName.Location = new System.Drawing.Point(10, 70);
            this.lblVendorName.Name = "lblVendorName";
            this.lblVendorName.Size = new System.Drawing.Size(45, 16);
            this.lblVendorName.TabIndex = 3;
            this.lblVendorName.Text = "Name";
            // 
            // lblVendorCode
            // 
            this.lblVendorCode.AutoSize = true;
            this.lblVendorCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendorCode.Location = new System.Drawing.Point(10, 40);
            this.lblVendorCode.Name = "lblVendorCode";
            this.lblVendorCode.Size = new System.Drawing.Size(88, 16);
            this.lblVendorCode.TabIndex = 2;
            this.lblVendorCode.Text = "Vendor Code";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(10, 10);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(21, 16);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // erpVendor
            // 
            this.erpVendor.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.erpVendor.ContainerControl = this;
            // 
            // frmVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 614);
            this.ControlBox = false;
            this.Controls.Add(this.pnlDetail);
            this.Controls.Add(this.pnlListing);
            this.Name = "frmVendor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmVendor_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVendor_FormClosing);
            this.pnlListing.ResumeLayout(false);
            this.pnlListing.PerformLayout();
            this.gpbControlListing.ResumeLayout(false);
            this.pnlDetail.ResumeLayout(false);
            this.pnlDetail.PerformLayout();
            this.grpControlDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.erpVendor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlListing;
        public System.Windows.Forms.GroupBox gpbControlListing;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button btnEdit;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.ListView lstVendorListing;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtVendorSearch;
        public System.Windows.Forms.Panel pnlDetail;
        public System.Windows.Forms.TextBox txtVendorCountry;
        public System.Windows.Forms.Label lblVendorCountry;
        public System.Windows.Forms.GroupBox grpControlDetail;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.TextBox txtVendorState;
        public System.Windows.Forms.TextBox txtVendorCity;
        public System.Windows.Forms.TextBox txtVendorPostCode;
        public System.Windows.Forms.TextBox txtVendorAddress2;
        public System.Windows.Forms.TextBox txtVendorAddress1;
        public System.Windows.Forms.TextBox txtVendorName;
        public System.Windows.Forms.TextBox txtVendorCode;
        public System.Windows.Forms.TextBox txtVendorID;
        public System.Windows.Forms.Label lblVendorState;
        public System.Windows.Forms.Label lblVendorCity;
        public System.Windows.Forms.Label lblVendorPostCode;
        public System.Windows.Forms.Label lblVendorAddress2;
        public System.Windows.Forms.Label lblVendorAddress1;
        public System.Windows.Forms.Label lblVendorName;
        public System.Windows.Forms.Label lblVendorCode;
        public System.Windows.Forms.Label lblID;
        public System.Windows.Forms.TextBox txtVendorContactPerson;
        public System.Windows.Forms.Label lblVendorContact;
        public System.Windows.Forms.TextBox txtVendorFax;
        public System.Windows.Forms.TextBox txtVendorTelephone;
        public System.Windows.Forms.Label lblVendorFax;
        public System.Windows.Forms.Label lblVendorTelephone;
        public System.Windows.Forms.ErrorProvider erpVendor;
        public System.Windows.Forms.Label lblVendorArea;
        public System.Windows.Forms.TextBox txtVendorArea;
    }
}