﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace STP.Logic
{
    public class General : Common
    {
        #region Fields

        private STP.Base.Code_Master_Base _Base_Code_Master = new STP.Base.Code_Master_Base();
        private STP.Base.Customer_Base _Base_Customer = new STP.Base.Customer_Base();
        private STP.Base.Vendor_Base _Base_Vendor = new STP.Base.Vendor_Base();

        #endregion

        #region Properties

        #endregion

        #region Methods

        public General()
        {
        }

        #region Customer

        public Boolean BuildCustomerList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean checkBox)
        {
            return this.BuildCustomerList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty, checkBox);
        }

        public Boolean BuildCustomerList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            return this.BuildCustomerList(listView, conditions, sortType, sortField, false);
        }

        public Boolean BuildCustomerList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean checkBox)
        {
            Boolean result = false;
            try
            {
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Customer.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, checkBox, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public AutoCompleteStringCollection CustomerStringCollection()
        {
            AutoCompleteStringCollection result = new AutoCompleteStringCollection();
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldCustomerName = new ND.Data.DataField();
                fieldCustomerName.Field = "customername";
                fieldCustomerName.Value = String.Empty;
                fieldCustomerName.DataType = ND.Data.DataObject.DataType.String;
                fieldCustomerName.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldCustomerName);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Customer.GetCustomDataTable(fields, conditions);
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    result.Add(dataRow["customername"].ToString());
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean SaveCustomer(STP.Base.Customer_Object objectCustomer, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Customer.SaveData(objectCustomer, (STP.Base.ABSTRACT_Base.SaveType)saveType);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteCustomer(int id)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Customer.DeleteData(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public STP.Base.Customer_Object GetCustomer(int id)
        {
            STP.Base.Customer_Object result = null;
            try
            {
                result = this._Base_Customer.GetSingleObject(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Customer_Object GetCustomer(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Customer_Object result = null;
            try
            {
                result = this._Base_Customer.GetSingleObject<STP.Base.Customer_Object>(conditions);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean CheckCustomerExist(List<ND.Data.ConditionField> conditions)
        {
            Boolean result = false;
            try
            {
                STP.Base.Customer_Object objectCustomer = this._Base_Customer.GetSingleObject<STP.Base.Customer_Object>(conditions);
                if (objectCustomer == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion

        #region Vendor

        public Boolean BuildVendorList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean checkBox)
        {
            return this.BuildVendorList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty, checkBox);
        }

        public Boolean BuildVendorList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            return this.BuildVendorList(listView, conditions, sortType, sortField, false);
        }

        public Boolean BuildVendorList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean checkBox)
        {
            Boolean result = false;
            try
            {
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Vendor.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, checkBox, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public AutoCompleteStringCollection VendorStringCollection()
        {
            AutoCompleteStringCollection result = new AutoCompleteStringCollection();
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldVendorName = new ND.Data.DataField();
                fieldVendorName.Field = "Vendorname";
                fieldVendorName.Value = String.Empty;
                fieldVendorName.DataType = ND.Data.DataObject.DataType.String;
                fieldVendorName.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldVendorName);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Vendor.GetCustomDataTable(fields, conditions);
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    result.Add(dataRow["Vendorname"].ToString());
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean SaveVendor(STP.Base.Vendor_Object objectVendor, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Vendor.SaveData(objectVendor, (STP.Base.ABSTRACT_Base.SaveType)saveType);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteVendor(int id)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Vendor.DeleteData(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public STP.Base.Vendor_Object GetVendor(int id)
        {
            STP.Base.Vendor_Object result = null;
            try
            {
                result = this._Base_Vendor.GetSingleObject(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Vendor_Object GetVendor(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Vendor_Object result = null;
            try
            {
                result = this._Base_Vendor.GetSingleObject<STP.Base.Vendor_Object>(conditions);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean CheckVendorExist(List<ND.Data.ConditionField> conditions)
        {
            Boolean result = false;
            try
            {
                STP.Base.Vendor_Object objectVendor = this._Base_Vendor.GetSingleObject<STP.Base.Vendor_Object>(conditions);
                if (objectVendor == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion

        #region CodeMaster

        public Boolean BuildCodeMasterComboBox(ComboBox comboBox, String codeType)
        {
            Boolean result = false;
            try
            {
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldCodeType = new ND.Data.ConditionField();
                conditionFieldCodeType.Field = "codetype";
                conditionFieldCodeType.Value = codeType;
                conditionFieldCodeType.DataType = ND.Data.DataObject.DataType.String;
                conditionFieldCodeType.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldCodeType);
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.String;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Code_Master.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateComboBox(ref comboBox, dataTable, "codedesc", "code", ND.Data.DataObject.SortType.None, false);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public String GetCodeDesc(String type, String code)
        {
            String result = String.Empty;
            try
            {
                STP.Base.Code_Master_Object objectCodeMaster = new STP.Base.Code_Master_Object();
                objectCodeMaster = this._Base_Code_Master.GetSingleObject(type, code);
                if (objectCodeMaster != null)
                {
                    result = objectCodeMaster.Description;
                }
                else
                {
                    result = String.Empty;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = String.Empty;
            }
            return result;
        }

        #endregion

        #endregion

    }
}
