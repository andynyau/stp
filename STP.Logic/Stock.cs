﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace STP.Logic
{
    public class Stock : Common
    {
        #region Fields

        private DataTable _UOM_Table;
        private STP.Base.Stock_Base _Base_Stock = new STP.Base.Stock_Base();
        private STP.Base.Category_Base _Base_Category = new STP.Base.Category_Base();
        private STP.Base.Sub_Category_Base _Base_Sub_Category = new STP.Base.Sub_Category_Base();

        #endregion

        #region Properties

        public DataTable UOM_Table
        {
            get { return this._UOM_Table; }
            set { this._UOM_Table = value; }
        }

        #endregion

        #region Methods

        public Stock()
        {
        }

        public Boolean BuildCategoryList(ListView listView, List<ND.Data.ConditionField> conditions)
        {
            return this.BuildCategoryList(listView, conditions, false);
        }

        public Boolean BuildCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean firstEmpty)
        {
            return this.BuildCategoryList(listView, conditions, false, firstEmpty);
        }

        public Boolean BuildCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean checkBox, Boolean firstEmpty)
        {
            return this.BuildCategoryList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty, checkBox, firstEmpty);
        }

        public Boolean BuildCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean firstEmpty)
        {
            return this.BuildCategoryList(listView, conditions, sortType, sortField, false, firstEmpty);
        }

        public Boolean BuildCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean checkBox, Boolean firstEmpty)
        {
            Boolean result = false;
            try
            {
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Category.GetDataTable(conditions);
                if (firstEmpty == true)
                {
                    ND.Data.DataObject.AddEmptyDataRow(ref dataTable);
                    dataTable.Rows[0]["categoryname"] = "(-All-)";
                }
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, checkBox, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildSubCategoryList(ListView listView, List<ND.Data.ConditionField> conditions)
        {
            return this.BuildSubCategoryList(listView, conditions, false);
        }

        public Boolean BuildSubCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean firstEmpty)
        {
            return this.BuildSubCategoryList(listView, conditions, false, firstEmpty);
        }

        public Boolean BuildSubCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean checkBox, Boolean firstEmpty)
        {
            return this.BuildSubCategoryList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty, checkBox, firstEmpty);
        }

        public Boolean BuildSubCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean firstEmpty)
        {
            return this.BuildSubCategoryList(listView, conditions, sortType, sortField, false, firstEmpty);
        }

        public Boolean BuildSubCategoryList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean checkBox, Boolean firstEmpty)
        {
            Boolean result = false;
            try
            {
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Sub_Category.GetDataTable(conditions);
                if (firstEmpty == true)
                {
                    ND.Data.DataObject.AddEmptyDataRow(ref dataTable);
                    dataTable.Rows[0]["subcategoryname"] = "(-All-)";
                }
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, checkBox, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildSubCategoryCombo(ComboBox comboBox, int category)
        {
            Boolean result = false;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldDisplay = new ND.Data.DataField();
                fieldDisplay.Field = "subcategoryshortcut" + " + '---' + " + "subcategoryname" + " AS display";
                fieldDisplay.Value = String.Empty;
                fieldDisplay.DataType = ND.Data.DataObject.DataType.String;
                fieldDisplay.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldDisplay);
                ND.Data.DataField fieldId = new ND.Data.DataField();
                fieldId.Field = "id";
                fieldId.Value = String.Empty;
                fieldId.DataType = ND.Data.DataObject.DataType.String;
                fieldId.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldId);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                conditionFieldStockCategory.Field = "stockcategory";
                conditionFieldStockCategory.Value = category;
                conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockCategory);
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Sub_Category.GetCustomDataTable(fields, conditions);
                result = ND.UI.Winform.Form.PopulateComboBox(ref comboBox, dataTable, "display", "id", ND.Data.DataObject.SortType.Ascending, true);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildCategoryCombo(ComboBox comboBox)
        {
            Boolean result = false;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldDisplay = new ND.Data.DataField();
                fieldDisplay.Field = "categoryshortcut" + " + '---' + " + "categoryname" + " AS display";
                fieldDisplay.Value = String.Empty;
                fieldDisplay.DataType = ND.Data.DataObject.DataType.String;
                fieldDisplay.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldDisplay);
                ND.Data.DataField fieldId = new ND.Data.DataField();
                fieldId.Field = "id";
                fieldId.Value = String.Empty;
                fieldId.DataType = ND.Data.DataObject.DataType.String;
                fieldId.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldId);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Category.GetCustomDataTable(fields, conditions);
                result = ND.UI.Winform.Form.PopulateComboBox(ref comboBox, dataTable, "display", "id", ND.Data.DataObject.SortType.Ascending, true);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildTypeCombo(ComboBox comboBox)
        {
            Boolean result = false;
            try
            {
                result = new General().BuildCodeMasterComboBox(comboBox, "STOCKTYPE");
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildStockList(ListView listView, List<ND.Data.ConditionField> conditions)
        {
            return this.BuildStockList(listView, conditions, false);
        }

        public Boolean BuildStockList(ListView listView, List<ND.Data.ConditionField> conditions, Boolean checkBox)
        {
            return this.BuildStockList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty, checkBox);
        }

        public Boolean BuildStockList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            return this.BuildStockList(listView, conditions, sortType, sortField, false);
        }

        public Boolean BuildStockList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField, Boolean checkBox)
        {
            Boolean result = false;
            try
            {
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Stock.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, checkBox, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public AutoCompleteStringCollection StringCollection()
        {
            AutoCompleteStringCollection result = new AutoCompleteStringCollection();
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldDisplay = new ND.Data.DataField();
                fieldDisplay.Field = STP.Base.Static_Item.Table_Stock + ".stockcode + '---' + " + STP.Base.Static_Item.Table_Stock + ".stockname AS display";
                fieldDisplay.Value = String.Empty;
                fieldDisplay.DataType = ND.Data.DataObject.DataType.String;
                fieldDisplay.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldDisplay);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = STP.Base.Static_Item.Table_Stock + ".flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                List<ND.Data.OrderField> orders = new List<ND.Data.OrderField>();
                ND.Data.OrderField orderFieldStockName = new ND.Data.OrderField();
                orderFieldStockName.Field = STP.Base.Static_Item.Table_Stock + ".stockname";
                orderFieldStockName.SortType = ND.Data.DataObject.SortType.Ascending;
                orders.Add(orderFieldStockName);
                DataTable dataTable = this._Base_Stock.GetCustomDataTable(fields, conditions, orders);
                foreach (DataRow dataRow in dataTable.Rows)
                {
                   result.Add(dataRow["display"].ToString());
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean SaveStock(STP.Base.Stock_Object objectStock, SaveType saveType, Boolean updateTransaction)
        {
            Boolean result = false;
            try
            {
                if (saveType == SaveType.Insert)
                {
                    objectStock.ID = this.GenerateNewStockID((int)objectStock.Stock_Category, (int)objectStock.Stock_Sub_Category);
                    objectStock.Stock_Code = this.GenerateStockCode(objectStock);
                }
                else
                {
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    ND.Data.ConditionField conditionFieldStockCode = new ND.Data.ConditionField();
                    conditionFieldStockCode.Field = "stockcode";
                    conditionFieldStockCode.Value = objectStock.Stock_Code;
                    conditionFieldStockCode.DataType = ND.Data.DataObject.DataType.String;
                    conditionFieldStockCode.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                    conditions.Add(conditionFieldStockCode);
                    STP.Base.Stock_Object objectStockOriginal = this._Base_Stock.GetSingleObject<STP.Base.Stock_Object>(conditions);
                    if (objectStockOriginal != null)
                    {
                        if ((objectStockOriginal.Stock_Category != objectStock.Stock_Category) ||
                            (objectStockOriginal.Stock_Sub_Category != objectStock.Stock_Sub_Category))
                        {
                            objectStock.ID = this.GenerateNewStockID((int)objectStock.Stock_Category, (int)objectStock.Stock_Sub_Category);
                            objectStock.Stock_Code = this.GenerateStockCode(objectStock);
                        }
                    }
                }
                result = this._Base_Stock.SaveData(objectStock, (STP.Base.ABSTRACT_Base.SaveType)saveType, updateTransaction);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteStock(int stockCategory, int stockSubCategory, int id)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Stock.DeleteData(stockCategory, stockSubCategory, id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public STP.Base.Stock_Object GetStock(int stockCategory, int stockSubCategory, int id)
        {
            STP.Base.Stock_Object result = null;
            try
            {
                result = this._Base_Stock.GetSingleObject(stockCategory, stockSubCategory, id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Stock_Object GetStock(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Stock_Object result = null;
            try
            {
                result = this._Base_Stock.GetSingleObject<STP.Base.Stock_Object>(conditions);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean CheckExist(List<ND.Data.ConditionField> conditions)
        {
            Boolean result = false;
            try
            {
                STP.Base.Stock_Object objectStock = this._Base_Stock.GetSingleObject<STP.Base.Stock_Object>(conditions);
                if (objectStock == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckExistCategory(List<ND.Data.ConditionField> conditions)
        {
            Boolean result = false;
            try
            {
                STP.Base.Category_Object objectCategory = this._Base_Category.GetSingleObject<STP.Base.Category_Object>(conditions);
                if (objectCategory == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckExistSubCategory(List<ND.Data.ConditionField> conditions)
        {
            Boolean result = false;
            try
            {
                STP.Base.Sub_Category_Object objectSubCategory = this._Base_Sub_Category.GetSingleObject<STP.Base.Sub_Category_Object>(conditions);
                if (objectSubCategory == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public int CheckSubCategoryAssociate(int category, int subCategory)
        {
            int result = 0;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldItemCount = new ND.Data.DataField();
                fieldItemCount.Field = "COUNT(id) AS Item_Count";
                fieldItemCount.Value = String.Empty;
                fieldItemCount.DataType = ND.Data.DataObject.DataType.Numeric;
                fieldItemCount.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldItemCount);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                conditionFieldStockCategory.Field = "stockcategory";
                conditionFieldStockCategory.Value = category;
                conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockCategory);
                ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
                conditionFieldStockSubCategory.Field = "stocksubcategory";
                conditionFieldStockSubCategory.Value = subCategory;
                conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockSubCategory);
                DataTable dataTable = this._Base_Stock.GetCustomDataTable(fields, conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        result = (int)dataTable.Rows[0]["Item_Count"];
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public STP.Base.Category_Object GetCategory(int id)
        {
            STP.Base.Category_Object result = null;
            try
            {
                result = this._Base_Category.GetSingleObject(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Category_Object GetCategory(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Category_Object result = null;
            try
            {
                result = this._Base_Category.GetSingleObject<STP.Base.Category_Object>(conditions);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Sub_Category_Object GetSubCategory(int id)
        {
            STP.Base.Sub_Category_Object result = null;
            try
            {
                result = this._Base_Sub_Category.GetSingleObject(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Sub_Category_Object GetSubCategory(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Sub_Category_Object result = null;
            try
            {
                result = this._Base_Sub_Category.GetSingleObject<STP.Base.Sub_Category_Object>(conditions);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public void BuildUOMComboBox(int stockCategory, int stockSubCategory, int id, ref ComboBox comboBox, ref DataTable dataTable)
        {
            try
            {
                STP.Base.Stock_Object objectStock = this._Base_Stock.GetSingleObject(stockCategory, stockSubCategory, id);
                dataTable = new DataTable();
                dataTable.Columns.Clear();
                dataTable.Columns.Add("stockuom", Type.GetType("System.String"));
                dataTable.Columns.Add("conversionfactor", Type.GetType("System.Int64"));
                dataTable.Columns.Add("stockprice", Type.GetType("System.Decimal"));
                if (objectStock != null)
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow["stockuom"] = objectStock.Stock_UOM;
                    dataRow["conversionfactor"] = 1;
                    dataRow["stockprice"] = objectStock.Stock_Price_1;
                    dataTable.Rows.Add(dataRow);
                    dataTable.AcceptChanges();
                    if ((objectStock.Package_UOM_1 != String.Empty) && (objectStock.Package_Size_1 != 0))
                    {
                        DataRow dataRow1 = dataTable.NewRow();
                        dataRow1["stockuom"] = objectStock.Package_UOM_1 + "(" + objectStock.Package_Size_1 + objectStock.Stock_UOM + ")";
                        dataRow1["conversionfactor"] = objectStock.Package_Size_1;
                        dataRow1["stockprice"] = objectStock.Package_Price_1;
                        dataTable.Rows.Add(dataRow1);
                        dataTable.AcceptChanges();
                    }
                    if ((objectStock.Package_UOM_2 != String.Empty) && (objectStock.Package_Size_2 != 0))
                    {
                        DataRow dataRow2 = dataTable.NewRow();
                        dataRow2["stockuom"] = objectStock.Package_UOM_2 + "(" + objectStock.Package_Size_2 + objectStock.Stock_UOM + ")";
                        dataRow2["conversionfactor"] = objectStock.Package_Size_2;
                        dataRow2["stockprice"] = objectStock.Package_Price_2;
                        dataTable.Rows.Add(dataRow2);
                        dataTable.AcceptChanges();
                    }
                    ND.UI.Winform.Form.PopulateComboBox(ref comboBox, dataTable, "stockuom", "stockuom");
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        public decimal? GetUOMPrice(String uom)
        {
            decimal? result = null;
            if (this._UOM_Table != null)
            {
                if (this._UOM_Table.Rows.Count > 0)
                {
                    DataRow[] dataRowFiltered = this._UOM_Table.Select("stockuom = '" + uom + "'");
                    if (dataRowFiltered.Length > 0)
                    {
                        result = (decimal)dataRowFiltered[0]["stockprice"];
                    }
                }
            }
            return result;
        }

        public long GetUOMConFactor(String uom)
        {
            long result = 1;
            if (this._UOM_Table != null)
            {
                if (this._UOM_Table.Rows.Count > 0)
                {
                    DataRow[] dataRowFiltered = this._UOM_Table.Select("stockuom = '" + uom + "'");
                    if (dataRowFiltered.Length > 0)
                    {
                        result = (long)dataRowFiltered[0]["conversionfactor"];
                    }
                }
            }
            return result;
        }

        public int GenerateNewStockID(int category, int subCategory)
        {
            int result = 0;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldNewId = new ND.Data.DataField();
                fieldNewId.Field = "ISNULL(MAX(id) + 1, 1) AS New_ID";
                fieldNewId.Value = String.Empty;
                fieldNewId.DataType = ND.Data.DataObject.DataType.Numeric;
                fieldNewId.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldNewId);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                conditionFieldStockCategory.Field = "stockcategory";
                conditionFieldStockCategory.Value = category;
                conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockCategory);
                ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
                conditionFieldStockSubCategory.Field = "stocksubcategory";
                conditionFieldStockSubCategory.Value = subCategory;
                conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockSubCategory);
                DataTable dataTable = this._Base_Stock.GetCustomDataTable(fields, conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        result = (int)dataTable.Rows[0]["New_ID"];
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public String GenerateStockCode(STP.Base.Stock_Object objectStock)
        {
            String result = String.Empty;
            try
            {
                int length = int.Parse(ConfigurationManager.AppSettings["STOCKCODE:LENGTH"].ToString());

                if (length >= objectStock.ID.ToString().Length)
                {
                    result = this._Base_Sub_Category.GetSingleObject((int)objectStock.Stock_Sub_Category).Sub_Category_Shortcut + objectStock.ID.ToString().PadLeft(length, '0');
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public int GenerateNewSubCategoryID(int category)
        {
            int result = 0;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldNewId = new ND.Data.DataField();
                fieldNewId.Field = "ISNULL(MAX(id) + 1, 1) AS New_ID";
                fieldNewId.Value = String.Empty;
                fieldNewId.DataType = ND.Data.DataObject.DataType.Numeric;
                fieldNewId.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldNewId);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                conditionFieldStockCategory.Field = "stockcategory";
                conditionFieldStockCategory.Value = category;
                conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockCategory);
                DataTable dataTable = this._Base_Stock.GetCustomDataTable(fields, conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        result = (int)dataTable.Rows[0]["New_ID"];
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean SaveCategory(STP.Base.Category_Object objectCategory, SaveType saveType, Boolean updateTransaction)
        {
            Boolean result = false;
            try
            {
                if (saveType == SaveType.Insert)
                {
                    objectCategory.Category_Shortcut = this.GenerateCategoryShortCut();
                }
                result = this._Base_Category.SaveData(objectCategory, (STP.Base.ABSTRACT_Base.SaveType)saveType, updateTransaction);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public String GenerateCategoryShortCut()
        {
            String result = String.Empty;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldCategoryShortcut = new ND.Data.DataField();
                fieldCategoryShortcut.Field = "MAX(categoryshortcut) AS categoryshortcut";
                fieldCategoryShortcut.Value = String.Empty;
                fieldCategoryShortcut.DataType = ND.Data.DataObject.DataType.Numeric;
                fieldCategoryShortcut.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldCategoryShortcut);
                 DataTable dataTable = this._Base_Category.GetCustomDataTable(fields);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        String categoryShortcut = (String)dataTable.Rows[0]["categoryshortcut"];
                        Char[] arrRunningChar = categoryShortcut.ToUpper().ToCharArray();
                        int runningCharCount = arrRunningChar.GetUpperBound(0);
                        if ((int)arrRunningChar[runningCharCount] + 1 <= 90)
                        {
                            result = ((Char)((int)arrRunningChar[runningCharCount] + 1)).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean DeleteCategory(int id)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Category.DeleteData(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean SaveSubCategory(STP.Base.Sub_Category_Object objectSubCategory, SaveType saveType, Boolean updateTransaction)
        {
            Boolean result = false;
            try
            {
                if (saveType == SaveType.Insert)
                {
                    objectSubCategory.ID = this.GenerateNewSubCategoryID((int)objectSubCategory.Category);
                    objectSubCategory.Sub_Category_Shortcut = this.GenerateSubCategoryShortCut((int)objectSubCategory.Category);
                }
                else if (saveType == SaveType.Update)
                {
                    STP.Base.Sub_Category_Object objectSubCategoryOriginal = this._Base_Sub_Category.GetSingleObject((int)objectSubCategory.ID);
                    if (objectSubCategoryOriginal != null)
                    {
                        if (objectSubCategory.Category != objectSubCategoryOriginal.Category)
                        {
                            objectSubCategory.Sub_Category_Shortcut = this.GenerateSubCategoryShortCut((int)objectSubCategory.Category);
                        }
                    }
                }
                result = this._Base_Sub_Category.SaveData(objectSubCategory, (STP.Base.ABSTRACT_Base.SaveType)saveType, updateTransaction);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public String GenerateSubCategoryShortCut(int category)
        {
            String result = String.Empty;
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldSubCategoryShortcut = new ND.Data.DataField();
                fieldSubCategoryShortcut.Field = "ISNULL(MAX(subcategoryshortcut),'') AS subcategoryshortcut";
                fieldSubCategoryShortcut.Value = String.Empty;
                fieldSubCategoryShortcut.DataType = ND.Data.DataObject.DataType.Numeric;
                fieldSubCategoryShortcut.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldSubCategoryShortcut);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
                conditionFieldStockCategory.Field = "stockcategory";
                conditionFieldStockCategory.Value = category;
                conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldStockCategory);
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);
                DataTable dataTable = this._Base_Sub_Category.GetCustomDataTable(fields, conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        if ((String)dataTable.Rows[0]["subcategoryshortcut"] != String.Empty)
                        {
                            String categoryShortcut = (String)dataTable.Rows[0]["subcategoryshortcut"];
                            Char[] arrRunningChar = categoryShortcut.ToUpper().ToCharArray();
                            int runningCharCount = arrRunningChar.GetUpperBound(0);
                            if ((int)arrRunningChar[runningCharCount] + 1 <= 90)
                            {
                                result = this.GetCategory(category).Category_Shortcut + ((Char)((int)arrRunningChar[runningCharCount] + 1)).ToString();
                            }
                        }
                        else
                        {
                            result = this.GetCategory(category).Category_Shortcut + "A";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean DeleteSubCategory(int id)
        {
            Boolean result = false;
            try
            {
                result = this._Base_Sub_Category.DeleteData(id);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion
    }
}
