﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace STP.Logic
{
    public class Report : Common
    {
        #region Declarations

        public class Parameter
        {
            private String _Name;
            private Panel _Panel;
            private Boolean _Enabled;

            public String Name
            {
                get { return this._Name; }
                set { this._Name = value; }
            }

            public Panel Panel
            {
                get { return this._Panel; }
                set { this._Panel = value; }
            }

            public Boolean Enabled
            {
                get { return this._Enabled; }
                set { this._Enabled = value; }
            }

            public Parameter(String name, Panel panel, Boolean enabled)
            {
                this._Name = name;
                this._Panel = panel;
                this._Enabled = enabled;
            }
        }

        #endregion

        #region Fields

        private STP.Base.Report_Base _Base_Report = new STP.Base.Report_Base();
        private List<Parameter> _Report_Parameter = new List<Parameter>();

        #endregion

        #region Properties

        public List<Parameter> Report_Parameter
        {
            get { return this._Report_Parameter; }
            set { this._Report_Parameter = value; }
        }

        #endregion

        #region Methods

        public Report()
        {
        }

        public Boolean BuildReportList(ListView listView, List<ND.Data.ConditionField> conditions)
        {
            return this.BuildReportList(listView, conditions, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public Boolean BuildReportList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                DataTable dataTable = this._Base_Report.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, false, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean SetParameterPanel(int id)
        {
            Boolean result = false;
            try
            {
                foreach (Parameter parameter in this._Report_Parameter)
                {
                    parameter.Panel.Visible = false;
                }
                DataTable dataTable = this._Base_Report.GetDataTable(id);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        int top = 10;
                        foreach (DataColumn column in dataTable.Columns)
                        {
                            foreach (Parameter parameter in this._Report_Parameter)
                            {
                                if (column.ColumnName == parameter.Name)
                                {
                                    parameter.Panel.Visible = (Boolean)dataTable.Rows[0][column.ColumnName];
                                    if (parameter.Panel.Visible == true)
                                    {
                                        parameter.Panel.Top = top;
                                        top += parameter.Panel.Height;
                                    }
                                }
                            }
                        }
                        result = true;
                    }                    
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion
    }
}
