﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Logic
{
    public abstract class Common
    {
        #region Declarations

        public enum SaveType
        {
            None = 0,
            Insert = 1,
            Update = 2,
            Delete = 3
        }

        public enum TransType
        {
            Sales = 1,
            Purchase = 2,
            Transfer = 3,
            Receive = 4,
            Adjustment = 5
        }

        public enum TransLineType
        {
            Stock = 1,
            Discount = 2,
            Return = 3
        }

        public enum StockType
        {
            FinishedGoods = 1,
            NonInventory = 2
        }

        public enum PaymentType
        {
            Cash = 1,
            Cheque = 2,
            CreditCard = 3
        }

        public enum PaymentStatus
        {
            Unpaid = 0,
            Paid = 1
        }

        public enum DeliveryStatus
        {
            Undelivered = 0,
            Delivered = 1
        }

        public enum ReportParameterType
        {
            Single = 0,
            SingleString = 1,
            Range = 2,
            RangeString = 3,
            Multi = 4,
            MultiString = 5
        }

        //Regex Pattern
        public static String RegexPatternDecimal = "^-?\\d+(\\.\\d{1,2})?$";
        public static String RegexPatternInteger = "^-?\\d+(\\.\\d{0})?$";

        #endregion
    }
}
