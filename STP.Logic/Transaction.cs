﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Windows.Forms;

namespace STP.Logic
{
    public class Transaction : Common
    {
        #region Fields

        private SaveType _SaveType;
        private TransType _TransType;
        private STP.Base.Transaction_Header_Object _Object_Transaction_Header;
        private DataTable _Detail_Table;
        private DataTable _Payment_Table;
        private STP.Base.Transaction_Header_Base _Base_Transaction_Header = new STP.Base.Transaction_Header_Base();
        private STP.Base.Transaction_Detail_Base _Base_Transaction_Detail = new STP.Base.Transaction_Detail_Base();
        private STP.Base.Transaction_Payment_Base _Base_Transaction_Payment = new STP.Base.Transaction_Payment_Base();

        #endregion

        #region Properties

        public SaveType Save_Type
        {
            get { return this._SaveType; }
            set { this._SaveType = value; }
        }

        public TransType Trans_Type
        {
            get { return this._TransType; }
            set { this._TransType = value; }
        }

        public STP.Base.Transaction_Header_Object Object_Transaction_Header
        {
            get { return this._Object_Transaction_Header; }
            set { this._Object_Transaction_Header = value; }
        }

        public DataTable Detail_Table
        {
            get { return this._Detail_Table; }
            set { this._Detail_Table = value; }
        }

        public DataTable Payment_Table
        {
            get { return this._Payment_Table; }
            set { this._Payment_Table = value; }
        }

        #endregion

        #region Methods

        public Transaction()
        {
        }

        public String GenerateTransactionNo(TransType transactionType, DateTime transactionDate)
        {
            String result = String.Empty;
            try
            {
                String prefix = String.Empty;
                String format = String.Empty;
                String postfix = String.Empty;
                int length = int.Parse(ConfigurationManager.AppSettings["TRANS:LENGTH"].ToString());
                int runningNo = 0;
                String currentNo = String.Empty;

                switch (transactionType)
                {
                    case TransType.Sales:
                        {
                            prefix = "SL";
                            break;
                        }
                    case TransType.Purchase:
                        {
                            prefix = "PC";
                            break;
                        }
                    case TransType.Receive:
                        {
                            prefix = "RC";
                            break;
                        }
                    case TransType.Transfer:
                        {
                            prefix = "TF";
                            break;
                        }
                    case TransType.Adjustment:
                        {
                            prefix = "AD";
                            break;
                        }
                }

                format = ConfigurationManager.AppSettings["TRANS:FORMAT"].ToString();

                String front = prefix;
                front = front.Replace("#YR2", String.Format("{0:yy}", transactionDate));
                front = front.Replace("#YR", String.Format("{0:yyyy}", transactionDate));
                front = front.Replace("#MT", String.Format("{0:MM}", transactionDate));
                front = front.Replace("#DY", String.Format("{0:dd}", transactionDate));

                String mid = format;
                mid = mid.Replace("#YR2", String.Format("{0:yy}", transactionDate));
                mid = mid.Replace("#YR", String.Format("{0:yyyy}", transactionDate));
                mid = mid.Replace("#MT", String.Format("{0:MM}", transactionDate));
                mid = mid.Replace("#DY", String.Format("{0:dd}", transactionDate));

                String back = postfix;
                back = back.Replace("#YR2", String.Format("{0:yy}", transactionDate));
                back = back.Replace("#YR", String.Format("{0:yyyy}", transactionDate));
                back = back.Replace("#MT", String.Format("{0:MM}", transactionDate));
                back = back.Replace("#DY", String.Format("{0:dd}", transactionDate));

                String transactionNo = front + mid + "%" + back;
                STP.Base.Transaction_Header_Base baseTransactionHeader = new STP.Base.Transaction_Header_Base();
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldCurrentNo = new ND.Data.DataField();
                fieldCurrentNo.Field = "CASE WHEN MAX(transactionno) IS NULL THEN '0' ELSE MAX(transactionno) END AS CurNo";
                fieldCurrentNo.Value = String.Empty;
                fieldCurrentNo.DataType = ND.Data.DataObject.DataType.String;
                fieldCurrentNo.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldCurrentNo);
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
                conditionFieldTransactionNo.Field = "transactionno";
                conditionFieldTransactionNo.Value = transactionNo;
                conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
                conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Like;
                conditions.Add(conditionFieldTransactionNo);
                DataTable dataTable = baseTransactionHeader.GetCustomDataTable(fields, conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        currentNo = (String)dataTable.Rows[0]["CurNo"];
                        currentNo = (front == String.Empty) ? currentNo : currentNo.Replace(front, String.Empty);
                        currentNo = (mid == String.Empty) ? currentNo : currentNo.Replace(mid, String.Empty);
                        currentNo = (back == String.Empty) ? currentNo : currentNo.Replace(back, String.Empty);
                        runningNo = int.Parse(currentNo);
                    }
                }
                runningNo++;
                if (length >= runningNo.ToString().Length)
                {
                    currentNo = runningNo.ToString().PadLeft(length, '0');
                }
                result = front + mid + currentNo + back;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean BuildDetailList(ListView listView)
        {
            return this.BuildDetailList(listView, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public Boolean BuildDetailList(ListView listView, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Detail_Table.Clone();
                    DataRow[] dataRowFiltered = this._Detail_Table.Select("flag = 1");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTableFiltered, false, false, sortType, sortField, true, "transactionline", "refline", "stockname");
                    this.ApplyDetailListColor(listView);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public void BuildDetailListColumn(ListView listView)
        {
            try
            {
                listView.Columns.Clear();
                listView.Columns.Add("transactionno", "Trans. No.");
                listView.Columns.Add("transactionline", "Line");
                listView.Columns.Add("transactionlinetype", "Line Type");
                listView.Columns.Add("refline", "Reference Line");
                listView.Columns.Add("stockid", "Stock ID");
                listView.Columns.Add("stockcategory", "Category ID");
                listView.Columns.Add("stockcategoryname", "Category");
                listView.Columns.Add("stocksubcategory", "Sub Category ID");
                listView.Columns.Add("stocksubcategoryname", "Sub Category");
                listView.Columns.Add("stocktype", "Stock Type");
                listView.Columns.Add("stocktypename", "Type");
                listView.Columns.Add("stockcode", "Item Code");
                listView.Columns.Add("stockname", "Item Name");
                listView.Columns.Add("stockdesc1", "Stock Description 1");
                listView.Columns.Add("stockdesc2", "Stock Description 2");
                listView.Columns.Add("stockprice", "Stock Price");
                listView.Columns.Add("stockcost", "Stock Cost");
                listView.Columns.Add("billingqty", "Quantity");
                listView.Columns.Add("billinguom", "Unit");
                listView.Columns.Add("conversionfactor", "Size");
                listView.Columns.Add("stockuom", "SK Unit");
                listView.Columns.Add("actualqty", "Quantity");
                listView.Columns.Add("billingprice", "Price");
                listView.Columns.Add("returnedqty", "Returned Qty");
                listView.Columns.Add("referencetransactionno", "Ref. Trans. No.");
                listView.Columns.Add("referenceline", "Ref. Trans. Line");
                listView.Columns.Add("referencedocumentno", "Ref. Doc. No.");
                listView.Columns.Add("totalprice", "Total Price");
                listView.Columns.Add("oldbalance", "Old Balance");
                listView.Columns.Add("newbalance", "New Balance");
                listView.Columns.Add("createddate", "Date");
                listView.Columns.Add("flag", "Flag");
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        public void CreateDetailDataTable()
        {
            try
            {
                this._Detail_Table = new DataTable();
                this._Detail_Table.Columns.Clear();
                this._Detail_Table.Columns.Add("transactionno", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("transactionline", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("transactionlinetype", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("refline", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("stockid", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("stockcategory", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("stockcategoryname", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stocksubcategory", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("stocksubcategoryname", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stocktype", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("stocktypename", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stockcode", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stockname", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stockdesc1", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stockdesc2", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("stockprice", Type.GetType("System.Decimal"));
                this._Detail_Table.Columns.Add("stockcost", Type.GetType("System.Decimal"));
                this._Detail_Table.Columns.Add("billingqty", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("billinguom", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("conversionfactor", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("stockuom", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("actualqty", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("billingprice", Type.GetType("System.Decimal"));
                this._Detail_Table.Columns.Add("returnedqty", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("referencetransactionno", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("referenceline", Type.GetType("System.Int32"));
                this._Detail_Table.Columns.Add("referencedocumentno", Type.GetType("System.String"));
                this._Detail_Table.Columns.Add("totalprice", Type.GetType("System.Decimal"));
                this._Detail_Table.Columns.Add("oldbalance", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("newbalance", Type.GetType("System.Int64"));
                this._Detail_Table.Columns.Add("createddate", Type.GetType("System.DateTime"));
                this._Detail_Table.Columns.Add("flag", Type.GetType("System.Boolean"));
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                this._Detail_Table = null;
            }
        }

        public Boolean AddDetailDataRow(STP.Base.Transaction_Detail_Object objectTransactionDetail)
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table == null)
                {
                    this.CreateDetailDataTable();
                }
                if (objectTransactionDetail != null)
                {
                    DataRow dataRow = this._Detail_Table.NewRow();
                    dataRow["transactionno"] = objectTransactionDetail.Transaction_No;
                    dataRow["transactionline"] = objectTransactionDetail.Line_No;
                    dataRow["transactionlinetype"] = objectTransactionDetail.Line_Type;
                    dataRow["refline"] = objectTransactionDetail.Reference_Line;
                    dataRow["stockid"] = objectTransactionDetail.Stock_ID;
                    dataRow["stockcategory"] = objectTransactionDetail.Stock_Category;
                    dataRow["stockcategoryname"] = objectTransactionDetail.Stock_Category_Name;
                    dataRow["stocksubcategory"] = objectTransactionDetail.Stock_Sub_Category;
                    dataRow["stocksubcategoryname"] = objectTransactionDetail.Stock_Sub_Category_Name;
                    dataRow["stocktype"] = objectTransactionDetail.Stock_Type;
                    dataRow["stocktypename"] = objectTransactionDetail.Stock_Type_Name;
                    dataRow["stockcode"] = objectTransactionDetail.Stock_Code;
                    dataRow["stockname"] = objectTransactionDetail.Stock_Name;
                    dataRow["stockdesc1"] = objectTransactionDetail.Stock_Description_1;
                    dataRow["stockdesc2"] = objectTransactionDetail.Stock_Description_2;
                    dataRow["stockprice"] = objectTransactionDetail.Stock_Price;
                    dataRow["stockcost"] = objectTransactionDetail.Stock_Cost;
                    dataRow["stockuom"] = objectTransactionDetail.Stock_UOM;
                    dataRow["actualqty"] = objectTransactionDetail.Actual_Quantity;
                    dataRow["billingqty"] = objectTransactionDetail.Billing_Quantity;
                    dataRow["billinguom"] = objectTransactionDetail.Billing_UOM;
                    dataRow["billingprice"] = objectTransactionDetail.Billing_Price;
                    dataRow["conversionfactor"] = objectTransactionDetail.Conversion_Factor;
                    dataRow["returnedqty"] = objectTransactionDetail.Returned_Quantity;
                    dataRow["referencetransactionno"] = objectTransactionDetail.Reference_Transaction_No;
                    dataRow["referenceline"] = objectTransactionDetail.Reference_Line_No;
                    dataRow["referencedocumentno"] = objectTransactionDetail.Reference_Document_No;
                    dataRow["totalprice"] = objectTransactionDetail.Total_Price;
                    dataRow["oldbalance"] = objectTransactionDetail.Old_Balance;
                    dataRow["newbalance"] = objectTransactionDetail.New_Balance;
                    dataRow["createddate"] = objectTransactionDetail.CreatedDate;
                    dataRow["flag"] = objectTransactionDetail.Flag;
                    this._Detail_Table.Rows.Add(dataRow);
                    this._Detail_Table.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean RemoveDetailDataRow(int lineNo)
        {
            Boolean result = false;
            try
            {
                foreach (DataRow dataRow in this._Detail_Table.Rows)
                {
                    if ((int)dataRow["transactionline"] == lineNo)
                    {
                        dataRow["flag"] = 0;
                        this._Detail_Table.AcceptChanges();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean ClearDetailDataRow()
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table != null)
                {
                    this._Detail_Table.Rows.Clear();
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckDetailDataRowEmpty()
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Detail_Table.Clone();
                    DataRow[] dataRowFiltered = this._Detail_Table.Select("flag = 1");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    if (dataTableFiltered.Rows.Count > 0)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckDetailDataRowExist(String stockCode)
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Detail_Table.Clone();
                    DataRow[] dataRowFiltered = this._Detail_Table.Select("flag = 1 AND stockcode ='" + stockCode + "'");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    if (dataTableFiltered.Rows.Count > 0)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildPaymentList(ListView listView)
        {
            return this.BuildPaymentList(listView, ND.Data.DataObject.SortType.None, String.Empty);
        }

        public Boolean BuildPaymentList(ListView listView, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                if (this._Payment_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Payment_Table.Clone();
                    DataRow[] dataRowFiltered = this._Payment_Table.Select("flag = 1");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTableFiltered, false, false, sortType, sortField);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public void BuildPaymentListColumn(ListView listView)
        {
            try
            {
                listView.Columns.Clear();
                listView.Columns.Add("transactionno", "Trans. No.");
                listView.Columns.Add("paymentline", "Line");
                listView.Columns.Add("paymentdate", "Date");
                listView.Columns.Add("paymenttype", "Type");
                listView.Columns.Add("paymenttypename", "Type");
                listView.Columns.Add("paymentamount", "Amount");
                listView.Columns.Add("paymentreference", "Reference");
                listView.Columns.Add("createddate", "Date");
                listView.Columns.Add("flag", "Flag");
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        public void CreatePaymentDataTable()
        {
            try
            {
                this._Payment_Table = new DataTable();
                this._Payment_Table.Columns.Clear();
                this._Payment_Table.Columns.Add("transactionno", Type.GetType("System.String"));
                this._Payment_Table.Columns.Add("paymentline", Type.GetType("System.Int32"));
                this._Payment_Table.Columns.Add("paymentdate", Type.GetType("System.DateTime"));
                this._Payment_Table.Columns.Add("paymenttype", Type.GetType("System.Int32"));
                this._Payment_Table.Columns.Add("paymenttypename", Type.GetType("System.String"));
                this._Payment_Table.Columns.Add("paymentamount", Type.GetType("System.Decimal"));
                this._Payment_Table.Columns.Add("paymentreference", Type.GetType("System.String"));
                this._Payment_Table.Columns.Add("createddate", Type.GetType("System.DateTime"));
                this._Payment_Table.Columns.Add("flag", Type.GetType("System.Boolean"));
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                this._Payment_Table = null;
            }
        }

        public Boolean AddPaymentDataRow(STP.Base.Transaction_Payment_Object objectTransactionPayment)
        {
            Boolean result = false;
            try
            {
                if (this._Payment_Table == null)
                {
                    this.CreatePaymentDataTable();
                }

                if (objectTransactionPayment != null)
                {
                    DataRow dataRow = this._Payment_Table.NewRow();
                    dataRow["transactionno"] = objectTransactionPayment.Transaction_No;
                    dataRow["paymentline"] = objectTransactionPayment.Line_No;
                    dataRow["paymentdate"] = objectTransactionPayment.Payment_Date;
                    dataRow["paymenttype"] = objectTransactionPayment.Payment_Type;
                    dataRow["paymenttypename"] = objectTransactionPayment.Payment_Type_Name;
                    dataRow["paymentamount"] = objectTransactionPayment.Payment_Amount;
                    dataRow["paymentreference"] = objectTransactionPayment.Payment_Reference;
                    dataRow["createddate"] = objectTransactionPayment.CreatedDate;
                    dataRow["flag"] = objectTransactionPayment.Flag;
                    this._Payment_Table.Rows.Add(dataRow);
                    this._Payment_Table.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean RemovePaymentDataRow(int lineNo)
        {
            Boolean result = false;
            try
            {
                foreach (DataRow dataRow in this._Payment_Table.Rows)
                {
                    if ((int)dataRow["paymentline"] == lineNo)
                    {
                        dataRow["flag"] = 0;
                        this._Payment_Table.AcceptChanges();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean ClearPaymentDataRow()
        {
            Boolean result = false;
            try
            {
                if (this._Payment_Table != null)
                {
                    this._Payment_Table.Rows.Clear();
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckPaymentDataRowEmpty()
        {
            Boolean result = false;
            try
            {
                if (this._Payment_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Payment_Table.Clone();
                    DataRow[] dataRowFiltered = this._Payment_Table.Select("flag = 1");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    if (dataTableFiltered.Rows.Count > 0)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean SaveTransaction()
        {
            Boolean result = false;
            try
            {
                if (this._Object_Transaction_Header != null)
                {
                    if (this._SaveType == SaveType.Insert)
                    {
                        DateTime transactionDateTime = this._Object_Transaction_Header.Transaction_DateTime != null ? (DateTime)this._Object_Transaction_Header.Transaction_DateTime : DateTime.Now;
                        this._Object_Transaction_Header.Transaction_No = this.GenerateTransactionNo(this._TransType, transactionDateTime);
                    }
                    this._Object_Transaction_Header.Transaction_Type = (int)this._TransType;
                    this._Object_Transaction_Header.Flag = true;
                    this._Object_Transaction_Header.Total_Amount = 0;
                    this._Object_Transaction_Header.Paid_Amount = 0;
                    this._Object_Transaction_Header.Transaction_Detail = new List<STP.Base.Transaction_Detail_Object>();
                    this._Object_Transaction_Header.Transaction_Payment = new List<STP.Base.Transaction_Payment_Object>();

                    if (this._Detail_Table != null)
                    {
                        foreach (DataRow dataRow in this._Detail_Table.Rows)
                        {
                            STP.Base.Transaction_Detail_Object objectTransactionDetail = new STP.Base.Transaction_Detail_Object();
                            objectTransactionDetail.Transaction_No = this._Object_Transaction_Header.Transaction_No;
                            objectTransactionDetail.Line_No = (int)dataRow["transactionline"];
                            objectTransactionDetail.Line_Type = (int)dataRow["transactionlinetype"];
                            objectTransactionDetail.Reference_Line = (int)dataRow["refline"];
                            objectTransactionDetail.Stock_ID = (int)dataRow["stockid"];
                            objectTransactionDetail.Stock_Category = (int)dataRow["stockcategory"];
                            objectTransactionDetail.Stock_Category_Name = (String)dataRow["stockcategoryname"];
                            objectTransactionDetail.Stock_Sub_Category = (int)dataRow["stocksubcategory"];
                            objectTransactionDetail.Stock_Sub_Category_Name = (String)dataRow["stocksubcategoryname"];
                            objectTransactionDetail.Stock_Type = (int)dataRow["stocktype"];
                            objectTransactionDetail.Stock_Type_Name = (String)dataRow["stocktypename"];
                            objectTransactionDetail.Stock_Code = (String)dataRow["stockcode"];
                            objectTransactionDetail.Stock_Name = (String)dataRow["stockname"];
                            objectTransactionDetail.Stock_Description_1 = (String)dataRow["stockdesc1"];
                            objectTransactionDetail.Stock_Description_2 = (String)dataRow["stockdesc2"];
                            objectTransactionDetail.Stock_Price = (decimal)dataRow["stockprice"];
                            objectTransactionDetail.Stock_Cost = (decimal)dataRow["stockcost"];
                            objectTransactionDetail.Stock_UOM = (String)dataRow["stockuom"];
                            objectTransactionDetail.Actual_Quantity = (long)dataRow["actualqty"];
                            objectTransactionDetail.Billing_Quantity = (long)dataRow["billingqty"];
                            objectTransactionDetail.Billing_UOM = (String)dataRow["billinguom"];
                            objectTransactionDetail.Billing_Price = (Decimal)dataRow["billingprice"];
                            objectTransactionDetail.Conversion_Factor = (long)dataRow["conversionfactor"];
                            objectTransactionDetail.Returned_Quantity = (long)dataRow["returnedqty"];
                            objectTransactionDetail.Reference_Transaction_No = (String)dataRow["referencetransactionno"];
                            objectTransactionDetail.Reference_Line_No = (int)dataRow["referenceline"];
                            objectTransactionDetail.Reference_Document_No = (String)dataRow["referencedocumentno"];
                            objectTransactionDetail.Total_Price = (decimal)dataRow["totalprice"];
                            objectTransactionDetail.Flag = (Boolean)dataRow["flag"];
                            this._Object_Transaction_Header.Transaction_Detail.Add(objectTransactionDetail);
                            if (objectTransactionDetail.Flag == true)
                            {
                                this._Object_Transaction_Header.Total_Amount += objectTransactionDetail.Total_Price;
                            }
                        }
                    }

                    if (this._Payment_Table != null)
                    {
                        foreach (DataRow dataRow in this._Payment_Table.Rows)
                        {
                            STP.Base.Transaction_Payment_Object objectTransactionPayment = new STP.Base.Transaction_Payment_Object();
                            objectTransactionPayment.Transaction_No = this._Object_Transaction_Header.Transaction_No;
                            objectTransactionPayment.Line_No = (int)dataRow["paymentline"];
                            objectTransactionPayment.Payment_Date = (DateTime)dataRow["paymentdate"];
                            objectTransactionPayment.Payment_Type = (int)dataRow["paymenttype"];
                            objectTransactionPayment.Payment_Type_Name = (String)dataRow["paymenttypename"];
                            objectTransactionPayment.Payment_Amount = (decimal)dataRow["paymentamount"];
                            objectTransactionPayment.Payment_Reference = (String)dataRow["paymentreference"];
                            objectTransactionPayment.Flag = (Boolean)dataRow["flag"];
                            this._Object_Transaction_Header.Transaction_Payment.Add(objectTransactionPayment);
                            if (objectTransactionPayment.Flag == true)
                            {
                                this._Object_Transaction_Header.Paid_Amount += objectTransactionPayment.Payment_Amount;
                            }
                        }
                    }

                    result = this._Base_Transaction_Header.SaveData(this._Object_Transaction_Header, (STP.Base.ABSTRACT_Base.SaveType)this._SaveType);
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteTransaction()
        {
            Boolean result = false;
            try
            {
                if (this._Object_Transaction_Header != null)
                {
                    result = this._Base_Transaction_Header.DeleteData(this._Object_Transaction_Header.Transaction_No);
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean CheckDocumentNoExist(String documentNo)
        {
            Boolean result = false;
            try
            {
                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldDocumentNo = new ND.Data.ConditionField();
                conditionFieldDocumentNo.Field = "documentno";
                conditionFieldDocumentNo.Value = documentNo;
                conditionFieldDocumentNo.DataType = ND.Data.DataObject.DataType.NString;
                conditionFieldDocumentNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldDocumentNo);

                if (this._Base_Transaction_Header.GetSingleObject<STP.Base.Transaction_Header_Object>(conditions) == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public Boolean BuildTransactionList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                DataTable dataTable = this._Base_Transaction_Header.GetDataTable(conditions);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, false, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public AutoCompleteStringCollection TransactionStringCollection()
        {
            AutoCompleteStringCollection result = new AutoCompleteStringCollection();
            try
            {
                List<ND.Data.DataField> fields = new List<ND.Data.DataField>();
                ND.Data.DataField fieldDocumentNo = new ND.Data.DataField();
                fieldDocumentNo.Field = "documentno";
                fieldDocumentNo.Value = String.Empty;
                fieldDocumentNo.DataType = ND.Data.DataObject.DataType.String;
                fieldDocumentNo.ValidateType = ND.Data.DataObject.ValidateType.None;
                fields.Add(fieldDocumentNo);

                List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                ND.Data.ConditionField conditionFieldFlag = new ND.Data.ConditionField();
                conditionFieldFlag.Field = "flag";
                conditionFieldFlag.Value = 1;
                conditionFieldFlag.DataType = ND.Data.DataObject.DataType.Numeric;
                conditionFieldFlag.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                conditions.Add(conditionFieldFlag);

                DataTable dataTable = this._Base_Transaction_Header.GetCustomDataTable(fields, conditions);
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    result.Add(dataRow["documentno"].ToString());
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Transaction_Header_Object GetTransaction(String transactionNo)
        {
            STP.Base.Transaction_Header_Object result = null;
            try
            {
                result = this._Base_Transaction_Header.GetSingleObject(transactionNo);
                if (result != null)
                {
                    List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                    ND.Data.ConditionField conditionField = new ND.Data.ConditionField();
                    conditionField.Field = "transactionno";
                    conditionField.Value = result.Transaction_No;
                    conditionField.DataType = ND.Data.DataObject.DataType.String;
                    conditionField.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                    conditions.Add(conditionField);
                    result.Transaction_Detail = this._Base_Transaction_Detail.GetMultipleObject<STP.Base.Transaction_Detail_Object>(conditions);
                    result.Transaction_Payment = this._Base_Transaction_Payment.GetMultipleObject<STP.Base.Transaction_Payment_Object>(conditions);
                    this._Detail_Table = this._Base_Transaction_Detail.GetDataTable(conditions);
                    this._Payment_Table = this._Base_Transaction_Payment.GetDataTable(conditions);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public STP.Base.Transaction_Header_Object GetTransaction(List<ND.Data.ConditionField> conditions)
        {
            STP.Base.Transaction_Header_Object result = null;
            try
            {
                result = this._Base_Transaction_Header.GetSingleObject<STP.Base.Transaction_Header_Object>(conditions);
                if (result != null)
                {
                    if (conditions == null)
                    {
                        conditions = new List<ND.Data.ConditionField>();
                    }
                    ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
                    conditionFieldTransactionNo.Field = "transactionno";
                    conditionFieldTransactionNo.Value = result.Transaction_No;
                    conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
                    conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                    conditions.Add(conditionFieldTransactionNo);
                    result.Transaction_Detail = this._Base_Transaction_Detail.GetMultipleObject<STP.Base.Transaction_Detail_Object>(conditions);
                    result.Transaction_Payment = this._Base_Transaction_Payment.GetMultipleObject<STP.Base.Transaction_Payment_Object>(conditions);
                    this._Detail_Table = this._Base_Transaction_Detail.GetDataTable(conditions);
                    this._Payment_Table = this._Base_Transaction_Payment.GetDataTable(conditions);
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = null;
            }
            return result;
        }

        public Boolean BuildPaymentTypeComboBox(ComboBox comboBox)
        {
            Boolean result = false;
            try
            {
                result = new General().BuildCodeMasterComboBox(comboBox, "PAYMENTTYPE");
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public void BuildReturnListColumn(ListView listView)
        {
            try
            {
                listView.Columns.Clear();
                listView.Columns.Add("transactionno", "Trans. No.");
                listView.Columns.Add("transactionline", "Line");
                listView.Columns.Add("stockid", "Stock ID");
                listView.Columns.Add("stockcategory", "Stock Category");
                listView.Columns.Add("stockcategoryname", "Category");
                listView.Columns.Add("stocksubcategory", "Stock Category");
                listView.Columns.Add("stocksubcategoryname", "Category");
                listView.Columns.Add("stocktype", "Stock Type");
                listView.Columns.Add("stocktypename", "Type");
                listView.Columns.Add("stockcode", "Item Code");
                listView.Columns.Add("stockname", "Item Name");
                listView.Columns.Add("stockprice", "Price");
                listView.Columns.Add("stockuom", "Unit");
                listView.Columns.Add("actualqty", "Quantity");
                listView.Columns.Add("totalprice", "Total Price");
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
        }

        public Boolean BuildReturnList(ListView listView, List<ND.Data.ConditionField> conditions, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                ND.Data.DataObject objectData = new ND.Data.DataObject();
                objectData.TableName = "vw_stp_transaction_return";
                objectData.AddField("transactionno", String.Empty, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("transactionline", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockid", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockcategory", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockcategoryname", String.Empty, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stocksubcategory", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stocksubcategoryname", String.Empty, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stocktype", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stocktypename", String.Empty, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockcode", String.Empty, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockname", String.Empty, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockprice", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("stockuom", String.Empty, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("actualqty", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                objectData.AddField("totalprice", String.Empty, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                DataTable dataTable = this._Base_Transaction_Header.GetCustomDataTable(objectData.Fields, conditions, null, objectData.TableName);
                result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTable, false, false, sortType, sortField);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean BuildDiscountList(ListView listView, ND.Data.DataObject.SortType sortType, String sortField)
        {
            Boolean result = false;
            try
            {
                if (this._Detail_Table != null)
                {
                    DataTable dataTableFiltered = new DataTable();
                    dataTableFiltered = this._Detail_Table.Clone();
                    DataRow[] dataRowFiltered = this._Detail_Table.Select("flag = 1 AND transactionlinetype = 1");
                    foreach (DataRow dataRow in dataRowFiltered)
                    {
                        dataTableFiltered.ImportRow(dataRow);
                    }
                    dataTableFiltered.AcceptChanges();
                    result = ND.UI.Winform.Form.PopulateListView(ref listView, dataTableFiltered, false, false, sortType, sortField, true, "transactionline", "refline", "stockname");
                    this.ApplyDetailListColor(listView);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean ApplyDetailListColor(ListView listView)
        {
            Boolean result = false;
            try
            {
                if (listView.Columns[2].Name == "transactionlinetype")
                {
                    foreach (ListViewItem listViewItem in listView.Items)
                    {
                        if (listViewItem.SubItems[2].Text == "2")
                        {
                            listViewItem.ForeColor = System.Drawing.Color.Blue;
                        }
                        else if (listViewItem.SubItems[2].Text == "3")
                        {
                            listViewItem.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        #endregion
    }
}
