﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Static_Item
    {
        //table names
        public static String Table_Code_Master = "stp_code_master";
        public static String Table_Customer = "stp_customer";
        public static String Table_Vendor = "stp_vendor";
        public static String Table_Category = "stp_category";
        public static String Table_Sub_Category = "stp_sub_category";
        public static String Table_Stock = "stp_stock";
        public static String Table_Transaction_Header = "stp_transaction_header";
        public static String Table_Transaction_Detail = "stp_transaction_detail";
        public static String Table_Transaction_Payment = "stp_transaction_payment";
        public static String Table_Report = "stp_report";

        //mappings
        public static String[,] Mapping_Code_Master = new String[,]
        {
            { "Type", "codetype", "String" },
            { "Code", "code", "String" },
            { "Description", "codedesc", "String" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Customer = new String[,] 
        {
            { "ID", "id", "Numeric" },
            { "Customer_Code", "customercode", "String" },
            { "Customer_Name", "customername", "NString" },
            { "Address_1", "address1", "NString" },
            { "Address_2", "address2", "NString" },
            { "Post_Code", "postcode", "NString" },
            { "City", "city", "NString" },
            { "Area", "area", "NString" },
            { "State", "state", "NString" },
            { "Country", "country", "NString" },
            { "Telephone", "tel", "NString" },
            { "Fax", "fax", "NString" },
            { "Contact_Person", "contact", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Vendor = new String[,] 
        {
            { "ID", "id", "Numeric" },
            { "Vendor_Code", "vendorcode", "String" },
            { "Vendor_Name", "vendorname", "NString" },
            { "Address_1", "address1", "NString" },
            { "Address_2", "address2", "NString" },
            { "Post_Code", "postcode", "NString" },
            { "City", "city", "NString" },
            { "Area", "area", "NString" },
            { "State", "state", "NString" },
            { "Country", "country", "NString" },
            { "Telephone", "tel", "NString" },
            { "Fax", "fax", "NString" },
            { "Contact_Person", "contact", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Category = new String[,]
        {
            { "ID", "id", "Numeric" },
            { "Category_Name", "categoryname", "NString" },
            { "Category_Shortcut", "categoryshortcut", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Sub_Category = new String[,]
        {
            { "Category", "stockcategory", "Numeric" },
            { "ID", "id", "Numeric" },
            { "Sub_Category_Name", "subcategoryname", "NString" },
            { "Sub_Category_Shortcut", "subcategoryshortcut", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Stock = new String[,]
        {
            { "ID", "id", "Numeric" },
            { "Stock_Category", "stockcategory", "Numeric" },
            { "Stock_Sub_Category", "stocksubcategory", "Numeric" },
            { "Stock_Type", "stocktype", "Numeric" },
            { "Stock_Code", "stockcode", "String" },
            { "Stock_Name", "stockname", "NString" },
            { "Stock_Description_1", "stockdesc1", "NString" },
            { "Stock_Description_2", "stockdesc2", "NString" },
            { "Stock_Cost", "stockcost", "Numeric" },
            { "Stock_UOM", "stockuom", "NString" },
            { "Package_Size_1", "packagesize1", "Numeric" },
            { "Package_UOM_1", "packageuom1", "NString" },
            { "Package_Size_2", "packagesize2", "Numeric" },
            { "Package_UOM_2", "packageuom2", "NString" },
            { "Stock_Price_1", "stockprice1", "Numeric" },
            { "Package_Price_1", "packageprice1", "Numeric" },
            { "Stock_Price_2", "stockprice2", "Numeric" },
            { "Package_Price_2", "packageprice2", "Numeric" },
            { "Stock_Price_3", "stockprice3", "Numeric" },
            { "Package_Price_3", "packageprice3", "Numeric" },
            { "Stock_Price_4", "stockprice4", "Numeric" },
            { "Package_Price_4", "packageprice4", "Numeric" },
            { "Stock_Price_5", "stockprice5", "Numeric" },
            { "Package_Price_5", "packageprice5", "Numeric" },
            { "Stock_Balance", "stockbalance", "Numeric" },
            { "GUID", "guid", "String" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Transaction_Header = new String[,]
        {
            { "Transaction_No", "transactionno", "String" },
            { "Transaction_Type", "transactiontype", "Numeric" },
            { "Customer_ID", "customer", "Numeric" },
            { "Customer_Code", "customercode", "String" },
            { "Customer_Name", "customername", "NString" },
            { "Document_No", "documentno", "String" },
            { "Transaction_DateTime", "transactiondatetime", "DateTime" },
            { "Total_Amount", "totalamount", "Numeric" },
            { "Paid_Amount", "paidamount", "Numeric" },
            { "Remark", "remark", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Transaction_Detail = new String[,]
        {
            { "Transaction_No", "transactionno", "String" },
            { "Line_No", "transactionline", "Numeric" },
            { "Line_Type", "transactionlinetype", "Numeric" },
            { "Reference_Line", "refline", "Numeric" },
            { "Stock_ID", "stockid", "Numeric" },
            { "Stock_Category", "stockcategory", "Numeric" },
            { "Stock_Category_Name", "stockcategoryname", "NString" },
            { "Stock_Sub_Category", "stocksubcategory", "Numeric" },
            { "Stock_Sub_Category_Name", "stocksubcategoryname", "NString" },
            { "Stock_Type", "stocktype", "Numeric" },
            { "Stock_Type_Name", "stocktypename", "NString" },
            { "Stock_Code", "stockcode", "String" },
            { "Stock_Name", "stockname", "NString" },
            { "Stock_Description_1", "stockdesc1", "NString" },
            { "Stock_Description_2", "stockdesc2", "NString" },
            { "Stock_Price", "stockprice", "Numeric" },
            { "Stock_Cost", "stockcost", "Numeric" },
            { "Stock_UOM", "stockuom", "NString" },
            { "Actual_Quantity", "actualqty", "Numeric" },
            { "Billing_Quantity", "billingqty", "Numeric" },
            { "Billing_UOM", "billinguom", "NString" },
            { "Billing_Price", "billingprice", "Numeric" },
            { "Conversion_Factor", "conversionfactor", "Numeric" },
            { "Returned_Quantity", "returnedqty", "Numeric" },
            { "Reference_Transaction_No", "referencetransactionno", "String" },
            { "Reference_Line_No", "referenceline", "Numeric" },
            { "Reference_Document_No", "referencedocumentno", "String" },
            { "Total_Price", "totalprice", "Numeric" },
            { "Old_Balance", "oldbalance", "Numeric" },
            { "New_Balance", "newbalance", "Numeric" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Transaction_Payment = new String[,]
        {
            { "Transaction_No", "transactionno", "String" },
            { "Line_No", "paymentline", "Numeric" },
            { "Payment_Date", "paymentdate", "DateTime" },
            { "Payment_Type", "paymenttype", "Numeric" },
            { "Payment_Type_Name", "paymenttypename", "NString" },
            { "Payment_Amount", "paymentamount", "Numeric" },
            { "Payment_Reference", "paymentreference", "NString" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };

        public static String[,] Mapping_Report = new String[,]
        {
            { "ID", "id", "Numeric" },
            { "Report_Code", "reportcode", "String" },
            { "Report_Name", "reportname", "String" },
            { "Report_File", "reportfile", "String" },
            { "Parameter_Year", "pr_year", "Numeric" },
            { "Parameter_Year_Range", "pr_year_range", "Numeric" },
            { "Parameter_Month", "pr_month", "Numeric" },
            { "Parameter_Month_Range", "pr_month_range", "Numeric" },
            { "Parameter_Date", "pr_date", "Numeric" },
            { "Parameter_Date_Range", "pr_date_range", "Numeric" },
            { "Parameter_Customer", "pr_customer", "Numeric" },
            { "Parameter_Category", "pr_category", "Numeric" },
            { "Parameter_Sub_Category", "pr_subcategory", "Numeric" },
            { "Parameter_Stock", "pr_stock", "Numeric" },
            { "CreatedDate", "createddate", "DateTime" },
            { "Flag", "flag", "Numeric" }
        };
    }
}
