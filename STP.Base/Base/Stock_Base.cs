﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Stock_Base : ABSTRACT_Base
    {
        public Stock_Base()
        {
            this._TableName = Static_Item.Table_Stock;
            this._Mapping = Static_Item.Mapping_Stock;
        }

        public DataTable GetDataTable(int stockCategory, int stockSubCategory, int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
            conditionFieldStockCategory.Field = "stockcategory";
            conditionFieldStockCategory.Value = stockCategory;
            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldStockCategory);
            ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
            conditionFieldStockSubCategory.Field = "stocksubcategory";
            conditionFieldStockSubCategory.Value = stockSubCategory;
            conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldStockSubCategory);
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetDataTable(conditions);
        }

        public Stock_Object GetSingleObject(int stockCategory, int stockSubCategory, int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldStockCategory = new ND.Data.ConditionField();
            conditionFieldStockCategory.Field = "stockcategory";
            conditionFieldStockCategory.Value = stockCategory;
            conditionFieldStockCategory.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldStockCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldStockCategory);
            ND.Data.ConditionField conditionFieldStockSubCategory = new ND.Data.ConditionField();
            conditionFieldStockSubCategory.Field = "stocksubcategory";
            conditionFieldStockSubCategory.Value = stockSubCategory;
            conditionFieldStockSubCategory.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldStockSubCategory.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldStockSubCategory);
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetSingleObject<Stock_Object>(conditions);
        }

        public Boolean SaveData(Stock_Object objectStock, SaveType saveType, Boolean updateTransaction)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Stock_Object).GetProperty(this._Mapping[count, 0]);
                    this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(objectStock), dataType, ND.Data.DataObject.ValidateType.None);
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("guid", objectStock.GUID, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                if (updateTransaction == true)
                {
                    if (saveType == SaveType.Update)
                    {
                        List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
                        ND.Data.ConditionField conditionFieldGuid = new ND.Data.ConditionField();
                        conditionFieldGuid.Field = "guid";
                        conditionFieldGuid.Value = objectStock.GUID;
                        conditionFieldGuid.DataType = ND.Data.DataObject.DataType.String;
                        conditionFieldGuid.ConditionType = ND.Data.DataObject.ConditionType.Equal;
                        conditions.Add(conditionFieldGuid);
                        Stock_Object oldObjectStock = this.GetSingleObject<Stock_Object>(conditions);
                        if (oldObjectStock != null)
                        {
                            this._DataObject.TableName = Static_Item.Table_Transaction_Detail;
                            this._DataObject.AddField("stockid", objectStock.ID, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stockcategory", objectStock.Stock_Category, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stockcategoryname", new Category_Base().GetSingleObject((int)objectStock.Stock_Category).Category_Name, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stocksubcategory", objectStock.Stock_Sub_Category, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stocksubcategoryname", new Sub_Category_Base().GetSingleObject((int)objectStock.Stock_Sub_Category).Sub_Category_Name, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stocktype", objectStock.Stock_Type, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stocktypename", new Code_Master_Base().GetSingleObject("STOCKTYPE", objectStock.Stock_Type.ToString()).Description, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stockcode", objectStock.Stock_Code, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("stockname", objectStock.Stock_Name, ND.Data.DataObject.DataType.NString, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddField("edit", 1, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                            this._DataObject.AddCondition("stockid", oldObjectStock.ID, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            this._DataObject.AddCondition("stockcategory", oldObjectStock.Stock_Category, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            this._DataObject.AddCondition("stocksubcategory", oldObjectStock.Stock_Sub_Category, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            arrSQL.Add(sql);
                            sql = "EXECUTE stp_recalculate_inventory";
                            arrSQL.Add(sql);
                        }
                    }
                }
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(int stockCategory, int stockSubCategory, int id)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("stockcategory", stockCategory, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                this._DataObject.AddCondition("stocksubcategory", stockSubCategory, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                this._DataObject.AddCondition("id", id, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
