﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Code_Master_Base : ABSTRACT_Base
    {
        public Code_Master_Base()
        {
            this._TableName = Static_Item.Table_Code_Master;
            this._Mapping = Static_Item.Mapping_Code_Master;
        }

        public DataTable GetDataTable(String type, String code)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldType = new ND.Data.ConditionField();
            conditionFieldType.Field = "codetype";
            conditionFieldType.Value = type;
            conditionFieldType.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldType.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldType);
            ND.Data.ConditionField conditionFieldCode = new ND.Data.ConditionField();
            conditionFieldCode.Field = "code";
            conditionFieldCode.Value = code;
            conditionFieldCode.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldCode.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldCode);
            return this.GetDataTable(conditions);
        }

        public Code_Master_Object GetSingleObject(String type, String code)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldType = new ND.Data.ConditionField();
            conditionFieldType.Field = "codetype";
            conditionFieldType.Value = type;
            conditionFieldType.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldType.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldType);
            ND.Data.ConditionField conditionFieldCode = new ND.Data.ConditionField();
            conditionFieldCode.Field = "code";
            conditionFieldCode.Value = code;
            conditionFieldCode.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldCode.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldCode);
            return this.GetSingleObject<Code_Master_Object>(conditions);
        }

        public Boolean SaveData(Code_Master_Object codeMasterObject, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Code_Master_Object).GetProperty(this._Mapping[count, 0]);
                    this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(codeMasterObject), dataType, ND.Data.DataObject.ValidateType.None);
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("codetype", codeMasterObject.Type, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                            this._DataObject.AddCondition("code", codeMasterObject.Code, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(String type, String code)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("codetype", type, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                this._DataObject.AddCondition("code", code, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
