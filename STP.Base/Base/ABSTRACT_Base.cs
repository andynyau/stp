﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace STP.Base
{
    public abstract class ABSTRACT_Base
    {
        #region Declaration

        public enum SaveType
        {
            None = 0,
            Insert = 1,
            Update = 2,
            Delete = 3
        }

        #endregion

        #region Fields

        protected ND.Data.SQL.DataAccess _DataAccess;
        protected ND.Data.DataObject _DataObject;
        protected String _TableName;
        protected String[,] _Mapping;

        #endregion

        #region Properties

        public ND.Data.SQL.DataAccess DataAccess { get { return this._DataAccess; } set { this._DataAccess = value; } }
        public ND.Data.DataObject DataObject { get { return this._DataObject; } set { this._DataObject = value; } }

        #endregion

        #region Methods

        public ABSTRACT_Base()
        {
            this._DataAccess = new ND.Data.SQL.DataAccess();
            this._DataObject = new ND.Data.DataObject();
        }

        public DataTable GetDataTable(List<ND.Data.ConditionField> conditions)
        {
            DataTable result = null;
            try
            {
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    this._DataObject.AddField(this._Mapping[count, 1], String.Empty, dataType, ND.Data.DataObject.ValidateType.None);
                }
                if (conditions != null)
                {
                    foreach (ND.Data.ConditionField conditionField in conditions)
                    {
                        this._DataObject.AddCondition(conditionField.Field, conditionField.Value, conditionField.DataType, conditionField.ConditionType);
                    }
                }
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Select);
                result = this._DataAccess.LoadData(sql, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public T GetSingleObject<T>(List<ND.Data.ConditionField> conditions) where T : new()
        {
            T result = default(T);
            try
            {
                DataTable dataTable = this.GetDataTable(conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        DataRow dataRow = dataTable.Rows[0];
                        result = ND.Data.DataBase.LoadEntity<T>(dataRow, this._Mapping);
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public List<T> GetMultipleObject<T>(List<ND.Data.ConditionField> conditions) where T : new()
        {
            List<T> result = new List<T>();
            try
            {
                DataTable dataTable = this.GetDataTable(conditions);
                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            T newObject = ND.Data.DataBase.LoadEntity<T>(dataRow, this._Mapping);
                            result.Add(newObject);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }

        public DataTable GetCustomDataTable(List<ND.Data.DataField> fields)
        {
            return this.GetCustomDataTable(fields, null);
        }

        public DataTable GetCustomDataTable(List<ND.Data.DataField> fields, List<ND.Data.ConditionField> conditions)
        {
            return this.GetCustomDataTable(fields, conditions, null);
        }

        public DataTable GetCustomDataTable(List<ND.Data.DataField> fields, List<ND.Data.ConditionField> conditions, List<ND.Data.OrderField> orders)
        {
            return this.GetCustomDataTable(fields, conditions, null, this._TableName);
        }

        public DataTable GetCustomDataTable(List<ND.Data.DataField> fields, List<ND.Data.ConditionField> conditions, List<ND.Data.OrderField> orders, String tableName)
        {
            DataTable result = null;
            try
            {
                this._DataObject.TableName = tableName;
                if (fields != null)
                {
                    foreach (ND.Data.DataField dataField in fields)
                    {
                        this._DataObject.AddField(dataField.Field, dataField.Value, dataField.DataType, dataField.ValidateType);
                    }
                }
                if (conditions != null)
                {
                    foreach (ND.Data.ConditionField conditionField in conditions)
                    {
                        this._DataObject.AddCondition(conditionField.Field, conditionField.Value, conditionField.DataType, conditionField.ConditionType);
                    }
                }
                if (orders != null)
                {
                    foreach (ND.Data.OrderField orderField in orders)
                    {
                        this._DataObject.AddOrder(orderField.Field, orderField.SortType);
                    }
                }
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Select);
                result = this._DataAccess.LoadData(sql, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
            }
            return result;
        }
        #endregion
    }
}
