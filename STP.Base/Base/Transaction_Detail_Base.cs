﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Transaction_Detail_Base : ABSTRACT_Base
    {
        public Transaction_Detail_Base()
        {
            this._TableName = Static_Item.Table_Transaction_Detail;
            this._Mapping = Static_Item.Mapping_Transaction_Detail;
        }

        public DataTable GetDataTable(String transactionNo, int lineNo)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
            conditionFieldTransactionNo.Field = "transactionno";
            conditionFieldTransactionNo.Value = transactionNo;
            conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionNo);
            ND.Data.ConditionField conditionFieldTransactionLine = new ND.Data.ConditionField();
            conditionFieldTransactionLine.Field = "transactionline";
            conditionFieldTransactionLine.Value = lineNo;
            conditionFieldTransactionLine.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldTransactionLine.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionLine);
            return this.GetDataTable(conditions);
        }

        public Transaction_Detail_Object GetSingleObject(String transactionNo, int lineNo)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
            conditionFieldTransactionNo.Field = "transactionno";
            conditionFieldTransactionNo.Value = transactionNo;
            conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionNo);
            ND.Data.ConditionField conditionFieldTransactionLine = new ND.Data.ConditionField();
            conditionFieldTransactionLine.Field = "transactionline";
            conditionFieldTransactionLine.Value = lineNo;
            conditionFieldTransactionLine.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldTransactionLine.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionLine);
            return this.GetSingleObject<Transaction_Detail_Object>(conditions);
        }

        public Boolean SaveData(Transaction_Detail_Object objectTransactionDetail, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Transaction_Detail_Object).GetProperty(this._Mapping[count, 0]);
                    this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(objectTransactionDetail), dataType, ND.Data.DataObject.ValidateType.None);
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("transactionno", objectTransactionDetail.Transaction_No, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                            this._DataObject.AddCondition("transactionline", objectTransactionDetail.Line_No, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(String transactionNo, int lineNo)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("transactionno", transactionNo, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                this._DataObject.AddCondition("transactionline", lineNo, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
