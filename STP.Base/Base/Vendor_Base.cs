﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Vendor_Base : ABSTRACT_Base
    {
        public Vendor_Base()
        {
            this._TableName = Static_Item.Table_Vendor;
            this._Mapping = Static_Item.Mapping_Vendor;
        }

        public DataTable GetDataTable(int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetDataTable(conditions);
        }

        public Vendor_Object GetSingleObject(int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetSingleObject<Vendor_Object>(conditions);   
        }

        public Boolean SaveData(Vendor_Object objectVendor, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Vendor_Object).GetProperty(this._Mapping[count, 0]);
                    if (this._Mapping[count, 1] != "id")
                    {
                        this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(objectVendor), dataType, ND.Data.DataObject.ValidateType.None);
                    }
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("id", objectVendor.ID, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(int id)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("id", id, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
