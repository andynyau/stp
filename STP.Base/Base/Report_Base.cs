﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Report_Base : ABSTRACT_Base
    {
        public Report_Base()
        {
            this._TableName = Static_Item.Table_Report;
            this._Mapping = Static_Item.Mapping_Report;
        }

        public DataTable GetDataTable(int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetDataTable(conditions);
        }

        public Report_Object GetSingleObject(int id)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldId = new ND.Data.ConditionField();
            conditionFieldId.Field = "id";
            conditionFieldId.Value = id;
            conditionFieldId.DataType = ND.Data.DataObject.DataType.Numeric;
            conditionFieldId.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldId);
            return this.GetSingleObject<Report_Object>(conditions);
        }

        public Boolean SaveData(Report_Object objectReport, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Report_Object).GetProperty(this._Mapping[count, 0]);
                    this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(objectReport), dataType, ND.Data.DataObject.ValidateType.None);
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("id", objectReport.ID, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(int id)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("id", id, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
