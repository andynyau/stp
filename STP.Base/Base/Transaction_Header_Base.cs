﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

namespace STP.Base
{
    public class Transaction_Header_Base : ABSTRACT_Base
    {
        public Transaction_Header_Base()
        {
            this._TableName = Static_Item.Table_Transaction_Header;
            this._Mapping = Static_Item.Mapping_Transaction_Header;
        }

        public DataTable GetDataTable(String transactionNo)
        {            
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
            conditionFieldTransactionNo.Field = "transactionno";
            conditionFieldTransactionNo.Value = transactionNo;
            conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionNo);
            return this.GetDataTable(conditions);
        }

        public Transaction_Header_Object GetSingleObject(String transactionNo)
        {
            List<ND.Data.ConditionField> conditions = new List<ND.Data.ConditionField>();
            ND.Data.ConditionField conditionFieldTransactionNo = new ND.Data.ConditionField();
            conditionFieldTransactionNo.Field = "transactionno";
            conditionFieldTransactionNo.Value = transactionNo;
            conditionFieldTransactionNo.DataType = ND.Data.DataObject.DataType.String;
            conditionFieldTransactionNo.ConditionType = ND.Data.DataObject.ConditionType.Equal;
            conditions.Add(conditionFieldTransactionNo);
            return this.GetSingleObject<Transaction_Header_Object>(conditions);
        }

        public Boolean SaveData(Transaction_Header_Object objectTransactionHeader, SaveType saveType)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                for (int count = 0; count <= this._Mapping.GetUpperBound(0); count++)
                {
                    ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), this._Mapping[count, 2]);
                    PropertyInfo property = typeof(Transaction_Header_Object).GetProperty(this._Mapping[count, 0]);
                    this._DataObject.AddField(this._Mapping[count, 1], property.GetValue(objectTransactionHeader), dataType, ND.Data.DataObject.ValidateType.None);
                }
                String sql = String.Empty;
                switch (saveType)
                {
                    case SaveType.Insert:
                        {
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                            break;
                        }
                    case SaveType.Update:
                        {
                            this._DataObject.AddCondition("transactionno", objectTransactionHeader.Transaction_No, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                            sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                            break;
                        }
                }
                arrSQL.Add(sql);
                if (objectTransactionHeader.Transaction_Detail != null)
                {
                    foreach (Transaction_Detail_Object objectTransactionDetail in objectTransactionHeader.Transaction_Detail)
                    {
                        this._DataObject.TableName = Static_Item.Table_Transaction_Detail;
                        for (int count = 0; count <= Static_Item.Mapping_Transaction_Detail.GetUpperBound(0); count++)
                        {
                            ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), Static_Item.Mapping_Transaction_Detail[count, 2]);
                            PropertyInfo property = typeof(Transaction_Detail_Object).GetProperty(Static_Item.Mapping_Transaction_Detail[count, 0]);
                            this._DataObject.AddField(Static_Item.Mapping_Transaction_Detail[count, 1], property.GetValue(objectTransactionDetail), dataType, ND.Data.DataObject.ValidateType.None);
                        }
                        this._DataObject.AddField("edit", 1, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                        sql = String.Empty;
                        switch (saveType)
                        {
                            case SaveType.Insert:
                                {
                                    sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                                    arrSQL.Add(sql);
                                    break;
                                }
                            case SaveType.Update:
                                {
                                    if (new Transaction_Detail_Base().GetSingleObject(objectTransactionDetail.Transaction_No, (int)objectTransactionDetail.Line_No) == null)
                                    {
                                        sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                                        arrSQL.Add(sql);
                                    }
                                    else
                                    {
                                        this._DataObject.AddCondition("transactionno", objectTransactionDetail.Transaction_No, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                                        this._DataObject.AddCondition("transactionline", objectTransactionDetail.Line_No, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                                        sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                                        arrSQL.Add(sql);
                                    }
                                    break;
                                }
                        }
                    }
                }
                if (objectTransactionHeader.Transaction_Payment != null)
                {
                    foreach (Transaction_Payment_Object objectTransactionPayment in objectTransactionHeader.Transaction_Payment)
                    {
                        this._DataObject.TableName = Static_Item.Table_Transaction_Payment;
                        for (int count = 0; count <= Static_Item.Mapping_Transaction_Payment.GetUpperBound(0); count++)
                        {
                            ND.Data.DataObject.DataType dataType = (ND.Data.DataObject.DataType)Enum.Parse(typeof(ND.Data.DataObject.DataType), Static_Item.Mapping_Transaction_Payment[count, 2]);
                            PropertyInfo property = typeof(Transaction_Payment_Object).GetProperty(Static_Item.Mapping_Transaction_Payment[count, 0]);
                            this._DataObject.AddField(Static_Item.Mapping_Transaction_Payment[count, 1], property.GetValue(objectTransactionPayment), dataType, ND.Data.DataObject.ValidateType.None);
                        }
                        sql = String.Empty;
                        switch (saveType)
                        {
                            case SaveType.Insert:
                                {
                                    sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                                    arrSQL.Add(sql);
                                    break;
                                }
                            case SaveType.Update:
                                {
                                    if (new Transaction_Payment_Base().GetSingleObject(objectTransactionPayment.Transaction_No, (int)objectTransactionPayment.Line_No) == null)
                                    {
                                        sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Insert);
                                        arrSQL.Add(sql);
                                    }
                                    else
                                    {
                                        this._DataObject.AddCondition("transactionno", objectTransactionPayment.Transaction_No, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                                        this._DataObject.AddCondition("paymentline", objectTransactionPayment.Line_No, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ConditionType.Equal);
                                        sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                                        arrSQL.Add(sql);
                                    }
                                    break;
                                }
                        }
                    }
                }
                sql = "EXECUTE stp_recalculate_inventory";
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }

        public Boolean DeleteData(String transactionNo)
        {
            Boolean result = false;
            try
            {
                ArrayList arrSQL = new ArrayList();
                this._DataObject.TableName = this._TableName;
                this._DataObject.AddField("flag", 0, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("transactionno", transactionNo, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                String sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                this._DataObject.TableName = Static_Item.Table_Transaction_Detail;
                this._DataObject.AddField("edit", 1, ND.Data.DataObject.DataType.Numeric, ND.Data.DataObject.ValidateType.None);
                this._DataObject.AddCondition("transactionno", transactionNo, ND.Data.DataObject.DataType.String, ND.Data.DataObject.ConditionType.Equal);
                sql = this._DataObject.GenerateSQL(ND.Data.DataObject.SQLType.Update);
                arrSQL.Add(sql);
                sql = "EXECUTE stp_recalculate_inventory";
                arrSQL.Add(sql);
                result = this._DataAccess.BatchExecuteQuery(arrSQL, CommandType.Text);
            }
            catch (Exception ex)
            {
                ND.Log.LogWriter.WriteLog(ex.ToString());
                result = false;
            }
            return result;
        }
    }
}
