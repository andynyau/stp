﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Stock_Object : ABSTRACT_Object
    {
        private int? _ID;
        private int? _Stock_Category;
        private int? _Stock_Sub_Category;
        private int? _Stock_Type;
        private String _Stock_Code;
        private String _Stock_Name;
        private String _Stock_Description_1;
        private String _Stock_Description_2;
        private decimal? _Stock_Cost;
        private String _Stock_UOM;
        private long? _Package_Size_1;
        private String _Package_UOM_1;
        private long? _Package_Size_2;
        private String _Package_UOM_2;
        private decimal? _Stock_Price_1;
        private decimal? _Package_Price_1;
        private decimal? _Stock_Price_2;
        private decimal? _Package_Price_2;
        private decimal? _Stock_Price_3;
        private decimal? _Package_Price_3;
        private decimal? _Stock_Price_4;
        private decimal? _Package_Price_4;
        private decimal? _Stock_Price_5;
        private decimal? _Package_Price_5;
        private long? _Stock_Balance;
        private Guid? _GUID;

        public int? ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public int? Stock_Category
        {
            get { return this._Stock_Category; }
            set { this._Stock_Category = value; }
        }

        public int? Stock_Sub_Category
        {
            get { return this._Stock_Sub_Category; }
            set { this._Stock_Sub_Category = value; }
        }

        public int? Stock_Type
        {
            get { return this._Stock_Type; }
            set { this._Stock_Type = value; }
        }

        public String Stock_Code
        {
            get { return this._Stock_Code; }
            set { this._Stock_Code = value; }
        }

        public String Stock_Name
        {
            get { return this._Stock_Name; }
            set { this._Stock_Name = value; }
        }

        public String Stock_Description_1
        {
            get { return this._Stock_Description_1; }
            set { this._Stock_Description_1 = value; }
        }

        public String Stock_Description_2
        {
            get { return this._Stock_Description_2; }
            set { this._Stock_Description_2 = value; }
        }

        public decimal? Stock_Cost
        {
            get { return this._Stock_Cost; }
            set { this._Stock_Cost = value; }
        }

        public String Stock_UOM
        {
            get { return this._Stock_UOM; }
            set { this._Stock_UOM = value; }
        }

        public long? Package_Size_1
        {
            get { return this._Package_Size_1; }
            set { this._Package_Size_1 = value; }
        }

        public String Package_UOM_1
        {
            get { return this._Package_UOM_1; }
            set { this._Package_UOM_1 = value; }
        }

        public long? Package_Size_2
        {
            get { return this._Package_Size_2; }
            set { this._Package_Size_2 = value; }
        }

        public String Package_UOM_2
        {
            get { return this._Package_UOM_2; }
            set { this._Package_UOM_2 = value; }
        }

        public decimal? Stock_Price_1
        {
            get { return this._Stock_Price_1; }
            set { this._Stock_Price_1 = value; }
        }

        public decimal? Package_Price_1
        {
            get { return this._Package_Price_1; }
            set { this._Package_Price_1 = value; }
        }

        public decimal? Stock_Price_2
        {
            get { return this._Stock_Price_2; }
            set { this._Stock_Price_2 = value; }
        }

        public decimal? Package_Price_2
        {
            get { return this._Package_Price_2; }
            set { this._Package_Price_2 = value; }
        }

        public decimal? Stock_Price_3
        {
            get { return this._Stock_Price_3; }
            set { this._Stock_Price_3 = value; }
        }

        public decimal? Package_Price_3
        {
            get { return this._Package_Price_3; }
            set { this._Package_Price_3 = value; }
        }

        public decimal? Stock_Price_4
        {
            get { return this._Stock_Price_4; }
            set { this._Stock_Price_4 = value; }
        }

        public decimal? Package_Price_4
        {
            get { return this._Package_Price_4; }
            set { this._Package_Price_4 = value; }
        }

        public decimal? Stock_Price_5
        {
            get { return this._Stock_Price_5; }
            set { this._Stock_Price_5 = value; }
        }

        public decimal? Package_Price_5
        {
            get { return this._Package_Price_5; }
            set { this._Package_Price_5 = value; }
        }

        public long? Stock_Balance
        {
            get { return this._Stock_Balance; }
            set { this._Stock_Balance = value; }
        }

        public Guid? GUID
        {
            get { return this._GUID; }
            set { this._GUID = value; }
        }

        public Stock_Object()
        {
            this._ID = null;
            this._Stock_Category = null;
            this._Stock_Sub_Category = null;
            this._Stock_Type = null;
            this._Stock_Code = null;
            this._Stock_Name = null;
            this._Stock_Description_1 = null;
            this._Stock_Description_2 = null;
            this._Stock_Cost = null;
            this._Stock_UOM = null;
            this._Package_Size_1 = null;
            this._Package_UOM_1 = null;
            this._Package_Size_2 = null;
            this._Package_UOM_2 = null;
            this._Stock_Price_1 = null;
            this._Package_Price_1 = null;
            this._Stock_Price_2 = null;
            this._Package_Price_2 = null;
            this._Stock_Price_3 = null;
            this._Package_Price_3 = null;
            this._Stock_Price_4 = null;
            this._Package_Price_4 = null;
            this._Stock_Price_5 = null;
            this._Package_Price_5 = null;
            this._Stock_Balance = null;
            this._GUID = null;
        }
    }
}
