﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Category_Object : ABSTRACT_Object
    {
        private int? _ID;
        private String _Category_Name;
        private String _Category_Shortcut;

        public int? ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public String Category_Name
        {
            get { return this._Category_Name; }
            set { this._Category_Name = value; }
        }

        public String Category_Shortcut
        {
            get { return this._Category_Shortcut; }
            set { this._Category_Shortcut = value; }
        }

        public Category_Object()
        {
            this._ID = null;
            this._Category_Name = null;
            this._Category_Shortcut = null;
        }
    }
}
