﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Report_Object : ABSTRACT_Object
    {
        private int? _ID;
        private String _Report_Code;
        private String _Report_Name;
        private String _Report_File;
        private Boolean? _Parameter_Year;
        private Boolean? _Parameter_Year_Range;
        private Boolean? _Parameter_Month;
        private Boolean? _Parameter_Month_Range;
        private Boolean? _Parameter_Date;
        private Boolean? _Parameter_Date_Range;
        private Boolean? _Parameter_Customer;
        private Boolean? _Parameter_Category;
        private Boolean? _Parameter_Sub_Category;
        private Boolean? _Parameter_Stock;

        public int? ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public String Report_Code
        {
            get { return this._Report_Code; }
            set { this._Report_Code = value; }
        }

        public String Report_Name
        {
            get { return this._Report_Name; }
            set { this._Report_Name = value; }
        }

        public String Report_File
        {
            get { return this._Report_File; }
            set { this._Report_File = value; }
        }

        public Boolean? Parameter_Year
        {
            get { return this._Parameter_Year; }
            set { this._Parameter_Year = value; }
        }

        public Boolean? Parameter_Year_Range
        {
            get { return this._Parameter_Year_Range; }
            set { this._Parameter_Year_Range = value; }
        }

        public Boolean? Parameter_Month
        {
            get { return this._Parameter_Month; }
            set { this._Parameter_Month = value; }
        }

        public Boolean? Parameter_Month_Range
        {
            get { return this._Parameter_Month_Range; }
            set { this._Parameter_Month_Range = value; }
        }

        public Boolean? Parameter_Date
        {
            get { return this._Parameter_Date; }
            set { this._Parameter_Date = value; }
        }

        public Boolean? Parameter_Date_Range
        {
            get { return this._Parameter_Date_Range; }
            set { this._Parameter_Date_Range = value; }
        }

        public Boolean? Parameter_Customer
        {
            get { return this._Parameter_Customer; }
            set { this._Parameter_Customer = value; }
        }

        public Boolean? Parameter_Category
        {
            get { return this._Parameter_Category; }
            set { this._Parameter_Category = value; }
        }

        public Boolean? Parameter_Sub_Category
        {
            get { return this._Parameter_Sub_Category; }
            set { this._Parameter_Sub_Category = value; }
        }

        public Boolean? Parameter_Stock
        {
            get { return this._Parameter_Stock; }
            set { this._Parameter_Stock = value; }
        }

        public Report_Object()
        {
            this._ID = null;
            this._Report_Code = null;
            this._Report_Name = null;
            this._Report_File = null;
            this._Parameter_Year = null;
            this._Parameter_Year_Range = null;
            this._Parameter_Month = null;
            this._Parameter_Month_Range = null;
            this._Parameter_Date = null;
            this._Parameter_Date_Range = null;
            this._Parameter_Customer = null;
            this._Parameter_Category = null;
            this._Parameter_Stock = null;
        }
    }
}
