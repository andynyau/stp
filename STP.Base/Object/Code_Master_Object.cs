﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Code_Master_Object : ABSTRACT_Object
    {
        private String _Type;
        private String _Code;
        private String _Description;

        public String Type
        {
            get { return this._Type; }
            set { this._Type = value; }
        }

        public String Code
        {
            get { return this._Code; }
            set { this._Code = value; }
        }

        public String Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        public Code_Master_Object()
        {
            this._Type = null;
            this._Code = null;
            this._Description = null;
        }
    }
}
