﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Transaction_Payment_Object : ABSTRACT_Object
    {
        private String _Transaction_No;
        private int? _Line_No;
        private DateTime? _Payment_Date;
        private int? _Payment_Type;
        private String _Payment_Type_Name;
        private decimal? _Payment_Amount;
        private String _Payment_Reference;

        public String Transaction_No
        {
            get { return this._Transaction_No; }
            set { this._Transaction_No = value; }
        }

        public int? Line_No
        {
            get { return this._Line_No; }
            set { this._Line_No = value; }
        }

        public DateTime? Payment_Date
        {
            get { return this._Payment_Date; }
            set { this._Payment_Date = value; }
        }

        public int? Payment_Type
        {
            get { return this._Payment_Type; }
            set { this._Payment_Type = value; }
        }

        public String Payment_Type_Name
        {
            get { return this._Payment_Type_Name; }
            set { this._Payment_Type_Name = value; }
        }

        public decimal? Payment_Amount
        {
            get { return this._Payment_Amount; }
            set { this._Payment_Amount = value; }
        }

        public String Payment_Reference
        {
            get { return this._Payment_Reference; }
            set { this._Payment_Reference = value; }
        }

        public Transaction_Payment_Object()
        {
            this._Transaction_No = null;
            this._Line_No = null;
            this._Payment_Type = null;
            this._Payment_Type_Name = null;
            this._Payment_Amount = null;
            this._Payment_Reference = null;
        }
    }
}
