﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Vendor_Object : ABSTRACT_Object
    {
        private int? _ID;
        private String _Vendor_Code;
        private String _Vendor_Name;
        private String _Address_1;
        private String _Address_2;
        private String _Post_Code;
        private String _City;
        private String _Area;
        private String _State;
        private String _Country;
        private String _Telephone;
        private String _Fax;
        private String _Contact_Person;

        public int? ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public String Vendor_Code
        {
            get { return this._Vendor_Code; }
            set { this._Vendor_Code = value; }
        }

        public String Vendor_Name
        {
            get { return this._Vendor_Name; }
            set { this._Vendor_Name = value; }
        }

        public String Address_1
        {
            get { return this._Address_1; }
            set { this._Address_1 = value; }
        }

        public String Address_2
        {
            get { return this._Address_2; }
            set { this._Address_2 = value; }
        }

        public String Post_Code
        {
            get { return this._Post_Code; }
            set { this._Post_Code = value; }
        }

        public String Area
        {
            get { return this._Area; }
            set { this._Area = value; }
        }

        public String City
        {
            get { return this._City; }
            set { this._City = value; }
        }

        public String State
        {
            get { return this._State; }
            set { this._State = value; }
        }

        public String Country
        {
            get { return this._Country; }
            set { this._Country = value; }
        }

        public String Telephone
        {
            get { return this._Telephone; }
            set { this._Telephone = value; }
        }

        public String Fax
        {
            get { return this._Fax; }
            set { this._Fax = value; }
        }

        public String Contact_Person
        {
            get { return this._Contact_Person; }
            set { this._Contact_Person = value; }
        }

        public Vendor_Object()
        {
            this._ID = null;
            this._Vendor_Code = null;
            this._Vendor_Name = null;
            this._Address_1 = null;
            this._Address_2 = null;
            this._Post_Code = null;
            this._City = null;
            this._Area = null;
            this._State = null;
            this._Country = null;
            this._Telephone = null;
            this._Fax = null;
            this._Contact_Person = null;
        }
    }
}
