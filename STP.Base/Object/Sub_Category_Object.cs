﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Sub_Category_Object : ABSTRACT_Object
    {
        private int? _Category;
        private int? _ID;
        private String _Sub_Category_Name;
        private String _Sub_Category_Shortcut;

        public int? Category
        {
            get { return this._Category; }
            set { this._Category = value; }
        }

        public int? ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public String Sub_Category_Name
        {
            get { return this._Sub_Category_Name; }
            set { this._Sub_Category_Name = value; }
        }

        public String Sub_Category_Shortcut
        {
            get { return this._Sub_Category_Shortcut; }
            set { this._Sub_Category_Shortcut = value; }
        }

        public Sub_Category_Object()
        {
            this._ID = null;
            this._Sub_Category_Name = null;
            this._Sub_Category_Shortcut = null;
        }
    }
}
