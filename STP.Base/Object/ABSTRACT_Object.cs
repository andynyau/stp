﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public abstract class ABSTRACT_Object
    {
        private DateTime? _CreatedDate;
        private Boolean? _Flag;

        public DateTime? CreatedDate
        {
            get { return this._CreatedDate; }
            set { this._CreatedDate = value; }
        }

        public Boolean? Flag
        {
            get { return this._Flag; }
            set { this._Flag = value; }
        }

        public ABSTRACT_Object()
        {
            this._CreatedDate = null;
            this._Flag = null;
        }
    }
}
