﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Transaction_Detail_Object : ABSTRACT_Object
    {
        private String _Transaction_No;
        private int? _Line_No;
        private int? _Line_Type;
        private int? _Reference_Line;
        private int? _Stock_ID;
        private int? _Stock_Category;
        private String _Stock_Category_Name;
        private int? _Stock_Sub_Category;
        private String _Stock_Sub_Category_Name;
        private int? _Stock_Type;
        private String _Stock_Type_Name;
        private String _Stock_Code;
        private String _Stock_Name;
        private String _Stock_Description_1;
        private String _Stock_Description_2;
        private decimal? _Stock_Price;
        private decimal? _Stock_Cost;
        private String _Stock_UOM;
        private long? _Actual_Quantity;
        private long? _Billing_Quantity;
        private String _Billing_UOM;
        private decimal? _Billing_Price;
        private long? _Conversion_Factor;
        private decimal? _Total_Price;
        private long? _Returned_Quantity;
        private String _Reference_Transaction_No;
        private int? _Reference_Line_No;
        private String _Reference_Document_No;
        private long? _Old_Balance;
        private long? _New_Balance;

        public String Transaction_No
        {
            get { return this._Transaction_No; }
            set { this._Transaction_No = value; }
        }

        public int? Line_No
        {
            get { return this._Line_No; }
            set { this._Line_No = value; }
        }

        public int? Line_Type
        {
            get { return this._Line_Type; }
            set { this._Line_Type = value; }
        }

        public int? Reference_Line
        {
            get { return this._Reference_Line; }
            set { this._Reference_Line = value; }
        }

        public int? Stock_ID 
        {
            get { return this._Stock_ID; }
            set { this._Stock_ID = value; }
        }

        public int? Stock_Category
        {
            get { return this._Stock_Category; }
            set { this._Stock_Category = value; }
        }

        public String Stock_Category_Name
        {
            get { return this._Stock_Category_Name; }
            set { this._Stock_Category_Name = value; }
        }

        public int? Stock_Sub_Category
        {
            get { return this._Stock_Sub_Category; }
            set { this._Stock_Sub_Category = value; }
        }

        public String Stock_Sub_Category_Name
        {
            get { return this._Stock_Sub_Category_Name; }
            set { this._Stock_Sub_Category_Name = value; }
        }

        public int? Stock_Type
        {
            get { return this._Stock_Type; }
            set { this._Stock_Type = value; }
        }

        public String Stock_Type_Name
        {
            get { return this._Stock_Type_Name; }
            set { this._Stock_Type_Name = value; }
        }

        public String Stock_Code
        {
            get { return this._Stock_Code; }
            set { this._Stock_Code = value; }
        }

        public String Stock_Name
        {
            get { return this._Stock_Name; }
            set { this._Stock_Name = value; }
        }

        public String Stock_Description_1
        {
            get { return this._Stock_Description_1; }
            set { this._Stock_Description_1 = value; }
        }

        public String Stock_Description_2
        {
            get { return this._Stock_Description_2; }
            set { this._Stock_Description_2 = value; }
        }

        public decimal? Stock_Price
        {
            get { return this._Stock_Price; }
            set { this._Stock_Price = value; }
        }

        public decimal? Stock_Cost
        {
            get { return this._Stock_Cost; }
            set { this._Stock_Cost = value; }
        }

        public String Stock_UOM
        {
            get { return this._Stock_UOM; }
            set { this._Stock_UOM = value; }
        }

        public long? Actual_Quantity
        {
            get { return this._Actual_Quantity; }
            set { this._Actual_Quantity = value; }
        }

        public long? Billing_Quantity
        {
            get { return this._Billing_Quantity; }
            set { this._Billing_Quantity = value; }
        }

        public String Billing_UOM
        {
            get { return this._Billing_UOM; }
            set { this._Billing_UOM = value; }
        }

        public decimal? Billing_Price
        {
            get { return this._Billing_Price; }
            set { this._Billing_Price = value; }
        }

        public long? Conversion_Factor
        {
            get { return this._Conversion_Factor; }
            set { this._Conversion_Factor = value; }
        }

        public long? Returned_Quantity
        {
            get { return this._Returned_Quantity; }
            set { this._Returned_Quantity = value; }
        }

        public String Reference_Transaction_No
        {
            get { return this._Reference_Transaction_No; }
            set { this._Reference_Transaction_No = value; }
        }

        public int? Reference_Line_No
        {
            get { return this._Reference_Line_No; }
            set { this._Reference_Line_No = value; }
        }

        public String Reference_Document_No
        {
            get { return this._Reference_Document_No; }
            set { this._Reference_Document_No = value; }
        }

        public decimal? Total_Price
        {
            get { return this._Total_Price; }
            set { this._Total_Price = value; }
        }

        public long? Old_Balance
        {
            get { return this._Old_Balance; }
            set { this._Old_Balance = value; }
        }

        public long? New_Balance
        {
            get { return this._New_Balance; }
            set { this._New_Balance = value; }
        }

        public Transaction_Detail_Object()
        {
            this._Transaction_No = null;
            this._Line_No = null;
            this._Line_Type = null;
            this._Reference_Line = null;
            this._Stock_ID = null;
            this._Stock_Category = null;
            this._Stock_Category_Name = null;
            this._Stock_Sub_Category = null;
            this._Stock_Sub_Category_Name = null;
            this._Stock_Type = null;
            this._Stock_Type_Name = null;
            this._Stock_Code = null;
            this._Stock_Name = null;
            this._Stock_Description_1 = null;
            this._Stock_Description_2 = null;
            this._Stock_Price = null;
            this._Stock_Cost = null;
            this._Stock_UOM = null;
            this._Actual_Quantity = null;
            this._Billing_Quantity = null;
            this._Billing_UOM = null;
            this._Billing_Price = null;
            this._Conversion_Factor = null;
            this._Returned_Quantity = null;
            this._Reference_Transaction_No = null;
            this._Reference_Line_No = null;
            this._Reference_Document_No = null;
            this._Total_Price = null;
            this._Old_Balance = null;
            this._New_Balance = null;
        }
    }
}
