﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STP.Base
{
    public class Transaction_Header_Object : ABSTRACT_Object
    {
        private String _Transaction_No;
        private int? _Transaction_Type;
        private int? _Customer_ID;
        private String _Customer_Code;
        private String _Customer_Name;
        private String _Document_No;
        private DateTime? _Transaction_DateTime;
        private decimal? _Total_Amount;
        private decimal? _Paid_Amount;
        private String _Remark;
        private List<Transaction_Detail_Object> _Transaction_Detail;
        private List<Transaction_Payment_Object> _Transaction_Payment;

        public String Transaction_No
        {
            get { return this._Transaction_No; }
            set { this._Transaction_No = value; }
        }

        public int? Transaction_Type
        {
            get { return this._Transaction_Type; }
            set { this._Transaction_Type = value; }
        }

        public int? Customer_ID
        {
            get { return this._Customer_ID; }
            set { this._Customer_ID = value; }
        }

        public String Customer_Code
        {
            get { return this._Customer_Code; }
            set { this._Customer_Code = value; }
        }

        public String Customer_Name
        {
            get { return this._Customer_Name; }
            set { this._Customer_Name = value; }
        }

        public String Document_No
        {
            get { return this._Document_No; }
            set { this._Document_No = value; }
        }

        public DateTime? Transaction_DateTime
        {
            get { return this._Transaction_DateTime; }
            set { this._Transaction_DateTime = value; }
        }

        public decimal? Total_Amount
        {
            get { return this._Total_Amount; }
            set { this._Total_Amount = value; }
        }

        public decimal? Paid_Amount
        {
            get { return this._Paid_Amount; }
            set { this._Paid_Amount = value; }
        }

        public String Remark
        {
            get { return this._Remark; }
            set { this._Remark = value; }
        }

        public List<Transaction_Detail_Object> Transaction_Detail
        {
            get { return this._Transaction_Detail; }
            set { this._Transaction_Detail = value; }
        }

        public List<Transaction_Payment_Object> Transaction_Payment
        {
            get { return this._Transaction_Payment; }
            set { this._Transaction_Payment = value; }
        }

        public Transaction_Header_Object()
        {
            this._Transaction_No = null;
            this._Transaction_Type = null;
            this._Document_No = null;
            this._Transaction_DateTime = null;
            this._Total_Amount = null;
            this._Paid_Amount = null;
            this._Remark = null;
            this._Transaction_Detail = null;
            this._Transaction_Payment = null;
        }
    }
}
